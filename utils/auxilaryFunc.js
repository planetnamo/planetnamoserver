//---------------Auxxilary Functions------------------------------------------------

var safeToString = exports.safeToString = function(x) {
  switch (typeof x) {
    case 'object':
      return 'object';
    case 'function':
      return 'function';
    default:
      return x + '';
  }
}

var constants_ = exports.constants = {
	minbussinessprofilelimit: 50,
	minRentalTimeinMinutes: 43200, // 30 days
	minAuctionDurationInMinutes: 43200,
	auctionExtensionDuration:  1440, // 1 day,
	admin_email: "karunk@live.com",
	maxCartItems: 25,
	host: '54.200.74.162:3000'
};

var validateEmail = exports.validateEmail = function(email){
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
};

var DateDifferenceInDays = exports.DateDifferenceInDays = function(date1, date2){
	var one_day=1000*60*60*24;
	var date1_ms = date1.getTime();
	var date2_ms = date2.getTime();
	var difference_ms = date2_ms - date1_ms;
	return Math.round(difference_ms/one_day);
};

var DateDifferenceInMinutes = exports.DateDifferenceInMinutes = function(date1, date2){
	var one_minute=1000*60;
	var date1_ms = date1.getTime();
	var date2_ms = date2.getTime();
	var difference_ms = date2_ms - date1_ms;
	return Math.round(difference_ms/one_minute);	
};

var CalculateRentAmountForProduct = exports.CalculateRentAmountForProduct = function(productPerMonthPrice, date1, date2){
	var difference_ms = DateDifferenceInMinutes(date1,date2);
	if(difference_ms<=constants_.minRentalTimeinMinutes){
		return -1;
	}else{
		var Months = Math.round(difference_ms/constants_.minRentalTimeinMinutes);
		return Months*productPerMonthPrice;
	}
};

exports.ConvertArrayToDict = function(array){
	var dict = {};
	if(Array.isArray(array)){
		for(var i=0; i<array.length; i++){
			dict[array[i]] = array[i];
		}
	}
	return dict;
};

exports.ConvertDictToArray = function(dict){
	var array = [];
	for(key in dict){
		array.push(key);
	}
	return array;
};

exports.ConvertDictToArrayPushValues = function(dict){
	var array = [];
	for(key in dict){
		array.push(dict[key]);
	}
	return array;
};

exports.compare = function compare(a,b) {
  if (a.numberOfOrders > b.numberOfOrders)
    return -1;
  if (a.numberOfOrders < b.numberOfOrders)
    return 1;
  return 0;
};


