var config = require(__base + '/config/config');

exports.getImageUrl = function(fileName){
	return  config.baseURL+"/getFile/"+fileName;
};

exports.getDefaultImageUrl = function(fileName){
	return config.baseURL+"/getFile/defaults/"+fileName;
};

exports.getDocUrl = function(fileName){
	return config.baseURL+"/getFile/userDocs/"+fileName;
};

exports.getProfilePicture = function(fileName){
	return config.baseURL+"/getFile/profilePicture/"+fileName;
};

exports.getLotPicture = function(fileName){
	return config.baseURL+"/getFile/lotPicture/"+fileName;
};

exports.getBaseProductPicture = function(fileName){
	return config.baseURL+"/getFile/baseProductPicture/"+fileName;
};

exports.getProductPicture = function(productImageId){
	return config.baseURL+"/getFile/productPicture/"+productImageId;
};

exports.getProductCategoryPicture = function(fileName){
	return config.baseURL+"/getFile/productCategoryPicture/"+fileName;
};