var crypto = require('crypto');
var request = require('request');
var Wallet        = require(__base + '/models/wallet/wallet');
var WalletTransaction = require(__base + '/models/wallet/wallet-transactions');
var WalletWithDrawl   = require(__base + '/models/wallet/wallet-withdrawl-requests');
var Transaction   = require(__base + '/models/transactions/transaction');

var RazorPayKey = 'rzp_test_Z8g1MeZjHFpVjA';
var RazorPaySecret = 'k0S7kWfPj7hXU9CtzkRktbzC';


exports.createWalletTransactionId = function() {
    var current_date = (new Date()).valueOf().toString();
    var random = Math.random().toString();
    return crypto.createHash('sha1').update(current_date + random).digest('hex');
}

exports.createMobileOtp = function() {
    return Math.floor(Math.random()*(10000-1000+1)+1000);
}

exports.createEmailverificationCode = function() {
    var current_date = (new Date()).valueOf().toString();
	var random = Math.random().toString();
	return crypto.createHash('sha1').update(current_date + random).digest('hex');
}

exports.createAuthToken = function() {
  var current_date = (new Date()).valueOf().toString();
	var random = Math.random().toString();
	return crypto.createHash('sha1').update(current_date + random).digest('hex');
}

exports.validatePaymentRazorPay = function(payMentRazorPayId, amountLocal, cb){
    Transaction.findOne({txnid: payMentRazorPayId}, function(err, transaction){
            if(err){
                cb(ErrorParser.parseErrorreturnErrorMessage(err));
            }else if(transaction){
                cb('Transaction id already exists');
            }else{
                request({
                      method: 'POST',
                      url: 'https://'+RazorPayKey+':'+RazorPaySecret+'@api.razorpay.com/v1/payments/'+payMentRazorPayId+'/capture',
                      form: {
                        amount: amountLocal
                      }
                    }, function (error, response, bodyString) {
                      if(error){
                        cb(ErrorParser.parseErrorreturnErrorMessage(error));
                      }else if(response.statusCode == 200 && bodyString){
                        var body = JSON.parse(bodyString);
                        if(body.error_code){
                            cb(body.error_description);
                        }else if((body.status == 'captured') || body.captured){
                            cb(null, 'success', body);
                        }else{
                            cb('Payment Failure - Unable to accept payment.\nPlease Try Again');
                        }
                      }else{
                        cb('Payment Failure with code: '+response.statusCode);
                      }
                    });
            }
        });
}

var createWalletTransaction = function(amountInvloved, paymentType, walletId){
    var current_date = (new Date()).valueOf().toString();
    var random = Math.random().toString();
    var createParams = {
            transactionId: crypto.createHash('sha1').update(current_date + random).digest('hex'),
            amountInvloved: amountInvloved,
            paymentType: paymentType,
            transactionInfo: {
                remarks: 'Auction Transactions',
                otherInfo: {}
            },
            walletId: walletId
    }
    WalletTransaction.create(createParams, function(err, result){
        if(err){
            res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});
        }else{
            res.jsonp({status: 'success', successData:'Added Rs.'+amountToAdd+' to your wallet.'});
        }
    });
}

exports.checkAndReturnIfBidAcceptable = function(user, minWalletAmount, auctionId, childLotId, cb){
    if(user){
        Wallet.findOne({user: user._id}, function(err, wallet){
            if(err){
                cb({error: ErrorParser.parseErrorreturnErrorMessage(err)});
            }else if(wallet){
                var isInArray = wallet.freezedForAuctions.some(function (freezedAuction) {
                    console.log(freezedAuction, childLotId);
                    return freezedAuction.equals(childLotId);
                });
                console.log("isInArray", isInArray);
                if(isInArray){
                    cb('already done');
                }else{
                        if(wallet.amount >= minWalletAmount){
                            var freezedamount = 0;
                            if(wallet.freezedAmount){
                                freezedamount = wallet.freezedAmount;
                            }
                            var walletFreezedAuctionIds = [];
                            if(wallet.freezedForAuctions){
                                walletFreezedAuctionIds = wallet.freezedForAuctions;
                            }
                            walletFreezedAuctionIds.push(childLotId);
                            Wallet.update({user: user._id}, {freezedAmount : (freezedamount+minWalletAmount),
                                            amount: ((wallet.amount)-minWalletAmount), 
                                            freezedForAuctions: walletFreezedAuctionIds}, 
                                            function(err, result){
                                if(err){
                                    cb({error: ErrorParser.parseErrorreturnErrorMessage(err)});
                                }else{
                                    var current_date = (new Date()).valueOf().toString();
                                    var random = Math.random().toString();
                                    var createParams = {
                                            transactionId: crypto.createHash('sha1').update(current_date + random).digest('hex'),
                                            amountInvloved: minWalletAmount,
                                            paymentType: "freezed",
                                            transactionInfo: {
                                                remarks: 'Auction Transactions',
                                                otherInfo: {}
                                            },
                                            walletId: wallet._id
                                    }
                                    WalletTransaction.create(createParams, function(err, result){
                                        if(err){
                                           cb({error: ErrorParser.parseErrorreturnErrorMessage(err)});
                                        }else{
                                            cb(null,'Freezed '+minWalletAmount+' from your wallet.');
                                        }
                                    });
                                }
                            });
                        }else{
                            var walletAmount = 0;
                            if(wallet.amount){
                                walletAmount = wallet.amount;
                            }
                            var differenceRequired = 0;
                            if(minWalletAmount){
                                differenceRequired = minWalletAmount - walletAmount;
                                if(differenceRequired<0){
                                    differenceRequired = 0;
                                }
                            }
                            cb({error: 'Wallet amount below required minimum. Mimimum required: '+minWalletAmount,
                                    amountRequiredExtra: differenceRequired});
                        }
                }
            }else{
                Wallet.create({user: user._id, amount: 0}, function(err, result){
                    if(minWalletAmount<=0){
                        cb(null, 'success');
                    }else{
                        cb({error: 'Wallet amount below minimum', amountRequiredExtra: minWalletAmount});
                    }
                });
            }
        });
    }else{
        cb({error: 'user not found'});
    }
}



exports.checkAndReturnIfAggregateOrderPossible = function(user, minWalletAmount, buyingOrderId, cb){
    if(user){
        Wallet.findOne({user: user._id}, function(err, wallet){
            if(err){
                cb({error: ErrorParser.parseErrorreturnErrorMessage(err)});
            }else if(wallet){
                var isInArray = wallet.freezedForAggregate.some(function (aggregateBuyingOrderId) {
                    return aggregateBuyingOrderId.equals(buyingOrderId);
                });
                if(isInArray){
                    cb('already done');
                }else{
                        if(wallet.amount >= minWalletAmount){
                            var freezedamount = 0;
                            if(wallet.freezedAmount){
                                freezedamount = wallet.freezedAmount;
                            }
                            var walletFreezedAggregateIds = [];
                            if(wallet.freezedForAggregate){
                                walletFreezedAggregateIds = wallet.freezedForAggregate;
                            }
                            walletFreezedAggregateIds.push(buyingOrderId);
                            Wallet.update({user: user._id}, {freezedAmount : (freezedamount+minWalletAmount),
                                            amount: ((wallet.amount)-minWalletAmount), 
                                            freezedForAggregate: walletFreezedAggregateIds}, 
                                            function(err, result){
                                if(err){
                                    cb({error: ErrorParser.parseErrorreturnErrorMessage(err)});
                                }else{
                                    var current_date = (new Date()).valueOf().toString();
                                    var random = Math.random().toString();
                                    var createParams = {
                                            transactionId: crypto.createHash('sha1').update(current_date + random).digest('hex'),
                                            amountInvloved: minWalletAmount,
                                            paymentType: 'freezed',
                                            transactionInfo: {
                                                remarks: 'Aggregate EMD Transaction',
                                                otherInfo: {}
                                            },
                                            walletId: wallet._id
                                    }
                                    WalletTransaction.create(createParams, function(err, result){
                                        if(err){
                                            cb({error: ErrorParser.parseErrorreturnErrorMessage(err)});
                                        }else{
                                            cb(null,'Freezed '+minWalletAmount+' from your wallet.');
                                        }
                                    });
                                }
                            });
                        }else{
                            var walletAmount = 0;
                            if(wallet.amount){
                                walletAmount = wallet.amount;
                            }
                            var differenceRequired = 0;
                            if(minWalletAmount){
                                differenceRequired = minWalletAmount - walletAmount;
                                if(differenceRequired<0){
                                    differenceRequired = 0;
                                }
                            }
                            cb({message: 'Wallet amount below required minimum. Mimimum required: '+minWalletAmount,
                                    amountRequiredExtra: differenceRequired});
                        }
                }
            }else{
                Wallet.create({user: user._id, amount: 0}, function(err, result){
                    if(minWalletAmount<=0){
                        cb(null, 'success');
                    }else{
                        cb({message: 'Wallet amount below minimum', amountRequiredExtra: minWalletAmount});
                    }
                });
            }
        });
    }else{
        cb({error: 'user not found'});
    }
};


exports.checkAndReturnIfDirectBuyPossible = function(user, minWalletAmount, buyingOrderId, cb){
    if(user){
        Wallet.findOne({user: user._id}, function(err, wallet){
            if(err){
                cb({error: ErrorParser.parseErrorreturnErrorMessage(err)});
            }else if(wallet){
                if(wallet.amount >= minWalletAmount){
                    Wallet.update({user: user._id}, {amount: (wallet.amount-minWalletAmount)}, function(err, result){
                                    if(err){
                                        cb({error: ErrorParser.parseErrorreturnErrorMessage(err)});
                                    }else{
                                        var current_date = (new Date()).valueOf().toString();
                                        var random = Math.random().toString();
                                        var createParams = {
                                                transactionId: crypto.createHash('sha1').update(current_date + random).digest('hex'),
                                                amountInvloved: minWalletAmount,
                                                paymentType: 'debit',
                                                transactionInfo: {
                                                    remarks: 'Debit for direct buy order',
                                                    otherInfo: {}
                                                },
                                                walletId: wallet._id
                                        }
                                        WalletTransaction.create(createParams, function(err, result){
                                            if(err){
                                                cb({error: ErrorParser.parseErrorreturnErrorMessage(err)});
                                            }else{
                                                let response = {};
                                                response['message'] = 'Debited '+minWalletAmount+' from your wallet.';
                                                response['walletTransactionId'] = result._id;
                                                cb(null,response);
                                            }
                                        });
                                    }
                                });
                }else{
                    var walletAmount = 0;
                    if(wallet.amount){
                        walletAmount = wallet.amount;
                    }
                    var differenceRequired = 0;
                    if(minWalletAmount){
                        differenceRequired = minWalletAmount - walletAmount;
                        if(differenceRequired<0){
                            differenceRequired = 0;
                        }
                    }
                    cb({message: 'Wallet amount below required minimum. Mimimum required: '+minWalletAmount,
                            amountRequiredExtra: differenceRequired});
                }
            }else{
                Wallet.create({user: user._id, amount: 0}, function(err, result){
                    if(minWalletAmount<=0){
                        let response = {};
                        response['message'] = 'success';
                        cb(null, response);
                    }else{
                        cb({message: 'Wallet amount below minimum', amountRequiredExtra: minWalletAmount});
                    }
                });
            }
        });
    }else{
        cb({error: 'user not found'});
    }
}


exports.checkAndReturnIfAggregateCanBeCompleted = function(user, minWalletAmount, buyingOrder, cb){
    if(user){
        Wallet.findOne({user: user._id}, function(err, wallet){
            if(err){
                cb({error: ErrorParser.parseErrorreturnErrorMessage(err)});
            }else if(wallet){
                if(wallet.amount >= minWalletAmount){
                    Wallet.update({user: user._id}, {amount: (wallet.amount-minWalletAmount), $pullAll: {freezedForAggregate: [buyingOrder._id] }, freezedAmount: wallet.freezedAmount - buyingOrder.emd}, function(err, result){
                                    if(err){
                                        cb({error: ErrorParser.parseErrorreturnErrorMessage(err)});
                                    }else{
                                        var current_date = (new Date()).valueOf().toString();
                                        var random = Math.random().toString();
                                        var createParams = {
                                                transactionId: crypto.createHash('sha1').update(current_date + random).digest('hex'),
                                                amountInvloved: minWalletAmount,
                                                paymentType: 'debit',
                                                transactionInfo: {
                                                    remarks: 'Debit for direct buy order',
                                                    otherInfo: {}
                                                },
                                                walletId: wallet._id
                                        }
                                        WalletTransaction.create(createParams, function(err, result){
                                            if(err){
                                                cb({error: ErrorParser.parseErrorreturnErrorMessage(err)});
                                            }else{
                                                let response = {};
                                                response['message'] = 'Debited '+minWalletAmount+' from your wallet.';
                                                response['walletTransactionId'] = result._id;
                                                cb(null,response);
                                            }
                                        });
                                    }
                                });
                }else{
                    var walletAmount = 0;
                    if(wallet.amount){
                        walletAmount = wallet.amount;
                    }
                    var differenceRequired = 0;
                    if(minWalletAmount){
                        differenceRequired = minWalletAmount - walletAmount;
                        if(differenceRequired<0){
                            differenceRequired = 0;
                        }
                    }
                    cb({message: 'Wallet amount below required minimum. Mimimum required: '+minWalletAmount,
                            amountRequiredExtra: differenceRequired});
                }
            }else{
                Wallet.create({user: user._id, amount: 0}, function(err, result){
                    if(minWalletAmount<=0){
                        let response = {};
                        response['message'] = 'success';
                        cb(null, response);
                    }else{
                        cb({message: 'Wallet amount below minimum', amountRequiredExtra: minWalletAmount});
                    }
                });
            }
        });
    }else{
        cb({error: 'user not found'});
    }   
}




exports.checkAndReturnIfAuctionCanBeBought = function(user, minWalletAmount, frozenEMD, auctionId, childLotId, cb){
    if(user){
        Wallet.findOne({user: user._id}, function(err, wallet){
            if(err){
                cb({error: ErrorParser.parseErrorreturnErrorMessage(err)});
            }else if(wallet){
                if(wallet.amount >= minWalletAmount){
                    let purgedFreezedForAuctions = [];
                    for(let k=0; k<wallet.freezedForAuctions.length; k++){
                        if(wallet.freezedForAuctions[k].equals(childLotId)){
                            continue;
                        }else{
                            purgedFreezedForAuctions.push(wallet.freezedForAuctions[k]);
                        }
                    }
                    updateChanges = {
                        amount: wallet.amount - minWalletAmount,
                        freezedAmount: wallet.freezedAmount - frozenEMD,
                        freezedForAuctions: purgedFreezedForAuctions
                    }
                    Wallet.update({user: user._id}, updateChanges, function(err, result){
                                    if(err){
                                        cb({error: ErrorParser.parseErrorreturnErrorMessage(err)});
                                    }else{
                                        var current_date = (new Date()).valueOf().toString();
                                        var random = Math.random().toString();
                                        var createParams = {
                                                transactionId: crypto.createHash('sha1').update(current_date + random).digest('hex'),
                                                amountInvloved: minWalletAmount + frozenEMD,
                                                paymentType: 'debit',
                                                transactionInfo: {
                                                    remarks: 'Debit for buying from auction',
                                                    otherInfo: {}
                                                },
                                                walletId: wallet._id
                                        }
                                        WalletTransaction.create(createParams, function(err, result){
                                            if(err){
                                                cb({error: ErrorParser.parseErrorreturnErrorMessage(err)});
                                            }else{
                                                let response = {};
                                                response['message'] = 'Debited '+minWalletAmount+frozenEMD+' from your wallet.';
                                                response['walletTransactionId'] = result._id;
                                                cb(null,response);
                                            }
                                        });
                                    }
                                });
                }else{
                    var walletAmount = 0;
                    if(wallet.amount){
                        walletAmount = wallet.amount;
                    }
                    var differenceRequired = 0;
                    if(minWalletAmount){
                        differenceRequired = minWalletAmount - walletAmount;
                        if(differenceRequired<0){
                            differenceRequired = 0;
                        }
                    }
                    cb({message: 'Wallet amount below required minimum. Mimimum required: '+minWalletAmount,
                            amountRequiredExtra: differenceRequired});
                }
            }else{
                Wallet.create({user: user._id, amount: 0}, function(err, result){
                    if(minWalletAmount<=0){
                        let response = {};
                        response['message'] = 'success';
                        cb(null, response);
                    }else{
                        cb({message: 'Wallet amount below minimum', amountRequiredExtra: minWalletAmount});
                    }
                });
            }
        });
    }else{
        cb({error: 'user not found'});
    }
}










