var mongoose = require('mongoose');
var moment = require('moment');

var aux = require(__utils + '/auxilaryFunc');
var ErrorParser = require(__utils + '/errorParse');
var UsersUtils = require(__utils + '/users');
var config = require(__base + '/config/config');

var User = require(__base + '/models/user/user');
var RegisteredLot = require(__base + '/models/lots/registered-lot');
var ApprovedLot = require(__base + '/models/lots/approved-lot');
var Lot = require(__base + '/models/lots/lot');
var SellingOrder = require(__base + '/models/orders/selling-req');
var Product = require(__base + '/models/products/product');
var BaseProduct = require(__base + '/models/products/base-product');
var BuyingOrder = require(__base + '/models/orders/buying-req');
var UserRating = require(__base + '/models/stats/user-rating');
var ProductCategory = require(__base + '/models/products/product-category.js');
var ProductImages = require(__base + '/models/products/productImages.js');
var Wishlist = require(__base + '/models/user/wishlist.js');
var Bid 			= require(__base + '/models/bids/bid');
var AuctionReq  = require(__base + '/models/orders/auction-req.js');
var Auction 		= require(__base + '/models/auctions/auction');


var multer = require('multer');
var fs = require('fs');
var mime = require('mime');
var crypto = require('crypto');


var ParseFewLotDetails = exports.ParseFewLotDetails = function(lot, userId, callback) {
	if (lot) {

		var lotDetails = {};
		var lotItemDetails = [];

		lotDetails['lotId'] = lot._id;
		lotDetails['lotName'] = lot.lotName;
		lotDetails['dateLotCreated'] = lot.dateCreated;
		lotDetails['lotType'] = lot.lotType;
		lotDetails['numberOfItems'] = lot.lotContents.length;
		lotDetails['dateWindow'] = {};
		lotDetails['dateWindow']['endDate'] = lot.endDate;
		lotDetails['dateWindow']['startDate'] = lot.startDate;
		lotDetails['lotSoldOut'] = lot.lotSoldOut;
		lotDetails['datelotSoldOut'] = lot.datelotSoldOut;
		lotDetails['lotImageURL'] = config.baseURL + "/getFile/lotPicture/" + lot._id;
		lotDetails['createdAt'] = lot.createdAt;
		lotDetails['lotTermsAndConditions'] = lot.lotTermsAndConditions;

		lotDetails['tags'] = {};
		lotDetails['tags']['productCategoryTags'] = CalculateProductCategoryTags(lot);
		if (lot.brands.length > 0)
			lotDetails['tags']['brandNameTags'] = lot.brands;
		if (lot.color.length > 0)
			lotDetails['tags']['colorTags'] = lot.color;
		if (lot.model_number.length > 0)
			lotDetails['tags']['modelNumberTags'] = lot.model_number;
		if (lot.processor_brand.length > 0)
			lotDetails['tags']['processorBrandTags'] = lot.processor_brand;
		if (lot.operating_system.length > 0)
			lotDetails['tags']['operatingSystemTags'] = lot.operating_system;

		var response = {};
		response['lotDetails'] = lotDetails;
		response['lotItemDetails'] = lotItemDetails;


		let j = 0;
		function asyncFor(j) {
			if (j < lot.lotContents.length) {
				var item = {};
				item['productId'] = lot.lotContents[j].pId._id;
				item['quantityAvailable'] = lot.lotContents[j].quantity;
				item['productName'] = lot.lotContents[j].pId.baseProductId.productName;

				if(lotDetails['lotType'] === "auction" || lotDetails['lotType'] === "buy" || lotDetails['lotType'] === "clearance"){
					item['perItemPrice'] = lot.lotContents[j].price;
					item['perItemEmd'] = lot.lotContents[j].emdDeposit;
				}
				if(lotDetails['lotType'] === "rental"){
					item['rentalPricePerMonth'] = lot.lotContents[j].rentalPricePerMonth;
					item['rentalSecurityDeposit'] = lot.lotContents[j].rentalSecurityDeposit;
					item['rentalPriceByNumberOfMonths'] = lot.lotContents[j].rentalPriceByNumberOfMonths;
					item['perItemPrice'] = lot.lotContents[j].price;
					item['perItemEmd'] = lot.lotContents[j].emdDeposit;
					item['originalPrice'] = lot.lotContents[j].pId.baseProductId.marketCost;
				}

				//item accessories
				item['itemAccessories'] = [];
				for (var k = 0; k < lot.lotContents[j].pId.baseProductId.accessoriesList.length; k++) {
					var accTmp = {};
					accTmp['accessoryname'] = lot.lotContents[j].pId.baseProductId.accessoriesList[k]['accessoryname'];
					accTmp['score'] = lot.lotContents[j].pId.baseProductId.accessoriesList[k]['score'];
					accTmp['isAccAvailable'] = lot.lotContents[j].pId.accessorieslistavail[k];
					item['itemAccessories'].push(accTmp);
				}

				//item condition
				item['itemCondition'] = {};
				item['itemCondition']['workingcondition'] = lot.lotContents[j].pId.workingcondition;
				item['itemCondition']['originalbox'] = lot.lotContents[j].pId.originalbox;
				item['itemCondition']['validbill'] = lot.lotContents[j].pId.validbill;
				item['itemCondition']['condition'] = lot.lotContents[j].pId.condition;
				item['itemCondition']['ageinmonths'] = lot.lotContents[j].pId.ageinmonths;
				item['itemCondition']['itemProblems'] = [];

				for (var k = 0; k < lot.lotContents[j].pId.baseProductId.problemsList.length; k++) {
					var problemTmp = {};
					problemTmp['problemname'] = lot.lotContents[j].pId.baseProductId.problemsList[k]['problemname'];
					problemTmp['score'] = lot.lotContents[j].pId.baseProductId.problemsList[k]['score'];
					problemTmp['remarks'] = lot.lotContents[j].pId.baseProductId.problemsList[k]['remarks'];
					problemTmp['isProblemBeingFaced'] = lot.lotContents[j].pId.problemslistavail[k];
					item['itemCondition']['itemProblems'].push(problemTmp);
				}

				//item specifications
				item['specifications'] = lot.lotContents[j].pId.baseProductId.specifications;


				lotItemDetails.push(item);
				j += 1;
				asyncFor(j);
			} else {
				if (userId) {
					IsInWishlist(lot._id, userId, function(err, isInWishlist) {
						if (err) {
							lotDetails['isInWishlist'] = false;
						} else {
							lotDetails['isInWishlist'] = isInWishlist;
						}
						if (lot.lotType === "buy" || lot.lotType === "clearance") {
							CalculateBuyOrders(lot._id, function(err, buyingOrderCount) {
								if (err) {
									callback(err);
								} else {
									lotDetails['noOfOrders'] = buyingOrderCount;
									lotDetails['isClearanceStock'] = lot.isClearanceStock;
									lotDetails['clearanceDiscountPercent'] = lot.clearanceDiscountPercent;
									var response = {};
									response['lotDetails'] = lotDetails;
									response['lotItemDetails'] = lotItemDetails;
									callback(null, response);
								}
							});
						} else if (lot.lotType == "rental") {
							var response = {};
							response['lotDetails'] = lotDetails;
							response['lotItemDetails'] = lotItemDetails;
							callback(null, response);
						} else if (lot.lotType == "auction") {
							let endDateNow = new Date(lot.endDate).getTime();
							var currentTime = new Date().getTime();
							lotDetails['auctionTimeLeftInMS'] = endDateNow + 2*60*1000 - currentTime;
							var response = {};
							response['lotDetails'] = lotDetails;
							response['lotItemDetails'] = lotItemDetails;
							callback(null, response);
						} else {

							callback("Invalid lot type");
						}
					});
				} else {
					if (lot.lotType == "buy" || lot.lotType == "clearance") {
						CalculateBuyOrders(lot._id, function(err, buyingOrderCount) {
							if (err) {
								callback(err);
							} else {
								lotDetails['noOfOrders'] = buyingOrderCount;
								lotDetails['isClearanceStock'] = lot.isClearanceStock;
								lotDetails['clearanceDiscountPercent'] = lot.clearanceDiscountPercent;
								var response = {};
								response['lotDetails'] = lotDetails;
								response['lotItemDetails'] = lotItemDetails;
								callback(null, response);
							}
						});
					} else if (lot.lotType == "rental") {
						var response = {};
						response['lotDetails'] = lotDetails;
						response['lotItemDetails'] = lotItemDetails;
						callback(null, response);
					} else if (lot.lotType == "auction") {
						let endDateNow = new Date(lot.endDate).getTime();
						var currentTime = new Date().getTime();
						lotDetails['auctionTimeLeftInMS'] = endDateNow + 2*60*1000 - currentTime;
						var response = {};
						response['lotDetails'] = lotDetails;
						response['lotItemDetails'] = lotItemDetails;
						callback(null, response);
					} else {
						callback("Invalid lot type");
					}
				}
			}
		}
		asyncFor(j);
	} else {
		callback({
			message: "Lot not found"
		});
	}
};

var ParseLotDetails = exports.ParseLotDetails = function(lot, userId, callback) {
	if (lot) {
		var lotDetails = {};
		var lotItemDetails = [];

		lotDetails['lotId'] = lot._id;
		lotDetails['lotName'] = lot.lotName;
		lotDetails['dateLotCreated'] = lot.dateCreated;
		lotDetails['lotType'] = lot.lotType;
		lotDetails['numberOfItems'] = lot.lotContents.length;
		lotDetails['lotImage'] = lot.lotImageURL;
		lotDetails['dateWindow'] = {};
		lotDetails['dateWindow']['endDate'] = lot.endDate;
		lotDetails['dateWindow']['startDate'] = lot.startDate;
		lotDetails['lotSoldOut'] = lot.lotSoldOut;
		lotDetails['lotImageURL'] = "http://urbanriwaaz.com:8080/api/getFile/lotPicture/" + lot._id;
		lotDetails['createdAt'] = lot.createdAt;
		lotDetails['lotTermsAndConditions'] = lot.lotTermsAndConditions;

		
		if (lot.lotSoldOut == true) lotDetails['datelotSoldOut'] = lot.datelotSoldOut;

		lotDetails['tags'] = {};
		lotDetails['tags']['productCategoryTags'] = CalculateProductCategoryTags(lot);
		if (lot.brands.length > 0)
			lotDetails['tags']['brandNameTags'] = lot.brands;
		if (lot.color.length > 0)
			lotDetails['tags']['colorTags'] = lot.color;
		if (lot.model_number.length > 0)
			lotDetails['tags']['modelNumberTags'] = lot.model_number;
		if (lot.processor_brand.length > 0)
			lotDetails['tags']['processorBrandTags'] = lot.processor_brand;
		if (lot.operating_system.length > 0)
			lotDetails['tags']['operatingSystemTags'] = lot.operating_system;

		IsInWishlist(lot._id, userId, function(err, isInWishlist) {
			if (!err) lotDetails['isInWishlist'] = isInWishlist;
			CalculateLotRating(lot._id, userId, function(err, ratingResult) {
				if (err) {
					callback(err);
				} else {
					lotDetails['lotRating'] = ratingResult;
					let i = 0;

					function asyncFor(i) {
						if (i < lot.lotContents.length) {
							var item = {};

							item['productId'] = lot.lotContents[i].pId._id;
							item['baseProductId'] = lot.lotContents[i].pId.baseProductId._id;
							item['quantityAvailable'] = lot.lotContents[i].quantity;
							item['productName'] = lot.lotContents[i].pId.baseProductId.productName;


							if(lotDetails['lotType'] === "auction" || lotDetails['lotType'] === "buy" || lotDetails['lotType'] === "clearance"){
								item['perItemPrice'] = lot.lotContents[i].price;
								item['perItemEmd'] = lot.lotContents[i].emdDeposit;
							}
							if(lotDetails['lotType'] === "rental"){
								item['rentalPricePerMonth'] = lot.lotContents[i].rentalPricePerMonth;
								item['rentalSecurityDeposit'] = lot.lotContents[i].rentalSecurityDeposit;
								item['rentalPriceByNumberOfMonths'] = lot.lotContents[i].rentalPriceByNumberOfMonths;
								item['perItemPrice'] = lot.lotContents[i].price;
								item['perItemEmd'] = lot.lotContents[i].emdDeposit;
							}
							if(lotDetails['lotType'] === "auction"){
								item['auctionMinDeposit'] = lot.lotContents[i].auctionMinDeposit;
							}

							GetProductImagesURLArray(lot.lotContents[i].pId._id, function(err, productImagesArray) {
								if (err) {
									callback(err);
								} else {
									item['imageURLS'] = productImagesArray;
									//item accessories
									item['itemAccessories'] = [];
									for (var k = 0; k < lot.lotContents[i].pId.baseProductId.accessoriesList.length; k++) {
										var accTmp = {};
										accTmp['accessoryname'] = lot.lotContents[i].pId.baseProductId.accessoriesList[k]['accessoryname'];
										accTmp['score'] = lot.lotContents[i].pId.baseProductId.accessoriesList[k]['score'];
										accTmp['isAccAvailable'] = lot.lotContents[i].pId.accessorieslistavail[k];
										item['itemAccessories'].push(accTmp);
									}
									//item condition
									item['itemCondition'] = {};
									item['itemCondition']['workingcondition'] = lot.lotContents[i].pId.workingcondition;
									item['itemCondition']['originalbox'] = lot.lotContents[i].pId.originalbox;
									item['itemCondition']['validbill'] = lot.lotContents[i].pId.validbill;
									item['itemCondition']['condition'] = lot.lotContents[i].pId.condition;
									item['itemCondition']['ageinmonths'] = lot.lotContents[i].pId.ageinmonths;
									item['itemCondition']['itemProblems'] = [];

									for (var k = 0; k < lot.lotContents[i].pId.baseProductId.problemsList.length; k++) {
										var problemTmp = {};
										problemTmp['problemname'] = lot.lotContents[i].pId.baseProductId.problemsList[k]['problemname'];
										problemTmp['score'] = lot.lotContents[i].pId.baseProductId.problemsList[k]['score'];
										problemTmp['remarks'] = lot.lotContents[i].pId.baseProductId.problemsList[k]['remarks'];
										problemTmp['isProblemBeingFaced'] = lot.lotContents[i].pId.problemslistavail[k];
										item['itemCondition']['itemProblems'].push(problemTmp);
									}

									//item specifications
									item['specifications'] = lot.lotContents[i].pId.baseProductId.specifications;
									lotItemDetails.push(item);
									i += 1;
									asyncFor(i);
								}
							});;
						} else {
							if (lot.lotType == "buy" || lot.lotType == "clearance") {
								CalculateBuyOrders(lot._id, function(err, buyingOrderCount) {
									if (err) {
										callback(err);
									} else {
										lotDetails['noOfOrders'] = buyingOrderCount;
										lotDetails['isClearanceStock'] = lot.isClearanceStock; //only buy type lot can be clearance stock
										lotDetails['clearanceDiscountPercent'] = lot.clearanceDiscountPercent;
										var response = {};
										response['lotDetails'] = lotDetails;
										response['lotItemDetails'] = lotItemDetails;
										callback(null, response);
									}
								});
							} else if (lot.lotType == "rental") {

								//perMonthPrice
								//numberOfMonthsPrice
								//securityDeposit
								
								


								var response = {};
								response['lotDetails'] = lotDetails;
								response['lotItemDetails'] = lotItemDetails;
								callback(null, response);
							} else if (lot.lotType == "auction") {
								lotDetails['bidIncreaseDetails'] = lot.overallBidIncreaseDetails;
								let endDateNow = new Date(lot.endDate).getTime();
								var currentTime = new Date().getTime();
								lotDetails['auctionTimeLeftInMS'] = endDateNow + 2*60*1000 - currentTime;
								var response = {};
								response['lotDetails'] = lotDetails;
								response['lotItemDetails'] = lotItemDetails;
								var bidParams = {};
								bidParams.parentLotId = lot._id;
								bidParams.emdPaid = true;
								bidParams.bidCancelled = {$ne: true};
								Bid.find(bidParams, function(err, bidsInParentLot){
									if(err){
										callback(err);
									}else{
										var userBids = {};
										var HighestLotBids = {};

										if(bidsInParentLot.length>0){

											for(let k=0; k<bidsInParentLot.length; k++){
												lotDetails['auctionId'] = bidsInParentLot[k].auctionId;
												let tmp = {};
												tmp['childLotId'] = bidsInParentLot[k].childLotId;
												tmp['orderDetails'] = bidsInParentLot[k].detailedOrderDetails;
												tmp['biddingProcessStarted'] = bidsInParentLot[k].biddingProcessStarted;
												tmp['emdPaid'] = bidsInParentLot[k].emdPaid;
												tmp['emdFrozen'] = bidsInParentLot[k].emdFrozen;
												tmp['bidAmount'] = bidsInParentLot[k].bidAmount;
												tmp['userId'] = bidsInParentLot[k].userId;

												if(bidsInParentLot[k].userId.equals(userId)){
													userBids[tmp['childLotId']] = {};
													userBids[tmp['childLotId']]['orderDetails'] = tmp['orderDetails'];
													userBids[tmp['childLotId']]['biddingProcessStarted'] = tmp['biddingProcessStarted'];
													userBids[tmp['childLotId']]['emdPaid'] = tmp['emdPaid'];
													userBids[tmp['childLotId']]['emdFrozen'] = tmp['emdFrozen'];
													userBids[tmp['childLotId']]['bidAmount'] = tmp['bidAmount'];
												}	
												if(tmp['childLotId'] in HighestLotBids){
													if(HighestLotBids[tmp['childLotId']]['bidAmount'] < tmp['bidAmount']){
														HighestLotBids[tmp['childLotId']] = {};
														HighestLotBids[tmp['childLotId']]['orderDetails'] = tmp['orderDetails'];
														HighestLotBids[tmp['childLotId']]['biddingProcessStarted'] = tmp['biddingProcessStarted'];
														HighestLotBids[tmp['childLotId']]['emdPaid'] = tmp['emdPaid'];
														HighestLotBids[tmp['childLotId']]['emdFrozen'] = tmp['emdFrozen'];
														HighestLotBids[tmp['childLotId']]['bidAmount'] = tmp['bidAmount'];
														HighestLotBids[tmp['childLotId']]['userId'] = tmp['userId'];
													}
												}else{
													if(tmp['biddingProcessStarted'] === true){
														HighestLotBids[tmp['childLotId']] = {};
														HighestLotBids[tmp['childLotId']]['orderDetails'] = tmp['orderDetails'];
														HighestLotBids[tmp['childLotId']]['biddingProcessStarted'] = tmp['biddingProcessStarted'];
														HighestLotBids[tmp['childLotId']]['emdPaid'] = tmp['emdPaid'];
														HighestLotBids[tmp['childLotId']]['emdFrozen'] = tmp['emdFrozen'];
														HighestLotBids[tmp['childLotId']]['bidAmount'] = tmp['bidAmount'];
														HighestLotBids[tmp['childLotId']]['userId'] = tmp['userId'];
													}
												}
												console.log(HighestLotBids,'$');										
											}
										}
										if(userId){
											response['userBids'] = userBids;
											response['HighestLotBids'] = HighestLotBids;
										}
										callback(null, response);
									}
								});






								






							} else {
								callback("Invalid lot type");
							}
						}
					}
					asyncFor(i);

				}
			});
		});
	} else {
		callback({
			message: "Lot not found"
		});
	}
};

var CalculateLotRating = exports.CalculateLotRating = function(lotId, userId, callback) {
	Lot.findById(lotId)
		.exec(function(err, lot) {
			if (err) {
				callback(err);
			} else {
				if (lot) {
					UserRating.aggregate([{
						'$match': {
							'lotId': mongoose.Types.ObjectId(lotId)
						}
					}, {
						'$group': {
							'_id': '$rating',
							'numberOfStars': {
								'$sum': 1
							}
						}
					}], function(err, result) {
						if (err) {
							callback(err);
						} else {
							var resultObj = {};
							resultObj['lotId'] = lotId;
							resultObj['stars_dist'] = result;
							var stars = {};
							var totalStars = 0;
							var weightedAvg = 0;
							stars[1] = 0;
							stars[2] = 0;
							stars[3] = 0;
							stars[4] = 0;
							stars[5] = 0;
							for (var i = 0; i < resultObj['stars_dist'].length; i++) {
								totalStars += resultObj['stars_dist'][i]['numberOfStars'];
								stars[resultObj['stars_dist'][i]['_id']] += resultObj['stars_dist'][i]['numberOfStars'];
							}
							for (var i = 1; i <= 5; i++) {
								weightedAvg += (stars[i] * i);
							}
							weightedAvg /= totalStars;
							resultObj['averageRating'] = weightedAvg;
							if (userId) {
								UserRating.findOne({
									userId: userId,
									lotId: lotId
								}, function(err, ratingGivenByUser) {
									if (err) {
										callback(err);
									} else {
										if (ratingGivenByUser) {
											resultObj['ratingGivenByUser'] = {};
											resultObj['ratingGivenByUser']['rating'] = ratingGivenByUser.rating;
											resultObj['ratingGivenByUser']['userId'] = ratingGivenByUser.userId;
										} else {
											resultObj['ratingGivenByUser'] = "User did not rate lot";
										}
										callback(null, resultObj);
									}
								});
							} else {
								callback(null, resultObj);
							}
						}
					});
				} else {
					let err = {};
					err['message'] = "No lot found for lotId: " + lotId;
					callback(err);
				}
			}
		});
};

var CalculateProductCategoryTags = exports.CalculateProductCategoryTags = function(lot) {
	var tags = {};
	for (var i = 0; i < lot.lotContents.length; i++) {
		tags[lot.lotContents[i].pId.baseProductId.productCategoryId.categoryName] = 1;
	}
	return aux.ConvertDictToArray(tags);
};


var CalculateBuyOrders = exports.CalculateBuyOrders = function(lotId, callback) {
	var errorMsg = "";
	var returnObj = null;
	BuyingOrder.count({
		lotId: lotId,
		cancelBuyingOrder: false
	}, function(err, buyingCount) {
		if (err) {
			errorMsg = err;
		} else {
			returnObj = buyingCount;
		}
		callback(errorMsg, returnObj);
	});
};


var IsInWishlist = exports.IsInWishlist = function(lotId, userId, callback) {
	var errorMsg = "";
	var returnObj = null;
	if (lotId) {
		if (userId) {
			Wishlist.findOne({
				userId: userId
			}, function(err, userWishlist) {
				if (err) {
					errorMsg = err;
				} else {
					if (userWishlist) {
						console.log("karun");
						console.log(userWishlist);
						userWishlist.lots.some(function(lot) {
							returnObj = lot.equals(lotId);
						});
						if (returnObj === null)
							returnObj = false;
					} else {
						errorMsg = "Could not find user wishlist";
					}
				}
				callback(errorMsg, returnObj);
			});
		} else {
			errorMsg = "userId not sent";
			callback(errorMsg, returnObj);
		}
	} else {
		errorMsg = "lotId not sent";
		callback(errorMsg, returnObj);
	}
};



var GetProductImagesURLArray = exports.GetProductImagesURLArray = function(pId, callback) {
	var errorMsg = "";
	var returnObj = null;
	ProductImages.find({
		pId: pId
	}, function(err, images) {
		if (err) {
			errorMsg = err;
		} else {
			if (images) {
				var tmpArr = [];
				for (var i = 0; i < images.length; i++) {
					tmpArr.push(images[i].imageURL);
				}
				returnObj = tmpArr;
			} else {
				errorMsg = "No images found.";
			}
		}
		callback(errorMsg, returnObj);
	});
};