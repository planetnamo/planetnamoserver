		   					if(lot.lotType == "buy"){
		   						var lotDetails = {};
		   						lotDetails['lotId'] 							= lot._id;
		   						lotDetails['lotName']							= lot.lotName;
		   						lotDetails['dateLotCreated']					= lot.dateCreated;
		   						lotDetails['lotType']							= lot.lotType;
		   						lotDetails['numberOfItems']						= lot.lotContents.length;
		   						lotDetails['lotRating']							= CalculateLotRating(lot._id);
		   						lotDetails['noOfOrders']						= CalculateBuyOrders(lot._id);
		   						lotDetails['lotImage']							= lot.lotImageURL;
		   						
		   						lotDetails['tags']								= {};
		   						lotDetails['tags']['productCategoryTags']		= CalculateProductCategoryTags(lot);
		   						if(lot.brands.length>0)
		   							lotDetails['tags']['brandNameTags']					= lot.brands;
		   						if(lot.color.length>0)
		   							lotDetails['tags']['colorTags']						= lot.color;
		   						if(lot.model_number.length>0)
		   							lotDetails['tags']['modelNumberTags']				= lot.model_number;
		   						if(lot.processor_brand.length>0)
		   							lotDetails['tags']['processorBrandTags']			= lot.processor_brand;		   						
		   						if(lot.operating_system.length>0)
		   							lotDetails['tags']['operatingSystemTags']			= lot.operating_system;	


		   						var lotItemDetails = [];
		   						for(var i=0; i<lot.lotContents.length; i++){
		   							var item = {};
		   							item['productId']			= lot.lotContents[i].pId._id;
		   							item['quantityAvailable']	= lot.lotContents[i].quantity;
		   							item['perItemPrice']		= lot.lotContents[i].price;
		   							item['perItemEmd']			= lot.lotContents[i].emdDeposit;
		   							item['rating']				= null
		   							item['images']				= GetProductImagesURLArray(lot.lotContents[i].pId._id);

		   							item['baseProductDetails'] = {};
		   							item['baseProductDetails']['specifications'] = {};
		   							item['baseProductDetails']['productName']
		   							item['baseProductDetails']['baseProductImageURL']
		   							item['baseProductDetails']['marketCost']
		   							item['baseProductDetails']['releaseDate']

		   							item['conditionOfProduct'] = {};
		   							item['conditionOfProduct']['conditonRating'] 
		   							item['conditionOfProduct']['validbill']
		   							item['conditionOfProduct']['originalbox']
		   							item['conditionOfProduct']['workingcondition']
		   							item['conditionOfProduct']['ageinmonths']
		   							item['conditionOfProduct']['condition']
		   							item['conditionOfProduct']['accessoriesAvailability']
		   							item['conditionOfProduct']['problemsFaced']

		   							lotItemDetails.push(item);
		   						}

		   						var response = {};
		   						response['lotDetails'] 		= lotDetails;
		   						response['lotItemDetails']	= lotItemDetails;
