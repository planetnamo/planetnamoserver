var config = require('../config/key')

exports.APIClientVerification = function(req, res, next){
	// check header or url parameters or post parameters for clientkey
	var clientkey = req.body.clientkey || req.query.clientkey || req.headers['x-access-clientkey'] || req.body.clientkey || req.headers.clientkey;
	if(clientkey == config.clientkey)
		next();
	else
		res.status(401).send({
			message: 'clientkey is missing or invalid. Request cannot be granted to unauthorized client!'
		});
};