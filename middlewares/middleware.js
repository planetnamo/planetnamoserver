//---------------MIDDLEWARES------------------------------------------------
exports.CheckIfAdmin = function(req, res, next) {
	if(res.req.user.role == 2)
		next();
	else
		return res.status(200).send({status: "error", message: "Unauthorized access!"});
};

exports.AllowLoggedInUserOrAdmin = function(req, res, next) {
	if(req.params.userId){
		if(res.req.user._id.equals(req.params.userId))
			next();
		else if(res.req.user.role == 2)
			next();
		else
			return res.status(200).send({status: "error", message: "Unauthorized access! Only logged in user can access this route."});
	}else if(res.req.user.role == 2){
		next();
	}else{
		return res.status(200).send({status: "error", message: "Unauthorized access!"});
	}

};
