//CALL THE PACKAGES --------------------------

//using the express framework
var express 		= require('express');
//path module provides utilities for working with file and directory paths
var path    		= require('path');
//mongodb framwork
var mongoose 		= require('mongoose');
//Morgan is used for logging request details. 
var morgan 			= require('morgan');
//Parse Cookie header and populate req.cookies with an object keyed by the cookie names. Optionally you may enable signed cookie support by passing a secret string, which assigns req.secret so it may be used by other middleware.
var cookieParser 	= require('cookie-parser');
//in order to read HTTP POST data , we have to use "body-parser" node module
var bodyParser 		= require('body-parser');
//jwt Token
var jwt = require('jsonwebtoken');
//multer
var multer = require('multer');
//Initialize express app
var app     = express();

var server = require('http').Server(app);

var io = require('socket.io')(server);
module.exports.io = io;


//Passport JWT Dependecies------------------
var passport = require("passport");
var passportJWT = require("passport-jwt");
var ExtractJwt = passportJWT.ExtractJwt;
var JwtStrategy = passportJWT.Strategy;
//---------------------------

//LOCAL SETTINGS----------------
var port    = process.env.PORT || 3000;
var configDB = require('./config/db.js');
//---------------------------


//MongoDB Connection---------------------------
//mongoose.connect(configDB.url); // MLabs connection
// mongoURI = 'mongodb://karun_sylv:Sylvester2017@ds159497.mlab.com:59497/planetnamo';
mongoURI = 'mongodb://devraj:devraj@ds139082.mlab.com:39082/planetnamo';
mongoose.connect(process.env.MONGODB_URI || mongoURI);
//mongoose.connect('mongodb://localhost/projectnamo-scratch'); //localhost connection
//---------------------------



//Directory path for modules
global.__modules = __dirname + '/modules'
global.__utils = __dirname + '/utils'
global.__base = __dirname 



//Passport Strategy--------------------------------------------------
var User        = require(__base + '/models/user/user.js')
var JWTKey      = require('./config/key');
var jwtOptions = {};
jwtOptions.secretOrKey = JWTKey.jwtKEY;
jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeader();
var strategy = new JwtStrategy(jwtOptions, function(jwt_payload, next) {
	console.log('payload received', jwt_payload);
  	User.findById(jwt_payload.id).exec(function(err, user){
	   if (user) {
	   	  //REMOVE PASSWORD HASH FROM THIS EXTRACTED USER
	      next(null, user);
 	    } else {
	      next(null, false);
	    }
    })
});
passport.use(strategy);
//--------------------------------------------------------------------








//Initializing passport to app // Passport-JWT 
 app.use(passport.initialize());
 app.use(passport.session());


//API ROUTE DEPENDENCY MODULES
var userRoutes = require('./modules/user/routes/user-routes')(app, express);
var productRoutes = require('./modules/product/routes/product-routes')(app, express);
var rentalRoutes = require('./modules/rental/routes/rental-routes')(app, express);
var auctionRoutes = require('./modules/auction/routes/auction-routes')(app, express);
var bidRoutes = require('./modules/auction/routes/bid-routes')(app, express);
//var transactionRoutes = require('./modules/transaction/routes/transaction-routes')(app, express);
var authRoutes = require('./modules/auth/routes/auth-routes')(app, express);
var sellingRoutes = require('./modules/selling/routes/selling-routes')(app, express);
var buyingRoutes = require('./modules/buying/routes/buying-routes')(app, express);
var lotRoutes = require('./modules/lots/routes/lots-routes')(app, express);
var walletRoutes = require('./modules/wallet/routes/wallet-routes')(app, express);
var miscRoutes = require('./modules/misc/routes/misc-routes')(app, express);
var cartRoutes = require('./modules/cart/routes/cart-routes')(app, express);
var deliveryRoutes = require('./modules/delivery/routes/delivery-routes')(app, express);

//MIDDLEWARE DEPENDENCIES--------------------------------------------------
var ClientVerification = require('./middlewares/APIclientverification');



//--------------------------------------------------------------------------



//app middleware stack ---------------------------------------------
app.use(morgan('dev'));
app.use(cookieParser());
app.use(bodyParser.urlencoded({
	extended: true
}));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')));
// middleware to configure our app to handle CORS requests
app.use(function(req, res, next) {
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
	res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With,content-type, Authorization, authorization, Content-Type');
	next();
});



//Only trusted third party apps having clientKEY can access any part of this api
//app.use(ClientVerification.APIClientVerification) 
//---------------------------------------------------------------------






//API ROUTES

app.use('/api/auth', authRoutes);
app.use('/api/user', userRoutes); 
app.use('/api/product', productRoutes); 
app.use('/api/lots', lotRoutes);
app.use('/api/auction',  auctionRoutes); 
app.use('/api/bid', bidRoutes);
app.use('/api/rental', rentalRoutes); 
app.use('/api/buy', buyingRoutes); 
app.use('/api/sell', sellingRoutes); 
app.use('/api/wallet', walletRoutes);
app.use('/api/misc', miscRoutes);
app.use('/api/cart', cartRoutes);
app.use('/api/delivery',deliveryRoutes);
app.use('/api/getFile', require(__modules+'/getFile/getFileCtrl'));

//Transaction API is completely protected from guests
//app.use('/api/transaction', passport.authenticate('jwt', { session: false }), transactionRoutes); 
 

var storage = multer.diskStorage({
  destination: function (request, file, callback) {
    callback(null, __base + '/uploads');
  },
  filename: function (request, file, callback) {
    console.log(file);
    callback(null, file.originalname)
  }
});
var upload = multer({storage: storage}).single('photo');

//Posting the file upload
app.post('/upload', function(request, response) {
  upload(request, response, function(err) {
	  if(err) {
	    console.log('Error Occured');
	    response.end('error');
	    return;
	  }
  	response.end('Your File Uploaded');
  	console.log('Photo Uploaded');
  })
});



//------------------------------------------------


//SERVER START
server.listen(port);
console.log('Magic happens at port ' + port);

io.on('connection', function (socket) {
  socket.on('joinAuction', function (auctionId) {
    socket.join(auctionId);
  });
  socket.on('leaveAuction', function(auctionId){
  	socket.leave(auctionId);
  });
});


