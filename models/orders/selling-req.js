var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/**
 *            Selling Schema
 */


var SellingSchema = new Schema({
  registeredLotId: {
    type: Schema.ObjectId,
    ref: 'RegisteredLot',
    required: true,
    unique: true
  },
  userId: {
    type: Schema.ObjectId,
    ref: 'User',
    required: true
  },
  transactionId: {
    type: Schema.ObjectId,
    ref: 'Transaction'
  },
  deliveryId:{
    type: Schema.ObjectId,
    ref: 'Delivery'
  },
  amount: {
    type: Number,
    required: true
  },
  amountAfterPhysicalCheckPerItem: {
    type: Number
  },
  amountAfterPhysicalCheckTotal: {
    type: Number
  },
  userApproved: {
    type: Boolean
  },
  planetNamoPoints: {
    type: Number
  },
  dateSellingOrderCancelled: {
    type: Date
  },
  dateSellingOrderFulfilled: {
    type: Date
  },
  cancelSellingOrder: {
    type: Boolean,
    default: false
  },
  cancelledBy: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  cancellationRemarks: {
    type: String
  },
  sellingOrderFulfilled: {
    type: Boolean,
    default: false
  },
  transactionSuccess: {
    type: Boolean,
    default: false
  },
  sellingOrderApproved: {
    type: Boolean,
    default: false
  },
  cancelledByAdminOrUser: {
    type: String
  }

}, {
  timestamps: true
});


module.exports = mongoose.model('Selling', SellingSchema);
