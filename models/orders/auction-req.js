var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var AuctionRequestSchema = new Schema({
  parentLotId: {
    type: Schema.ObjectId,
    ref: 'Lot'
  },
  auctionId: {
    type: Schema.ObjectId,
    ref: 'Auction'
  },
  userId: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  userBidByChildLots: [{
    childLotId: {type: Schema.ObjectId, ref: 'Lot'},
    bidId: {type: Schema.ObjectId, ref: 'Bid'},
    bidAmount: {type: Number},
    isWithdrawn: {type: Boolean},
    dateWithdrawn: {type: Boolean},
    amountForfeitted: {type: Number},
    emdPaid: {type: Boolean},
    emdAmount: {type: Number},
    dateEMDPaid: {type: Boolean},
    won: {type: Boolean},
    totalAmountToPay: {type: Number},
    orderDetails: [{
      productName: {type: String},
      productId: {type: Schema.ObjectId, ref: 'Product'},
      productStartingBidPrice: {type: Number},
      quantity: {type: Number},
      emdDeposit: {type: Number},
      productPerItemPrice: {type: Number},
      _id: false
    }],
    _id: false
  }],
  bids: [{
    type: Schema.ObjectId,
    ref: 'Bid',
    _id: false
  }],
  auctionInteractionHistory: [{
    dateOfInteraction: {type: Date},
    details: {type: String},
    _id: false
  }]
},{
  timestamps: true
});



module.exports = mongoose.model('AuctionRequest', AuctionRequestSchema);
