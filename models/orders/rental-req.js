var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/**
 *              Rental-Request Schema
 */


var OrderDetailsSchema = new Schema({
  pId: {
    type: Schema.ObjectId,
    ref: 'Product'
  },
  quantity: {
    type: Number
  }
});

var RentalSchema = new Schema({
  lotId: {
    type: Schema.ObjectId,
    ref: 'Lot'
  },
  userId: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  transactionId: {
    type: Schema.ObjectId,
    ref: 'Transaction'
  },
  deliveryId:{
    type: Schema.ObjectId,
    ref: 'Delivery'
  },
  rentAmountToPay: {
    type: Number
  },
  rentAmountPaid: {
    type: Number
  },
  rentPaymentSuccess: {
    type: Boolean
  },
  dateRentalPaymentSuccess: {
    type: Date
  },
  dateRentalOrderPlaced: {
    type: Date,
    default: new Date()
  },
  startRentalDate: {
    type: Date
  },
  endRentalDate: {
    type: Date
  },
  cancelRentalOrder: {
    type: Boolean,
    default: false
  },
  cancelledBy: {
    type: String
  },
  cancellationRemarks: {
    type: String
  },
  dateRentalOrderCancelled: {
    type: Date
  },
  orderDetails: [OrderDetailsSchema],
  rentalReturned: {
    type: Boolean,
    default: false
  },
  dateReturned: {
    type: Date
  }
},{
  timestamps: true
});


module.exports = mongoose.model('Rental', RentalSchema);
