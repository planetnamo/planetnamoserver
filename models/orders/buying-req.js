var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/**
 *    Buying-Request Schema
 */

var OrderDetailsSchema = new Schema({
  pId: {
    type: Schema.ObjectId,
    ref: 'Product'
  },
  quantity: {
    type: Number
  }
});


var BuyingSchema = new Schema({
  lotId: {
    type: Schema.ObjectId,
    ref: 'Lot'
  },
  isAggregate: {
    type: Boolean,
    default: true
  },
  userId: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  transactionId: {
    type: Schema.ObjectId,
    ref: 'Transaction'
  },
  walletTransactionId: {
    type: Schema.ObjectId,
    ref: 'WalletTransaction'
  },
  deliveryId:{
    type: Schema.ObjectId,
    ref: 'Delivery'
  },
  totalAmount: {
    type: Number
  },
  emd: {
    type: Number
  },
  emdPaid: {
    type: Boolean,
    default: false
  },
  totalAmountPaid:{
    type: Boolean,
    default: false
  },
  dateTotalAmountPaid: {
    type: Date
  },
  dateTotalEmdPaid: {
    type: Date
  },
  dateBuyingOrderCancelled: {
    type: Date
  },
  dateBuyingOrderFulfilled: {
    type: Date
  },
  cancelBuyingOrder: {
    type: Boolean,
    default: false
  },
  cancelledBy: {
    type: String
  },
  cancellationRemarks: {
    type: String
  },
  buyingOrderFulfilled: {
    type: Boolean,
    default: false
  },
  transactionSuccess: {
    type: Boolean,
    default: false
  },
  orderDetails: [OrderDetailsSchema],
  deliveryAddress: {
    line1: {
      type: String,
      trim: true,
    },
    line2: {
      type: String,
      trim: true
    },
    locality: {
      type: String,
      trim: true
    },
    city: {
      type: String,
      trim: true,
    },
    state: {
      type: String,
      trim: true,
    },
    country: {
      type: String,
      trim: true,
    },
    zipCode: {
      type: String,
      trim: true,
    },
    locationLat: {
      type: Number
    },
    locationLng: {
      type: Number
    },
    _id: false
  }
},{
  timestamps: true
});



module.exports = mongoose.model('Buying', BuyingSchema);
