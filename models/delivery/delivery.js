var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var moment = require('moment');
/**
 *                Delivery Schema
 */


var DeliverySchema = new Schema({
  orderType: {
    type: String,
    enum: ['rental', 'buy', 'auction', 'sell'],
    required: true
  },
  rentalId: {
    type: Schema.ObjectId,
    ref: 'Rental'
  },
  auctionId: {
    type: Schema.ObjectId,
    ref: 'Auction'
  },
  sellingId: {
    type: Schema.ObjectId,
    ref: 'Selling'
  },
  buyingId: {
    type: Schema.ObjectId,
    ref: 'Buying'
  },
  deliveryType: {
    type: String,
    enum: ['delivery', 'pickup'],
    required: true
  },
  deliveryStage: {
    type: Number,
    default: 1,
    enum: [1, 2, 3]
  },
  pickupStage: {
    type: Number,
    default: 1,
    enum: [1, 2, 3] // 1-Schedule Pickup, 2-Physical Check, 3-Sold
  },
  remarks: [{
    _id: false,
    remarkComment: {
      type: String
    },
    remarkDate: {
      type: Date
    }
  }],
  schedulePickUpDate: {
    type: Date
  },
  requestedPickUpDate: {
    type: Date
  },
  deliveryAddress: {
    type: Schema.ObjectId,
    ref: 'Address'
  }

}, {
  timestamps: true
});

DeliverySchema.pre('save', function(callback) {
  var delivery = this;
  if(delivery.isModified('requestedPickUpDate')){
    delivery.schedulePickUpDate = delivery.requestedPickUpDate;
    callback();
  }
});

module.exports = mongoose.model('Delivery', DeliverySchema);