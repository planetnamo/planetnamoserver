var mongoose 	= require('mongoose');
var Schema 		= mongoose.Schema;
var FileHandler = require(__utils + '/FileHandler.js');

/**
 * 				LOT SCHEMA -- One that actually engages in transactions/orders
 *				
 */

//SUBDOCUMENTS FOR LOT

var LotContentsSchema = new Schema({
	_id: false,
	pId: {
		type: Schema.ObjectId,
		ref: 'Product'
	},
	approvedLotId: {
		type: Schema.ObjectId,
		ref: 'ApprovedLot'
	},
	quantity: {
		type: Number
	},
	price: {
		type: Number
	},
	emdDeposit: {
		type: Number
	},
	rentalPricePerMonth: {
		type: Number
	},
	rentalSecurityDeposit: {
		type: Number
	},
	rentalPriceByNumberOfMonths: [{
		_id: false,
		numberOfMonths: {type: Number},
		price: {type: Number}
	}],
	auctionMinDeposit: {
		type: Number
	},
	bidIncreaseDetails: {
		type: {type: String},
		value: {type: Number}
	},
	registeredByUser: {
		type: Schema.ObjectId,
		ref: 'User'
	},
	specifications: [{
		_id: false,
		displayName: {type: String},
		specificationDescription: {type: String},
		specificationValue: {type: String} 
	}]
});

var OrderDetailsSchema = new Schema({
  pId: {
    type: Schema.ObjectId,
    ref: 'Product'
  },
  quantity: {
    type: Number
  }
});

var RentalLotSchema = new Schema({
	startDate: {
		type: Date
	},
	endDate: {
		type: Date
	},
	rentalId: {
		type: Schema.ObjectId,
		ref: 'Rental'
	},
	orderDetails: [OrderDetailsSchema]
});


var LotsSchema = new Schema({
	createdByUser: {
		type: Schema.ObjectId,
		ref: 'User'
	},
	lotContents: [LotContentsSchema],
	originalLotContents: [LotContentsSchema],
	lotType: {
		type: String,
		enum: ['buy','auction','rental','clearance']
	},
	isLinkedToAuction: {
		type: Boolean
	},
	lotSoldOut: {
		type: Boolean,
		default: false
	},
	datelotSoldOut: {
		type: Date
	},
	rentalLotDetails: [RentalLotSchema],
	startDate: {
		type: Date
	},
	endDate: {
		type: Date
	},
	isDirectBuyLot: {
		type: false,
		default: false
	},
	isChildAuctionLot: {
		type: Boolean,
		default: false
	},
	parentLotId: {
		type: Schema.ObjectId,
		ref: 'Lot'
	},
	isAuctionOver: {
		type: Boolean
	},
	currentHighestBid: {
		type: Schema.ObjectId,
		ref: 'Bid'
	},
	emdToBePaidForChildLot: {
		type: Number
	},
	isClearanceStock: {
		type: Boolean,		//only applicable for buy type
		default: false
	},
	clearanceDiscountPercent: {
		type: Number
	},
	brands: [{
		type: String										//FILTERING FIELDS
	}],
	productCategories: [{
		type: Schema.ObjectId ,								//FILTERING FIELDS
		ref: 'ProductCategory'
	}],
	screen_size: [{
		type: String
	}],
	screen_resolution: [{
		type: String
	}],
	processor: [{
		type: String
	}],
	display_technology: [{
		type: String
	}],
	ram: [{
		type: String
	}],
	hard_drive: [{
		type: String
	}],
	model_number: [{
		type: String
	}],
	operating_system: [{
		type: String
	}],	
	color: [{
		type: String
	}],
	processor_count: [{
		type: String
	}],
	processor_brand: [{
		type: String
	}],
	releaseDate: [{
		type: Date						//
	}],
	ratings: [{
		type: Number				//
	}],
	workingcondition: [{
		type: Boolean				//
	}],
	originalbox: [{
		type: Boolean							//
	}],
	validbill: [{
		type: Boolean				//
	}],
	condition: [{
		type: Number				//
	}],
	lotImage: {
		fieldname: { type: String },
		originalname: {type: String},
		encoding: {type: String},
		mimetype: {type: String},
		size: {type: Number},
		destination: {type: String},
		filename: {type: String},
		path: {type: String}
	},
	lotImageURL: {
		type: String
	},
	lotName: {
		type: String,
		default: "untitled lot"
	},
	lotTermsAndConditions: {
		type: String
	},
	backedOutUserInfo: [{
		_id: false,
		userId: {type: Schema.ObjectId, ref: 'User'},
		buyingOrderId: {type: Schema.ObjectId, ref: 'Buying'},
		auctionId: {type: Schema.ObjectId, ref: 'Auction'},
		childLotId: {type: Schema.ObjectId, ref: 'Lot'},
		backedOutUsersAmount: {type: Number}
	}],
	overallBidIncreaseDetails: {
		type: {type: String},
		value: {type: Number}
	},
	totalPriceToPayConsideringQuantity: {
		type: Number
	},
	bidWinnerDetails: [{
		userId: {type: Schema.ObjectId, ref: 'User'},
		bidId: {type: Schema.ObjectId, ref: 'Bid'},
		childLotId: {type: Schema.ObjectId, ref: 'Lot'},
	    orderDetails: [{
	      productName: {type: String},
	      productId: {type: Schema.ObjectId, ref: 'Product'},
	      productStartingBidPrice: {type: Number},
	      quantity: {type: Number},
	      emdDeposit: {type: Number},
	      productPerItemPrice: {type: Number},
	      _id: false
	    }],
	    auctionRequestId: {type: Schema.ObjectId, ref: 'AuctionRequest'},
	    auctionId: {type: Schema.ObjectId, ref: 'Auction'}
	}]

},
{
	timestamps: true
});




module.exports = mongoose.model('Lot', LotsSchema);