var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/**
 * 				APPROVED-LOT
 *				Lots approved by the ADMIN 
 */


var ApprovedLotSchema = new Schema({
	pId: {
		type: Schema.ObjectId,
		ref: 'Product',
		required: true,
		unique: true
	},
	quantity: {
		type: Number,
		required: true
	},
	dateRegisteredByUser: {
		type: Date
	},
	registeredByUser: {
		type: Schema.ObjectId,
		ref: 'User',
		required: true
	},
	approvedByUser: {
		type: Schema.ObjectId,
		ref: 'User',
		required: true
	},
	participatingInFinalLot: {
		type: Boolean,
		default: false
	},
	linkedFinalLots: [{
		type: Schema.ObjectId,
		ref: 'Lot'
	}]
},
{
	timestamps: true
});

module.exports = mongoose.model('ApprovedLot', ApprovedLotSchema);