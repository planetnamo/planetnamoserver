var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/**
 *			REGISTERED-LOT 
 *			Contains products having same :pId 
 */


var RegisteredLotSchema = new Schema({
	pId: {                 
		type: Schema.ObjectId,
		ref: 'Product',
		unique: true
	},
	quantity: {
		type: Number
	},
	dateRegisteredByUser: {
		type: Date,
		default: Date.now
	},
	registeredByUser: {
		type: Schema.ObjectId,
		ref: 'User'
	},
	approved: {
		type: Boolean,
		default: false
	}

},{
	timestamps: true
});

module.exports = mongoose.model('RegisteredLot', RegisteredLotSchema);