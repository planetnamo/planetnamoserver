var mongoose 		= require('mongoose');
var FileHandler  	= require(__utils + '/FileHandler.js');

var Schema = mongoose.Schema;

/**
 * 		METADETAIL ABOUT A PRODUCT ---- ProductCategorySchema
 *		Category (mobile, pc, laptop etc.) to which a product belongs to
 */


var ProductCategorySchema = new Schema({
	categoryName: {
		type: String,
		trim: true,
		required: 'Category name cannot be blank',
		unique: true
	},
	accessories:[{
	  _id: false,
	  accessoryname: {
	    type: String,
	    required: true,
	    trim: true
	  },
	  score: {
	    type: Number,
	    required: true,
	  }
	}],
	problems: [{
	  _id: false,
	  problemname: {
	    type: String,
	    required: true,
	    trim: true
	  },
	  score: {
	    type: Number,
	    required: true
	  },
	  remarks: {
	    type: String,
	    trim: true
	  }
	}],
	specifications:[{
	    _id: false,
		displayName: {
	      type: String
	    },
	    specificationDescription: {
	      type: String
	    },
	    specificationValue: {
	      type: String
	    }
	}]
},
{
	timestamps: true
});



module.exports = mongoose.model('ProductCategory', ProductCategorySchema);