var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/**
 *        PRODUCT (:pId) This will referenced throughout now
 *        Contains all information needed for an induvidual product
 */


var ProductSchema = new Schema({
  baseProductId: {
    type: Schema.ObjectId,
    ref: 'BaseProduct',
    required: true
  },
  productImage: {
    fieldname: { type: String },
    originalname: {type: String},
    encoding: {type: String},
    mimetype: {type: String},
    size: {type: Number},
    destination: {type: String},
    filename: {type: String},
    path: {type: String}
  },
  planetNamoCost: {
    type: Number,
    default: 0
  },
  ageinmonths: {
    type: Number,
    required: true
  },
  condition: {
    type: Number,
    required: true,
    enum: [1,2,3]
  },
  validbill: {
    type: Boolean,
    required: true
  },
  originalbox: {
    type: Boolean,
    required: true
  },
  workingcondition: {
    type: Boolean,
    required: true
  },
  accessorieslistavail: [{  //Used for calculating planetNamoCost 
    type: Boolean,
  }],
  problemslistavail: [{ // Used for calculating planetNamoCost
    type: Boolean
  }],
  productRegisteredByUser: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  itemsSold: {
    type: Number
  },
  rating: {
    type: Number
  },
  finalLots: [{
    type: Schema.ObjectId,
    ref: 'Lot'
  }],
  buyLots: [{
    type: Schema.ObjectId,
    ref: 'Lot'
  }],
  clearanceLot: [{
    type: Schema.ObjectId,
    ref: 'Lot'
  }],
  rentalLot: [{
    type: Schema.ObjectId,
    ref: 'Lot'
  }],
  auctionLot: [{
    type: Schema.ObjectId,
    ref: 'Lot'
  }],
  specifications: [{
    _id: false,
    displayName: {
      type: String
    },
    specificationDescription: {
      type: String
    },
    specificationValue: {
      type: String
    }
  }]

},{
  timestamps: true
});


module.exports = mongoose.model('Product', ProductSchema);
