var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var FileHandler = require(__utils + '/FileHandler.js');
var Lot = require(__base + '/models/lots/lot');
var Product = require(__base + '/models/products/product');


var AccessoriesSchema = new Schema({
  accessoryname: {
    type: String,
    required: true,
    trim: true
  },
  score: {
    type: Number,
    required: true,
  }
});
var ProblemsSchema = new Schema({
  problemname: {
    type: String,
    required: true,
    trim: true
  },
  score: {
    type: Number,
    required: true
  },
  remarks: {
    type: String,
    trim: true
  }
});


var BaseProductSchema = new Schema({
  productName: {
    type: String,
    trim: true,
    required: 'Product name cannot be blank',
    unique: true
  },
  productCategoryId: {
    type: Schema.ObjectId,
    ref: 'ProductCategory',
    required: true
  },
  productImage: {
    fieldname: {
      type: String
    },
    originalname: {
      type: String
    },
    encoding: {
      type: String
    },
    mimetype: {
      type: String
    },
    size: {
      type: Number
    },
    destination: {
      type: String
    },
    filename: {
      type: String
    },
    path: {
      type: String
    }
  },
  screen_size: {
    type: String,
    initial: true
  },
  screen_resolution: {
    type: String,
    initial: true
  },
  processor: {
    type: String,
    initial: true
  },
  display_technology: {
    type: String,
    initial: true
  },
  ram: {
    type: String,
    initial: true
  },
  hard_drive: {
    type: String,
    initial: true
  },
  brand_name: {
    type: String,
    initial: true
  },
  series: {
    type: String,
    initial: true
  },
  model_number: {
    type: String,
    initial: true
  },
  hardware_platform: {
    type: String,
    initial: true
  },
  operating_system: {
    type: String,
    initial: true
  },
  item_weight: {
    type: String,
    initial: true
  },
  product_dimensions: {
    type: String,
    initial: true
  },
  item_dimensions: {
    type: String,
    initial: true
  },
  color: {
    type: String,
    initial: true
  },
  processor_count: {
    type: String,
    initial: true
  },
  processor_brand: {
    type: String,
    initial: true
  },
  processor_speed: {
    type: String,
    initial: true
  },
  computer_memory_type: {
    type: String,
    initial: true
  },
  hard_drive_interface: {
    type: String,
    initial: true
  },
  hard_drive_rotational_speed: {
    type: String,
    initial: true
  },
  graphics_card: {
    type: String,
    initial: true
  },
  graphics_card_description: {
    type: String,
    initial: true
  },
  wireless_type: {
    type: String,
    initial: true
  },
  speaker_description: {
    type: String,
    initial: true
  },
  no_of_usb_ports_2_0: {
    type: Number,
    initial: true
  },
  no_of_usb_ports_3_0: {
    type: Number,
    initial: true
  },
  optical_drive_type: {
    type: String,
    initial: true
  },
  batteries: {
    type: String,
    initial: true
  },
  warranty_n_support: {
    type: String,
    initial: true
  },
  miscellaneous: {
    type: String,
    initial: true
  },
  chipset_brand: {
    type: String,
    initial: true
  },
  marketCost: {
    type: Number,
    default: null
  },
  releaseDate: {
    type: Date,
  },
  accessoriesList: [AccessoriesSchema],
  problemsList: [ProblemsSchema],
  goodRemark: {
    type: String,
    trim: true
  },
  averageRemark: {
    type: String,
    trim: true
  },
  belowAverageRemark: {
    type: String,
    trim: true
  },
  finalLots: [{
    type: Schema.ObjectId,
    ref: 'Lot'
  }],
  buyLots: [{
    type: Schema.ObjectId,
    ref: 'Lot'
  }],
  clearanceLot: [{
    type: Schema.ObjectId,
    ref: 'Lot'
  }],
  rentalLot: [{
    type: Schema.ObjectId,
    ref: 'Lot'
  }],
  auctionLot: [{
    type: Schema.ObjectId,
    ref: 'Lot'
  }],
  rating: {
    type: Number
  },
  specifications: [{
    _id: false,
    displayName: {
      type: String
    },
    specificationDescription: {
      type: String
    },
    specificationValue: {
      type: String
    }
  }]
},{
  timestamps: true
});



/*BaseProductSchema.pre('save', function(callback) {
  var baseProduct = this;

  var errorResponse = new Error('something went wrong');

  // Break out if the specifications hasn't changed
  if (!baseProduct.isModified('specifications')) return callback();
  else {
    var lots = baseProduct.finalLots;
    console.log(lots);
    var i=0;
    function asyncFor(i){
      //console.log("inside outer loop");
      if(i<lots.length){
        console.log(i, lots.length, "outer loop");
        var lotId = baseProduct.finalLots[i];
        //console.log("about to look for this lot: ",lotId);
        Lot.findById(lotId)
           .populate({
              path: 'lotContents.pId',
              model: Product
           })
           .exec(function(err, lot){
              //console.log("looking for said lot");
              if(err){
                //console.log("could not find lot with id", lotId);
                callback(errorResponse)
              }else{
                //console.log("found lot!");
                //console.log(lot);
                //console.log("iterating the lotContents of this lot");
                var j=0;
                console.log("looking for lotContents in lotId:",lotId);
                function asyncFor1(j){
                  //console.log("inside inner loop");
                  if(j<lot.lotContents.length){
                    console.log("BaseProduct In lot: ",lot.lotContents[j].pId.baseProductId);
                    console.log("BaseProduct we are looking for",baseProduct._id);
                    console.log("are we going to change specifications of this baseproduct?");
                    if(lot.lotContents[j].pId.baseProductId.equals(baseProduct._id)){
                      console.log("YES");
                      lot.lotContents[j].specifications = baseProduct.specifications;
                      lot.save(function(err){
                        if(err){
                          callback(errorResponse);
                        }else{
                          console.log("changed!");
                          console.log("modified specification in lot: ",lot._id);
                          //console.log(lot.lotContents[j]);
                          //console.log('----------------');
                          j+=1;
                          asyncFor1(j);
                        }
                      });
                    }else{
                      console.log("NO!!");
                      //console.log(lot.lotContents[j]);
                      //console.log('----------------');
                      j+=1;
                      asyncFor1(j);
                      }
                  }else{
                    //console.log('********',i);
                    //console.log(lot.lotContents);
                    i+=1;
                    asyncFor(i);
                  }
                }
                asyncFor1(j);
              }
           });
      }else{
        console.log("done iterating");
        callback();
      }
    }
    asyncFor(i);
  }
});*/

module.exports = mongoose.model('BaseProduct', BaseProductSchema);