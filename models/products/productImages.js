var mongoose 		= require('mongoose');
var Schema 			= mongoose.Schema;
var FileHandler  	= require(__utils + '/FileHandler.js');


var ProductImagesSchema = new Schema({
	image:{
		fieldname: { type: String },
		originalname: {type: String},
		encoding: {type: String},
		mimetype: {type: String},
		size: {type: Number},
		destination: {type: String},
		filename: {type: String},
		path: {type: String}
	},
	pId: {
		type: Schema.ObjectId,
		ref: 'Product',
		required: true
	},
	imageURL: {
		type: String
	}
},{
	timestamps: true
});


ProductImagesSchema.pre('save', function(callback) {
	var productImages = this;

	// Break out if the image hasn't changed
	if (!productImages.isModified('image')) return callback();
	else{
  		productImages.imageURL = FileHandler.getProductPicture(productImages._id);
  		callback();
  	}
});

module.exports = mongoose.model('ProductImages', ProductImagesSchema);