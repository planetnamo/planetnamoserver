var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/**
 *                              Transaction Schema
 */


var TransactionSchema = new Schema({
  dateCreated: {
    type: Date,
    default: Date.now
  },
  userId: {
    type: Schema.ObjectId,
    ref: 'User',
    required: true
  },
  lotId: {
    type: Schema.ObjectId,
    ref: 'Lot'
  },
  mode: {
    type: String,
    required: true
  },
  status: {
    type: String,
    required: true
  },
  txnid: {
    type: String,
    unique: true,
    required: true
  },
  mihPayId: {
    type: String
  },
  amount: {
    type: String,
    required: true
  },
  completeData: {
    type: Schema.Types.Mixed,
    required: true
  },
  transactionCancelled: {
    type: Boolean,
    default: false
  },
  date_cancelled: {
    type: Date
  },
  walletTransanction: {
    type: Schema.ObjectId,
    ref: 'WalletTransaction'
  }

},{
  timestamps: true
});

module.exports = mongoose.model('Transaction', TransactionSchema);