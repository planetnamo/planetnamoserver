var mongoose        = require('mongoose');
var Schema          = mongoose.Schema;

//Details about an auction being held

var AuctionSchema = new Schema({
  auctionOwnerId: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  parentLotId: {
    type: Schema.ObjectId,
    ref: 'Lot',
    unique: true
  },
  childLotIds: [{
    type: Schema.ObjectId,
    ref: 'Lot'
  }],
  auctionStartDate: {
    type: Date,
    default: new Date()
  },
  auctionEndDate: {
    type: Date,
    default: new Date + 2*24*60*60*1000
  },
  auctionCompleted: {
    type: Boolean,
    default: false
  },
  isLive: {
    type: Boolean,
    default: true
  },
  auctionCancelled: {
    type: Boolean,
    default: false
  },
  auctionCancelledOnDate: {
    type: Date
  },
  overallBidIncreaseDetails: {
    type: {type: String},
    value: {type: Number}
  }


},{
  timestamps: true
});


module.exports = mongoose.model('Auction', AuctionSchema);