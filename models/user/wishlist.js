var mongoose        = require('mongoose');
var Schema          = mongoose.Schema;


var WishlistSchema = new Schema({
    userId: {
      type: Schema.ObjectId,
      ref: 'User',
      unique: true,
      required: true
    },
    lots: [{
      type: Schema.ObjectId,
      ref: 'lot'
    }]

},{
  timestamps: true
});


module.exports = mongoose.model('Wishlist', WishlistSchema);