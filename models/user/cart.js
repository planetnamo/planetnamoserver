var mongoose        = require('mongoose');
var Schema          = mongoose.Schema;


var CartSchema = new Schema({
    userId: {
      type: Schema.ObjectId,
      ref: 'User',
      unique: true,
      required: true
    },
    cartItems: [{
      type: Schema.ObjectId,
      ref: 'cartItem'
    }]

},
{
  timestamps: true
});


module.exports = mongoose.model('Cart', CartSchema);