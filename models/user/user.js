var mongoose        = require('mongoose');
var Schema          = mongoose.Schema;
var bcrypt          = require('bcrypt-nodejs');
var FileHandler     = require(__utils + '/FileHandler.js');

var UserSchema = new Schema({
    username: {
        type: String,
    },
    password: {
        type: String,
        required: true
    },
    name: {
      type: String,
      required: true,
      trim: true
    },
    email: {
      type: String,
      required: true,
      trim: true,
      unique: true
    },
    panCard: {
      type: String,
      trim: true
    },
    vatNo: {
      type: String,
      trim: true
    },
    serviceTaxNo: {
      type: String,
      trim: true
    },
    registerationDoc: {
      fieldname: { type: String },
      originalname: {type: String},
      encoding: {type: String},
      mimetype: {type: String},
      size: {type: Number},
      destination: {type: String},
      filename: {type: String},
      path: {type: String}
    },
    profilePicture: {
      fieldname: { type: String },
      originalname: {type: String},
      encoding: {type: String},
      mimetype: {type: String},
      size: {type: Number},
      destination: {type: String},
      filename: {type: String},
      path: {type: String}
    },
    mobileNumber: {
      type: String,
      trim: true
    },
    mobileVerified: {
      type: Boolean,
      default: false
    },
    emailVerified: {
      type: Boolean,
      default: false
    },
    emailVerificationToken: {
      type: String
    },
    mobileVerificationOtp: {
      type: String
    },
    facebookUserId: {
      type: String
    },
    googleUserId: {
      type: String
    },
    passwordUpdateToken: {
      type: String
    },
    role: {
      type: Number,
      enum: [1,2,3], // 1 - default_user 2 - admin 3 - banned_user
      default: 1
    },
    buySellPermission: {
      type: Number,
      enum: [1,2,3],  // 1 - buy only 2 - sell only 3 - both buy and sell
      default: 3
    },
    accountType: {
      type: Number,
      enum: [1,2], // 1 - induvidual 2 - bussiness
      default: 1
    },
    greenPoints: {
      type: Number,
      default: 0
    },
    treesGrown: {
      type: Number,
      default: 0
    },
    address: [{
      type: Schema.ObjectId,
      ref: 'Address'
    }],
    primaryAddress: {
      type: Schema.ObjectId,
      ref: 'Address'
    },
    lastLoginTime: {
      type: Date
    },
    bankAccountInfo: {
      id: false,
      beneficiaryName: String,
      bankName: String,
      IFSCCode: String,
      beneficiaryAccountNumber: String,
      branchName: String,
      city: String
    }

},{
  timestamps: true
});

UserSchema.pre('validate', function(callback){
  var user = this;
  console.log("inside validate");
  return callback();
});


UserSchema.pre('save', function(callback) {
  var user = this;
  console.log("inside save");
  // Break out if the password hasn't changed
  if (!user.isModified('password')) return callback();

  // Password changed so we need to hash it
  bcrypt.genSalt(5, function(err, salt) {
    if (err) return callback(err);

    bcrypt.hash(user.password, salt, null, function(err, hash) {
      if (err) return callback(err);
      user.password = hash;
      callback();
    });
  });
});


UserSchema.methods.comparePassword = function(password) {
    var user = this;
    return bcrypt.compareSync(password, user.password);
};

module.exports = mongoose.model('User', UserSchema);