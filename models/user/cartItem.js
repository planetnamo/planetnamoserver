var mongoose        = require('mongoose');
var Schema          = mongoose.Schema;

var OrderDetailsSchema = new Schema({
  pId: {
    type: Schema.ObjectId,
    ref: 'Product'
  },
  quantity: {
    type: Number
  },
  priceOfProductWhileAdding: {
    type: Number
  }
});

var CartItemSchema = new Schema({
    userId: {
      type: Schema.ObjectId,
      ref: 'User'
    },
    cartId: {
      type: Schema.ObjectId,
      ref: 'Cart'
    },
    lotId: {
      type: Schema.ObjectId,
      ref: 'Lot'
    },
    orderDetails: [OrderDetailsSchema]
},
{
  timestamps: true
});


module.exports = mongoose.model('CartItem', CartItemSchema);