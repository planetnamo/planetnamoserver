var mongoose        = require('mongoose');
var Schema          = mongoose.Schema;



var WalletSchema = new Schema({
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  amount: {
    type: Number,
    default: 0
  },
  freezedAmount: {
    type: Number,
    default: 0
  },
  freezedForAuctions: [{
    type: Schema.ObjectId,
    ref: 'Lot'
  }],
  freezedForAggregate: [{
    type: Schema.ObjectId,
    ref: 'Buying'
  }]

},{
  timestamps: true
});

module.exports = mongoose.model('Wallet', WalletSchema);