var mongoose        = require('mongoose');
var Schema          = mongoose.Schema;


var TransactionInfoSchema = new Schema({
  remarks: {
    type: String
  },
  otherInfo: {
    type: Schema.Types.Mixed
  }
});

var WalletTransactionSchema = new Schema({
  walletId: {
    type: Schema.ObjectId,
    ref: 'Wallet'
  },
  transactionId: {
    type: String
  },
  amountInvloved: {
    type: Number,
    default: 0
  },
  paymentType: {
    type: String
  },
  transactionInfo: {TransactionInfoSchema}
},{
  timestamps: true
});

module.exports = mongoose.model('WalletTransaction', WalletTransactionSchema);