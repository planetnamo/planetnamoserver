var mongoose        = require('mongoose');
var Schema          = mongoose.Schema;



var WalletWithDrawlRequestsSchema = new Schema({
  createdByUser: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  withdrawlInitiated: {
    type: Boolean,
    default: false
  },
  amount: {
    type: Number,
    default: 0
  },
  withdrawlSuccess: {
    type: Boolean,
    default: false
  }
},{
  timestamps: true
});

module.exports = mongoose.model('WalletWithDrawl', WalletWithDrawlRequestsSchema);