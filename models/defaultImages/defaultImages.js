var mongoose        = require('mongoose');
var Schema          = mongoose.Schema;


var DefaultImagesSchema = new Schema({
  defaultImageName: {
    type: String,
    required: true
  },
  defaultImage: {
    fieldname: { type: String },
    originalname: {type: String},
    encoding: {type: String},
    mimetype: {type: String},
    size: {type: Number},
    destination: {type: String},
    filename: {type: String},
    path: {type: String}
  }

},{
  timestamps: true
});


module.exports = mongoose.model('DefaultImages', DefaultImagesSchema);