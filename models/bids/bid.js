var mongoose        = require('mongoose');
var Schema          = mongoose.Schema;


var BidSchema = new Schema({
    userId: {
      type: Schema.ObjectId,
      ref: 'User',
      required: true
    },
    auctionId: {
      type: Schema.ObjectId,
      ref: 'Auction',
      required: true
    },
    childLotId: {   //child lot id
      type: Schema.ObjectId,
      ref: 'Lot',
      required: true
    },
    parentLotId: {
      type: Schema.ObjectId,
      ref: 'Lot',
      required: true
    },
    bidAmount: {
      type: Number
    },
    emdFrozen: {    //emd for this bid, each bid has an emd
      type: Number
    },
    emdPaid: {        
      type: Boolean,
      default: false
    },
    dateEMDPaid: {
      type: Date
    },
    bidCancelled: {
      type: Boolean,
      default: false
    },
    dateBidPlaced: {
      type: Date
    },
    dateBidCancelled: {
      type: Date
    },
    bidWon: {
      type: Boolean,
      default: 'false'
    },
    dateBidWon: {
      type: Date
    },
    orderDetails: [{
      pId: {type: Schema.ObjectId, ref: 'Product'},
      quantity: {type: Number},
      _id: false
    }],
    totalAmountToPay: {type: Number},
    detailedOrderDetails: [{
      productName: {type: String},
      productId: {type: Schema.ObjectId, ref: 'Product'},
      productStartingBidPrice: {type: Number},
      quantity: {type: Number},
      emdDeposit: {type: Number},
      productPerItemPrice: {type: Number},
      _id: false
    }],
    biddingProcessStarted: {
      type: Boolean,
      default: false
    },
    bidSuccessTransactionId: {
      type: Schema.ObjectId,
      ref: 'WalletTransaction'
    }


},{
  timestamps: true
});


module.exports = mongoose.model('Bid', BidSchema);