var mongoose = require('mongoose');
var Schema = mongoose.Schema;



var UserRatingSchema = new Schema({
  lotId: {
    type: Schema.ObjectId,
    ref: 'Lot'
  },
  rating: {
    type: Number,
    enum: [1,2,3,4,5]
  },
  userId: {
    type: Schema.ObjectId,
    ref: 'User'
  }

},{
  timestamps: true
});


module.exports = mongoose.model('UserRating', UserRatingSchema);
