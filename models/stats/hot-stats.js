var mongoose = require('mongoose');
var Schema = mongoose.Schema;



var HotStatsSchema = new Schema({
  BaseProductId: {
    type: Schema.ObjectId,
    ref: 'BaseProduct'
  },
  ProductId: {
    type: Schema.ObjectId,
    ref: 'Product'
  },
  Lot: {
    type: Schema.ObjectId,
    ref: 'Lot'
  },
  NumberOfOrders: {
    type: Number,
    default: 0
  }

},{
  timestamps: true
});


module.exports = mongoose.model('HotStats', HotStatsSchema);
