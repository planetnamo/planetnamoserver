var User     = require('../modules/user/model/user-model');
var passportJWT = require("passport-jwt");
var ExtractJwt = passportJWT.ExtractJwt;
var JwtStrategy = passportJWT.Strategy;

var jwtOptions = {};
jwtOptions.secretOrKey = 'tasmanianDevil';
jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeader();



exports.strategy = new JwtStrategy(jwtOptions, function(jwt_payload, next) {
  console.log('payload received', jwt_payload);
  
  
User.findById(jwt_payload.id).exec(function(err, user){
	if (user) {
	  next(null, user);
	} else {
	  next(null, false);
	}
  })
});

