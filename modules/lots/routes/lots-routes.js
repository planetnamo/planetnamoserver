var passport   = require('passport');
var Middleware = require(__base + '/middlewares/middleware.js');

var lotCtrl 					= require(__modules + '/lots/controllers/lots-controller.js');
var RegistrationLotCtrl 		= require(__modules + '/lots/controllers/register-lots-controller.js');
var ApprovedLotCtrl 			= require(__modules + '/lots/controllers/approved-lots-controller.js');
var FinalLotCrudCtrl			= require(__modules + '/lots/controllers/final-lot-crud-controller.js');
var LotRatingCtrl				= require(__modules + '/lots/controllers/lot-rating-controller.js');
var LotFiltersCtrl				= require(__modules + '/lots/controllers/lot-filters-controller.js');
var ClearanceLotCtrl			= require(__modules + '/lots/controllers/clearance-lot-controller.js');


module.exports = function(app, express){

	var apiRouter = express.Router();



	//LOT ROUTES



	apiRouter.route('/RegisterLot').post(passport.authenticate('jwt', { session: false }), //done
										 RegistrationLotCtrl.RegisterLot);

	apiRouter.route('/DeleteRegisteredLot/:rLotId').delete(passport.authenticate('jwt', { session: false }), //done
											  RegistrationLotCtrl.DeleteRegisteredLot);





	apiRouter.route('/ApproveLot/:rLotId').post(passport.authenticate('jwt', { session: false }), //done
											  Middleware.CheckIfAdmin,
											  ApprovedLotCtrl.ApproveLot);

	apiRouter.route('/DeleteApprovedLot/:aLotId').delete(passport.authenticate('jwt', { session: false }), //done
											  Middleware.CheckIfAdmin,
											  ApprovedLotCtrl.DeleteApprovedLot);





	apiRouter.route('/CreateFinalLot').post(passport.authenticate('jwt', { session: false }), //done
											  Middleware.CheckIfAdmin,
											  FinalLotCrudCtrl.CreateFinalLot);

	apiRouter.route('/EditFinalLot').post(passport.authenticate('jwt', { session: false }), //done
											  Middleware.CheckIfAdmin,
											  FinalLotCrudCtrl.EditFinalLot);

	apiRouter.route('/DeleteFinalLot').post(passport.authenticate('jwt', { session: false }), //done
											  Middleware.CheckIfAdmin,
											  FinalLotCrudCtrl.DeleteFinalLot);

	apiRouter.route('/UploadLotImage/:lotId').post(passport.authenticate('jwt', { session: false }), //done
											  Middleware.CheckIfAdmin,
											  FinalLotCrudCtrl.UploadLotImage);

	apiRouter.route('/directFinalLotCreation').post(passport.authenticate('jwt', { session: false }), //done
											  Middleware.CheckIfAdmin,
											  FinalLotCrudCtrl.DirectFinalLotCreation);

	apiRouter.route('/createFinalBuyingLotFromAdminPanel').post(passport.authenticate('jwt', { session: false }), //done
											  Middleware.CheckIfAdmin,
											  FinalLotCrudCtrl.CreateFinalBuyingLotFromAdminPanel);	



	apiRouter.route('/getLots').post(LotFiltersCtrl.GetLots); //done







	apiRouter.route('/GetRatingOfLotByUser/:lotId').get(passport.authenticate('jwt', { session: false }), //done
											  LotRatingCtrl.GetRatingOfLotByUser);

	apiRouter.route('/RateLot').post(passport.authenticate('jwt', { session: false }), //done
											  LotRatingCtrl.RateLot);

	apiRouter.route('/GetOverallRatingOfLot/:lotId').get(LotRatingCtrl.GetOverallRatingOfLot); //done


	apiRouter.route('/isInWishlist/:lotId').get(passport.authenticate('jwt', { session: false }), //done
											  lotCtrl.IsInWishlist);


	apiRouter.route('/MakeClearanceLot').post(passport.authenticate('jwt', { session: false }), //done
											  Middleware.CheckIfAdmin,
											  ClearanceLotCtrl.MakeClearanceLot);

	apiRouter.route('/RemoveClearance').post(passport.authenticate('jwt', { session: false }), //done
											  Middleware.CheckIfAdmin,
											  ClearanceLotCtrl.RemoveClearance);





	apiRouter.route('/FindSimilarLots/:lotId').get(lotCtrl.FindSimilarLots); //done

	apiRouter.route('/getLotDetail/:lotId').get(lotCtrl.GetLotDetails);

	apiRouter.route('/CompareLots').post(lotCtrl.CompareLots);

	apiRouter.route('/GetHotSellingLots').get(lotCtrl.GetHotSellingLots); //done




	
	return apiRouter;

};

