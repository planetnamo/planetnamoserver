
var mongoose 	= require('mongoose');
var moment = require('moment');
var multer  = require('multer');
var fs = require('fs');
var mime = require('mime');
var crypto = require('crypto');


var aux 		= require(__utils + '/auxilaryFunc');
var ErrorParser = require(__utils + '/errorParse');
var UsersUtils  = require(__utils + '/users')
var LotsUtils	= require(__utils + '/lots');

var User 			= require(__base + '/models/user/user');
var RegisteredLot 	= require(__base + '/models/lots/registered-lot');
var ApprovedLot 	= require(__base + '/models/lots/approved-lot');
var Lot 	        = require(__base + '/models/lots/lot');
var SellingOrder 	= require(__base + '/models/orders/selling-req'); 
var Product 		= require(__base + '/models/products/product');
var BaseProduct 	= require(__base + '/models/products/base-product');
var BuyingOrder     = require(__base + '/models/orders/buying-req');
var UserRating      = require(__base + '/models/stats/user-rating');
var ProductCategory = require(__base + '/models/products/product-category.js');



var storage_lotImages = multer.diskStorage({
	destination: function (request, file, callback) {
		callback(null, __base + '/uploads/lotPictures');
	},
	filename: function (request, file, callback) {
		console.log(file);
		//callback(null, file.originalname);
		crypto.pseudoRandomBytes(16, function (err, raw) {
		  callback(null, raw.toString('hex') + Date.now() + '.' + mime.extension(file.mimetype));
		});
	}
});
var upload_lotImages = multer({storage: storage_lotImages}).single('lotImage');




//-----------------M A K E----L O T-----C L E A R A N C E------T Y P E---------------------------------------------------------


/**
@api {post} /lots/MakeClearanceLot Make Clearance Lot
@apiName MakeClearanceLot
@apiGroup Lot
@apiPermission admin
@apiDescription Convert a buying type final lot into a clearance lot. Clearance lot have associated with them a Discount %

@apiHeader {String="application/json"} Content-Type Content Type for the api 
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
    {
      "Content-Type": "application/json",
   	 "Authorization": "JWT eyJhbGHVbz5y8i-HRRY"
    }
 
@apiParam {String} lotId unique id identyfing final lot, only buy type lots will be considered
@apiParam {Number} clearanceDiscountPercent A discount percentage between 0 and 99
  @apiParamExample {json} Request-Example:
	{
		"lotId": "58ad715d79741f81ea88260f",
		"clearanceDiscountPercent": 50
	}

@apiSuccess {String} status Status of the API Call.
@apiSuccess {JSON} response Details (json) of the final lot which was just converted into a clearance lot
@apiSuccessExample Success-Response:
      HTTP/1.1 200 OK
		{
		  "status": "success",
		  "response": {
		    "clearanceDiscountPercent": 50,
		    "_id": "58ad715d79741f81ea88260f",
		    "endDate": "2017-11-11T18:30:00.000Z",
		    "startDate": "2015-01-11T18:30:00.000Z",
		    "lotType": "buy",
		    "createdByUser": "58a70a784ded7c86e5e829e6",
		    "__v": 0,
		    "isClearanceStock": true,
		    "isChildAuctionLot": false,
		    "rentalLotDetails": [],
		    "lotSoldOut": false,
		    "lotContents": [
		      {
		        "pId": "58a73bcd71cd35dab7a47adf",
		        "quantity": 1,
		        "price": 900,
		        "emdDeposit": 23,
		        "approvedLotId": "58a73d1565c0fadc289a5631",
		        "registeredByUser": "58a70a784ded7c86e5e829e6",
		        "_id": "58ad715d79741f81ea882610"
		      }
		    ],
		    "dateCreated": "2017-02-22T11:09:17.113Z"
		  }
		}

@apiError error Error message.
@apiErrorExample Error-Response:
HTTP/1.1 200 Error
	{	
		"error": "Invalid lot id"
	}
*/
exports.MakeClearanceLot = function (req, res) {
	//Only buy type lot can be made a clearance
	var lotId = req.body.lotId;
	var clearanceDiscountPercent = req.body.clearanceDiscountPercent;
	if(lotId && clearanceDiscountPercent){
		Lot.findOne({_id: lotId, isClearanceStock: false}, function(err, lot){
			if(err){
				return res.status(200).send({error: err});
			}else{
				if(lot && clearanceDiscountPercent<=100){
					if(lot.lotType === 'buy'){
						lot.isClearanceStock = true;
						lot.clearanceDiscountPercent = clearanceDiscountPercent;
						lot.save(function(err, lot){
							if(err){
								return res.status(200).send({error: err});
							}else{
								return res.status(200).send({status: 'success', response: lot});
							}
						});
					}else{
						return res.status(200).send({status: 'error', message: "Invalid lot type"});
					}
				}else{
					return res.status(200).send({status:'error', message: "Invalid lot id"});
				}
			}
		});
	}else{
		return res.status(200).send({status:'error', message: "Inadequate information"});
	}
};

//---------------------------------------------------------------------------------------------------------------------------
























//-----------------R E M O V E-----C L E A R A N C E------T Y P E---------------------------------------------------------


/**
@api {post} /lots/RemoveClearance Remove Clearance
@apiName RemoveClearance
@apiGroup Lot
@apiDescription Convert a buying type clearance lot back to normal.
@apiPermission admin

@apiHeader {String="application/json"} Content-Type Content Type for the api 
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization": "JWT eyJhbGHVbz5y8i-HRRY"
	}


@apiParam {String} lotId unique id identyfing final lot, only buy type lots will be considered
@apiParamExample {json} Request-Example:
	{
		"lotId": "58ad715d79741f81ea88260f"
	}

@apiSuccess {String} status Status of the API Call.
@apiSuccess {JSON} response Details (json) of the final lot which was just converted back to normal
@apiSuccessExample Success-Response:
	HTTP/1.1 200 OK
	{
	  "status": "success",
	  "response": {
	    "clearanceDiscountPercent": 0,
	    "_id": "58ad715d79741f81ea88260f",
	    "endDate": "2017-11-11T18:30:00.000Z",
	    "startDate": "2015-01-11T18:30:00.000Z",
	    "lotType": "buy",
	    "createdByUser": "58a70a784ded7c86e5e829e6",
	    "__v": 0,
	    "isClearanceStock": false,
	    "isChildAuctionLot": false,
	    "rentalLotDetails": [],
	    "lotSoldOut": false,
	    "lotContents": [
	      {
	        "pId": "58a73bcd71cd35dab7a47adf",
	        "quantity": 1,
	        "price": 900,
	        "emdDeposit": 23,
	        "approvedLotId": "58a73d1565c0fadc289a5631",
	        "registeredByUser": "58a70a784ded7c86e5e829e6",
	        "_id": "58ad715d79741f81ea882610"
	      }
	    ],
	    "dateCreated": "2017-02-22T11:09:17.113Z"
	  }
	}

@apiError error Error message.
@apiErrorExample Error-Response:
HTTP/1.1 200 Error
	{	
		"status": 'error',
		"message": "Invalid lot type"
	}
*/
exports.RemoveClearance = function (req, res) {
	//Only buy type lot can be made a clearance and changed back to normal
	var lotId = req.body.lotId;
	if(lotId){
		Lot.findOne({_id: lotId, isClearanceStock: true}, function(err, lot){
			if(err){
				return res.status(200).send({error: err});
			}else{
				if(lot){
					if(lot.lotType === 'buy'){
						lot.isClearanceStock = false;
						lot.clearanceDiscountPercent = 0;
						lot.save(function(err, lot){
							if(err){
								return res.status(200).send({error: err});
							}else{
								return res.status(200).send({status: 'success', response: lot});
							}
						});
					}else{
						return res.status(200).send({status: 'error', message: "Invalid lot type"});
					} 
				}else{
					return res.status(200).send({status: 'error', message: "Invalid lot id or lot not found"});
				}
			}
		});
	}else{
		return res.status(200).send({status: 'error', message: "Inadequate information"});
	}
};

//---------------------------------------------------------------------------------------------------------------------------






