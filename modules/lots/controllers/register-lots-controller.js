
var mongoose 	= require('mongoose');
var moment = require('moment');
var multer  = require('multer');
var fs = require('fs');
var mime = require('mime');
var crypto = require('crypto');


var aux 		= require(__utils + '/auxilaryFunc');
var ErrorParser = require(__utils + '/errorParse');
var UsersUtils  = require(__utils + '/users')
var LotsUtils	= require(__utils + '/lots');

var User 			= require(__base + '/models/user/user');
var RegisteredLot 	= require(__base + '/models/lots/registered-lot');
var ApprovedLot 	= require(__base + '/models/lots/approved-lot');
var Lot 	        = require(__base + '/models/lots/lot');
var SellingOrder 	= require(__base + '/models/orders/selling-req'); 
var Product 		= require(__base + '/models/products/product');
var BaseProduct 	= require(__base + '/models/products/base-product');
var BuyingOrder     = require(__base + '/models/orders/buying-req');
var UserRating      = require(__base + '/models/stats/user-rating');
var ProductCategory = require(__base + '/models/products/product-category.js');



var storage_lotImages = multer.diskStorage({
	destination: function (request, file, callback) {
		callback(null, __base + '/uploads/lotPictures');
	},
	filename: function (request, file, callback) {
		console.log(file);
		//callback(null, file.originalname);
		crypto.pseudoRandomBytes(16, function (err, raw) {
		  callback(null, raw.toString('hex') + Date.now() + '.' + mime.extension(file.mimetype));
		});
	}
});
var upload_lotImages = multer({storage: storage_lotImages}).single('lotImage');



//------------------- R E G I S T E R A T I O N --- L O T S ------------------------------------------------------------------------------

/**
@api {post} /lots/RegisterLot Register a Lot
@apiName Register a Lot
@apiGroup Lot Registeration
@apiPermission authentication_required
@apiDescription Creating a product for selling involves 3 steps. Register a product, register a lot and finally create a lot.

This is the second step of the process. Here you pass along the Id of the product you just registered and the quanitity of such products. 

A registeration lot consists one or more of products of the same kind, registered for selling. These registered lots are then seen by the administrators and he approves them. 

@apiHeader {String="application/json"} Content-Type Content Type for the api 
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization": "JWT eyJhbGHVbz5y8i-HRRY"
	}

@apiParam {Number} quantity Quantity of the product to be registered
@apiParam {String} productId Unique product Id
@apiParamExample {json} Request-Example:
	{
		"quantity": 5,
		"productId": "58a72056b134bbad2b5f6b21",
	}
 
 
@apiSuccess {String} status Status of the API Call. It can take values of "error" or "success".
@apiSuccess {JSON} message Message describing the status of the API call.
@apiSuccess {JSON} response Details of the newly registered lot.
@apiSuccessExample Success-Response:
	HTTP/1.1 200 OK
 		{
 		  "status": "success",
 		  "message": "New lot registered",
 		  "registeredLot": {
 		    "__v": 0,
 		    "registeredByUser": "58a70a784ded7c86e5e829e6",
 		    "quantity": 4,
 		    "pId": "58a72056b134bbad2b5f6b21",
 		    "_id": "58b592dadea2128e1fd58eac",
 		    "approved": false,
 		    "dateRegisteredByUser": "2017-02-28T15:10:18.552Z"
 		  }
 		}
 
@apiError error Error message.
@apiErrorExample Error-Response:
    HTTP/1.1 200 Error
		{	
 			"error": "E11000 duplicate key error index: projectnamo-scratch.registeredlots.$pId_1 dup key: { : ObjectId('58a72056b134bbad2b5f6b21') }"
		}
 */
///RegisterLot has been verified on 11/3/17 11:47AM
exports.RegisterLot = function (req, res) {
	var quantity  	= req.body.quantity;
	var productId 	= req.body.productId;
	var userId 		= res.req.user._id;

	if(quantity && productId){
		Product.findById(productId, function(err, product) {
			if(err) {
				return res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});
			}
			else{
				if(!product){
					return res.status(200).send({status: 'error', message: "No product found for productId: "+ productId});
				}else {
						var registeredLot = new RegisteredLot();
						registeredLot.pId = productId;
						registeredLot.quantity = quantity;
						registeredLot.registeredByUser = userId;
						registeredLot.save(function(err, registeredLot){
						if(err){
							return res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});
						}else {
							res.status(200).send({
								status: 'success',
								message: 'New lot registered',
								response: registeredLot
							});
						}
					});
				}
			}
		});
	}else {
		res.status(200).send({status: 'error', message: "Inadequate data sent."});
	}
};

//-------------------------------------------------------------------------------------------------------------------------------------









//---------------D E L E T E------R E G I S T E R E D--------L O T-----------------------------------------------------------


/**
@api {delete} /lots/DeleteLot/:rLotId Delete Registered Lot
@apiName Delete a registered lot
@apiGroup Lot Registeration
@apiPermission authentication_required
@apiDescription Delete a registered lot. 

Admin users can delete any registered lot while normal users can only delete the lot they created. 

If a selling order exists for a lot, it has to be deleted first. If the registered lot has been approved, it cannot be deleted.


@apiHeader {String="application/json"} Content-Type Content Type for the api 
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization": "JWT eyJhbGHVbz5y8i-HRRY"
	}

@apiParam {String} rLotId Unique registered lot Id identyfing the registered lot to be deleted
@apiParamExample {url} Request-Example:
	http://localhost:8080/api/lots/deletelot/58a72e3fa2dda7c3bba61011

@apiSuccess {String} status Status of the API Call. It can take values of "error" or "success".
@apiSuccess {JSON} message Message describing the status of the API call.
@apiSuccess {JSON} response Details of the newly registered lot.
@apiSuccessExample Success-Response:
	HTTP/1.1 200 OK
	{
	  "status": "success",
	  "message": "Successfully deleted!"
	}
 
@apiError error Error message.
@apiErrorExample Error-Response:
    HTTP/1.1 200 Error
		{
		  "status": "error",
		  "message": "Lot with registered lot id: 58a72e3fa2dda7c3bba61011 not found"
		}

 */
///DeleteLot has been verified on 11/3/17 12:04PM
exports.DeleteRegisteredLot = function (req, res) {
	var rLotId 	= req.params.rLotId;
	var user 	= res.req.user;
	if(rLotId && user){
		RegisteredLot.findById(rLotId, function(err, registeredLot){
			if(err) {
				return res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});
			}else {
				if(!registeredLot){
					return res.status(200).send({status: "error", message: "Lot with registered lot id: "+rLotId+" not found"});
				}
				if(registeredLot.registeredByUser!=user._id && user.role == 1){
					return res.status(200).send({status: "error", message: "Cannot delete some other user's lot"});
				}
				RegisteredLot.remove({
					_id: rLotId
				}, function(err, registeredLot){
					if(err){
						return res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});
					}
					else{
						res.status(200).send({
							status: "success",
							message: 'Successfully deleted!'
						});
					}
				});
			}
		});
	}else {
		res.status(200).send({status: "error", message: "Inadequate information."});
	}
};


//-------------------------------------------------------------------------------------------------------------------------------------







