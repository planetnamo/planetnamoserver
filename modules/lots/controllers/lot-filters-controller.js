var mongoose 	= require('mongoose');
var moment = require('moment');
var multer  = require('multer');
var fs = require('fs');
var mime = require('mime');
var crypto = require('crypto');
var passport = require("passport");
var passportJWT = require("passport-jwt");
var ExtractJwt = passportJWT.ExtractJwt;
var JwtStrategy = passportJWT.Strategy;
var jwt = require('jsonwebtoken');
var async = require('async');

var aux 		= require(__utils + '/auxilaryFunc');
var ErrorParser = require(__utils + '/errorParse');
var UsersUtils  = require(__utils + '/users')
var LotsUtils	= require(__utils + '/lots');

var JWTKey  = require(__base + '/config/key');


var User 			= require(__base + '/models/user/user');
var RegisteredLot 	= require(__base + '/models/lots/registered-lot');
var ApprovedLot 	= require(__base + '/models/lots/approved-lot');
var Lot 	        = require(__base + '/models/lots/lot');
var SellingOrder 	= require(__base + '/models/orders/selling-req'); 
var Product 		= require(__base + '/models/products/product');
var BaseProduct 	= require(__base + '/models/products/base-product');
var BuyingOrder     = require(__base + '/models/orders/buying-req');
var UserRating      = require(__base + '/models/stats/user-rating');
var ProductCategory = require(__base + '/models/products/product-category.js');



var storage_lotImages = multer.diskStorage({
	destination: function (request, file, callback) {
		callback(null, __base + '/uploads/lotPictures');
	},
	filename: function (request, file, callback) {
		console.log(file);
		//callback(null, file.originalname);
		crypto.pseudoRandomBytes(16, function (err, raw) {
		  callback(null, raw.toString('hex') + Date.now() + '.' + mime.extension(file.mimetype));
		});
	}
});
var upload_lotImages = multer({storage: storage_lotImages}).single('lotImage');





//---------------------F I L T E R ------------L O T S------------------------------------------------------------------

/**
@api {get} /lots/GetLots Filter final Lots
@apiName GetLots
@apiGroup Lot
@apiDescription Use various filtering options to filter final lots. Each filter can be sent as an array. 

For example, brands can be an array of one or more brand names. Lots matching any one of the brand name present in the array will be filtered.

@apiHeader {String="application/json"} Content-Type Content Type for the api 
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json"
	}

@apiParam {Date} [startDate] startDate Specifies the starting point of the lot duration time. Specified in 'YYYY-MM-DD hh:mm:ss a' format. 
@apiParam {Date} [endDate] endDate Specifies the ending point of the lot duration time. Specified in 'YYYY-MM-DD hh:mm:ss a' format. 
@apiParam {String} [lotType] lotType Array of lot types, can be 'buy','rental' or 'auction' type.
@apiParam {String} [brands] brands Array of brands
@apiParam {String} [productCategories] productCategories Array of productCategories
@apiParam {String} [screen_size] screen_size Array of screen_size
@apiParam {String} [screen_resolution] screen_resolution Array of screen_resolution
@apiParam {String} [processor] processor Array of processor
@apiParam {String} [display_technology] display_technology Array of display_technology
@apiParam {String} [ram] ram Array of ram
@apiParam {String} [hard_drive] hard_drive Array of hard_drive
@apiParam {String} [model_number] model_number Array of model_number
@apiParam {String} [processor_count] processor_count Array of processor_count
@apiParam {String} [processor_brand] processor_brand Array of processor_brand
@apiParam {String} [releaseDate]  releaseDate Array of releaseDate
@apiParam {String} [ratings] ratings Array of ratings
@apiParam {String} [workingcondition] workingcondition Array of workingcondition
@apiParam {String} [condition] condition Array of condition
@apiParam {String} [originalbox] originalbox Array of originalbox
@apiParam {String} [validbill] validbill Array of validbill
@apiParamExample {json} Request-Example:
	{
		"filters": {
			"lot_type": ['buy', 'auction'],
			"brand_name": ['Samsung', 'Apple'],
			"StartingDate": "2017-01-01 11:31:23 PM",
			"EndingDate": "2017-01-01 11:31:23 PM",
		}
	}
 
@apiSuccess {String} status Status of the API Call.
@apiSuccess {String} numberOfLots Number of lots filtered according to the input conditions.
@apiSuccess {String} response An array of filtered lots.

@apiSuccess {JSON} lot Details (json) of the newly lot (FinalLot)
@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK


@apiError error Error message.
@apiErrorExample Error-Response:
    HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "Quantity too large for approvedLotId: 58c3b04b248e1526a93b6c6d"
	}
@apiErrorExample Error-Response:
    HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "Invalid pId sent. Send a valid approved lot id."
	}

 */


var findLotsFunction = function(queryParams, limit, userId, callback){
  Lot.find(queryParams)
     .populate({
        path: 'lotContents.pId',
        model: Product,
        populate: {
          path: 'baseProductId',
          model: BaseProduct,
          populate: {
            path: 'productCategoryId',
            model: ProductCategory              
          }
        }
     })
     .limit(limit)
     .sort('-createdAt')
     .exec(function(err, QueryResult){
        if(err){
          callback(err);
        }else{
          if(QueryResult){
            var response = {};
            response['status'] = "success";
            response['response'] = [];
            let i = 0;
            function asyncFor(i){
              if(i<QueryResult.length){
                if(!QueryResult[i].isLinkedToAuction && QueryResult[i].lotType === "auction"){
                  i+=1;
                  asyncFor(i);
                }
                LotsUtils.ParseFewLotDetails(QueryResult[i], userId, function(err, induvidualParsedLot){
                  if(err){
                    callback(err);
                  }else{
                    response['response'].push(induvidualParsedLot);
                  }
                  i+=1;
                  asyncFor(i);
                });
              }else{
                response['numberOfLots'] = response['response'].length;
                callback(null, response);
              }
            }
            asyncFor(i);
          }else{
            callback("No lots found.");
          }
        }
     });
}

exports.GetLots = function(req, res){
  var filters = req.body.filters;
  var limit = req.body.limit;
  var andQueries = {};
  var orQueries = [];

  if(filters){
    if(filters.createdOnBefore){
      andQueries.createdAt = { $lte: filters.createdOnBefore };
    }
    if(filters.lotType && filters.lotType.length>0){
      andQueries.lotType = {$in: filters.lotType};
    }
    if(filters.startDate && moment(filters.startDate, 'YYYY-MM-DD hh:mm:ss a', true).isValid()){
      andQueries.startDate = {$gte: moment(filters.startDate, 'YYYY-MM-DD hh:mm:ss a', true)};
    }
    if(filters.endDate && moment(filters.endDate, 'YYYY-MM-DD hh:mm:ss a', true).isValid()){
      andQueries.endDate = {$lte: moment(filters.endDate, 'YYYY-MM-DD hh:mm:ss a', true)};
    }
    if(filters.brands && filters.brands.length>0){
      andQueries.brands = {$in: filters.brands};
    }
    if(filters.productCategories && filters.productCategories.length>0){
      andQueries.productCategories = {$in: filters.productCategories};
    }
    if(filters.releaseDate && moment(filters.releaseDate, 'YYYY-MM-DD hh:mm:ss a', true).isValid()){
      andQueries.releaseDate = {$not: {$lt: filters.releaseDate} };
    }
    if(filters.ratings && !isNaN(filters.ratings)){
      andQueries.ratings = {$not: {$lt: filters.ratings} };
    }
    if(filters.workingcondition && !isNaN(filters.workingcondition)){
      andQueries.workingcondition = {$not: {$eq: filters.workingcondition} };
    }
    if(filters.condition && !isNaN(filters.workingcondition)){
      andQueries.condition = {$not: {$lt: filters.condition}};
    }
    if((filters.originalbox)!= undefined){
      andQueries.originalbox = {$not: {$eq: filters.originalbox} };
    }
    if((filters.validbill)!= undefined){
      andQueries.validbill = {$not: {$eq: filters.validbill} };
    }
    if(filters.color && filters.color.length>0){
      andQueries.color = {$in: filters.color};
    }
    if(filters.queryParam){
      orQueries.push(({lotName: { "$regex": filters.queryParam, "$options": "i" }}));
    }

    //compulsory filters
    andQueries.lotSoldOut = {$ne: true};
    andQueries.isChildAuctionLot = {$ne: true};
  }

  var queryParams= {};
  if(orQueries.length > 0){
    queryParams = {$and: [andQueries, { $or: orQueries }]};
  }else{
    queryParams = andQueries;
  }
  if(req.headers && req.headers.authorization){
    var authorization = req.headers.authorization.split(' ')[1];
    try {
      var decodedJWT = jwt.verify(authorization, JWTKey.jwtKEY);
      var userId = decodedJWT.id;
    } catch (e) {
      return res.status(200).send({status: "error", message: "Invalid JWT"});
    } 
    findLotsFunction(queryParams, limit, userId, function(err, response){
      if(err){
        return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
      }else{
        return res.status(200).send(response);
      }
    });
  }else{
    findLotsFunction(queryParams, limit, null, function(err, response){
      if(err){
        return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
      }else{
        return res.status(200).send(response);
      }
    });
  }

};


//----------------------------------------------------------------------------------------------------------------------


