var mongoose 	= require('mongoose');
var moment = require('moment');
var multer  = require('multer');
var fs = require('fs');
var mime = require('mime');
var crypto = require('crypto');


var aux 		= require(__utils + '/auxilaryFunc');
var ErrorParser = require(__utils + '/errorParse');
var UsersUtils  = require(__utils + '/users')
var LotsUtils	= require(__utils + '/lots');

var User 			= require(__base + '/models/user/user');
var RegisteredLot 	= require(__base + '/models/lots/registered-lot');
var ApprovedLot 	= require(__base + '/models/lots/approved-lot');
var Lot 	        = require(__base + '/models/lots/lot');
var SellingOrder 	= require(__base + '/models/orders/selling-req'); 
var Product 		= require(__base + '/models/products/product');
var BaseProduct 	= require(__base + '/models/products/base-product');
var BuyingOrder     = require(__base + '/models/orders/buying-req');
var UserRating      = require(__base + '/models/stats/user-rating');
var ProductCategory = require(__base + '/models/products/product-category.js');



var storage_lotImages = multer.diskStorage({
	destination: function (request, file, callback) {
		callback(null, __base + '/uploads/lotPictures');
	},
	filename: function (request, file, callback) {
		console.log(file);
		//callback(null, file.originalname);
		crypto.pseudoRandomBytes(16, function (err, raw) {
		  callback(null, raw.toString('hex') + Date.now() + '.' + mime.extension(file.mimetype));
		});
	}
});
var upload_lotImages = multer({storage: storage_lotImages}).single('lotImage');




//---------------A P P R O V E ------R E G I S T E R E D--------L O T S--------------------------------------------------------


/**
@api {post} /lots/ApproveLot/:rLotId Approve Registered Lot 
@apiName Approve Registered Lot
@apiGroup Lot Approval
@apiPermission admin
@apiDescription Approve a registered lot. Registered lots are created by users interested in selling their products to PlanetNamo.

Once lots are approved, selling orders associated with that lot (if any) are also approved so that their corresponding delivery and transaction processes are allowed to proceed.

@apiHeader {String="application/json"} Content-Type Content Type for the api 
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization": "JWT eyJhbGHVbz5y8i-HRRY"
	}

@apiParam {String} rLotId Unique registered lot Id identyfing the registered lot to be deleted
@apiParamExample {url} Request-Example:
	http://localhost:8080/api/lots/ApproveLot/58a73376cc35c9ccf1f541c0

@apiSuccess {String} status Status of the API Call. It can take values of "error" or "success".
@apiSuccess {JSON} message Message describing the status of the API call.
@apiSuccess {JSON} response Details of the newly approved lot and the corresponding selling order. Will containt two/three fields, sellingOrder, approvedLot and isSellingOrderPresent. isSellingOrderPresent is true if sellingOrder is present.
@apiSuccessExample Success-Response:
	HTTP/1.1 200 OK
	{
	  "status": "success",
	  "message": "Lot approved!",
	  "response": {
	    "approvedLot": {
	      "__v": 0,
	      "approvedByUser": "58c14475ef21b44e74ed817d",
	      "registeredByUser": "58c14475ef21b44e74ed817d",
	      "dateRegisteredByUser": "2017-03-10T07:48:49.387Z",
	      "quantity": 12,
	      "pId": "58c2531708b1918e667c853d",
	      "_id": "58c3a2e8aaab151bd18cb695"
	    },
	    "isSellingOrderPresent": false
	  }
	}
 
@apiError error Error message.
@apiErrorExample Error-Response:
    HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "No registered lot found with Id : 58a73376cc35c9ccf1f541c0"
	}
@apiErrorExample Error-Response:
    HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "Lot already approved with Id : 58c25a70a25e1d9493299f0e"
	}

 */
///ApproveLot has been verified on 11/3/17 12:42PM
exports.ApproveLot = function (req, res) {
	var rLotId = req.params.rLotId;
	var user = res.req.user;
	if(rLotId && user){
		RegisteredLot.findById(rLotId, function(err, registeredLot){
			if(err){
				return res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});
			}else {
				if(!registeredLot){
					return res.status(200).send({status: 'error', message: "No registered lot found with Id : "+rLotId});
				}
				if(registeredLot.approved == true){
					return res.status(200).send({status: 'error', message: "Lot already approved with Id : "+rLotId});
				}
				var approvedLot = new ApprovedLot();
				approvedLot.pId 					= registeredLot.pId;
				approvedLot.quantity 				= registeredLot.quantity;
				approvedLot.dateRegisteredByUser 	= registeredLot.dateRegisteredByUser;
				approvedLot.registeredByUser 		= registeredLot.registeredByUser;
				approvedLot.approvedByUser 			= user._id;
				approvedLot.save(function(err, approvedLot){
					if(err){
						return res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});
					}else {
						registeredLot.approved = true;
						registeredLot.save(function(err, registeredLot){
							if(err){
								return res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});
							}else {
								SellingOrder.findOne({registeredLotId: registeredLot._id}, function(err, sellingOrder){
									if(err){
										return res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});
									}else{
										let response = {};
										response['approvedLot'] = approvedLot;
										if(!sellingOrder){
											response['isSellingOrderPresent'] = false;
											return res.status(200).send({
												status: 'success',
												message: 'Lot approved!',
												response: response
											});
										}else{
											response['isSellingOrderPresent'] = true;
											sellingOrder.sellingOrderApproved = true;
											sellingOrder.save(function(err, sellingOrder){
												if(err){
													return res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});
												}else {
													response['sellingOrder'] = sellingOrder;
													return res.status(200).send({
														status: 'success',
														message: 'Lot approved!',
														response: response
													});
												}
											})
										}
									}
								});
							}
						});
					}
				});
			}
		});
	}else{
		return res.status(200).send({status: 'error', message: "Inadequate information."});
	}
};


//-------------------------------------------------------------------------------------------------------------------------------------
















//--------------D E L E T E-------------------A P P R O V E D--------------L O T--------------------------------------------------
/**
@api {delete} /lots/DeleteApprovedLot/:aLotId Delete Approved Lot 
@apiName Delete Approved Lot 
@apiGroup Lot Approval
@apiPermission admin
@apiDescription Delete an approved lot. Only admin users can do this. 

Approved lot can only be deleted if no Final Lots have been created by them or the quantity of the products they house have been depleted.

@apiHeader {String="application/json"} Content-Type Content Type for the api 
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization": "JWT eyJhbGHVbz5y8i-HRRY"
	}

@apiParam {String} aLotId Unique Approved lot Id identyfing the approved lot to be deleted
@apiParamExample {url} Request-Example:
	http://localhost:8080/api/lots/DeleteApprovedLot/58a73376cc35c9ccf1f541c0

@apiSuccess {String} status Status of the API Call. It can take values of "error" or "success".
@apiSuccess {JSON} message Message describing the status of the API call.
	HTTP/1.1 200 OK
	{
	  "status": "success",
	  "message": "Successfully deleted!"
	}
 
@apiError error Error message.
@apiErrorExample Error-Response:
    HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "Approved Lot lot id: 58c3a301f832f21bf3262784 not found"
	}
@apiErrorExample Error-Response:
    HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "Cannot delete approved lot participating in final lot whoose quantity is not zero."
	}

 */
///DeleteApprovedLot has been verified on 11/3/17 1:05PM
exports.DeleteApprovedLot = function(req, res) {
	var aLotId = req.params.aLotId;
	if(aLotId){
		ApprovedLot.findById(aLotId, function(err, approvedLot){
			if(err) {
				return res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});
			}else {
				if(approvedLot){
					if(approvedLot.quantity == 0 || approvedLot.participatingInFinalLot == false){
						ApprovedLot.remove({
						_id: aLotId
						}, function(err, approvedLot){
							if(err){
								return res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});
							}
							else{
								res.status(200).send({
									status: 'success',
									message: 'Successfully deleted!'
								});
							}
						});
					}else{
						if(approvedLot.participatingInFinalLot && approvedLot.quantity>0){
							return res.status(200).send({status: 'error', message: 'Cannot delete approved lot participating in final lot whoose quantity is not zero.'});
						}
					}
				}else{
					return res.status(200).send({status: 'error', message: "Approved Lot lot id: "+aLotId+" not found"});
				}
			}
		});
	}else {
		res.status(200).send({status: 'error', message: "Inadequate information."});
	}
};

//----------------------------------------------------------------------------------------------------------------------
