
var mongoose 	= require('mongoose');
var moment = require('moment');
var multer  = require('multer');
var fs = require('fs');
var mime = require('mime');
var crypto = require('crypto');
var jwt = require('jsonwebtoken');
var passport = require("passport");
var passportJWT = require("passport-jwt");
var ExtractJwt = passportJWT.ExtractJwt;
var JwtStrategy = passportJWT.Strategy;

var aux 		= require(__utils + '/auxilaryFunc');
var ErrorParser = require(__utils + '/errorParse');
var UsersUtils  = require(__utils + '/users')
var LotsUtils	= require(__utils + '/lots');

var JWTKey 	= require(__base + '/config/key');

var User 			= require(__base + '/models/user/user');
var RegisteredLot 	= require(__base + '/models/lots/registered-lot');
var ApprovedLot 	= require(__base + '/models/lots/approved-lot');
var Lot 	        = require(__base + '/models/lots/lot');
var SellingOrder 	= require(__base + '/models/orders/selling-req'); 
var Product 		= require(__base + '/models/products/product');
var BaseProduct 	= require(__base + '/models/products/base-product');
var BuyingOrder     = require(__base + '/models/orders/buying-req');
var UserRating      = require(__base + '/models/stats/user-rating');
var ProductCategory = require(__base + '/models/products/product-category.js');



var storage_lotImages = multer.diskStorage({
	destination: function (request, file, callback) {
		callback(null, __base + '/uploads/lotPictures');
	},
	filename: function (request, file, callback) {
		console.log(file);
		//callback(null, file.originalname);
		crypto.pseudoRandomBytes(16, function (err, raw) {
		  callback(null, raw.toString('hex') + Date.now() + '.' + mime.extension(file.mimetype));
		});
	}
});
var upload_lotImages = multer({storage: storage_lotImages}).single('lotImage');




//-----------------R A T E------F I N A L---------L O T-------------------------------------------------------------------
/**
@api {post} /lots/RateLot Rate Final Lot
@apiName RateFinalLot
@apiGroup Lot
@apiDescription Rate a Lot on a scale of 1-5. 
@apiPermission authentication_required

@apiHeader {String="application/json"} Content-Type Content Type for the api 
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization": "JWT eyJhbGHVbz5y8i-HRRY"
	}


@apiParam {String} lotId Unique lot Id identyfing the final lot to be rated
@apiParam {Number} Rating in the range of 1-5
@apiParamExample {json} Request-Example:
	{
		rating: 5,
		lotId: 58ad715d79741f81ea88260f
	}

@apiSuccess {String} status Status of the API Call. It can take values of "error" or "success".
@apiSuccess {JSON} response user rating of the lot

@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
	{
	  "status": "success",
	  "response": {
	    "__v": 0,
	    "lotId": "58c80b1439548bf3ce47304d",
	    "rating": 4,
	    "userId": "58c14475ef21b44e74ed817d",
	    "_id": "58caa55f983c7f77e4848530"
	  }
	}

@apiError error Error message.
@apiErrorExample Error-Response:
    HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "No registered lot found with Id : 58a73376cc35c9ccf1f541c0"
	}
@apiErrorExample Error-Response:
    HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "Lot already approved with Id : 58c25a70a25e1d9493299f0e"
	}
 */
exports.RateLot = function (req, res){
	var user = res.req.user;
	var lotId = req.body.lotId;
	var rating = req.body.rating;
	if(user && lotId){
		Lot.findById(lotId, function(err, lot){
			if(err){
				return res.status(200).send({error: err});
			}else{
				if(lot){
					if(rating == 1 || rating == 2 || rating == 3 || rating == 4 || rating == 5){
						UserRating.findOne({userId: user._id, lotId: lotId}, function(err, result){
							if(err){
								return res.status(200).send({error: err});
							}else{
								if(result){
									return res.status(200).send({status: 'error', message: "You have already rated this lot"});
								}else{
									var userRating 		= new UserRating();
									userRating.userId 	= user._id;
									userRating.rating 	= req.body.rating;
									userRating.lotId 	= req.body.lotId;
									userRating.save(function(err, userRating){
										if(err){
											return res.status(200).send({error: err});
										}else{
											return res.status(200).send({status: 'success', response: userRating});
										}
									});
								}
							}
						});
					}else{
						return res.status(200).send({status: 'error', message: "Rating should be on a scale of 1 to 5"});
					}
				}else{
					return res.status(200).send({status: 'error', message: "Invalid lot id"});
				}
			}
		});
	}else{
		return res.status(200).send({status: 'error', message: "Inadequate information"});
	}
};
//----------------------------------------------------------------------------------------------------------------------



//-----------------G E T------U S E R---------R A T I N G-------------------------------------------------------------------



/**
@api {get} /lots/GetRatingOfLotByUser/:lotId Get User Rating of Final Lot
@apiName GetUserRating 
@apiGroup Lot
@apiDescription Retrive what rating a user has rated a particular lot.
@apiPermission authentication_required

@apiHeader {String="application/json"} Content-Type Content Type for the api 
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization": "JWT eyJhbGHVbz5y8i-HRRY"
	}


@apiParam {String} lotId Unique lot Id identyfing the final lot to be rated
@apiParamExample {url} Request-Example:
	http://localhost:8080/api/lots/GetRatingOfLotByUser/58ad715d79741f81ea88260f

@apiSuccess {String} status Status of the API Call. It can take values of "error" or "success".
@apiSuccess {JSON} response Response of the API
@apiSuccess {JSON} response.rating User rating of the lot

@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
	{
	  "status": "success",
	  "response": {
	    "_id": "58caa55f983c7f77e4848530",
	    "lotId": "58c80b1439548bf3ce47304d",
	    "rating": 4,
	    "userId": "58c14475ef21b44e74ed817d",
	    "__v": 0
	  }
	}

@apiError error Error message.
@apiErrorExample Error-Response:
    HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "Invalid lot id"
	}
 */
exports.GetRatingOfLotByUser = function (req, res) {
	var lotId = req.params.lotId;
	var user = res.req.user;
	if(lotId && user){
		Lot.findById(lotId, function(err, lot){
			if(err){
				return res.status(200).send({error: err});
			}else{
				if(lot){
					UserRating.findOne({userId: user._id, lotId: lotId}, function(err, rating){
						if(err){
							return res.status(200).send({error: err});
						}else if(rating){
							return res.status(200).send({status: 'success', response: rating});
						}else{
							return res.status(200).send({status: 'error', message: "Lot not rated"});
						}
					});
				}else{
					return res.status(200).send({status: 'error', message: "Invalid lot id"});
				}
			}
		})
	}else{
		return res.status(200).send({status: 'error', message: "Inadequate information"});
	}
};
//----------------------------------------------------------------------------------------------------------------------





















//-----------------G E T------O V E R A L L--------R A T I N G-------O F--------L O T----------------------------------------------

/**
@api {get} /lots/GetOverallRatingOfLot/:lotId Get Overall Rating 
@apiName OverallRating
@apiGroup Lot
@apiDescription Get the weighted average rating of a final lot. 

Sending authorization is optional, if sent will give info about what the user has rated the lot.

@apiHeader {String="application/json"} Content-Type Content Type for the api 
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json"
	}


@apiParam {String} lotId Unique Id representin final lot. This is sent as a URL parameter.
@apiParamExample {url} Request-Example:
	http://localhost:8080/api/lots/GetOverallRatingOfLot/58ad715d79741f81ea88260f


@apiSuccess {String} status Status of the API Call. It can take values of "error" or "success".
@apiSuccess {JSON} response user rating of the lot
@apiSuccess {Array} stars_dist Stars distribuiton frequency
@apiSuccess {Number} averageRating Average rating of lot.

@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
	{
	  "status": "success",
	  "response": {
	    "lotId": "58c80b1439548bf3ce47304d",
	    "stars_dist": [
	      {
	        "_id": 2,
	        "numberOfStars": 1
	      },
	      {
	        "_id": 4,
	        "numberOfStars": 1
	      }
	    ],
	    "averageRating": 3
	  }
	}
@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
	{
	  "status": "success",
	  "response": {
	    "lotId": "58d657406a52a236f935ccd1",
	    "stars_dist": [
	      {
	        "_id": 4,
	        "numberOfStars": 1
	      }
	    ],
	    "averageRating": 4,
	    "ratingGivenByUser": {
	      "rating": 4,
	      "userId": "58d646a5a52e0c1a4f229a43"
	    }
	  }
	}
@apiError error Error message.
@apiErrorExample Error-Response:
    HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "No lot found with lotId: 58c80b1439548bf3ce47304e"
	}
 */
exports.GetOverallRatingOfLot = function (req, res) {

	var lotId = req.params.lotId;

	if(lotId){
		if(req.headers && req.headers.authorization) {
			var authorization = req.headers.authorization.split(' ')[1];
			try {
				var decodedJWT = jwt.verify(authorization, JWTKey.jwtKEY);
				var userId = decodedJWT.id;
				LotsUtils.CalculateLotRating(lotId, userId, function(err, ratingResult){
					if(err){
						return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
					}else{
						return res.status(200).send({status: "success", response: ratingResult});
					}
				});
			} catch (e) {
				LotsUtils.CalculateLotRating(lotId, null, function(err, ratingResult){
					if(err){
						return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
					}else{
						return res.status(200).send({status: "success", response: ratingResult});
					}
				});
			} 
		} else{
				LotsUtils.CalculateLotRating(lotId, null, function(err, ratingResult){
					if(err){
						return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
					}else{
						return res.status(200).send({status: "success", response: ratingResult});
					}
				});
		}
	}else{
		return res.status(200).send({status: "error", message: "Inadequate information. lotId not sent."});
	}
};


//---------------------------------------------------------------------------------------------------------------------------






