var mongoose = require('mongoose');
var moment = require('moment');
var multer = require('multer');
var fs = require('fs');
var mime = require('mime');
var crypto = require('crypto');
var jwt = require('jsonwebtoken');
var passport = require("passport");
var passportJWT = require("passport-jwt");
var ExtractJwt = passportJWT.ExtractJwt;
var JwtStrategy = passportJWT.Strategy;
var JWTKey = require(__base + '/config/key');

var aux = require(__utils + '/auxilaryFunc');
var ErrorParser = require(__utils + '/errorParse');
var UsersUtils = require(__utils + '/users')
var LotsUtils = require(__utils + '/lots');

var User = require(__base + '/models/user/user');
var RegisteredLot = require(__base + '/models/lots/registered-lot');
var ApprovedLot = require(__base + '/models/lots/approved-lot');
var Lot = require(__base + '/models/lots/lot');
var SellingOrder = require(__base + '/models/orders/selling-req');
var Product = require(__base + '/models/products/product');
var BaseProduct = require(__base + '/models/products/base-product');
var BuyingOrder = require(__base + '/models/orders/buying-req');
var UserRating = require(__base + '/models/stats/user-rating');
var ProductCategory = require(__base + '/models/products/product-category.js');
var ProductImages = require(__base + '/models/products/productImages.js');
var config = require(__base+ '/config/config.js');
var Auction     = require(__base + '/models/auctions/auction');


var storage_lotImages = multer.diskStorage({
	destination: function(request, file, callback) {
		callback(null, __base + '/uploads/lotPictures');
	},
	filename: function(request, file, callback) {
		console.log(file);
		//callback(null, file.originalname);
		crypto.pseudoRandomBytes(16, function(err, raw) {
			callback(null, raw.toString('hex') + Date.now() + '.' + mime.extension(file.mimetype));
		});
	}
});
var upload_lotImages = multer({
	storage: storage_lotImages
}).single('lotImage');



//---------------------H O T -------S E L L I N G------L O T S---------------------------------------------------------

/**
@api {get} /lots/GetHotSellingLots Hot Selling final Lots
@apiName GetHotSellingLots
@apiGroup Lot
@apiDescription Get list of buy type lots sorted by decreasing about of buying orders placed on them.

@apiHeader {String="application/json"} Content-Type Content Type for the api 
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json"
	}

@apiSuccess {String} status Status of the API Call.
@apiSuccess {JSON} result Array of selling lot Ids

@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
{
  "status": "success",
  "response": [
    {
      "numberOfOrders": 1,
      "parsedLot": {
        "lotDetails": {
          "lotId": "58eb2458bb85797273aa005a",
          "lotName": "untitled lot",
          "lotType": "clearance",
          "numberOfItems": 1,
          "dateWindow": {
            "endDate": "2018-01-01T23:31:23.000Z",
            "startDate": "2017-01-01T23:31:23.000Z"
          },
          "lotSoldOut": true,
          "lotImageURL": "http://urbanriwaaz.com:8080/api/getFile/lotPicture/58eb2458bb85797273aa005a",
          "createdAt": "2017-04-10T06:21:12.926Z",
          "datelotSoldOut": "2017-04-11T05:33:39.203Z",
          "tags": {
            "productCategoryTags": [
              "MOBILE PHONES"
            ],
            "brandNameTags": [
              "Apple"
            ],
            "colorTags": [
              "Silver"
            ],
            "modelNumberTags": [
              "5s"
            ],
            "operatingSystemTags": [
              "iOS"
            ]
          },
          "lotRating": {
            "lotId": "58eb2458bb85797273aa005a",
            "stars_dist": [],
            "averageRating": null
          },
          "noOfOrders": 1,
          "isClearanceStock": false
        },
        "lotItemDetails": [
          {
            "productId": "58d654666a52a236f935ccb0",
            "baseProductId": "58d64724a52e0c1a4f229a44",
            "quantityAvailable": 10,
            "productName": "iPhone 5s",
            "perItemPrice": 35000,
            "perItemEmd": 5200,
            "imageURLS": [
              "http://urbanriwaaz.com:8080/api/getFile/productPicture/58dca91b5513e363809fcec2",
              "http://urbanriwaaz.com:8080/api/getFile/productPicture/58dca9465513e363809fcec3"
            ],
            "itemAccessories": [
              {
                "accessoryname": "Orignal Charger",
                "score": 50,
                "isAccAvailable": true
              },
              {
                "accessoryname": "Earphones",
                "score": 50,
                "isAccAvailable": true
              }
            ],
            "itemCondition": {
              "workingcondition": true,
              "originalbox": true,
              "validbill": true,
              "condition": 3,
              "ageinmonths": 1,
              "itemProblems": [
                {
                  "problemname": "Power Button",
                  "score": 50,
                  "remarks": "Power/home button fautly, hard or not working",
                  "isProblemBeingFaced": false
                },
                {
                  "problemname": "Camera",
                  "score": 50,
                  "remarks": "Front or back camera not working or faulty",
                  "isProblemBeingFaced": false
                },
                {
                  "problemname": "Glass Broken",
                  "score": 50,
                  "remarks": "Screen glass broken",
                  "isProblemBeingFaced": false
                }
              ]
            },
            "specifications": [
              {
                "displayName": "Color",
                "specificationValue": "Black"
              }
            ]
          }
        ]
      }
    },
    {
      "numberOfOrders": 4,
      "parsedLot": {
        "lotDetails": {
          "lotId": "58d6648afb649e40d520885a",
          "lotName": "untitled lot",
          "lotType": "clearance",
          "numberOfItems": 2,
          "dateWindow": {
            "endDate": "2019-01-01T18:01:23.000Z",
            "startDate": "2017-01-01T18:01:23.000Z"
          },
          "lotSoldOut": false,
          "lotImageURL": "http://urbanriwaaz.com:8080/api/getFile/lotPicture/58d6648afb649e40d520885a",
          "createdAt": "2017-04-01T11:34:47.648Z",
          "tags": {
            "productCategoryTags": [
              "LAPTOPS",
              "MOBILE PHONES"
            ],
            "brandNameTags": [
              "Lenovo",
              "Samsung"
            ],
            "colorTags": [
              "Black",
              "Gold"
            ],
            "modelNumberTags": [
              "YG1384",
              "S7e"
            ],
            "operatingSystemTags": [
              "Windows 10",
              "Android"
            ]
          },
          "lotRating": {
            "lotId": "58d6648afb649e40d520885a",
            "stars_dist": [],
            "averageRating": null
          },
          "noOfOrders": 0,
          "isClearanceStock": false
        },
        "lotItemDetails": [
          {
            "productId": "58d654a26a52a236f935ccb8",
            "baseProductId": "58d6532d5f5fe43655d0530b",
            "quantityAvailable": 7,
            "productName": "Lenovo YogaPad",
            "perItemPrice": 10000,
            "perItemEmd": 200,
            "imageURLS": [
              "http://urbanriwaaz.com:8080/api/getFile/productPicture/58dca9945513e363809fcecb"
            ],
            "itemAccessories": [
              {
                "accessoryname": "Orignal Charger",
                "score": 50,
                "isAccAvailable": true
              },
              {
                "accessoryname": "Earphones",
                "score": 50,
                "isAccAvailable": false
              }
            ],
            "itemCondition": {
              "workingcondition": true,
              "originalbox": true,
              "validbill": true,
              "condition": 3,
              "ageinmonths": 5,
              "itemProblems": [
                {
                  "problemname": "Power Button",
                  "score": 50,
                  "remarks": "Power/home button fautly, hard or not working",
                  "isProblemBeingFaced": false
                },
                {
                  "problemname": "Camera",
                  "score": 50,
                  "remarks": "Front or back camera not working or faulty",
                  "isProblemBeingFaced": true
                },
                {
                  "problemname": "Glass Broken",
                  "score": 50,
                  "remarks": "Screen glass broken",
                  "isProblemBeingFaced": false
                }
              ]
            },
            "specifications": []
          },
          {
            "productId": "58d654966a52a236f935ccb6",
            "baseProductId": "58d6488becd24d1ea45e8ccd",
            "quantityAvailable": 10,
            "productName": "Samsung S7 Edge",
            "perItemPrice": 35000,
            "perItemEmd": 5200,
            "imageURLS": [
              "http://urbanriwaaz.com:8080/api/getFile/productPicture/58dca9815513e363809fcec9"
            ],
            "itemAccessories": [
              {
                "accessoryname": "Orignal Charger",
                "score": 50,
                "isAccAvailable": true
              },
              {
                "accessoryname": "Earphones",
                "score": 50,
                "isAccAvailable": false
              }
            ],
            "itemCondition": {
              "workingcondition": true,
              "originalbox": true,
              "validbill": true,
              "condition": 3,
              "ageinmonths": 5,
              "itemProblems": [
                {
                  "problemname": "Power Button",
                  "score": 50,
                  "remarks": "Power/home button fautly, hard or not working",
                  "isProblemBeingFaced": false
                },
                {
                  "problemname": "Camera",
                  "score": 50,
                  "remarks": "Front or back camera not working or faulty",
                  "isProblemBeingFaced": true
                },
                {
                  "problemname": "Glass Broken",
                  "score": 50,
                  "remarks": "Screen glass broken",
                  "isProblemBeingFaced": false
                }
              ]
            },
            "specifications": [
              {
                "displayName": "Color",
                "specificationValue": "Pink"
              },
              {
                "displayName": "Operating System",
                "specificationValue": "Linux"
              },
              {
                "displayName": "Weight",
                "specificationValue": "100kgs"
              }
            ]
          }
        ]
      }
    }
  ]
}
@apiError error Error message.
@apiErrorExample Error-Response:
    HTTP/1.1 200 Error

@apiErrorExample Error-Response:
    HTTP/1.1 200 Error

 */


var findLotsFunction = function(userId, callback){
	BuyingOrder.aggregate([{
		'$group': {
			_id: '$lotId',
			numberOfOrders: {
				'$sum': 1
			}
		}
	}, {
		'$sort': {
			'count': -1
		}
	}], function(err, results) {
		if (err) {
			callback(err);
		} else {
			var i = 0;
			var response = [];
			function asyncFor(i){
				if(i<results.length){
					Lot.findById(results[i]._id)
					   .populate({
					        path: 'lotContents.pId',
					        model: Product,
					        populate: {
					          path: 'baseProductId',
					          model: BaseProduct,
					          populate: {
					            path: 'productCategoryId',
					            model: ProductCategory              
					          }
					        }
					   })
					   .exec(function(err, populatedLot){
					   		if(err){
					   			callback(err);
					   		}else{
					   			let tmp = {};
					   			//tmp['lotId'] = results[i]._id;
					   			tmp['numberOfOrders'] = results[i].numberOfOrders;
					   			LotsUtils.ParseLotDetails(populatedLot, null, function(err, parsedLot){
					   				if(err){
					   					callback(err);
					   				}else{
					   					tmp['parsedLot'] = parsedLot;
					   					response.push(tmp);
					   					i+=1;
					   					asyncFor(i);
					   				}
					   			});
					   		}
					   });
				}else{
					callback(null, response);
				}
			}
			asyncFor(i);
		}
	});
}


exports.GetHotSellingLots = function(req, res){

	if(req.headers && req.headers.authorization){
		var authorization = req.headers.authorization.split(' ')[1];
	try {
		var decodedJWT = jwt.verify(authorization, JWTKey.jwtKEY);
		var userId = decodedJWT.id;
	} catch (e) {
		return res.status(200).send({status: "error", message: "Invalid JWT"});
	} 
		findLotsFunction(userId, function(err, response){
		  if(err){
		    return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
		  }else{
		  	response.sort(aux.compare);
		    return res.status(200).send({status: "success", response: response});
		  }
		});
	}else{
		findLotsFunction(null, function(err, response){
		  if(err){
		    return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
		  }else{
		  	response.sort(aux.compare);
		    return res.status(200).send({status: "success", response: response});
		  }
		});
	}


};

//----------------------------------------------------------------------------------------------------------------------


//-----------------F I N D-----S I M I L A R------L O T S---------------------------------------------------------

/**
@api {post} /lots/FindSimilarLots/:lotId Find Similar Lots
@apiName FindSimilarLots
@apiGroup Lot
@apiDescription Find lots having similar products as the lot with :lotId. 1 or more matches of products shown.

@apiHeader {String="application/json"} Content-Type Content Type for the api 
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization": "JWT eyJhbGHVbz5y8i-HRRY"
	}


@apiParam {String} lotId unique id identyfing final lot, only buy type lots will be considered
@apiParamExample {URL} Request-Example:
http://localhost:8080/api/lots/FindSimilarLots/58c80b1439548bf

@apiSuccess {String} status Status of the API Call.
@apiSuccess {JSON} response Response of the API
@apiSuccess {JSON} response.noOfSimilarLots Number of similar lots found.
@apiSuccess {JSON} response.similarLots An array of similar lot Ids along with their similarity score.

@apiSuccessExample Success-Response:
	HTTP/1.1 200 OK
	{
	  "status": "success",
	  "response": {
	    "noOfSimilarLots": 4,
	    "similarLots": [
		 {
		   "lotId": "58c3b7265520b02d9f76a1a0",
		   "similarity": 2
		 },
		 {
		   "lotId": "58c3e1447573273ed747a1c5",
		   "similarity": 2
		 },
		 {
		   "lotId": "58c80ab53096cdf3988fc97b",
		   "similarity": 2
		 },
		 {
		   "lotId": "58c80b1439548bf3ce47304d",
		   "similarity": 2
		 }
	    ]
	  }
	}

@apiError error Error message.
@apiErrorExample Error-Response:
HTTP/1.1 200 Error
	{    
		"status": 'error',
		"message": "Invalid lot type"
	}
*/
exports.FindSimilarLots = function(req, res) {
	var lotId = req.params.lotId;
	if (lotId) {
		Lot.findById(lotId)
			.populate({
				path: 'lotContents.pId',
				model: Product
			})
			.exec(function(err, lot) {
				if (err) {
					return res.status(200).send({
						status: "error",
						message: ErrorParser.parseErrorreturnErrorMessage(err)
					});
				} else {
					if (lot) {
						var productInfo = lot.lotContents;
						var similarLots = {};
						for (var i = 0; i < productInfo.length; i++) {
							var finalLots = productInfo[i].pId.finalLots;
							for (var j = 0; j < finalLots.length; j++) {
								if (finalLots[j] in similarLots)
									similarLots[finalLots[j]] += 1;
								else
									similarLots[finalLots[j]] = 1;
							}
						}
						var similarLotsArray = [];
						for (lotId in similarLots) {
							var tmp = {};
							tmp['lotId'] = lotId;
							tmp['similarity'] = similarLots[lotId];
							similarLotsArray.push(tmp);
						}
						var response = {};
						response['noOfSimilarLots'] = similarLotsArray.length;
						response['similarLots'] = similarLotsArray;
						return res.status(200).send({
							status: 'success',
							response: response
						});
					} else {
						return res.status(200).send({
							status: "error",
							message: "Lot not found with lotId: " + lotId
						});
					}
				}
			});
	} else {
		return res.status(200).send({
			status: 'error',
			message: "LotId not sent in request"
		});
	}
};

//---------------------------------------------------------------------------------------------------------------------------


//----------------G E T---------------L O T---------------D E T A I L S-----------------------------------------------------------------------------



/**
@api {post} /lot/GetLotDetails/:lotId Get Lot Details
@apiName GetLotDetails
@apiGroup Lot
@apiDescription Get descriptive details about a lot. 


@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization": "JWT eyJhbGHVbz5y8i-HRRY"
	} 

@apiParam {String} :lotId Unique Id representing a final lot. Sent as a URL parameter
	http://localhost:8080/api/lots/GetLotDetails/58c80b1439548bf3ce47304d

@apiSuccess {String} status Status of the API Call.
@apiSuccess {String} message Message describing the status
@apiSuccess {String} response Response of the API
@apiSuccess {String} response.lotImageURL the new image URL

@apiSuccessExample Success-Response:
	HTTP/1.1 200 OK
{
  "status": "success",
  "response": {
    "lotDetails": {
      "lotId": "58f08b0ff29a0a6c3866e867",
      "lotName": "untitled lot",
      "lotType": "auction",
      "numberOfItems": 2,
      "dateWindow": {
        "endDate": "2017-04-19T18:30:00.000Z",
        "startDate": "2017-04-13T18:30:00.000Z"
      },
      "lotSoldOut": false,
      "lotImageURL": "http://urbanriwaaz.com:8080/api/getFile/lotPicture/58f08b0ff29a0a6c3866e867",
      "createdAt": "2017-04-14T08:40:48.492Z",
      "tags": {
        "productCategoryTags": [
          "MOBILE PHONES"
        ],
        "brandNameTags": [
          "Apple"
        ],
        "colorTags": [
          "Silver"
        ],
        "modelNumberTags": [
          "7",
          "5s"
        ],
        "operatingSystemTags": [
          "iOS"
        ]
      },
      "lotRating": {
        "lotId": "58f08b0ff29a0a6c3866e867",
        "stars_dist": [],
        "averageRating": null,
        "ratingGivenByUser": "User did not rate lot"
      },
      "bidIncreaseDetails": {
        "value": 500,
        "type": "Number"
      },
      "auctionTimeLeftInMS": 393775227
    },
    "lotItemDetails": [
      {
        "productId": "58d654736a52a236f935ccb1",
        "baseProductId": "58d64724a52e0c1a4f229a44",
        "quantityAvailable": 1,
        "productName": "iPhone 5s",
        "perItemPrice": 30000,
        "perItemEmd": 5000,
        "auctionMinDeposit": 7000,
        "imageURLS": [
          "http://urbanriwaaz.com:8080/api/getFile/productPicture/58dca95e5513e363809fcec4"
        ],
        "itemAccessories": [
          {
            "accessoryname": "Orignal Charger",
            "score": 50,
            "isAccAvailable": true
          },
          {
            "accessoryname": "Earphones",
            "score": 50,
            "isAccAvailable": false
          }
        ],
        "itemCondition": {
          "workingcondition": true,
          "originalbox": true,
          "validbill": false,
          "condition": 2,
          "ageinmonths": 11,
          "itemProblems": [
            {
              "problemname": "Power Button",
              "score": 50,
              "remarks": "Power/home button fautly, hard or not working",
              "isProblemBeingFaced": false
            },
            {
              "problemname": "Camera",
              "score": 50,
              "remarks": "Front or back camera not working or faulty",
              "isProblemBeingFaced": false
            },
            {
              "problemname": "Glass Broken",
              "score": 50,
              "remarks": "Screen glass broken",
              "isProblemBeingFaced": false
            }
          ]
        },
        "specifications": [
          {
            "displayName": "Color",
            "specificationValue": "Black"
          }
        ]
      },
      {
        "productId": "58daa1888e1b981cb77336a6",
        "baseProductId": "58d64822ecd24d1ea45e8cc7",
        "quantityAvailable": 1,
        "productName": "iPhone 7",
        "perItemPrice": 35000,
        "perItemEmd": 5200,
        "auctionMinDeposit": 7000,
        "imageURLS": [
          "http://urbanriwaaz.com:8080/api/getFile/productPicture/58e734c054f682e67e113475"
        ],
        "itemAccessories": [
          {
            "accessoryname": "Orignal Charger",
            "score": 50,
            "isAccAvailable": true
          },
          {
            "accessoryname": "Earphones",
            "score": 50,
            "isAccAvailable": true
          }
        ],
        "itemCondition": {
          "workingcondition": true,
          "originalbox": true,
          "validbill": true,
          "condition": 3,
          "ageinmonths": 33,
          "itemProblems": [
            {
              "problemname": "Power Button",
              "score": 50,
              "remarks": "Power/home button fautly, hard or not working",
              "isProblemBeingFaced": false
            },
            {
              "problemname": "Camera",
              "score": 50,
              "remarks": "Front or back camera not working or faulty",
              "isProblemBeingFaced": false
            },
            {
              "problemname": "Glass Broken",
              "score": 50,
              "remarks": "Screen glass broken",
              "isProblemBeingFaced": false
            }
          ]
        },
        "specifications": [
          {
            "displayName": "Color",
            "specificationValue": "Pink"
          },
          {
            "displayName": "RAM",
            "specificationValue": "6GB",
            "specificationDescription": "Ram of the device"
          }
        ]
      }
    ],
    "userBids": {},
    "HighestLotBids": {}
  }
}
@apiSuccessExample Success-Response:
  HTTP/1.1 200 OK
  ewk

	
@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "Inadequate information sent"
	}
*/

exports.GetLotDetails = function(req, res) {
	var lotId = req.params.lotId;

	if (lotId) {
		if (req.headers && req.headers.authorization) {
			var authorization = req.headers.authorization.split(' ')[1];
			try {
				var decodedJWT = jwt.verify(authorization, JWTKey.jwtKEY);
				var userId = decodedJWT.id;
			} catch (e) {
				return res.status(200).send({
					status: "error",
					message: "Invalid JWT"
				});
			}
			Lot.findById(lotId)
				.populate({
					path: 'lotContents.pId',
					model: Product,
					populate: {
						path: 'baseProductId',
						model: BaseProduct,
						populate: {
							path: 'productCategoryId',
							model: ProductCategory
						}
					}
				})
				.exec(function(err, lot) {
					if (err) {
						return res.status(200).send({
							status: "error",
							message: ErrorParser.parseErrorreturnErrorMessage(err)
						});
					} else {
						if (lot) {
							LotsUtils.ParseLotDetails(lot, userId, function(err, parsedLot) {
								if (err) {
									return res.status(200).send({
										status: "error",
										message: ErrorParser.parseErrorreturnErrorMessage(err)
									});
								} else {
									return res.status(200).send({
										status: "success",
										response: parsedLot
									});
								}
							});
						} else {
							return res.status(200).send({
								status: "error",
								message: "Lot not found with lotId: " + lotId
							});
						}
					}
				});

		} else {
			Lot.findById(lotId)
				.populate({
					path: 'lotContents.pId',
					model: Product,
					populate: {
						path: 'baseProductId',
						model: BaseProduct,
						populate: {
							path: 'productCategoryId',
							model: ProductCategory
						}
					}
				})
				.exec(function(err, lot) {
					if (err) {
						return res.status(200).send({
							status: "error",
							message: ErrorParser.parseErrorreturnErrorMessage(err)
						});
					} else {
						if (lot) {
							LotsUtils.ParseLotDetails(lot, null, function(err, parsedLot) {
								if (err) {
									return res.status(200).send({
										status: "error",
										message: ErrorParser.parseErrorreturnErrorMessage(err)
									});
								} else {
									return res.status(200).send({
										status: "success",
										response: parsedLot
									});
								}
							});
						} else {
							return res.status(200).send({
								status: "error",
								message: "Lot not found with lotId: " + lotId
							});
						}
					}
				});
		}
	} else {
		return res.status(200).send({
			status: "error",
			message: "Inadequate information. lotId not sent."
		});
	}
};



//-------------------------------------------------------------------------------------------------------------------------------------



//-----------------C O M P A R E------------L O T S----------------------------------------------------------------------------------------



/**
@api {post} /lot/CompareLots Compare Lots
@apiName CompareLots
@apiGroup Lot
@apiDescription Comparision of lots.


@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
  {
    "Content-Type": "application/json",
    "Authorization": "JWT eyJhbGHVbz5y8i-HRRY"
  } 

@apiParam {Array} lotIdArray Array of length greater than 1 and less than 3 of lotIds.

  
@apiError error Error message.
@apiErrorExample Error-Response:
  HTTP/1.1 200 Error
  {
    "status": "error",
    "message": "Inadequate information sent"
  }
*/
exports.CompareLots = function(req, res) {
  var lotIdArray = req.body.lotIdArray;
  if(lotIdArray){
    if(Array.isArray(lotIdArray)){
      if(lotIdArray.length<=3 && lotIdArray.length>1){
        var isValid = true;
        for(let i=0; i<lotIdArray.length; i++){
          if(mongoose.Types.ObjectId.isValid(lotIdArray[i]) === false)
            isValid = false;
        }
        if(isValid === false){
          return res.status(200).send({status: "error", message: "Invalid request."});
        }

        var populatedLotArray = [];
        var i = 0;
        function asyncFor(i){
          if(i<lotIdArray.length){
            Lot.findById(lotIdArray[i])
               .populate({
                  path: 'lotContents.pId',
                  model: Product,
                  populate: {
                    path: 'baseProductId',
                    model: BaseProduct,
                    populate: {
                      path: 'productCategoryId',
                      model: ProductCategory
                    }
                  }
               })
               .exec(function(err, populatedLot){
                  if(err){
                    return res.status(200).send({
                      status: "error",
                      message: ErrorParser.parseErrorreturnErrorMessage(err)
                    });
                  }else{
                    populatedLotArray.push(populatedLot);
                    i+=1;
                    asyncFor(i);
                  }
               });
          }else{
            if(populatedLotArray.length<3)
              populatedLotArray.push(null);
            var compareResponse = {};
            compareResponse['lotType'] = [];
            compareResponse['lotIds'] = [];
            compareResponse['lotName'] = [];
            compareResponse['lotTypeSpecificProperties'] = []; //<--- left
            compareResponse['lotContents'] = [];
            compareResponse['lotDates'] = [];
            compareResponse['lotImageURLs'] = [];
            compareResponse['isLotSoldOut'] = [];
            compareResponse['datelotSoldOut'] = [];
            //compareResponse['isDirectBuyLot'] = [];
            compareResponse['brandNameTags'] = [];
            compareResponse['productCategories'] = [];
            compareResponse['lotTermsAndConditions'] = [];
            compareResponse['productDescription'] = [];

            for(var i=0; i<3; i++){
              if(populatedLotArray[i]){
                if(populatedLotArray[i].isChildAuctionLot === true){
                  return res.status(200).send({status: "error", message: "Invalid lot type: "+populatedLotArray[i]._id});
                }

                compareResponse['lotType'].push(populatedLotArray[i].lotType);
                compareResponse['lotIds'].push(populatedLotArray[i]._id);
                compareResponse['lotName'].push(populatedLotArray[i].lotName);
                compareResponse['lotDates'].push({
                  startDate: populatedLotArray[i].startDate,
                  endDate: populatedLotArray[i].endDate
                });
                compareResponse['lotImageURLs'].push(config.baseURL + '/getFile/lotPicture/'+populatedLotArray[i]._id);
                compareResponse['isLotSoldOut'].push(populatedLotArray[i].lotSoldOut);
                if(populatedLotArray[i].datelotSoldOut && compareResponse['isLotSoldOut'][i] === true)
                  compareResponse['datelotSoldOut'].push(populatedLotArray[i].datelotSoldOut);
                else
                  compareResponse['datelotSoldOut'].push(null);
                //compareResponse['isDirectBuyLot'].push(populatedLotArray[i].isDirectBuyLot);
                compareResponse['brandNameTags'].push(populatedLotArray[i].brands);
                
                let tmpArray = [];
                let tmpLotContent = [];
                let tmpProdDesc = [];
                let tmpClearanceData = [];
                let tmpDirectData = [];
                let auctionData = [];
                let rentalData = [];

                for(let j=0; j<populatedLotArray[i].lotContents.length; j++){
                  tmpArray.push(populatedLotArray[i].lotContents[j].pId.baseProductId.productCategoryId.categoryName);
                  tmpLotContent.push({
                    productId: populatedLotArray[i].lotContents[j].pId._id,
                    productName: populatedLotArray[i].lotContents[j].pId.baseProductId.productName,
                    quantityAvailable: populatedLotArray[i].lotContents[j].quantity,
                    price: populatedLotArray[i].lotContents[j].price
                  })

                  tmpProdDesc.push({
                    productName: populatedLotArray[i].lotContents[j].pId.baseProductId.productName,
                    productCategory: populatedLotArray[i].lotContents[j].pId.baseProductId.productCategoryId.categoryName,
                    specifications: populatedLotArray[i].lotContents[j].specifications,
                    ageinmonths: populatedLotArray[i].lotContents[j].pId.ageinmonths,
                    condition: populatedLotArray[i].lotContents[j].pId.condition,
                    validbill: populatedLotArray[i].lotContents[j].pId.validbill,
                    originalbox: populatedLotArray[i].lotContents[j].pId.originalbox,
                    workingcondition: populatedLotArray[i].lotContents[j].pId.workingcondition
                  })

                  if(populatedLotArray[i].lotType === "clearance"){

                  }else if(populatedLotArray[i].lotType === "auction"){
                    auctionData.push({
                      productId: populatedLotArray[i].lotContents[j].pId._id,
                      productName: populatedLotArray[i].lotContents[j].pId.baseProductId.productName
                    })
                  }else if(populatedLotArray[i].lotType === "buy"){

                  }else if(populatedLotArray[i].lotType === rental){

                  }
                }
                compareResponse['lotContents'].push(tmpLotContent);
                compareResponse['productDescription'].push(tmpProdDesc);
                compareResponse['productCategories'].push(tmpArray);
                if(populatedLotArray[i].lotTermsAndConditions)
                  compareResponse['lotTermsAndConditions'].push(populatedLotArray[i].lotTermsAndConditions);
                else
                  compareResponse['lotTermsAndConditions'].push(null);
              }
              else{
                compareResponse['lotType'].push(null);
                compareResponse['lotIds'].push(null);
                compareResponse['lotName'].push(null);
                compareResponse['lotContents'].push(null);
                compareResponse['lotDates'].push({
                  startDate: null,
                  endDate: null
                });
                compareResponse['lotImageURLs'].push(null);
                compareResponse['isLotSoldOut'].push(null);
                compareResponse['datelotSoldOut'].push(null);
                //compareResponse['isDirectBuyLot'].push(null);
                compareResponse['brandNameTags'].push(null);
                compareResponse['productCategories'].push(null);
                compareResponse['lotTermsAndConditions'].push(null);
                compareResponse['productDescription'].push(null);
              }
            }

            return res.status(200).send({status: "success", response: compareResponse});
          }
        }
        asyncFor(i);
      }else{
        return res.status(200).send({status: "error", message: "Invalid request."});
      }
    }else{
      return res.status(200).send({status: "error", message: "Invalid request."});
    }
  }else{
    return res.status(200).send({status: "error", message: "Invalid request."});
  }
};

//-------------------------------------------------------------------------------------------------------------------------------------



//----------------C H E C K ---------- I F--------- L O T --------I S------- I N -----------U S E R------- W I S H L I S T---------------------------------------------------------------------------------------

exports.IsInWishlist = function(req, res) {
	var user = req.user;
	var lotId = req.params.lotId;
	if (user) {
		if (lotId) {
			LotsUtils.IsInWishlist(lotId, user._id, function(err, isInWishlist) {
				if (err) {
					return res.status(200).send({
						status: "error",
						message: ErrorParser.parseErrorreturnErrorMessage(err)
					});
				} else {
					return res.status(200).send({
						status: "success",
						isInWishlist: isInWishlist
					});
				}
			});
		} else {
			return res.status(200).send({
				status: "error",
				message: "lotId not sent"
			});
		}
	} else {
		return res.status(200).send({
			status: "error",
			message: "User not authenticated"
		});
	}
};

//-------------------------------------------------------------------------------------------------------------------------------------