var mongoose = require('mongoose');
var moment = require('moment');
var multer = require('multer');
var fs = require('fs');
var mime = require('mime');
var crypto = require('crypto');


var aux = require(__utils + '/auxilaryFunc');
var ErrorParser = require(__utils + '/errorParse');
var UsersUtils = require(__utils + '/users')
var LotsUtils = require(__utils + '/lots');

var User = require(__base + '/models/user/user');
var RegisteredLot = require(__base + '/models/lots/registered-lot');
var ApprovedLot = require(__base + '/models/lots/approved-lot');
var Lot = require(__base + '/models/lots/lot');
var SellingOrder = require(__base + '/models/orders/selling-req');
var Product = require(__base + '/models/products/product');
var BaseProduct = require(__base + '/models/products/base-product');
var BuyingOrder = require(__base + '/models/orders/buying-req');
var UserRating = require(__base + '/models/stats/user-rating');
var ProductCategory = require(__base + '/models/products/product-category.js');

var baseProductCtrl = require(__modules + '/product/controllers/baseproduct-controller.js');
var productCtrl = require(__modules + '/product/controllers/product-controller.js');



var storage_lotImages = multer.diskStorage({
	destination: function(request, file, callback) {
		callback(null, __base + '/uploads/lotPictures');
	},
	filename: function(request, file, callback) {
		console.log(file);
		//callback(null, file.originalname);
		crypto.pseudoRandomBytes(16, function(err, raw) {
			callback(null, raw.toString('hex') + Date.now() + '.' + mime.extension(file.mimetype));
		});
	}
});
var upload_lotImages = multer({
	storage: storage_lotImages
}).single('lotImage');



//----------------------------------F I N A L -------L O T S--------------------------------------------------------------

/**
@api {post} /lots/CreateFinalLot Create Final Lot
@apiName Create Final Lot
@apiGroup Lot 
@apiPermission admin
@apiDescription Create a final lot. A final lot can contain different types of products of varying quanity. The products required to create the final lot are taken from the approved lots.

Final lot is the customer interfacing lot. Customers can interact with these final lots. 

Final lot is of three types - 'buying', 'auction' or 'rental'. 

Final lot also have a duration period, only during this period can the final lot be available for interaction with customers.

Final lot is also simple known as Lot in this api documentation, thus distinguising itself from the two other kinds of lots - registered lots and approved lots.

When creating a final lot, the corresponding approved lots from which the final lot was created will have their respective quantities reduced and will be linked to this final lot. The corresponding products of the approved lot will also be linked to the final lot.


@apiHeader {String="application/json"} Content-Type Content Type for the api 
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization": "JWT eyJhbGHVbz5y8i-HRRY"
	}


@apiParam {Date} startDate Specifies the starting point of the lot duration time. Specified in 'YYYY-MM-DD hh:mm:ss a' format. 
@apiParam {Date} endDate Specifies the ending point of the lot duration time. Specified in 'YYYY-MM-DD hh:mm:ss a' format. 
@apiParam {String} lotType Type of lot, can be 'buy','rental' or 'auction' type.
@apiParam {Array} lotContents lotContents is an array of objects. Each element of this array represents the product which is to be added to the final lot.
@apiParam {String} lotContents.pId Each object of the lotContents array must contain this. It specifies the APPROVED LOT ID to which the product belongs.
@apiParam {Number} lotContents.quantity Each object of the lotContents array must contain this. It specifies the quantity of the product with product Id pId which is to be added to the lot. The quantity should be less than or equal to the quantity present in the approved lot.
@apiParam {String} lotContents.pId Each object of the lotContents array must contain this. It specifies the product with product Id pId which is to be added to the lot.
@apiParam {String} lotContents.price Each object of the lotContents array must contain this. It specifies the price of this induvidual product. This is the full price the customers will pay in case this is a 'buy' type lot.
@apiParam {String} lotContents.emdDeposit Each object of the lotContents array must contain this. It specifies the minimum emd deposit to be paid for a single product. Emd deposit is paid in case of a 'buy' and 'auction' type lot.
@apiParam {String} [lotContents.auctionMinDeposit] lotContents.auctionMinDeposit Only for auction type lots. It specifies the minimum (first)bid for the induvidual product.
@apiParam {String} [lotContents.auctionPercentageDepositIncrease] lotContents.auctionPercentageDepositIncrease Only for auction type lots. It specifies the minimum % of bid increase on this product in successive bids on an auction.


@apiParamExample {json} Request-Example:
{
	"startDate": "2016-01-01 11:31:23 PM",
	"endDate": "2018-01-01 11:31:23 PM",
	"lotContents": [{
		"pId": "58a733ba7d30b5cd6135bf64",
		"quantity": 1,
		"price": 343,
		"emdDeposit": 50,
		"auctionMinDeposit": 300,
		"auctionPercentageDepositIncrease": 5
	},{
		"pId": "58a73d1565c0fadc289a5631",
		"quantity": 1,
		"price": 900,
		"emdDeposit": 100,
		"auctionMinDeposit": 850,
		"auctionPercentageDepositIncrease": 7
	}],
	"lotType": "auction"
}
@apiParamExample {json} Request-Example:
{
	"startDate": "2017-01-01 11:31:23 PM",
	"endDate": "2018-01-01 11:31:23 PM",
	"lotContents": [{
		"pId": "58c3b04b248e1526a93b6c6d",
		"quantity": 1,
		"price": 30000,
		"emdDeposit": 5000
	},{
		"pId": "58c3b058248e1526a93b6c6e",
		"quantity": 1,
		"price": 35000,
		"emdDeposit": 5200
	}],
	"lotType": "buy"
}
@apiParamExample {json} Request-Example:
{
  "startDate": "2017-01-01 11:31:23 PM",
  "endDate": "2018-01-01 11:31:23 PM",
  "lotContents": [{
    "pId": "58d656bf6a52a236f935ccca",
    "quantity": 2,
    "price": 30000,
    "emdDeposit": 5000,
    "rentalSecurityDeposit": 2000,
    "rentalPricePerMonth": 200,
    "rentalPriceByNumberOfMonths": [{
    	"numberOfMonths": 3,
    	"price": 1000
    },{
     	"numberOfMonths": 6,
    	"price": 1000   	
    },{
     	"numberOfMonths": 9,
    	"price": 1000     	
    }]
  },{
    "pId": "58d656b16a52a236f935ccc8",
    "quantity": 1,
    "price": 35000,
    "emdDeposit": 5200,
    "rentalSecurityDeposit": 2000,
    "rentalPricePerMonth": 200,
    "rentalPriceByNumberOfMonths": [{
    	"numberOfMonths": 3,
    	"price": 1000
    },{
     	"numberOfMonths": 6,
    	"price": 1000   	
    },{
     	"numberOfMonths": 9,
    	"price": 1000     	
    }]
  }],
  "lotType": "rental"
}


@apiSuccess {String} status Status of the API Call.
@apiSuccess {String} message Message describing the status of the API call.
@apiSuccess {String} response Will contain the details of the newly created lot.

@apiSuccess {JSON} lot Details (json) of the newly lot (FinalLot)
@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
	{
	  "status": 'error',
	  "message": "Successfully created lot!",
	  "response": {
	    "__v": 0,
	    "endDate": "2018-01-01T18:01:23.000Z",
	    "startDate": "2017-01-01T18:01:23.000Z",
	    "lotType": "buy",
	    "createdByUser": "58c14475ef21b44e74ed817d",
	    "_id": "58c3b0f9248e1526a93b6c70",
	    "isClearanceStock": false,
	    "isChildAuctionLot": false,
	    "rentalLotDetails": [],
	    "lotSoldOut": false,
	    "lotContents": [
	      {
	        "pId": "58c2532708b1918e667c853e",
	        "quantity": 1,
	        "price": 30000,
	        "emdDeposit": 5000,
	        "approvedLotId": "58c3b04b248e1526a93b6c6d",
	        "registeredByUser": "58c14475ef21b44e74ed817d",
	        "_id": "58c3b0f9248e1526a93b6c72"
	      },
	      {
	        "pId": "58c2531708b1918e667c853d",
	        "quantity": 1,
	        "price": 35000,
	        "emdDeposit": 5200,
	        "approvedLotId": "58c3b058248e1526a93b6c6e",
	        "registeredByUser": "58c14475ef21b44e74ed817d",
	        "_id": "58c3b0f9248e1526a93b6c71"
	      }
	    ],
	    "dateCreated": "2017-03-11T08:10:33.842Z"
	  }
	}


@apiError error Error message.
@apiErrorExample Error-Response:
    HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "Quantity too large for approvedLotId: 58c3b04b248e1526a93b6c6d"
	}
@apiErrorExample Error-Response:
    HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "Invalid pId sent. Send a valid approved lot id."
	}


 */
///CreateFinalLot has been verified on 11/3/17 2:17PM
exports.CreateFinalLot = function(req, res) {

	var user = res.req.user;
	var lotContents = req.body.lotContents;
	var startDate = moment(req.body.startDate, 'YYYY-MM-DD hh:mm:ss a', true);
	var endDate = moment(req.body.endDate, 'YYYY-MM-DD hh:mm:ss a', true);
	var lotType = req.body.lotType;
	var lotName = req.body.lotName;
	var bidIncreaseDetails = req.body.bidIncreaseDetails;
	
	if(!bidIncreaseDetails && lotType === "auction"){
		return res.status(200).send({status: "error", message: "bidIncreaseDetails not specified."});
	}

	if (user && lotContents && startDate && endDate && lotType) {
		if (startDate.isValid() && endDate.isValid()) {
			if (Array.isArray(lotContents)) {
				if (lotContents.length > 0) {
					var lot = new Lot();
					//Collect filtering info from products
					let brands = {};
					let productCategories = {};
					let screen_size = {};
					let screen_resolution = {};
					let processor = {};
					let display_technology = {};
					let ram = {};
					let hard_drive = {};
					let model_number = {};
					let operating_system = {};
					let color = {};
					let processor_count = {};
					let processor_brand = {};
					let releaseDate = {};
					let ratings = {};
					let workingcondition = {};
					let originalbox = {};
					let validbill = {};
					let condition = {};
					let specifications = {};

					//////////////////
					function asyncFor(i) {
						if (i < lotContents.length) {
							if ('pId' in lotContents[i] && 'quantity' in lotContents[i] && 'price' in lotContents[i] && 'emdDeposit' in lotContents[i]) {
								if (lotType != 'auction' || (lotType == 'auction' && 'auctionMinDeposit' in lotContents[i])) {
									if (lotType != 'rental' || (lotType == 'rental' && 'rentalSecurityDeposit' in lotContents[i] && 'rentalPricePerMonth' in lotContents[i])) {
										try {
											var approvedLotId = mongoose.Types.ObjectId(lotContents[i].pId);
										} catch (err) {
											return res.status(200).send({
												status: 'error',
												message: "Invalid pId sent. Send a valid approved lot id."
											});
										}
										ApprovedLot.findById(approvedLotId, function(err, approvedLot) {
											if (err) {
												return res.status(200).send({
													error: ErrorParser.parseErrorreturnErrorMessage(err)
												});
											} else {
												if (approvedLot) {
													if (lotContents[i].quantity <= approvedLot.quantity) {
														lotContents[i].pId = approvedLot.pId; //change approved lot Id to product Id
														lotContents[i]['approvedLotId'] = approvedLot._id;
														lotContents[i]['registeredByUser'] = approvedLot.registeredByUser;
														approvedLot.quantity = approvedLot.quantity - lotContents[i]['quantity'];
														approvedLot.participatingInFinalLot = true;
														if (approvedLot.linkedFinalLots) {
															approvedLot.linkedFinalLots.push(lot._id);
														} else {
															approvedLot.linkedFinalLots = [];
															approvedLot.linkedFinalLots.push(lot._id);
														}
														approvedLot.save(function(err, approvedLot) {
															if (err) {
																return res.status(200).send({
																	error: ErrorParser.parseErrorreturnErrorMessage(err)
																});
															} else {
																asyncFor(i + 1);
															}
														});
													} else {
														return res.status(200).send({
															status: 'error',
															message: "Quantity too large for approvedLotId: " + approvedLotId
														});
													}

												} else {
													return res.status(200).send({
														status: 'error',
														message: "Cannot find approvedLot for Id: " + approvedLotId
													});
												}
											}
										});
									} else {
										return res.status(200).send({
											status: 'error',
											message: "lotContents of type rental is incorrect data."
										});
									}
								} else {
									return res.status(200).send({
										status: 'error',
										message: "lotContents of type auction is incorrect data."
									});
								}
							} else {
								return res.status(200).send({
									status: 'error',
									message: "lotContents is incorrect data."
								});
							}
						} else {
							lot.createdByUser = user._id;
							lot.lotContents = lotContents;
							lot.originalLotContents = lotContents;
							lot.lotType = lotType;
							lot.lotName = lotName;
							
							lot.startDate = startDate;
							lot.endDate = endDate;
						
							if(req.body.lotTermsAndConditions) lot.lotTermsAndConditions = req.body.lotTermsAndConditions;
							lot.save(function(err, lot) {
								if (err) {
									return res.status(200).send({
										error: ErrorParser.parseErrorreturnErrorMessage(err)
									});
								} else {
									function asyncFor1(i) {
										if (i < lotContents.length) {
											Product.findById(lotContents[i].pId, function(err, product) {
												console.log("here");
												if (err) {
													console.log(lotContents[i].pId);
													console.log("ERROR");
													return res.status(200).send({
														error: err
													});
												} else if (product) {
													///////Collect filtering Information///////////////////////////

													if (product.ratings) ratings[product.ratings] = product.ratings;
													if (product.workingcondition) workingcondition[product.workingcondition] = product.workingcondition;
													if (product.originalbox) originalbox[product.originalbox] = product.originalbox;
													if (product.validbill) validbill[product.validbill] = product.validbill;
													if (product.condition) condition[product.condition] = product.condition;

													/////////////////////////////////////////////////////////////
													var tmpDict = aux.ConvertArrayToDict(product.finalLots);
													tmpDict[lot._id] = lot._id;
													product.finalLots = aux.ConvertDictToArray(tmpDict);

													if(lotType === "auction"){
														var tmpDictAuctions = aux.ConvertArrayToDict(product.auctionLot);
														tmpDictAuctions[lot._id] = lot._id;
														product.auctionLot = aux.ConvertDictToArray(tmpDictAuctions);
													}else if(lotType === "buy"){
														var tmpDictBuy = aux.ConvertArrayToDict(product.buyLots);
														tmpDictBuy[lot._id] = lot._id;
														product.buyLots = aux.ConvertDictToArray(tmpDictBuy);
													}else if(lotType === "clearance"){
														var tmpDictClearance = aux.ConvertArrayToDict(product.clearanceLot);
														tmpDictClearance[lot._id] = lot._id;
														product.clearanceLot = aux.ConvertDictToArray(tmpDictClearance);
													}else if(lotType === "rental"){
														var tmpDictRental = aux.ConvertArrayToDict(product.rentalLot);
														tmpDictRental[lot._id] = lot._id;
														product.rentalLot = aux.ConvertDictToArray(tmpDictRental);	
													}

													product.save(function(err, product) {
														if (err) {
															return res.status(200).send({
																error: ErrorParser.parseErrorreturnErrorMessage(err)
															});
														} else {
															BaseProduct.findById(product.baseProductId, function(err, baseProduct) {
																if (err) {
																	return res.status(200).send({
																		error: err
																	});
																} else {
																	if (baseProduct) {
																		///////Collect filtering Information///////////////////////////

																		if (baseProduct.brand_name) brands[baseProduct.brand_name] = baseProduct.brand_name;
																		if (baseProduct.productCategoryId) productCategories[baseProduct.productCategoryId] = baseProduct.productCategoryId;
																		if (baseProduct.screen_size) screen_size[baseProduct.screen_size] = baseProduct.screen_size;
																		if (baseProduct.screen_resolution) screen_resolution[baseProduct.screen_resolution] = baseProduct.screen_resolution;
																		if (baseProduct.processor) processor[baseProduct.processor] = baseProduct.processor;
																		if (baseProduct.display_technology) display_technology[baseProduct.display_technology] = baseProduct.display_technology;
																		if (baseProduct.ram) ram[baseProduct.ram] = baseProduct.ram;
																		if (baseProduct.hard_drive) hard_drive[baseProduct.hard_drive] = baseProduct.hard_drive;
																		if (baseProduct.model_number) model_number[baseProduct.model_number] = baseProduct.model_number;
																		if (baseProduct.operating_system) operating_system[baseProduct.operating_system] = baseProduct.operating_system;
																		if (baseProduct.color) color[baseProduct.color] = baseProduct.color;
																		if (baseProduct.processor_count) processor_count[baseProduct.processor_count] = baseProduct.processor_count;
																		if (baseProduct.processor_brand) processor_brand[baseProduct.processor_brand] = baseProduct.processor_brand;
																		if (baseProduct.releaseDate) releaseDate[baseProduct.releaseDate] = baseProduct.releaseDate;
																		if (baseProduct.specifications) specifications[product._id] = baseProduct.specifications;

																		/////////////////////////////////////////////////////////////
																		var tmpDict2 = aux.ConvertArrayToDict(baseProduct.finalLots);
																		tmpDict2[lot._id] = lot._id;
																		baseProduct.finalLots = aux.ConvertDictToArray(tmpDict2);
																		baseProduct.save(function(err, baseProduct) {
																			if (err) {
																				return res.status(200).send({
																					error: ErrorParser.parseErrorreturnErrorMessage(err)
																				});
																			} else {
																				for (let i = 0; i < lot.lotContents.length; i++) {
																					let productId = lot.lotContents[i].pId;
																					if (specifications[productId]) {
																						lot.lotContents[i].specifications = specifications[productId];
																						console.log(specifications[productId]);
																					}


																				}
																				lot.save(function(err) {
																					if (err) {
																						return res.status(200).send({
																							error: ErrorParser.parseErrorreturnErrorMessage(err)
																						});
																					} else {
																						asyncFor1(i + 1);
																					}
																				});
																			}
																		});
																	} else {
																		return res.status(200).send({
																			status: 'error',
																			message: "Cannot register lotId for corresponding product!"
																		});
																	}
																}
															});
														}
													});
												} else {
													return res.status(200).send({
														status: 'error',
														message: "Cannot register lotId for corresponding product!"
													});
												}
											});
										} else {

											lot.brands = aux.ConvertDictToArray(brands);
											lot.productCategories = aux.ConvertDictToArray(productCategories);
											lot.screen_size = aux.ConvertDictToArray(screen_size);
											lot.screen_resolution = aux.ConvertDictToArray(screen_resolution);
											lot.processor = aux.ConvertDictToArray(processor);
											lot.display_technology = aux.ConvertDictToArray(display_technology);
											lot.ram = aux.ConvertDictToArray(ram);
											lot.hard_drive = aux.ConvertDictToArray(hard_drive);
											lot.model_number = aux.ConvertDictToArray(model_number);
											lot.operating_system = aux.ConvertDictToArray(operating_system);
											lot.color = aux.ConvertDictToArray(color);
											lot.processor_count = aux.ConvertDictToArray(processor_count);
											lot.processor_brand = aux.ConvertDictToArray(processor_brand);
											lot.releaseDate = aux.ConvertDictToArray(releaseDate);

											lot.ratings = aux.ConvertDictToArray(ratings);
											lot.workingcondition = aux.ConvertDictToArray(workingcondition);
											lot.originalbox = aux.ConvertDictToArray(originalbox);
											lot.condition = aux.ConvertDictToArray(condition);
											lot.validbill = aux.ConvertDictToArray(validbill);
											if(lotType === "auction"){
												lot.overallBidIncreaseDetails = bidIncreaseDetails;
											}

											lot.save(function(err) {
												if (err) {
													return res.status(200).send({
														error: ErrorParser.parseErrorreturnErrorMessage(err)
													});
												} else {
													return res.status(200).send({
														status: 'success',
														message: 'Successfully created lot!',
														response: lot
													});
												}
											});
										}
									}
									asyncFor1(0);
								}
							});
						}
					}
					asyncFor(0);
				} else {
					return res.status(200).send({
						error: "lotContents cannot be empty."
					});
				}
			} else {
				return res.status(200).send({
					errorr: "Array of objects expected in lot contents."
				});
			}
		} else {
			return res.status(200).send({
				error: "Invalid date format, should be of type YYYY-MM-DD hh:mm:ss a"
			});
		}
	} else {
		return res.status(200).send({
			error: "Inadequate information."
		});
	}

	//var startDate = moment('2016-01-01 11:31:23 PM', 'YYYY-MM-DD hh:mm:ss a', true);
	//res.status(200).send({start: moment(startDate).format('hh:mm:ss a')});
	//startDate.isValid()
};



//----------------------------------------------------------------------------------------------------------------------



//--------------U P L O A D-----------------L O T--------------I M A G E--------------------------------------------------------------------

/**
@api {post} /lot/UploadLotImage/:lotId Upload Lot Image
@apiName UploadLotImage
@apiGroup Lot
@apiPermission admin
@apiDescription Upload an image which will be representative of the lot as a whole. Only one image can act as this. 

If this api route is used when previously another image was uploaded, the older one will be deleted and replaced by the newly uploaded image.

@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization": "JWT eyJhbGHVbz5y8i-HRRY"
	} 

@apiParam {File} baseProductImages Image file representing the product category
@apiParam {String} :baseProductId Unique Id representing a base product. Sent as a URL parameter
	{
		lotImage: xyz.jpg
	}

@apiSuccess {String} status Status of the API Call.
@apiSuccess {String} message Message describing the status
@apiSuccess {String} response Response of the API
@apiSuccess {String} response.lotImageURL the new image URL

@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "Inadequate information sent"
	}
*/



exports.UploadLotImage = function(req, res) {
	upload_lotImages(req, res, function(err) {
		if (err) {
			return res.status(200).send({
				status: 'error',
				message: ErrorParser.parseErrorreturnErrorMessage(err)
			});
		} else {
			var file = req.file;
			var lotId = req.params.lotId;
			if (file && lotId) {
				Lot.findById(lotId, function(err, lot) {
					if (err) {
						return res.status(200).send({
							status: 'error',
							message: ErrorParser.parseErrorreturnErrorMessage(err)
						});
					} else {
						if (lot) {
							if (lot.lotImage.path) {
								fs.unlink(lot.lotImage.path, function(err) {
									if (err) {
										lot.lotImage = file;
										lot.save(function(err, updatedLot) {
											if (err) {
												return res.status(200).send({
													status: 'error',
													message: ErrorParser.parseErrorreturnErrorMessage(err)
												});
											} else {
												return res.status(200).send({
													status: "success",
													message: "Successfully added image to lot. Previous image could not be found in database."
												});
											}
										});
									} else {
										lot.lotImage = file;
										lot.save(function(err, updatedLot) {
											if (err) {
												return res.status(200).send({
													status: 'error',
													message: ErrorParser.parseErrorreturnErrorMessage(err)
												});
											} else {
												return res.status(200).send({
													status: "success",
													message: "Successfully added image to lot. Previous image Successfully replaced and deleted."
												});
											}
										});
									}
								});
							} else {

								lot.lotImage = file;
								lot.save(function(err, updatedLot) {
									if (err) {
										return res.status(200).send({
											status: 'error',
											message: ErrorParser.parseErrorreturnErrorMessage(err)
										});
									} else {
										return res.status(200).send({
											status: "success",
											message: "Successfully added image to lot"
										});
									}
								});
							}
						} else {
							return res.status(200).send({
								status: 'error',
								message: "Lot not found with lotId: " + lotId
							});
						}
					}
				});
			} else {
				return res.status(200).send({
					status: 'error',
					message: "Inadequate information sent"
				});
			}
		}
	});
};


//-------------------------------------------------------------------------------------------------------------------------------------



//-------------------E D I T----------F I N A L--------L O T--------------------------------------------------------------------------------

exports.EditFinalLot = function(req, res) {

};

//-------------------------------------------------------------------------------------------------------------------------------------



//------------------------D E L E T E----------F I N A L-------L O T-----------------------------------------------------------------------------

exports.DeleteFinalLot = function(req, res) {

};

//-------------------------------------------------------------------------------------------------------------------------------------



//------------------------DIRECT FINAL LOT CREATION FROM PRODUCT--------------------------------------------------------------------------------

exports.DirectFinalLotCreation = function(req, res) {
	var user = res.req.user;
	var lotContents = req.body.lotContents;
	var startDate = moment(req.body.startDate, 'YYYY-MM-DD hh:mm:ss a', true);
	var endDate = moment(req.body.endDate, 'YYYY-MM-DD hh:mm:ss a', true);
	var lotType = req.body.lotType;
	var lotName = req.body.lotName;

	if (user) {
		if (lotType) {
			if (startDate.isValid() && endDate.isValid()) {
				if (lotContents) {
					if (Array.isArray(lotContents)) {
						if (lotContents.length > 0) {
							var i = 0;

							function asyncFor(i) {
								if (i < lotContents.length) {
									if ('pId' in lotContents[i] && 'quantity' in lotContents[i] && 'price' in lotContents[i] && 'emdDeposit' in lotContents[i]) {
										if (lotType != 'auction' || (lotType == 'auction' && 'auctionMinDeposit' in lotContents[i] && 'auctionPercentageDepositIncrease' in lotContents[i])) {
											if (lotType != 'rental' || (lotType == 'rental' && 'rentalPrice' in lotContents[i])) {

												try {
													var pId = mongoose.Types.ObjectId(lotContents[i].pId);
												} catch (err) {
													return res.status(200).send({
														status: 'error',
														message: "Invalid pId sent. Send a valid approved lot id."
													});
												}
												Product.findById(pId, function(err, product) {
													if (err) {
														return res.status(200).send({
															error: ErrorParser.parseErrorreturnErrorMessage(err)
														});
													} else {
														if (product) {
															var baseProductId = product.baseProductId;
															BaseProduct.findById(baseProductId, function(err, baseProduct) {
																if (err) {
																	return res.status(200).send({
																		error: ErrorParser.parseErrorreturnErrorMessage(err)
																	});
																} else {
																	if (baseProduct) {
																		lotContents[i].specifications = baseProduct.specifications;
																		i += 1;
																		asyncFor(i);
																	} else {
																		return res.status(200).send({
																			status: 'error',
																			message: "Invalid baseProduct for product."
																		});
																	}
																}
															});
														} else {
															return res.status(200).send({
																status: 'error',
																message: "Invalid pId."
															});
														}
													}
												});

											} else {
												return res.status(200).send({
													status: 'error',
													message: "lotContents of type rental is incorrect data."
												});
											}
										} else {
											return res.status(200).send({
												status: 'error',
												message: "lotContents of type auction is incorrect data."
											});
										}
									} else {
										return res.status(200).send({
											status: 'error',
											message: "lotContents has incorrect data."
										});
									}
								} else {
									var lot = new Lot();
									lot.createdByUser = user._id;
									lot.lotContents = lotContents;
									lot.lotType = lotType;
									lot.lotName = lotName;
									lot.startDate = startDate;
									lot.endDate = endDate;
									lot.save(function(err, lot) {
										if (err) {
											return res.status(200).send({
												error: ErrorParser.parseErrorreturnErrorMessage(err)
											});
										} else {
											return res.status(200).send({
												status: "success",
												message: "Lot saved successfully."
											});
										}
									});

								}
								asyncFor(i);
							}
						} else {
							return res.status(200).send({
								status: "error",
								message: "lotContents cannot be empty."
							});
						}
					} else {
						return res.status(200).send({
							status: "error",
							message: "Invalid lot contents."
						});
					}
				} else {
					return res.status(200).send({
						status: "error",
						message: "lotContents not defined."
					});
				}
			} else {
				return res.status(200).send({
					error: "Invalid date format, should be of type YYYY-MM-DD hh:mm:ss a"
				});
			}
		} else {
			return res.status(200).send({
				status: "error",
				message: "lotType not defined."
			});
		}
	} else {
		return res.status(200).send({
			status: "error",
			message: "User not authenticated."
		});
	}



	//var startDate = moment('2016-01-01 11:31:23 PM', 'YYYY-MM-DD hh:mm:ss a', true);
	//res.status(200).send({start: moment(startDate).format('hh:mm:ss a')});
	//startDate.isValid()
};

//-----------------------------------------------------------------------------------------------------------------------------------------


var createFinalLotsFromProducts = function(index, user, arrayOfProductsInfo, cb){
	if(arrayOfProductsInfo && arrayOfProductsInfo.length > 0){
		if(index < arrayOfProductsInfo.length){
			var localProduct = arrayOfProductsInfo[index];
			var localLotCreate = {};
			localLotCreate.createdByUser = mongoose.Types.ObjectId(user._id);
			localLotCreate.lotType = 'buy';
			localLotCreate.isLinkedToAuction = false;
			localLotCreate.lotName = localProduct.productName;
			localLotCreate.lotContents = [];
			localLotCreate.startDate = moment().format("YYYY-MM-DD hh:mm:ss a");
			localLotCreate.endDate = moment().add('years', 10).format("YYYY-MM-DD hh:mm:ss a");
			var excelAnswers = localProduct.completeExcelAnswers;
			var specs = [];
			if(localProduct.specifications){
				for(var i=0; i<localProduct.specifications.length; i++){
					var localSpec = localProduct.specifications[i];
					if(excelAnswers[localProduct.specifications[i].displayName]){
						localSpec.specificationValue = excelAnswers[localProduct.specifications[i].displayName];
						specs.push(localSpec);
					}else{
						specs.push(localSpec);
					}
				}
			}
			localLotCreate.lotContents.push({'pId': mongoose.Types.ObjectId(localProduct.newlyCreatedProductId),
												'quantity': localProduct.noOfPieces,
												'price': localProduct.planetNamoCost,
												'emdDeposit': parseInt(localProduct.planetNamoCost*0.05),
												'registeredByUser': mongoose.Types.ObjectId(user._id),
												'specifications': specs});
			Lot.create(localLotCreate, function(err, newLotCreated){
				if(err){
					cb(err, null);
				}else{
					arrayOfProductsInfo[index].createdLotId = newLotCreated._id;
					createFinalLotsFromProducts(index + 1, user, arrayOfProductsInfo, cb);
				}
			});
		}else{
			cb(null, arrayOfProductsInfo);
		}
	}else{
		cb(null, []);
	}
}



exports.CreateFinalBuyingLotFromAdminPanel = function(req, res){
	var arrayOfBaseProductsInfo = req.body.baseProductsInfo;
	var categoryId = req.body.categoryId;
	var user = res.req.user;
	if(arrayOfBaseProductsInfo && arrayOfBaseProductsInfo.length > 0 && categoryId){
		var objOfBaseProductCheck = {};
		for(var i=0; i<arrayOfBaseProductsInfo.length; i++){
			objOfBaseProductCheck[arrayOfBaseProductsInfo[i].productName.replace(/\s/g, '')] = arrayOfBaseProductsInfo[i];
		}
		baseProductCtrl.getBaseProductsIdsIfNotCreateAndReturn(categoryId, objOfBaseProductCheck, function(responseObj1){
			if(responseObj1.data){
				for(var i=0; i<arrayOfBaseProductsInfo.length; i++){
					arrayOfBaseProductsInfo[i].baseProductId = responseObj1.data[arrayOfBaseProductsInfo[i].productName.replace(/\s/g, '')].baseProductId;
				}
				productCtrl.createNewProductsFromAdminPanel(user, arrayOfBaseProductsInfo, function(productsArrayRes){
					for(var i=0; i<arrayOfBaseProductsInfo.length; i++){
						arrayOfBaseProductsInfo[i].newlyCreatedProductId = productsArrayRes[i+"_"];
					}
					createFinalLotsFromProducts(0, user, arrayOfBaseProductsInfo, function(error, finalLots){
						if(error){
							res.status(200).send(error);
						}else{
							res.status(200).send({status: 'success', lotsFinally: finalLots});
						}
					});
				});
			}else{
				res.status(200).send(responseObj1);
			}
		});
	}else if(categoryId){	
		res.status(200).send({status: 'error', message: 'No Base Products Found'});
	}else{
		res.status(200).send({status: 'error', message: 'No Category Id Found'});
	}
}