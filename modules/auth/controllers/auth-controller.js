/**
 * Module dependencies.
 */
var request 	= require('request');
var mongoose    = require('mongoose');
var validator   = require('validator');
var jwt         = require('jsonwebtoken');
var passport    = require("passport");
var passportJWT = require("passport-jwt");
var ExtractJwt  = passportJWT.ExtractJwt;
var JwtStrategy = passportJWT.Strategy;
var mime 		= require('mime');
var crypto 		= require('crypto');

var User        	 = require(__base + '/models/user/user.js')
var JWTKey      	 = require(__base + '/config/key.js');
var usersUtils  	 = require(__utils + '/users.js');
var EmailHelper 	 = require(__utils + '/MailSender.js');
var ErrorParser 	 = require(__utils + '/errorParse.js');
var FileHandler      = require(__utils + '/FileHandler.js');
var aux         	 = require(__utils + '/auxilaryFunc.js');
var image_downloader = require('image-downloader');
var jwtOptions = {}
jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeader();
jwtOptions.secretOrKey = JWTKey.jwtKEY;


 /**
 * API functions
 */

exports.planetnamowelcome = function (req, res) {
	res.jsonp({status: 'success', data: "Welcome to the PlanetNamo API, auth module"});
};


/**
@api {post} /auth/signup Signup User
@apiName Signup
@apiGroup SignupLogin
@apiDescription Sign up / register a new planetnamo user. 

After a signing up a new user is registered on the server and an email verification link is sent to the registered email. Users can only log into planetnamo after clicking on that verification link.

@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json"
	} 

@apiParam {String} username 				Username of the user. Must be unique.
@apiParam {String} password 				Password of the user.
@apiParam {String} email 					User Email Id. Must be unique. Must be a valid email Id
@apiParam {String} [panCard] 				panCard of user.
@apiParam {String} [serviceTaxNo] 			serviceTaxNo of user.
@apiParam {String} [vatNo] 					vatNo of user.
@apiParam {String}  mobileNumber 			Valid mobile Number of User. Must be unique.
@apiParam {Number} buySellPermission 		Buyer/Seller code, 1 - buy only 2 - sell only 3 - both buy and sell
@apiParam {Number} accountType 				1 - induvidual 2 - bussiness
@apiParamExample {json} Request-Example:
{
	"username": "karunkalaivanan",
	"password": "sylvester2017",
	"email": "karunkalaivanan5@gmail.com",
	"panCard": "110022323",
	"mobileNumber": "9013473603",
	"buySellPermission" : 3,
	"accountType": 1,
	"name": "Karun"
}

@apiSuccess {String} status Status of the API Call.
@apiSuccess {JSON} data Verification email remarks
@apiSuccessExample Success-Response:
     HTTP/1.1 200 OK
	{
	  "status": "success",
	  "message": "Verification mail have been sent to your email. Please verify before login."
	}

@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "User already registered with email: karunk@live.com"
	}
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "User already registered with username: karunk"
	}
*/
exports.signup = function (req, res) {

	var username 		= aux.safeToString(req.body.username);
	var password 		= aux.safeToString(req.body.password);
	var email 	 		= aux.safeToString(req.body.email);
	var name     		= aux.safeToString(req.body.name);
	var panCard  		= aux.safeToString(req.body.panCard);
	var mobileNumber 	= aux.safeToString(req.body.mobileNumber);
	var vatNo 			= aux.safeToString(req.body.vatNo);
	var serviceTaxNo 	= aux.safeToString(req.body.serviceTaxNo);

	var buySellPermission 	= req.body.buySellPermission;  // 1 - buy only 2 - sell only 3 - both buy and sell
	var accountType 		= req.body.accountType; // 1 - induvidual 2 - bussiness

	if(!(buySellPermission == 1 || buySellPermission == 2 || buySellPermission == 3)){
		return res.status(200).send({error: "buySellPermission invalid"});
	}
	if(!(accountType == 1 || accountType == 2)){
		return res.status(200).send({error: "accountType invalid"});
	}

	if(username && password && email && name && mobileNumber){
		if(validator.isMobilePhone(mobileNumber,'en-IN')){
			if(validator.isEmail(email)){
				User.findOne({username: username})
					.exec(function(err, foundUsernameUser){
						if(err){
							return res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)}); 
						}else{
							if(foundUsernameUser){
								return res.status(200).send({status: 'error', message: "User already registered with username: "+username});
							}else{
								User.findOne({email: email})
									.exec(function(err, foundEmailUser){
										if(err){
											return res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});
										}else{
											if(foundEmailUser){
												return res.status(200).send({status: 'error' , message: "User already registered with email: "+email});
											}else{
												User.findOne({mobileNumber: mobileNumber})
													.exec(function(err, foundMobileUser){
														if(err){
															return res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});
														}else{
															if(foundMobileUser){
																return res.status(200).send({status: 'error', message: "User already registered with mobileNumber: "+mobileNumber})
															}else{
																var user = new User();
																user.username = username;
																user.password = password;
																user.email    = email;
																user.name     = name;

																//optional
																if(accountType == 1){
																	if(panCard) user.panCard    					= panCard;
																	if(vatNo) user.vatNo    						= vatNo;
																	if(serviceTaxNo) user.serviceTaxNo      		= serviceTaxNo;
																}

																if(mobileNumber) user.mobileNumber 				= mobileNumber;
																if(buySellPermission) user.buySellPermission    = buySellPermission; 
																if(accountType) user.accountType 				= accountType;

																//derived
																user.emailVerificationToken = usersUtils.createEmailverificationCode();
																user.mobileVerificationOtp = usersUtils.createMobileOtp();
																
																user.save(function(err){
																	if(err){
																		return res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)}); 
																	}
																	else{
															            var emailVerifyUrl = "http://"+aux.constants.host+"/api/auth/signup/verifyEmail/"+user._id+"/"+user.emailVerificationToken+"?email="+user.email;
															            EmailHelper.sendHtmlMail(EmailHelper.getEmailStringForEmailConfirm(user.name, emailVerifyUrl), user.email, "Please confirm your e-mail - PlanetNamo.", function(err, response){
															              if(err){
															              	console.log(err);
															                res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)}); 
															              }else{
															                res.jsonp({status: 'success', message: "Verification mail have been sent to your email. Please verify before login."})
															              }
															            });
																	}
																});
															}
														}
													});
											}
										}
									});
							}
						}
					});
			}else{
				return res.status(200).send({status: 'error', message: email+" is not a valid email address."});
			}
		}else{
			return res.status(200).send({status: 'error', message: mobileNumber+" is not a valid mobile number."});
		}
	}else {
		return res.status(200).send({status: 'error', message: 'Incomplete information'});
	} 
};

var createFbUserIfNotExists = function(userData, callback){
	console.log('in createFbUserIfNotExists')
	var userEmail = userData.email;
	if(userEmail){
		request('https://graph.facebook.com/me?access_token='+userData.facebookAccessToken, function (error, response, bodyString) {
			if (!error && response.statusCode == 200) {
				console.log('req verified');
	    		var body = JSON.parse(bodyString);
	    		if(body.id == userData.facebookUserId){
	    			console.log('body contains fb data');
	    			User.findOne({email: userEmail}, function(err, user){
						if(err){
		        			callback(ErrorParser.parseErrorreturnErrorMessage(err));
		      			}else if(user){
		      				console.log('user found');
		        			User.update({email: userEmail}, {facebookUserId: userData.facebookUserId}, function(err, result){
			          			if(err){
			            			callback(ErrorParser.parseErrorreturnErrorMessage(err));
			          			}else{
			            			callback(null, user);
			          			}
		        			});
		      			}else{
					        var userInfo = {};

					        userInfo['email']= userData.email;
					        userInfo['facebookUserId'] = userData.facebookUserId;
					        userInfo['name'] = userData.name;
		        			userInfo['emailVerified'] = true;
		        			userInfo['password'] = usersUtils.createEmailverificationCode();

				     		crypto.pseudoRandomBytes(16, function (err, raw) {
				     			if(err){
				     				callback(ErrorParser.parseErrorreturnErrorMessage(err));
				     			}
				     			var EncryptedName = raw.toString('hex') + Date.now() + '.jpg';
								var imageDownloadOptions = {
	    							url: userData.facebookImageUrl,
	    							dest: __base + '/uploads/profilePictures/'+EncryptedName,
		    						done: function(err, filename, image){
		    							if(err){
		    								callback(ErrorParser.parseErrorreturnErrorMessage(err));
		    							}else{
		    								console.log(__base + '/uploads/profilePictures/'+EncryptedName);
		    								userInfo['profilePicture'] = {};
		    								userInfo['profilePicture']['path'] = __base + '/uploads/profilePictures/'+EncryptedName;
						        			userInfo['profilePicture']['filename'] = EncryptedName;
						        			console.log(image);
											User.create(userInfo, function(err, user){
												if(err){
													console.log('in here 2');
													callback(ErrorParser.parseErrorreturnErrorMessage(err));
												}else{
													callback(null, user);
												}
											});
		    							}
		    						}
	    						}
	    						image_downloader(imageDownloadOptions)
							});
						}
	    			});
	    		}else{
	      			callback("user ids doesn't match - "+body);
	    		}
  			}else{
	    		callback('Invalid Facebook access token.');
	  		}
		});
	}else{
		callback('User Email is compulsory.');
	}
}

/**
@api {post} /auth/login Login
@apiName Login 
@apiGroup SignupLogin
@apiDescription Login/Authenticate the api. Authentication is based on Json Web Tokens (JWT). So after authorization/authentication you will recieve a token.

The token must be sent in every request where authentication is required. One can login via email & password or Facebook. To use facebook login, make sure 'loginType' is set to 'FB'. Make sure to send all the information reveived from FB in the req.body not just the FB access tokens.

Token must be sent in the header of every authenticated request with the name "Authorization and must be prepended with 'JWT '"

A Token lasts 7 days.

@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json"
	} 

@apiParam {String} email 										Registered email of the user. Must be a valid email address.
@apiParam {String} password 									Password of the user
@apiParam {String} loginType [loginType] 						Set it to 'FB' to use facebook login. If facebook login is used, username & password is not required.
@apiParam {String} facebookUserId [facebookUserId]  			Required when using FB login.
@apiParam {String} facebookAccessToken [facebookAccessToken] 	Required when using FB login.
@apiParamExample {json} Request-Example:
{
	"email": "karunk@live.com",
	"password": "sylvester2017"
}

@apiSuccess {String} status Status of the API Call.
@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
	{
	  "status": "success",
	  "userId": "58c14475ef21b44e74ed817d",
	  "name": "Karun",
	  "email": "karunk@live.com",
	  "mobileVerified": false,
	  "role": 2,
	  "buySellPermission": 3,
	  "accountType": 1,
	  "lastLoginTime": "2017-03-10T08:14:34.381Z",
	  "authToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjU4YzE0NDc1ZWYyMWI0NGU3NGVkODE3ZCIsImlhdCI6MTQ4OTEzMzY3NCwiZXhwIjoxNDg5NzM4NDc0fQ.WwmajPR9WyiHaKFpzmfj5bepcYg9o2Kpf6hEtvRXb98"
	}

@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "Verify your email before logging in."
	}
*/
exports.login = function(req, res) {
  
	var loginType = "LOGIN";
	if(req.body.loginType){
		loginType = req.body.loginType;
	}	
	if(loginType == "FB"){
		if(req.body.facebookUserId && req.body.facebookAccessToken){
			createFbUserIfNotExists(req.body, function(errorString, user){
				if(errorString){
		    		return res.status(200).send({status: "error", message: errorString});
				}
				else{
					var response = {};
					var payload = {id: user._id};
					var token = jwt.sign(payload, jwtOptions.secretOrKey);
					
					response['userId'] = user._id;
					response['name'] = user.name;
					response['email'] = user.email;
					response['mobile'] = user.mobileNumber;
					response['accountType'] = user.accountType;
					response['authToken'] = token;

					return res.status(200).send({status: 'success', response: response});
		  		}
			});
		}
		else{
			return res.status(200).send({status: 'error', message: "Facebook User Id / Access Token Missing"});
		}
	}
	else{
		let email = aux.safeToString(req.body.email);
		let password = aux.safeToString(req.body.password);

		if(email && password){
			//Email needs to be verified before logging in
			if(validator.isEmail(email)){
				User.findOne({
					email: req.body.email
				}).select('username password _id emailVerified name email mobile mobileVerified role buySellPermission accountType lastLoginTime').exec(function(err,user){

					if(err) throw err;
					if(!user){
						res.status(200).send({
							status: 'error',
							message: "Authentication failed! No such user found."
						});
					}
					else if(user){
						var validPassword = user.comparePassword(req.body.password);
						if(!validPassword) {
							return res.status(200).send({
								status: 'error',
								message: "Authentication failed! Password did not match."
							});
						}
						else if(user.emailVerified == false){
							return res.status(200).send({
								status: 'error',
								message: "Verify your email before logging in."
							});
						}
						else {
							var response = {};
							var payload = {id: user._id};
							var token = jwt.sign(payload, jwtOptions.secretOrKey,  { expiresIn: 60*60*24*7 });
							response['userId'] = user._id;
							response['name'] = user.name;
							response['email'] = user.email;
							response['mobile'] = user.mobileNumber;
							response['mobileVerified'] = user.mobileVerified;
							response['role'] = user.role;
							response['buySellPermission'] = user.buySellPermission;
							response['accountType'] = user.accountType;
							response['lastLoginTime'] = user.lastLoginTime;
							response['authToken'] = token;
							user.lastLoginTime = new Date();
							user.save(function(err, user){
								if(err){
									return res.status(200).send({error: err});
								}else{
									return res.status(200).send({status: 'success', response: response});
								}
							});
						}
					}
				});
			}else{
				return res.status(200).send({status: 'error', message: "Invalid email"});
			}
		}else{
			return res.status(200).send({status: 'error', message: "Login credentials missing."});
		}
	}		
};

/**
@api {get} /auth/signup/verifyEmail/:id/:emailToken Verify Email 
@apiName Verify Email 
@apiGroup SignupLogin
@apiDescription API route to verify email. This route would be recieved by the registered users in their emails after they sign up. On hitting the routes the respective emails will be verified.

Only users who have their emails verified can be authenticated when they log in via the username/password route.

This will be direct link the user's emails.

@apiParam {String} :_id Unique Id of user. This is a URL PARAMETER.
@apiParam {String} :emailVerificationToken Unique email verification token. This is a URL PARAMETER.
@apiParam {String} email Email of the user. URL QUERY PARAMETER
@apiParamExample {url} Request-Example:
http://localhost:8080/api/auth/signup/verifyEmail/58a70a784ded7c86e5e829e6/3fd15a9f862510182959da8928fd4b0bea1e7501?email=vkarun_k@yahoo.in

@apiSuccess {String} status Status of the API Call.
@apiSuccess {JSON} result Result will an object containing category information
@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
	{
	  "status": "success",
	  "result": "Email Verified Successfully"
	}

@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
	  "status": "success",
	  "message": "Invalid Email Token"
	}
*/
exports.verifyEmail = function(req, res) {
  var queryParams = {
  	'emailVerificationToken': req.params.emailToken,
  	'_id': req.params.id,
  	'email': req.query.email
  }
  User.findOne(queryParams, function (err, user) {
	  if (err) {
	    return res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});	
	  }else{
        if(user != null){
        	var updateInfo = {};
        	updateInfo['emailVerificationToken'] = "";
        	updateInfo['emailVerified'] = true;
        	User.update(queryParams, updateInfo, function( err, result ){
        		if (err) {
    				    return res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});	
    				}else{
    					return res.jsonp({status: "success", "message": "Email Verified Successfully", response: result});
    				}
        	});
        }else{
        	return res.status(200).jsonp({status: 'success', message: "Invalid Email Token"});
        }
	  }
	});
};

/**
@api {put} /auth/signup/verifyMobile Verify Mobile 
@apiName Verify Mobile 
@apiGroup SignupLogin
@apiDescription API route to verify mobile via OTP. Hit this route to verify mobile with OTP.

@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json"
	} 

@apiParam {Number} mobileNumber Unique mobile number of registered user to verify
@apiParam {Number} mobileVerificationOtp Unique OTP
@apiParam {String} _id Unique Id of user. 
@apiParamExample {url} Request-Example:
{
	'mobileNumber': 9013245674,
	'mobileVerificationOtp': 3244,
	'_id': '58a70a784ded7c86e5e829e6'
}
@apiSuccess {String} status Status of the API Call.
@apiSuccess {JSON} result Result will an object containing category information
@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
	{
	  "status": "success",
	  "result": "Mobile Verified Successfully"
	}

@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
	  "error": "Invalid OTP"
	}
*/
exports.verifyMobile = function(req, res) {
  var queryParams = {
  	'mobileNumber': req.body.mobileNumber,
  	'mobileVerificationOtp': req.body.otp
  }
  User.findOne(queryParams, function (err, user) {
	  if (err) {
	    res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});	
	  }else{
        if(user != null){
        	var updateInfo = {};
        	updateInfo['mobileVerificationOtp'] = "";
        	updateInfo['mobileVerified'] = true;
        	User.update(queryParams, updateInfo, function( err, result ){
        		if (err) {
    				    res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});	
    				}else{
    					res.jsonp({status: "success", "result": "Mobile Verified Successfully", response: result});
    				}
        	});
        }else{
        	res.status(200).jsonp({status: 'success', message: "Invalid OTP"});
        }
	  }
	});
};




/**
@api {post} /auth/forgotPassword/:email Forgot Password
@apiName ForgotPassword
@apiGroup SignupLogin

@apiDescription Forgot password. Reset your password via your email. A reset link will be sent to the registered email.

Link will only be sent when the email sent in the request matches with the registered email.

@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json"
	} 

@apiParam {String} :email Unique email address of user. This is a URL PARAMETER

@apiParamExample {url} Request-Example:
http://localhost:8080/api/auth/forgotPassword/vkarun_k@yahoo.in

@apiSuccess {String} status Status of the API Call.
@apiSuccess {JSON} result Result will an object containing category information
@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
	{
	  "status": "success",
	  "message": "Instructions to retrieve / update password have been sent to specified email."
	}

@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
	  error: "No User Found with that email"
	}
*/
exports.forgotPassword = function(req, res) {
  var emailId = req.params.email;
  if(emailId){
    User.findOne({email: emailId}, function(err, user){
      if(err){
        res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});
      }else if(user){
        var updateToken = usersUtils.createAuthToken();
        user.update({passwordUpdateToken: updateToken}, function(err, result){
          if(err){
            res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});
          }else{
            var link = 'http://'+aux.constants.host+'/api/forgotPassword/reset/'+user.email+'/'+updateToken;
            EmailHelper.sendHtmlMail(EmailHelper.getEmailStringForForgotPassword(user.name, link), user.email, 'Reset password - PlanetNamo', function(err, result){
              if(err){
                res.status(200).send({error: err});
              }else{
                res.status(200).send({status: 'success', message: 'Instructions to retrieve / update password have been sent to specified email.'});
              }
            });
          }
        });
      }else{
        res.status(200).send({status: 'error', message: "No User Found with that email"});
      }
    });
  }else{
    res.status(200).send({status: 'error', message: "No Email Id Provided"});
  }
};

/**
@api {post} /auth/forgotPassword/reset/:email/:updateToken Reset Password
@apiName Reset Password
@apiGroup SignupLogin
@apiDescription Reset your password. This link would be received in the registered email after forgot password route was hit


@apiParam {String} :email Unique email address of user. This is a URL PARAMETER
@apiParam {String} :updateToken Unique password reset token of user. This is a URL PARAMETER

@apiParamExample {url} Request-Example:
http://localhost:8080/api/auth/forgotPassword/reset/vkarun_k@yahoo.in/:updateToken

@apiSuccess {String} status Status of the API Call.
@apiSuccess {JSON} result Result will an object containing category information
@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
	{
	  "status": "success",
	  "message": "Password updated!"
	}

@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
	  error: "Invalid password reset link."
	}
*/
exports.resetPassword = function(req, res) {

  if(!req.body.password){
  	return res.status(200).send({status: 'error', message: "No Password Provided"});
  }

  var queryParams = {
  	'passwordUpdateToken': req.params.updateToken,
  	'email': req.params.email
  }
  User.findOne(queryParams, function (err, user) {
	  if (err) {
	    res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});	
	  }else{
        if(user != null){
			user.passwordUpdateToken = '';
			user.password = req.body.password;
	        user.save(function(err) {
	            if (err) return res.send(err);
	            res.status(200).send({ status: 'success', message: 'Password updated!' });
	        });
        }else{
        	res.status(200).jsonp({status: 'error', message: "Invalid password reset link."});
        }
	  }
	});
};


/**
@api {get} /auth/username_availability/:username Username Availability
@apiName Forgot Password
@apiGroup SignupLogin
@apiDescription Check if username is already registered by some user.

@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json"
	} 

@apiParam {String} :username Unique username. This is a URL PARAMETER

@apiParamExample {url} Request-Example:
http://localhost:8080/api/auth/username_availability/test_user

@apiSuccess {String} status Status of the API Call.
@apiSuccess {Boolean} availability True is username is available and has no such username has been previously registered.
@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
	{
	  "status": "success",
	  "availability": false
	}

@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
	  error: "some error"
	}
*/
exports.CheckifUsernameAvail = function(req, res) {
	var username = req.params.username;
	if(username){
		User.findOne({username: username}, function (err, user) {
		  if (err) {
		    res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});	
		  }else{
		    if(user != null){
		    	res.status(200).jsonp({status: "success", availability: false, remark: "Cannot use "+username });
		    }else{
		    	res.status(200).jsonp({status: "success", availability: true, remark: "Can use "+username });
		    }
		  }
		});
	}else{
		return res.status(200).send({status: 'error', message: "Username not sent"});
	}
};

/**
@api {get} /auth/email_availability/:email Email Availability
@apiName Email Availability
@apiGroup SignupLogin
@apiDescription Check if email is already registered by some user.

@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json"
	} 

@apiParam {String} :email Unique email address of user. This is a URL PARAMETER

@apiParamExample {url} Request-Example:
http://localhost:8080/api/auth/forgotPassword/vkarun_k@yahoo.in

@apiSuccess {String} status Status of the API Call.
@apiSuccess {Boolean} availability True is username is available and has no such username has been previously registered.
@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
	{
	  "status": "success",
	  "availability": false
	}

@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
	  error: "some error"
	}
*/
exports.CheckifEmailAvail = function(req, res) {

	var email = req.params.email;
	if(email){
		if(validator.isEmail(email)){
		  	User.findOne({email: email}, function (err, user) {
			  if (err) {
			    res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});	
			  }else{
		        if(user != null){
		        	res.status(200).jsonp({status: "success", availability: false, remark: "Cannot use "+email });
		        }else{
		        	res.status(200).jsonp({status: "success", availability: true, remark: "Can use "+email});
		        }
			  }
			});
		}else{
			return res.status(200).send({status: 'error', message: email+" is not a valid email"});
		}
	}else{
		return res.status(200).send({status: 'error', message: "Email not sent in request."});
	}
};














