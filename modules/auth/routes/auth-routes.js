var authCtrl = require(__modules + '/auth/controllers/auth-controller');




module.exports = function(app, express){

	var apiRouter = express.Router();

	apiRouter.route('/welcome').get(authCtrl.planetnamowelcome);
	apiRouter.route('/signup').post(authCtrl.signup);
	apiRouter.route('/login').post(authCtrl.login);
	apiRouter.route('/signup/verifyEmail/:id/:emailToken').get(authCtrl.verifyEmail); //MOVE THIS OUTSIDE CLIENTKEY PROTECTION
	apiRouter.route('/signup/verifyMobile').put(authCtrl.verifyMobile); 
	apiRouter.route('/forgotPassword/:email').get(authCtrl.forgotPassword);
	apiRouter.route('/forgotPassword/reset/:email/:updateToken').post(authCtrl.resetPassword);
	apiRouter.route('/username_availability/:username').get(authCtrl.CheckifUsernameAvail);
	apiRouter.route('/email_availability/:email').get(authCtrl.CheckifEmailAvail);

	return apiRouter;

};