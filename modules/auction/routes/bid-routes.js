var bidCtrl = require('../controllers/bid-controller');
var passport 	= require("passport");
var Middleware  = require(__base + '/middlewares/middleware');


module.exports = function(app, express){

	var apiRouter = express.Router();

	apiRouter.route('/welcome').get(bidCtrl.planetnamowelcome);

	apiRouter.route('/PlaceBid').post(passport.authenticate('jwt', { session: false }),
											bidCtrl.PlaceBid);

	apiRouter.route('/CancelBid').post(passport.authenticate('jwt', { session: false }),
											bidCtrl.CancelBid);

	apiRouter.route('/PayRemainingDeposit').post(passport.authenticate('jwt', { session: false }),
											bidCtrl.PayRemainingDeposit);
	
	return apiRouter;

};