var auctionCtrl = require('../controllers/auction-controller');
var passport 	= require("passport");
var Middleware  = require(__base + '/middlewares/middleware');


module.exports = function(app, express){

	var apiRouter = express.Router();

	apiRouter.route('/welcome').get(auctionCtrl.planetnamowelcome);

	apiRouter.route('/CreateAuction').post(passport.authenticate('jwt', { session: false }),
											Middleware.CheckIfAdmin,
											auctionCtrl.CreateAuction);

	apiRouter.route('/PayEMDOnAuction').post(passport.authenticate('jwt', { session: false }),
											auctionCtrl.PayEMDForBid);

	apiRouter.route('/UserAuctions').post(passport.authenticate('jwt', { session: false }),
											auctionCtrl.UserAuctions);



	return apiRouter;

};