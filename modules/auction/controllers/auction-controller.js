/**
 * Module dependencies.
 */
var mongoose 	= require('mongoose');

var aux 		= require(__utils + '/auxilaryFunc');
var ErrorParser = require(__utils + '/errorParse');
var UserUtils  = require(__utils + '/users')
var BaseProduct = require(__base + '/models/products/base-product.js');
var User 			= require(__base + '/models/user/user');
var RegisteredLot 	= require(__base + '/models/lots/registered-lot');
var ApprovedLot 	= require(__base + '/models/lots/approved-lot');
var Lot 	        = require(__base + '/models/lots/lot');
var SellingOrder 	= require(__base + '/models/orders/selling-req'); 
var BuyingOrder     = require(__base + '/models/orders/buying-req');
var Product 		= require(__base + '/models/products/product');
var Wallet				= require(__base + '/models/wallet/wallet');
var WalletTransaction	= require(__base + '/models/wallet/wallet-transactions');
var WalletWithDrawl		= require(__base + '/models/wallet/wallet-withdrawl-requests');
var Auction 		= require(__base + '/models/auctions/auction');
var Bid 			= require(__base + '/models/bids/bid');
var AuctionReq  = require(__base + '/models/orders/auction-req.js');
var moment = require('moment');
var io = require(__base+ '/server.js').io;
var config = require(__base+ '/config/config.js');
exports.planetnamowelcome = function (req, res) {
  res.send("Welcome to the PlanetNamo API, auction module")
};



/**
@api {post} /auction/CreateAuction Start an auction 
@apiName CreateAuction
@apiGroup Auction
@apiDescription Start an auction associated with an auction type lot. Only one auction can be associated with an auction type lot.
@apiPermission admin

@apiHeader {String="application/json"} Content-Type Content Type for the api 
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json"
	}

@apiParam {String} lotId Id associated with a auction type lot
@apiParamExample {json} Request-Example:
	{
		"lotId": 293032ndwecicj932jc
	}
 
@apiSuccess {String} status Status of the API Call.
@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
	{
	  "status": "success",
	  "message": "Auction on the lot has started."
	}

@apiError error Error message.
@apiErrorExample Error-Response:
    HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "Already auction is present for lot."
	}

*/

exports.CreateAuction = function (req, res) {
	var user = req.user;
	var lotId = req.body.lotId;
	if(user){
		if(lotId){
			Lot.findById(lotId, function(err, lot){
				if(err){
					return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
				}else{
					if(lot){
						if(lot.lotType === "auction"){
							Auction.findOne({parentLotId: lot._id}, function(err, alreadyAuctionPresent){
								if(err){
									return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
								}else{
									if(alreadyAuctionPresent){
										return res.status(200).send({status: "error", message: "Already auction is present for lot."});
									}else{
										var auction = new Auction();
										auction.auctionOwnerId = user._id;
										auction.parentLotId = lot._id;
										auction.auctionStartDate = lot.startDate;
										auction.auctionEndDate = lot.endDate;
										auction.overallBidIncreaseDetails = lot.overallBidIncreaseDetails;
										auction.isLive = true;
										var data = {};
										data.auctionDetails = auction;
				   						let endDateNow = new Date(auction.auctionEndDate).getTime();
				   						var currentTime = new Date().getTime();
				   						data.auctionTimeLeftInMS = endDateNow + 2*60*1000 - currentTime;
				   						if(data.auctionTimeLeftInMS<=1000){
				   							return res.status(200).send({status: "error", message: "Auction cannot be started as time left till lot ends is very small."});
				   						}
										auction.save(function(err){
											if(err){
												return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
											}else{
												lot.isLinkedToAuction = true;
												lot.save(function(err){
													if(err){
														return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
													}else{
														io.sockets.in(auction._id).emit('auctionDetails', JSON.stringify(data));
														return res.status(200).send({status: "success", message: "Auction on the lot has started.", auctionTimeLeftInMS: data.auctionTimeLeftInMS});
													}
												});
											}
										});
									}
								}
							});
						}else{
							return res.status(200).send({status: "error", message: "Lot type is not auction."});
						}
					}else{
						return res.status(200).send({status: "error", message: "lot not found."});
					}
				}
			});
		}else{
			return res.status(200).send({status: "error", message: "lotId not sent."});
		}
	}else{
		return res.status(200).send({status: "error", message: "User is not authenticated."});
	}
};



/**
@api {post} /auction/PayEMDOnAuction Pay EMD for auction 
@apiName PayEMDOnAuction
@apiGroup Auction
@apiDescription Every bid has associated with it an EMD amonunt. This amount if forfieted when the bid is cancelled. So prior to placing a bid, 

an EMD amount must be placed for that bid. This amount is frozen in the user wallet. 

@apiPermission authentication_required

@apiHeader {String="application/json"} Content-Type Content Type for the api 
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json"
	}

@apiParam {String} lotId Id associated with a auction type lot. (This is the parent lotId)
@apiParam {Object} orderDetails orderDetails asssociated.  In the form of a dictionary with keys = productId and values = quantity 
@apiParamExample {json} Request-Example:
{
	"lotId": "58dcb230cb4d156b1d66b2ff",
	"orderDetails": {
		"58d6542c6a52a236f935ccaf": 1
	}
}
 
@apiSuccess {String} status Status of the API Call.
@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
	{
	  "status": "success",
	  "message": "Emd paid successfully for auction."
	}

@apiError error Error message.
@apiErrorExample Error-Response:
    HTTP/1.1 200 Error
	{
		status: "error",
		message: "No active auction found which is associated with the lot."
	}

*/

var FindDetailedOrderDetails = function(lotId, verifiedOrderDetails, callback){
	var detailedResult = [];
	var i = 0;
	function asyncFor(i){
		if(i<verifiedOrderDetails.length){
			var productId = verifiedOrderDetails[i].pId;
			var quantity = verifiedOrderDetails[i].quantity;
			Product.findById(productId)
				   .populate('baseProductId')
				   .exec(function(err, product){
				   		if(err)
				   			callback(err)
				   		else{
				   			var tmp = {};
				   			tmp['productId'] = product._id;
				   			tmp['productName'] = product.baseProductId.productName;
				   			tmp['quantity'] = quantity;
				   			Lot.findById(lotId, function(err, lot){
				   				if(err)
				   					callback(err);
				   				else{
				   					if(lot){
										for(let j=0; j<lot.lotContents.length; j++){
											if(lot.lotContents[j].pId.equals(product._id)){
												tmp['productStartingBidPrice'] = lot.lotContents[j].auctionMinDeposit;
												tmp['emdDeposit'] = lot.lotContents[j].emdDeposit;
											}
										}
				   					}
				   					detailedResult.push(tmp);
				   					i+=1;
				   					asyncFor(i);
				   				}
				   			});
				   		}
				   });
		}else{	
			callback(null, detailedResult);
		}
	}
	asyncFor(i);
};

exports.PayEMDForBid = function (req, res) {
	var user = req.user;
	var lotId = req.body.lotId;
	var orderDetails = req.body.orderDetails;
	if(user){
		if(lotId && orderDetails){
			Lot.findById(lotId, function(err, lot){
				if(err){
					return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
				}else{
					if(lot){
						let currentDate = moment();
						let auctionQueryParams = {};
						auctionQueryParams.parentLotId = lotId;
						auctionQueryParams.auctionCompleted = {$ne: true};
						auctionQueryParams.isLive = {$ne: false};
						auctionQueryParams.auctionCancelled = {$ne: true};
						auctionQueryParams.auctionEndDate = {$gte: currentDate};
						Auction.findOne(auctionQueryParams, function(err, auction){
							if(err){
								return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
							}else{
								if(auction){
									var verifiedOrderDetails = [];
									var detailedOrderDetails = [];
									var emdAmount = 0;
									for (let i = 0; i < lot.originalLotContents.length; i++) {
										if (lot.originalLotContents[i].pId in orderDetails) {
											if (orderDetails[lot.originalLotContents[i].pId] <= lot.originalLotContents[i].quantity) {
												var tmp = {};
												tmp['pId'] = lot.originalLotContents[i].pId;
												tmp['quantity'] = orderDetails[lot.originalLotContents[i].pId];
												emdAmount = emdAmount + (orderDetails[lot.originalLotContents[i].pId] * lot.originalLotContents[i].emdDeposit);
												verifiedOrderDetails.push(tmp);
											} else {
												return res.status(200).send({
													status: "error",
													message: "Cannot buy more than existing quantity"
												});
											}
										}
									}
									if (verifiedOrderDetails.length === 0) {
										return res.status(200).send({
											status: "error",
											message: "Wrong order details. No productId matches with those in the lot."
										});
									}
									let modifiedSearchOrderDetails = verifiedOrderDetails;
									for(let k=0; k<verifiedOrderDetails.length; k++){
										modifiedSearchOrderDetails[k]['specifications'] = [];
										modifiedSearchOrderDetails[k]['rentalPriceByNumberOfMonths'] = [];
									}
									let childLotParams = {};
									childLotParams.lotType = 'auction';
									childLotParams.isChildAuctionLot = true;
									childLotParams.parentLotId = lotId;
									childLotParams.lotContents = {'$size': modifiedSearchOrderDetails.length, '$all': modifiedSearchOrderDetails};
									Lot.findOne(childLotParams, function(err, childLot){
										if(err){
											return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
										}else{
											if(childLot){
												FindDetailedOrderDetails(lotId, verifiedOrderDetails, function(err, detailedOrderDetails){
													if(err){
														return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
													}

													var newBid = new Bid();
													newBid.userId = user._id;
													newBid.auctionId = auction._id;
													newBid.childLotId = childLot._id;
													newBid.parentLotId = lotId;
													newBid.emdFrozen = emdAmount;
													newBid.emdPaid = false;
													newBid.orderDetails = verifiedOrderDetails;
													newBid.detailedOrderDetails = detailedOrderDetails;
													newBid.biddingProcessStarted = false;
													UserUtils.checkAndReturnIfBidAcceptable(user, emdAmount, auction._id, childLot._id, function(err, response){
														if(err){
																if(err === "already done"){
																	return res.status(200).send({
																		status: "error",
																		message: "Already paid EMD"
																	});
																}else{
																	return res.status(200).send({
																		status: "error",
																		message: err.message,
																		amountNeededMore: err.amountRequiredExtra
																	});
																}
														}else{
															newBid.emdPaid = true;
															newBid.dateEMDPaid = moment();
															newBid.save(function(err){
																if(err){
																	return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
																}else{
																	AuctionReq.findOne({userId: user._id, auctionId: auction._id}, function(err, auctionRequest){
																		if(err){
																			return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
																		}else{
																			if(auctionRequest){
																				auctionRequest.bids.push(newBid._id);
																				auctionRequest.userBidByChildLots.push({
																					childLotId: childLot._id,
																					bidId: newBid._id,
																					emdPaid: true,
																					dateEMDPaid: moment(),
																					emdAmount: emdAmount,
																					won: false,
																					orderDetails: detailedOrderDetails
																				});
																				auctionRequest.auctionInteractionHistory.push({
																					dateOfInteraction: moment(),
																					details: "EMD for auction for childLotId: "+childLot._id+"paid successfully."
																				});
																				auctionRequest.save(function(err){
																					if(err){
																						return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
																					}
																					return res.status(200).send({status: "success", message: "Emd paid successfully for auction."});
																				});
																			}else{
																				var newAuctionRequest = new AuctionReq();
																				newAuctionRequest.parentLotId = lotId;
																				newAuctionRequest.auctionId = auction._id;
																				newAuctionRequest.userId = user._id;
																				newAuctionRequest.userBidByChildLots.push({
																					childLotId: childLot._id,
																					bidId: newBid._id,
																					emdPaid: true,
																					dateEMDPaid: moment(),
																					emdAmount: emdAmount,
																					won: false,
																					orderDetails: detailedOrderDetails
																				});
																				newAuctionRequest.bids = [];
																				newAuctionRequest.bids.push(newBid._id);
																				newAuctionRequest.auctionInteractionHistory = [];
																				newAuctionRequest.auctionInteractionHistory.push({
																					dateOfInteraction: moment(),
																					details: "EMD for auction for childLotId: "+childLot._id+"paid successfully."
																				});
																				newAuctionRequest.save(function(err){
																					if(err){
																						return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
																					}
																					return res.status(200).send({status: "success", message: "Emd paid successfully for auction."});
																				});
																			}
																		}
																	});
																}
															});
														}
													});
												});
											}else{
												//NEW CHILD LOT TO BE CREATED
												//VERIFY IF verifiedOrderDetails IS IN ACCORDANCE TO ACTUAL LOT CONTENTS
												//---------verifying orderDetails with actual lot Contents and UPDATING actual lot contents----------------------------------------------
												console.log("new child lot creation module");
												var actualVerifiedOrderDetails = [];
												for (let i = 0; i < lot.lotContents.length; i++) {
													if (lot.lotContents[i].pId in orderDetails) {
														if (orderDetails[lot.lotContents[i].pId] <= lot.lotContents[i].quantity) {
															let tmp = {};
															tmp['pId'] = lot.lotContents[i].pId;
															tmp['quantity'] = orderDetails[lot.lotContents[i].pId];
															lot.lotContents[i].quantity = lot.lotContents[i].quantity - orderDetails[lot.lotContents[i].pId];
															actualVerifiedOrderDetails.push(tmp);
														} else {
															return res.status(200).send({
																status: "error",
																message: "Cannot buy more than existing quantity"
															});
														}
													}
												}
												console.log("updated lot contents should be like");
												if (actualVerifiedOrderDetails.length === 0) {
													return res.status(200).send({
														status: "error",
														message: "Wrong order details. No productId matches with those in the lot."
													});
												}

												let newChildLot = new Lot();
												newChildLot.lotContents = actualVerifiedOrderDetails;
												newChildLot.lotType = 'auction';
												newChildLot.isChildAuctionLot = true;
												newChildLot.parentLotId = lotId;
												newChildLot.lotName = "Child of "+lotId;
												
												FindDetailedOrderDetails(lotId, verifiedOrderDetails, function(err, detailedOrderDetails){
													if(err){
														return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
													}
													var newBid = new Bid();
													newBid.userId = user._id;
													newBid.auctionId = auction._id;
													newBid.childLotId = newChildLot._id;
													newBid.parentLotId = lotId;
													newBid.emdFrozen = emdAmount;
													newBid.emdPaid = false;
													newBid.orderDetails = verifiedOrderDetails;
													newBid.detailedOrderDetails = detailedOrderDetails;
													newBid.biddingProcessStarted = false;
													UserUtils.checkAndReturnIfBidAcceptable(user, emdAmount, auction._id, newChildLot._id, function(err, response){
														if(err){
																if(err === "already done"){
																	return res.status(200).send({
																		status: "error",
																		message: "Already paid EMD"
																	});
																}else{
																	return res.status(200).send({
																		status: "error",
																		message: err.message,
																		amountNeededMore: err.amountRequiredExtra
																	});
																}
														}else{
															newBid.emdPaid = true;
															newBid.dateEMDPaid = moment();
															newBid.save(function(err){
																if(err){
																	return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
																}else{
																	newChildLot.save(function(err){
																		if(err){
																			return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
																		}
																		console.log(lot.lotContents);
																		lot.save(function(err){
																			if(err){
																				return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
																			}
																				AuctionReq.findOne({userId: user._id, auctionId: auction._id}, function(err, auctionRequest){
																					if(err){
																						return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
																					}else{
																						if(auctionRequest){
																							auctionRequest.bids.push(newBid._id);
																							auctionRequest.userBidByChildLots.push({
																								childLotId: newChildLot._id,
																								bidId: newBid._id,
																								emdPaid: true,
																								dateEMDPaid: moment(),
																								emdAmount: emdAmount,
																								won: false,
																								orderDetails: detailedOrderDetails
																							});
																							auctionRequest.auctionInteractionHistory.push({
																								dateOfInteraction: moment(),
																								details: "EMD for auction for childLotId: "+newChildLot._id+"paid successfully."
																							});
																							auctionRequest.save(function(err){
																								if(err){
																									return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
																								}
																								return res.status(200).send({status: "success", message: "Emd paid successfully for auction."});
																							});
																						}else{
																							var newAuctionRequest = new AuctionReq();
																							newAuctionRequest.parentLotId = lotId;
																							newAuctionRequest.auctionId = auction._id;
																							newAuctionRequest.userId = user._id;
																							newAuctionRequest.userBidByChildLots.push({
																								childLotId: newChildLot._id,
																								bidId: newBid._id,
																								emdPaid: true,
																								dateEMDPaid: moment(),
																								emdAmount: emdAmount,
																								won: false,
																								orderDetails: detailedOrderDetails
																							});
																							newAuctionRequest.bids = [];
																							newAuctionRequest.bids.push(newBid._id);
																							newAuctionRequest.auctionInteractionHistory = [];
																							newAuctionRequest.auctionInteractionHistory.push({
																								dateOfInteraction: moment(),
																								details: "EMD for auction for childLotId: "+newChildLot._id+"paid successfully."
																							});
																							newAuctionRequest.save(function(err){
																								if(err){
																									return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
																								}
																								return res.status(200).send({status: "success", message: "Emd paid successfully for auction."});
																							});
																						}
																					}
																				});
																		});
																	});
																}
															});
														}
													});

												});
											}

										}
									});
								}else{
									return res.status(200).send({status: "error", message: "No active auction found which is associated with the lot."});
								}
							}
						});
					}else{
						return res.status(200).send({status: "error", message: "Lot not found."});
					}
				}
			});
		}else{
			return res.status(200).send({status: "error", message: "lotId not sent or orderDetails not sent."});
		}
	}else{
		return res.status(200).send({status: "error", message: "User is not authenticated."});
	}
};



/**
@api {post} /auction/UserAuctions Get Live & Expired Auctions of user
@apiName UserAuctions
@apiGroup Auction
@apiDescription Get live auctions of user and expired auctions of user.
@apiPermission authentication_required

@apiHeader {String="application/json"} Content-Type Content Type for the api 
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json"
	}

@apiParam {Boolean} [sendLive] sendLive Send true if we want live user auctions.
@apiParam {Boolean} [sendExpired] sendExpired Send true if we want expired user auctions.
@apiParam {Boolean} [sendWon] sendWon Send true if we want won / potential winning user auctions.
@apiParam {Date} createdOnBefore Date for pagination
@apiParam {Number} limit Limit the number of results.

@apiParamExample POST: 
{
	"sendLive": true,
	"limit": 10
}


@apiSuccess {String} status Status of the API Call.
@apiSuccess {Array} response Response contains an array of live auction information.
@apiSuccessExample User is not part of any auction:
    HTTP/1.1 200 OK
	{
	  "status": "success",
	  "response": []
	}
@apiSuccessExample User has the highest Bid:
    HTTP/1.1 200 OK
{
  "status": "success",
  "response": [
    {
      "auctionId": "58f08b7fba76696c4fbaaa90",
      "lotId": "58f08b0ff29a0a6c3866e867",
      "lotImageURL": "http://urbanriwaaz.com:8080/api/getFile/lotPicture/undefined",
      "createdAt": "2017-04-15T05:17:25.243Z",
      "userId": "58ec4b8cd5542615dbdcddfb",
      "auctionInteractionHistory": [
        {
          "dateOfInteraction": "2017-04-15T05:17:25.240Z",
          "details": "EMD for auction for childLotId: 58f1ace2e45227054a2c013dpaid successfully."
        },
        {
          "dateOfInteraction": "2017-04-15T05:20:12.299Z",
          "details": "Bid placed for 7001"
        }
      ],
      "auctionStartDate": "2017-04-13T18:30:00.000Z",
      "auctionEndDate": "2017-04-19T18:30:00.000Z",
      "auctionTimeLeftInMS": 393097473,
      "auctionCancelled": false,
      "lotName": "untitled lot",
      "userBids": {
        "58f1ace2e45227054a2c013d": {
          "currentUserBidAmount": 7001,
          "emdAmount": 5000,
          "emdPaid": true,
          "dateEMDPaid": true,
          "orderDetails": [
            {
              "productId": "58d654736a52a236f935ccb1",
              "productName": "iPhone 5s",
              "quantity": 1,
              "productStartingBidPrice": 7000,
              "emdDeposit": 5000
            }
          ],
          "userStartedBidding": true,
          "currentHighestBid": {
            "_id": "58f1ace3e45227054a2c013e",
            "updatedAt": "2017-04-15T05:20:12.308Z",
            "createdAt": "2017-04-15T05:17:23.854Z",
            "dateEMDPaid": "2017-04-15T05:17:23.848Z",
            "emdFrozen": 5000,
            "parentLotId": "58f08b0ff29a0a6c3866e867",
            "childLotId": "58f1ace2e45227054a2c013d",
            "auctionId": "58f08b7fba76696c4fbaaa90",
            "userId": "58ec4b8cd5542615dbdcddfb",
            "__v": 0,
            "bidAmount": 7001,
            "dateBidPlaced": "2017-04-15T05:20:12.296Z",
            "biddingProcessStarted": true,
            "detailedOrderDetails": [
              {
                "productId": "58d654736a52a236f935ccb1",
                "productName": "iPhone 5s",
                "quantity": 1,
                "productStartingBidPrice": 7000,
                "emdDeposit": 5000
              }
            ],
            "orderDetails": [
              {}
            ],
            "bidWon": false,
            "bidCancelled": false,
            "emdPaid": true
          }
        }
      }
    }
  ]
}
@apiSuccessExample User has only paid EMD not placed any bid: 
    HTTP/1.1 200 OK
{
  "status": "success",
  "response": [
    {
      "auctionId": "58f08b7fba76696c4fbaaa90",
      "lotId": "58f08b0ff29a0a6c3866e867",
      "lotImageURL": "http://urbanriwaaz.com:8080/api/getFile/lotPicture/undefined",
      "createdAt": "2017-04-15T05:17:25.243Z",
      "userId": "58ec4b8cd5542615dbdcddfb",
      "auctionInteractionHistory": [
        {
          "dateOfInteraction": "2017-04-15T05:17:25.240Z",
          "details": "EMD for auction for childLotId: 58f1ace2e45227054a2c013dpaid successfully."
        }
      ],
      "auctionStartDate": "2017-04-13T18:30:00.000Z",
      "auctionEndDate": "2017-04-19T18:30:00.000Z",
      "auctionTimeLeftInMS": 393267701,
      "auctionCancelled": false,
      "lotName": "untitled lot",
      "userBids": {
        "58f1ace2e45227054a2c013d": {
          "emdAmount": 5000,
          "emdPaid": true,
          "dateEMDPaid": true,
          "orderDetails": [
            {
              "productId": "58d654736a52a236f935ccb1",
              "productName": "iPhone 5s",
              "quantity": 1,
              "productStartingBidPrice": 7000,
              "emdDeposit": 5000
            }
          ],
          "userStartedBidding": false,
          "currentHighestBid": null
        }
      }
    }
  ]
}

@apiError error Error message.
@apiErrorExample Error-Response:
    HTTP/1.1 200 Error
{
	status: "error",
	message: "User is not authenticated"
}

*/



exports.UserAuctions = function(req, res){
	var user = req.user;
	var limit = (req.body.limit || 10);
	var createdOnBefore = req.body.createdOnBefore;
	var sendLive = req.body.sendLive;
	var sendExpired = req.body.sendExpired;
	var sendWon = req.body.sendWon;

	if(user){
		var auctionReqParam = {};
		auctionReqParam.userId = user._id;
		auctionReqParam.cancelAuctionRequest = {$ne: true};
		auctionReqParam.auctionWon = {$ne: true};
		if(createdOnBefore)
			auctionReqParam.createdAt = { $lt: createdOnBefore };

		AuctionReq.find(auctionReqParam)
				  .populate({
				  	path: 'userBidByChildLots.childLotId',
				  	model: Lot,
				  	populate: {
				  		path: 'currentHighestBid',
				  		model: Bid,
				  		model: 'Bid'
				  	}
				  })
				  .populate({
				  	path: 'userBidByChildLots.bidId',
				  	model: Bid
				  })
				  .limit(limit)
				  .sort('-createdAt')
				  .exec(function(err, userAuctionRequests){
				  		if(err){
				  			return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
				  		}else{
				  			if(userAuctionRequests.length>0){
				  				var TotalResponse = [];
				  				var LiveResponse = [];
				  				var ExpiredResponse = [];
				  				var WonResponse = [];
				  				var ActualResponse = [];
				  				let i = 0;
				  				function asyncFor(i){
				  					if(i<userAuctionRequests.length){
					  					let tmp = {};
					  					tmp['auctionId'] = userAuctionRequests[i].auctionId;
				  						tmp['lotId'] = userAuctionRequests[i].parentLotId;
				  						tmp['lotImageURL'] = config.baseURL + '/getFile/lotPicture/'+userAuctionRequests[i].lotId;
										tmp['createdAt'] = userAuctionRequests[i].createdAt;
										tmp['userId'] = user._id;
										tmp['auctionInteractionHistory'] = userAuctionRequests[i].auctionInteractionHistory;

										Auction.findById(tmp['auctionId'], function(err, auction){
											if(err){
												return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
											}else{
												tmp['auctionStartDate'] = auction.auctionStartDate;
												tmp['auctionEndDate'] = auction.auctionEndDate;
						   						let endDateNow = new Date(auction.auctionEndDate).getTime();
						   						var currentTime = new Date().getTime();
						   						tmp['auctionTimeLeftInMS'] = endDateNow + 2*60*1000 - currentTime;
						   						tmp['auctionCancelled'] = auction.auctionCancelled;
						   						tmp['bidIncreaseDetails'] = auction.overallBidIncreaseDetails;
						   						Lot.findById(tmp['lotId'], function(err, lot){
						   							if(err){
						   								return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
						   							}
						   							tmp['lotName'] = lot.lotName;
						   							tmp['userBids'] = {};
						   							let canUserWinAnything = false;
						   							let backedOutCount = 0;
						   							for(let j=0; j<userAuctionRequests[i].userBidByChildLots.length; j++){
						   								if(userAuctionRequests[i].userBidByChildLots[j].bidId.bidCancelled === true){
						   									backedOutCount+=1;
						   									continue;
						   								}
						   								let childLotId = userAuctionRequests[i].userBidByChildLots[j].childLotId._id
						   								tmp['userBids'][childLotId] = {};
						   								tmp['userBids'][childLotId]['bidId'] = userAuctionRequests[i].userBidByChildLots[j].bidId._id;
						   								tmp['userBids'][childLotId]['currentUserBidAmount'] =  userAuctionRequests[i].userBidByChildLots[j].bidId.bidAmount;
						   								if(userAuctionRequests[i].userBidByChildLots[j].bidId.bidAmount)
						   									tmp['userBids'][childLotId]['remainingAmountToPay'] = userAuctionRequests[i].userBidByChildLots[j].bidId.bidAmount - userAuctionRequests[i].userBidByChildLots[j].bidId.emdFrozen;
						   								tmp['userBids'][childLotId]['emdAmount'] = userAuctionRequests[i].userBidByChildLots[j].emdAmount;
						   								tmp['userBids'][childLotId]['emdPaid'] = userAuctionRequests[i].userBidByChildLots[j].emdPaid;
						   								tmp['userBids'][childLotId]['dateEMDPaid'] = userAuctionRequests[i].userBidByChildLots[j].dateEMDPaid;
						   								tmp['userBids'][childLotId]['orderDetails'] = userAuctionRequests[i].userBidByChildLots[j].orderDetails;
						   								if(!tmp['userBids'][childLotId]['currentUserBidAmount']){
						   									tmp['userBids'][childLotId]['userStartedBidding'] = false;
						   								}else{
						   									tmp['userBids'][childLotId]['userStartedBidding'] = true;
						   								}
						   								tmp['userBids'][childLotId]['currentHighestBid'] = userAuctionRequests[i].userBidByChildLots[j].childLotId.currentHighestBid;
						   								console.log(userAuctionRequests[i].userBidByChildLots[j].childLotId.currentHighestBid)
						   								if(!userAuctionRequests[i].userBidByChildLots[j].childLotId.currentHighestBid)
						   									tmp['userBids'][childLotId]['currentHighestBid'] = null;
						   								else{
						   									if(tmp['userBids'][childLotId]['currentHighestBid'].bidAmount && tmp['userBids'][childLotId]['currentHighestBid'].userId.equals(user._id)){
						   										canUserWinAnything = true;
						   									}
						   								}
						   							}
						   							if(backedOutCount != userAuctionRequests[i].userBidByChildLots.length){
														TotalResponse.push(tmp);
														if(tmp['auctionTimeLeftInMS'] > 1000){
															LiveResponse.push(tmp);
														}
														if(tmp['auctionTimeLeftInMS']<=0){
															ExpiredResponse.push(tmp);
														}
														if(canUserWinAnything === true){
															WonResponse.push(tmp);
														}
													}
													i+=1;
													asyncFor(i);
						   						});
											}
										});
				  					}else{
				  						if(sendLive === true){
				  							return res.status(200).send({status: "success", response: LiveResponse});
				  						}else if(sendExpired === true){
				  							return res.status(200).send({status: "success", response: ExpiredResponse});
				  						}else if(sendWon === true){
				  							return res.status(200).send({status: "success", response: WonResponse});
				  						}else{
				  							return res.status(200).send({status: "error", message: "Invalid request."});
				  						}
				  					}
				  				}	
				  				asyncFor(i);
				  			}else{
				  				return res.status(200).send({status: "success", response: []});
				  			}
				  		}

				  });
	}else{
		return res.status(200).send({status: "error", message: "User is not authenticated."});
	}
};







