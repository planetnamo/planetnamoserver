/**
 * Module dependencies.
 */
var mongoose 	= require('mongoose');

var aux 		= require(__utils + '/auxilaryFunc');
var ErrorParser = require(__utils + '/errorParse');
var UserUtils  = require(__utils + '/users')

var User 			= require(__base + '/models/user/user');
var RegisteredLot 	= require(__base + '/models/lots/registered-lot');
var ApprovedLot 	= require(__base + '/models/lots/approved-lot');
var Lot 	        = require(__base + '/models/lots/lot');
var SellingOrder 	= require(__base + '/models/orders/selling-req'); 
var BuyingOrder     = require(__base + '/models/orders/buying-req');
var Product 		= require(__base + '/models/products/product');
var Wallet				= require(__base + '/models/wallet/wallet');
var WalletTransaction	= require(__base + '/models/wallet/wallet-transactions');
var WalletWithDrawl		= require(__base + '/models/wallet/wallet-withdrawl-requests');
var Auction 		= require(__base + '/models/auctions/auction');
var Bid 			= require(__base + '/models/bids/bid');
var AuctionReq  = require(__base + '/models/orders/auction-req.js');
var io = require(__base+ '/server.js').io;

var moment = require('moment');
exports.planetnamowelcome = function (req, res) {
  res.send("Welcome to the PlanetNamo API, bid module")
};



exports.PlaceBid = function (req, res) {
	var user = req.user;
	var lotId = req.body.lotId;
	var orderDetails = req.body.orderDetails;
	var bidAmount = req.body.bidAmount;
	var socketInfo = {};
	if(user){
		if(lotId){
			let lotParamQuery = {};
			lotParamQuery._id = lotId;
			lotParamQuery.lotType = "auction";
			lotParamQuery.lotSoldOut = {$ne: true};
			lotParamQuery.isChildAuctionLot = {$ne: true};
			lotParamQuery.isAuctionOver = {$ne: true};
			Lot.findOne(lotParamQuery, function(err, parentLot){
				if(err){
					return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
				}else{
					if(parentLot){
						if(bidAmount){
							if(orderDetails){
								//STEP 1 VERIFICATION ---- Making sure orderDetails is in less than or equal to original quanitity
								var verifiedOrderDetails = [];
								var minDeposit = 0;
								for (let i = 0; i < parentLot.originalLotContents.length; i++) {
									if (parentLot.originalLotContents[i].pId in orderDetails) {
										if (orderDetails[parentLot.originalLotContents[i].pId] <= parentLot.originalLotContents[i].quantity) {
											var tmp = {};
											tmp['pId'] = parentLot.originalLotContents[i].pId;
											tmp['quantity'] = orderDetails[parentLot.originalLotContents[i].pId];
											tmp['specifications'] = [];
											tmp['rentalPriceByNumberOfMonths'] = [];
											minDeposit+=(parentLot.originalLotContents[i].auctionMinDeposit); //Quantity NOT CONSIDERED!!!
											verifiedOrderDetails.push(tmp);
										} else {
											return res.status(200).send({
												status: "error",
												message: "Cannot buy more than existing quantity"
											});
										}
									}
								}
								if (verifiedOrderDetails.length === 0) {
									return res.status(200).send({
										status: "error",
										message: "Wrong order details. No productId matches with those in the lot."
									});
								}
								if(bidAmount<=minDeposit)
									return res.status(200).send({status: "error", message: "Bid amount cannot be less than: "+minDeposit});

								//MAKING SURE AUCTION EXISTS FOR THE AUCTION LOT
								let currentDate = moment();
								let auctionQueryParams = {};
								auctionQueryParams.parentLotId = parentLot._id;
								auctionQueryParams.auctionCompleted = {$ne: true};
								auctionQueryParams.isLive = {$ne: false};
								auctionQueryParams.auctionCancelled = {$ne: true};
								auctionQueryParams.auctionEndDate = {$gte: currentDate};
								Auction.findOne(auctionQueryParams, function(err, auction){
									if(err){
										return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
									}else{
										if(auction){
											let auctionReqParams = {};
											auctionReqParams.userId = user._id;
											auctionReqParams.auctionId = auction._id;
											auctionReqParams.parentLotId = lotId;
											AuctionReq.findOne(auctionReqParams, function(err, auctionRequest){
												if(err){
													return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
												}else{
													if(auctionRequest){
														//CHECKING IF CHILD LOT WITH GIVEN SPECIFICATIONS EXISTS OR NOT
														let childLotParams = {};
														childLotParams.lotType = 'auction';
														childLotParams.isChildAuctionLot = true;
														childLotParams.parentLotId = parentLot._id;
														childLotParams.lotContents = {'$size': verifiedOrderDetails.length, '$all': verifiedOrderDetails};
														Lot.findOne(childLotParams, function(err, childLot){
															if(err){
																return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
															}else{
																if(childLot){
																	let bidParam = {};
																	bidParam.bidCancelled = {$ne: true};
																	bidParam.childLotId = childLot._id;
																	bidParam.auctionId = auction._id;
																	bidParam.userId = user._id;
																	bidParam.parentLotId = lotId
																	Bid.findOne(bidParam, function(err, bid){
																		if(bid){
																			Bid.findOne({childLotId: childLot._id, emdPaid: true, bidCancelled: {$ne: true}, biddingProcessStarted: true})
																				.limit(1)
																				.sort('-bidAmount')
																				.exec(function(err, previousBid){
																					if(err){
																						return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
																					}else{
																						let isBidAmountOK = false;
																						let minBidVal = 0;
																						if(auction.overallBidIncreaseDetails.type === "number" || auction.overallBidIncreaseDetails.type === "Number"){
																							if(previousBid === null){
																								isBidAmountOK = true;
																							}
																							else{
																								minBidVal = previousBid.bidAmount + auction.overallBidIncreaseDetails.value
																								if(bidAmount >= parseInt(minBidVal)){
																									isBidAmountOK = true
																								}else{
																									isBidAmountOK = false;
																								}
																							}
																						}else{
																							if(previousBid === null){
																								isBidAmountOK = true;
																							}
																							else{
																								minBidVal = previousBid.bidAmount + previousBid.bidAmount*(auction.overallBidIncreaseDetails.value/100)
																								if(bidAmount >= parseInt(minBidVal)){
																									isBidAmountOK = true
																								}else{
																									isBidAmountOK = false;
																								}
																							}
																						}
																						if(isBidAmountOK){
																							bid.bidAmount = bidAmount;
																							bid.dateBidPlaced = moment();
																							bid.biddingProcessStarted = true;
																							let firstBid = false;
																							let userBidByChildLotsElem = auctionRequest.userBidByChildLots;
																							for(let j=0; j<userBidByChildLotsElem.length; j++){
																								if(userBidByChildLotsElem[j].bidId.equals(bid._id)){
																									if(userBidByChildLotsElem[j].bidAmount === null)
																										firstBid = true;
																									userBidByChildLotsElem[j].bidAmount = bidAmount;
																								}
																							}
																							auctionRequest.auctionInteractionHistory.push({
																								dateOfInteraction: moment(),
																								details: "Bid placed for "+bidAmount
																							});
																							auctionRequest.userBidByChildLots;
																							bid.save(function(err){
																								if(err){
																									return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
																								}
																								Bid.findOne({childLotId: childLot._id, emdPaid: true, bidCancelled: {$ne: true}, biddingProcessStarted: true})
																									.limit(1)
																									.sort('-bidAmount')
																									.exec(function(err, greatestBid){
																										if(err){
																											return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
																										}
																										if(greatestBid)
																											childLot.currentHighestBid = greatestBid._id;
																										bid.save(function(err){
																											if(err){
																												return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
																											}else{
																												auctionRequest.save(function(err){
																													if(err){
																														return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
																													}else{
																														childLot.save(function(err){
																															if(err){
																																return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
																															}else{
																																var timeLeftLocal = 0;
																																var updateParams = {};
																																	try{
																																		var endDateNow = new Date(auction.auctionEndDate).getTime();
																																		var currentTime = new Date().getTime();
																																		if((endDateNow - currentTime)<2*60*1000){
																																			updateParams['auctionEndDateTime'] = new Date(endDateNow + 2*60*1000).toGMTString();
																																		}
																																		timeLeftLocal = endDateNow + 2*60*1000 - currentTime;
																																	}catch(err){

																																	}
																																	console.log('socket');
																																	
																																	//gather info to send to socket
																																	socketInfo = {};
																																	socketInfo['auctionId'] = auction._id;
																																	socketInfo['lotId'] = lotId;
																																	socketInfo['orderDetails'] = orderDetails;
																																	socketInfo['bidAmount'] = bidAmount;
																																	socketInfo['userId'] = user._id;
																																	console.log(socketInfo);
																																	auction.update(updateParams, function(err, result){
																																		if(updateParams.auctionEndDateTime){
																																			data.auctionEndDate = updateParams.auctionEndDateTime;
																																			data.timeLeft = timeLeftLocal;
																																			io.sockets.in(auction._id).emit('newBid', JSON.stringify(socketInfo));
																																			return res.status(200).send({status: 'success', message: 'Bid updated successfully!', autionEndDate : updateParams.auctionEndDateTime, timeLeft: timeLeftLocal, bid: bid});
																																		}else{
																																			io.sockets.in(auction._id).emit('newBid', JSON.stringify(socketInfo));
																																			return res.status(200).send({status: 'success', message: 'Bid updated successfully!', bid: bid});
																																		}
																																	});
																															}
																														})
																													}
																												})
																											}
																										})
																									});
																							});	
																						}else{
																							return res.status(200).send({status: "error", message: "bidAmount needs to be increased.", amountIncrease: minBidVal - bidAmount, bidIncreaseDetails: auction.overallBidIncreaseDetails});
																						}

																					}
																				});
																		}else{
																			return res.status(200).send({status: "error", message: "EMD not paid or some error occured."});
																		}
																	});
																}else{
																	return res.status(200).send({status: "error", message: "EMD not paid or some error occured."});
																}
															}
														});
													}else{
														return res.status(200).send({status: "error", message: "EMD not paid or some error occured."});
													}
												}
											});
										}else{
											return res.status(200).send({status: "error", message: "Auction not found."});
										}
									}
								});
							}else{
								return res.status(200).send({status: "error", message: "orderDetails not specified."});
							}
						}else{
							return res.status(200).send({status: "error", message: "bidAmount not specified."});
						}
					}else{
						return res.status(200).send({status: "error", message: "Incorrect lotId."});
					}
				}
			});
		}else{
			return res.status(200).send({status: "error", message: "lotId not sent."});
		}
	}else{
		return res.status(200).send({status: "error", message: "User is not authenticated."});
	}
};



exports.CancelBid = function (req, res) {
	var user = req.user;
	var bidId = req.body.bidId;

	if(user && bidId){
		let bidQueryParams = {};
		bidQueryParams._id = bidId;
		bidQueryParams.userId = user._id;
		bidQueryParams.biddingProcessStarted = true;
		Bid.findOne(bidQueryParams, function(err, bid){
			if(err){
				return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
			}else{
				console.log(bid);
				if(bid){
					console.log(bid);
					var childLotId = bid.childLotId;
					Lot.findById(childLotId, function(err, childLot){
						if(err){
							return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
						}else if(childLot){
							var bidIdFromLot = childLot.currentHighestBid;
							AuctionReq.findOne({userId: user._id, auctionId: bid.auctionId}, function(err, auctionRequest){
								if(err){
									return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
								}else{
									var auctionId = auctionRequest.auctionId;
									Auction.findById(auctionId, function(err, auction){
										if(err){
											return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
										}else{
											if(auction){
						   						let endDateNow = new Date(auction.auctionEndDate).getTime();
						   						let currentTime = new Date().getTime();
						   						let auctionTimeLeftInMS = endDateNow + 2*60*1000 - currentTime;
						   						if(1){
						   							if(bid.bidCancelled === false){
														if(auctionRequest){
															//FORFIET EMD
															Wallet.findOne({user: user._id}, function(err, wallet){
																if(err){
																	return res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});
																}else{
																	if(wallet){
																		var freezedEMD = bid.emdFrozen;
																		//wallet amount changes
																		wallet.freezedAmount = wallet.freezedAmount - freezedEMD;
																		let freezedForAuctions = wallet.freezedForAuctions;
																		let purgedFA = [];
																		for(let j=0; j<freezedForAuctions.length; j++){
																			if(freezedForAuctions[j].equals(childLot._id)){
																				continue;
																			}else{
																				purgedFA.push(freezedForAuctions[j]);
																			}
																		}
																		wallet.freezedForAuctions = purgedFA;
																		var newWalletTransaction = new WalletTransaction();
																		newWalletTransaction.walletId = wallet._id;
																		newWalletTransaction.transactionId = UserUtils.createWalletTransactionId();
																		newWalletTransaction.amountInvloved = freezedEMD;
																		newWalletTransaction.paymentType = "debit";
																		newWalletTransaction.transactionInfo = {
																			remarks: freezedEMD+" forfietted due to user back out from auction "+auctionRequest.auctionId
																		}
																		let auctionReqBids = [];
																		for(let k=0; k<auctionRequest.bids.length; k++){
																			if(auctionRequest.bids[k].equals(bid._id)){
																				continue;
																			}else{
																				auctionReqBids.push(auctionRequest.bids[k]);
																			}
																		}
																		auctionRequest.bids = auctionReqBids;
																		auctionRequest.auctionInteractionHistory.push({
																			dateOfInteraction: moment(),
																			details: "User backed out of auction for childLotId: "+childLotId+" and forfietted EMD of "+freezedEMD
																		});
																		for(let k=0; k<auctionRequest.userBidByChildLots.length; k++){
																			if(auctionRequest.userBidByChildLots[k].bidId.equals(bid._id)){
																				auctionRequest.userBidByChildLots[k]['isWithdrawn'] = true;
																				auctionRequest.userBidByChildLots[k]['dateWithdrawn'] = moment();
																				auctionRequest.userBidByChildLots[k]['amountForfeitted'] = freezedEMD;
																			}
																		}
																		Lot.findById(bid.parentLotId, function(err, parentLot){
																			if(err){
																				return res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});
																			}else{
																				if(parentLot){
																					parentLot.backedOutUserInfo.push({
																						userId: user._id,
																						auctionId: auctionRequest.auctionId,
																						childLotId: childLot._id,
																						backedOutUsersAmount: freezedEMD
																					});
																					//return res.status(200).send({status: "success", parentLot: parentLot, auctionRequest: auctionRequest, wallet: wallet, walletTransaction: newWalletTransaction});
																					wallet.save(function(err){
																						if(err){
																							return res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});
																						}
																						newWalletTransaction.save(function(err){
																							if(err){
																								return res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});
																							}
																							auctionRequest.save(function(err){
																								if(err){
																									return res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});
																								}
																								parentLot.save(function(err){
																									if(err){
																										return res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});
																									}
																										Bid.update({_id: bidId}, {bidCancelled: true, dateBidCancelled: moment()}, function(err, result){
																											if(err){
																												return res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});
																											}else if(bidIdFromLot == bidId){
																												var bidIdUpdate = undefined;
																												Bid.findOne({bidCancelled: {$ne: true}, auctionId: auctionRequest.auctionId, childLotId:childLot._id}).sort('-bidAmount').limit(1).exec(function(err, secondHighestBid){
																													if(err){
																														res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});
																													}else{
																														if(secondHighestBid){
																															bidIdUpdate = secondHighestBid._id;
																														}
																														Lot.update({_id: childLot._id}, {currentHighestBid: bidIdUpdate}, function(err, result){
																															if(err){
																																res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});
																															}else{
																																return res.status(200).send({status: 'success', message: "Bid withdrawn", currentHighestBid: secondHighestBid});
																															}
																														});
																													}
																												});
																											}else{
																												return res.status(200).send({status: 'success', message: "Bid withdrawn"});
																											}
																										});
																								});
																							});
																						});
																					});
																				}else{
																					return res.status(200).send({status: "error", message: "Some error occured."});
																				}
																			}
																		});	
																	}else{
																		return res.status(200).send({status: "error", message: "Some error occured accessing user wallet."});
																	}
																}
															});
														}else{
															return res.status(200).send({status: "error", message: "Some error occured."});
														}
						   							}else{
						   								return res.status(200).send({status: "error", message: "The bid has already been withdrawn."});
						   							}
						   						}else{
						   							return res.status(200).send({status: "error", message: "The auction has expired."});
						   						}
											}else{
												return res.status(200).send({status: "error", message: "Some error occured."});
											}
										}
									});
								}
							});
						}else{
							return res.status(200).send({status: "error", message: "No Lot Found for the corresponding bid"});
						}
					});
				}else{
					return res.status(200).send({status: "error", message: "Bid with bidId:"+bidId+" not found"});
				}
			}
		});
	}else{
		return res.status(200).send({status: "error", message: "Inadequate information."});
	}
};


function returnAllOtherEMDs(bidId, auctionId, parentLotId){
	console.log("done!");
};

exports.PayRemainingDeposit = function(req, res){
	var bidId = req.body.bidId;
	var user = req.user;
	if(user){
		if(bidId){
			let bidQueryParams = {};
			bidQueryParams._id = bidId;
			bidQueryParams.userId = user._id;
			bidQueryParams.emdPaid = true;
			bidQueryParams.bidCancelled = {$ne: true};
			bidQueryParams.biddingProcessStarted = true;
			Bid.findOne(bidQueryParams, function(err, bid){
				if(err){
					return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
				}else{
					if(bid){
						if(bid.bidWon === true && bid.bidSuccessTransactionId){
							return res.status(200).send({status: "error", message: "You have already paid the remaining deposit and purchased the product successfully."});
						}
						var auctionId = bid.auctionId;
						var childLotId = bid.childLotId;
						var parentLotId = bid.parentLotId;
						Lot.findById(parentLotId, function(err, parentLot){
							if(err){
								return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
							}else{
								if(parentLot){
									Lot.findById(childLotId, function(err, childLot){
										if(err){
											return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
										}else{
											if(childLot){
												Auction.findById(auctionId, function(err, auction){
													if(err){
														return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
													}else{
														if(auction){
									   						let endDateNow = new Date(auction.auctionEndDate).getTime();
									   						let currentTime = new Date().getTime();
									   						let auctionTimeLeftInMS = endDateNow + 2*60*1000 - currentTime;
									   						auctionTimeLeftInMS = 0; //temp
									   						if(auctionTimeLeftInMS<=0){
									   							if(auction.auctionCancelled === false){
									   								let auctionRequestParam = {};
									   								auctionRequestParam.userId = user._id;
									   								auctionRequestParam.auctionId = auctionId;
									   								auctionRequestParam.parentLotId = parentLotId;
									   								AuctionReq.findOne(auctionRequestParam, function(err, auctionRequest){
									   									if(err){
									   										return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
									   									}else{
									   										if(auctionRequest){
									   											if(childLot.currentHighestBid.equals(bidId)){
									   												Bid.findOne({userId: user._id, auctionId: auctionId, childLotId: childLotId, parentLotId: parentLotId})
									   												   .sort('-bidAmount')
									   												   .exec(function(err, highestBidByUser){
									   												   		if(err){
									   												   			return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
									   												   		}else{
									   												   			if(highestBidByUser && highestBidByUser._id.equals(bidId)){
									   												   				var RemainingAmountToPay = bid.bidAmount - bid.emdFrozen;
									   												   				UserUtils.checkAndReturnIfAuctionCanBeBought(user, RemainingAmountToPay, bid.emdFrozen, auctionId, childLotId, function(err, response){
									   												   					if(err){
																											return res.status(200).send({
																												status: "error",
																												message: err.message,
																												amountNeededMore: err.amountRequiredExtra
																											});
									   												   					}else{
									   												   						bid.bidWon = true;
									   												   						bid.dateBidWon = moment();
									   												   						bid.bidSuccessTransactionId = response.walletTransactionId;
									   												   						bid.save(function(err){
									   												   							if(err){
									   												   								return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
									   												   							}
									   												   							auctionRequest.auctionInteractionHistory.push({
									   												   								dateOfInteraction: moment(),
									   												   								details: "User won the bid and bought the childlot: "+childLotId+" by paying "+RemainingAmountToPay+" more."
									   												   							});
									   												   							for(let k=0; k<auctionRequest.userBidByChildLots.length; k++){
									   												   								if(auctionRequest.userBidByChildLots[k].bidId.equals(bidId)){
									   												   									auctionRequest.userBidByChildLots[k].won = true;
									   												   								}
									   												   							}
									   												   							auctionRequest.save(function(err){
									   												   								if(err){
									   												   									return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
									   												   								}else{
									   												   									parentLot.bidWinnerDetails.push({
									   												   										userId: user._id,
									   												   										bidId: bidId,
									   												   										childLotId: childLotId,
									   												   										orderDetails: bid.detailedOrderDetails,
									   												   										auctionRequestId: auctionRequest._id,
									   												   										auctionId: auction._id
									   												   									});
									   												   									parentLot.save(function(err){
									   												   										if(err){
									   												   											return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
									   												   										}
									   												   										return res.status(200).send({status: "success", message: "Successfully bought!"});
									   												   										returnAllOtherEMDs(bidId, auctionId, parentLotId);
									   												   									});
									   												   								}
									   												   							});
									   												   						});
									   												   					}
									   												   				});
									   												   			}else{
									   												   				return res.status(200).send({status: "error", message: "You are not the highest bidder. Cannot buy auction."});
									   												   			}
									   												   		}
									   												   });
									   											}else{
									   												return res.status(200).send({status: "error", message: "You are not the highest bidder. Cannot buy auction."});
									   											}
									   										}else{
									   											return res.status(200).send({status: "error", message: "Some error occured."});
									   										}
									   									}
									   								});
									   							}else{
									   								return res.status(200).send({status: "error", message: "Auction has been cancelled by the seller."});
									   							}
									   						}else{
									   							return res.status(200).send({status: "error", message: "Cannot buy while auction is still active.", auctionTimeLeftInMS: auctionTimeLeftInMS});
									   						}
														}else{
															return res.status(200).send({status: "error", message: "Some error occured."});
														}
													}
												});
											}else{
												return res.status(200).send({status: "error", message: "Some error occured."});
											}
										}
									});
								}else{
									return res.status(200).send({status: "error", message: "Some error occured."});
								}
							}
						});
					}else{
						return res.status(200).send({status: "error", message: "Invalid bid or bid not found."});
					}
				}
			});
		}else{
			return res.status(200).send({status: "error", message: "bidId not sent."});
		}
	}else{
		return res.status(200).send({status: "error", message: "User is not authenticated."});
	}
};
















