var mongoose        = require('mongoose');
var Schema          = mongoose.Schema;


var BidSchema = new Schema({
    createdByUser: {
      type: Schema.ObjectId,
      ref: 'User',
      required: true
    },
    belongsToAuction: {
      type: Schema.ObjectId,
      ref: 'Auction',
      required: true
    },
    bidAmount: {
      type: Number,
      required: true
    },
    bidCancelled: {
      type: Boolean,
      default: false
    }

});


module.exports = mongoose.model('Bid', BidSchema);