var mongoose        = require('mongoose');
var Schema          = mongoose.Schema;


var AuctionSchema = new Schema({
  auctionOwner: {
    type: Schema.ObjectId,
    ref: 'User',
    required: true
  },
  auction_product_id: {
    type: Schema.ObjectId,
    ref: 'Product',
    required: true
  },
  startDate: {
    type: Date,
    required: true,
    default: new Date()
  },
  endDate: {
    type: Date,
    required: true,
    default: new Date() + 2*24*60*60*1000
  },
  auctionCompleted: {
    type: Boolean,
    default: false
  },
  isLive: {
    type: Boolean,
    default: true
  },
  minimumDeposit: {
    type: Number,
    default: 0
  },
  auctionMinimumAmount: {
    type: Number,
    default: 0
  },
  Bids: [{
    type: Schema.ObjectId,
    ref: 'Bid'
  }],
  currentHighestBid: {
    type: Schema.ObjectId,
    ref: 'Bid'
  },
  soldToUser: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  soldToBid: {
    type: Schema.ObjectId,
    ref: 'Bid'
  },
  transactionID: {
    type: Schema.ObjectId,
    ref: 'Transaction'
  },
  auctionCancelled: {
    type: Boolean,
    default: false
  }


});


module.exports = mongoose.model('Auction', AuctionSchema);