var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/**
 * Delivery Schema
 */


var DeliverySchema = new Schema({
  rentalId: {
    type: Schema.ObjectId,
    ref: 'Rental'
  },
  auctionId: {
    type: Schema.ObjectId,
    ref: 'Auction'
  },
  sellingId: {
    type: Schema.ObjectId,
    ref: 'Selling'
  },
  buyingId: {
    type: Schema.ObjectId,
    ref: 'Buying'
  },  
  deliveryType: {
    type: String,
    enum: ['delivery','pickup'],
    required: true
  },
  deliveryStage: {
    type: Number,
    default: 1,
    enum: [1,2,3]
  },
  pickupStage: {
    type: Number,
    default: 1,
    enum: [1,2,3] // 1-Schedule Pickup, 2-Physical Check, 3-Sold
  },
  remarks: [{
    update_date: Date,
    update_remark: String
  }]

});


module.exports = mongoose.model('Delivery', DeliverySchema);
