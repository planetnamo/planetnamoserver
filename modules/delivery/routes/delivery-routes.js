var passport 	= require("passport");
var deliveryCtrl  = require(__modules + '/delivery/controllers/delivery-controller');
var Middleware  = require(__base + '/middlewares/middleware');


module.exports = function(app, express){

	var apiRouter = express.Router();

	
	
	apiRouter.route('/RequestChangeInPickUpDate').post(passport.authenticate('jwt', { session: false }),
													deliveryCtrl.RequestChangeInPickUpDate);
	

	return apiRouter;

};