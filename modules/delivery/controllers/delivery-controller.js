/**
 * Module dependencies.
 */
var mongoose = require('mongoose');

var aux = require(__utils + '/auxilaryFunc');
var ErrorParser = require(__utils + '/errorParse');
var UserUtils = require(__utils + '/users')
var moment = require('moment');
var User = require(__base + '/models/user/user');
var RegisteredLot = require(__base + '/models/lots/registered-lot');
var ApprovedLot = require(__base + '/models/lots/approved-lot');
var Lot = require(__base + '/models/lots/lot');
var SellingOrder = require(__base + '/models/orders/selling-req');
var BuyingOrder = require(__base + '/models/orders/buying-req');
var Product = require(__base + '/models/products/product');
var Wallet = require(__base + '/models/wallet/wallet');
var WalletTransaction = require(__base + '/models/wallet/wallet-transactions');
var WalletWithDrawl = require(__base + '/models/wallet/wallet-withdrawl-requests');
var Delivery = require(__base + '/models/delivery/delivery');



exports.AcceptPickUpDateOfSellingOrder = function(req, res) {
	var sellingOrderId = req.body.sellingOrderId;
	var user = req.user;

	if(user){
		if(sellingOrderId){
			SellingOrder.findById(sellingOrderId, function(err, sellingOrder){
				if(err){
					return res.status(200).send({
						status: "error",
						message: ErrorParser.parseErrorreturnErrorMessage(err)
					});
				}else{
					if(user.role === 2){
						if(sellingOrder.deliveryId){
							if(sellingOrder.cancelSellingOrder === false){
								if(sellingOrder.sellingOrderFulfilled === false){
									Delivery.findById(sellingOrder.deliveryId, function(err, delivery){
										if(err){
											return res.status(200).send({
												status: "error",
												message: ErrorParser.parseErrorreturnErrorMessage(err)
											});		
										}else{
											if(delivery){
												delivery.schedulePickUpDate = delivery.requestedPickUpDate;
												delivery.save(function(err){
													if(err){
														return res.status(200).send({
															status: "error",
															message: ErrorParser.parseErrorreturnErrorMessage(err)
														});	
													}else{
														return res.status(200).send({status: "success", message: "User's pickup date request accepted."})
													}
												});
											}else{
												return res.status(200).send({status: "error", message: "Delivery instance not found for selling order."});
											}
										}
									});
								}else{
									return res.status(200).send({status: "error", message: "Selling order has been fullfilled already."});
								}
							}else{
								return res.status(200).send({status: "error", message: "Cannot interact with a cancelled selling order."});
							}
						}else{
							return res.status(200).send({status: "error", message: "Delivery not initiated."});
						}
					}else{
						return res.status(200).send({status: "error", message: "Unauthorized user."});
					}
				}
			});
		}else{
			return res.status(200).send({status: "error", message: "Incomplete data sent."});
		}
	}else{
		return res.status(200).send({status: "error", message: "User is not authenticated."});
	}

};


exports.GetDeliveryStatusOfOrder = function(req, res) {

};




/**
@api {post} /delivery/RequestChangeInPickUpDate Request Change in PickUp date
@apiName RequestChangeInPickUpDate
@apiGroup Delivery 
@apiPermission authentication_required
@apiDescription Request a change in selling order product pickup date.

@apiHeader {String="application/json"} Content-Type Content Type for the api 
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization": "JWT eyJhbGHVbz5y8i-HRRY"
	}


@apiParam {String} sellingOrderId Id representing the selling order.
@apiParam {Date} newPickUpDate Date in the format of 'YYYY-MM-DD hh:mm:ss a', time is optional


@apiParamExample {json} Request-Example:
{
	"sellingOrderId": "58de76e53994af6cd23b843b",
	"newPickUpDate": "2016-03-31"
}


@apiSuccess {String} status Status of the API Call.
@apiSuccess {String} message Message describing the status of the API call.
@apiSuccess {Date} oldPickUpDate Old pick up date
@apiSuccess {String} requestedPickUpDate New pickup date (requested)


@apiSuccess {JSON} lot Details (json) of the newly lot (FinalLot)
@apiSuccessExample Success-Response:
{
  "status": "success",
  "message": "Delivery pickup date change requested.",
  "oldPickUpDate": "2017-04-05T18:32:23.643Z",
  "requestedPickUpDate": "2016-03-31T00:00:00.000Z"
}


 */
exports.RequestChangeInPickUpDate = function(req, res){
	var user = req.user;
	var sellingOrderId = req.body.sellingOrderId;
	var newPickUpDate = moment(req.body.newPickUpDate, 'YYYY-MM-DD hh:mm:ss a', false); 

	if(user){
		if(sellingOrderId){
			SellingOrder.findById(sellingOrderId, function(err, sellingOrder){
				if(err){
					return res.status(200).send({
						status: "error",
						message: ErrorParser.parseErrorreturnErrorMessage(err)
					});
				}else{
					if(sellingOrder){
						if(sellingOrder.userId.equals(user._id)){
							var deliveryId = sellingOrder.deliveryId
							if(deliveryId){
								Delivery.findById(deliveryId, function(err, delivery){
									if(err){
										return res.status(200).send({
											status: "error",
											message: ErrorParser.parseErrorreturnErrorMessage(err)
										});
									}else{
										if(delivery){
											delivery.requestedPickUpDate = newPickUpDate;
											delivery.save(function(err, updatedDelivery){
												if(err){
													return res.status(200).send({
														status: "error",
														message: ErrorParser.parseErrorreturnErrorMessage(err)
													});
												}else{
													return res.status(200).send({
														status: "success",
														message: "Delivery pickup date change requested.",
														oldPickUpDate: delivery.schedulePickUpDate,
														requestedPickUpDate: updatedDelivery.requestedPickUpDate
													})
												}
											});
										}else{
											return res.status(200).send({status: "error", message: "Selling order does not have any delivery instance associated with it."});
										}
									}
								});
							}else{
								return res.status(200).send({status: "error", message: "Selling order does not have any delivery instance associated with it."});
							}
						}else{
							return res.status(200).send({status: "error", message: "Unauthorized access."});
						}
					}else{
						return res.status(200).send({status: "error", message: "No selling order found."});
					}
				}
			});
		}else{
			return res.status(200).send({status: "error", message: "sellingOrderId not sent."});
		}
	}else{
		return res.status(200).send({status: "error", message: "User not authenticated."});
	}
};