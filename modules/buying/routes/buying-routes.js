var passport 	= require("passport");
var buyingCtrl  = require(__modules + '/buying/controllers/buying-controller');
var Middleware  = require(__base + '/middlewares/middleware');


module.exports = function(app, express){

	var apiRouter = express.Router();

	
	
	//Guests
	apiRouter.route('/welcome').get(buyingCtrl.planetnamowelcome);


	//Authenticated Users Only
	apiRouter.route('/PlaceDirectBuyOrder').post(passport.authenticate('jwt', { session: false }),
													buyingCtrl.PlaceDirectBuyOrder);

	apiRouter.route('/PayAggregateOrderDeposit').post(passport.authenticate('jwt', { session: false }),
													buyingCtrl.PayAggregateOrderDeposit);

	apiRouter.route('/WithdrawFromAggregateBuying').post(passport.authenticate('jwt', { session: false }),
															buyingCtrl.WithdrawFromAggregateBuying);
	
	apiRouter.route('/GetAggregateBuyingOrdersOfUser').post(passport.authenticate('jwt', { session: false }),
															buyingCtrl.GetAggregateBuyingOrdersOfUser);

	apiRouter.route('/GetDirectBuyingOrdersOfUser').post(passport.authenticate('jwt', { session: false }),
															buyingCtrl.GetDirectBuyingOrdersOfUser);

	apiRouter.route('/BuyAggregateOrder').post(passport.authenticate('jwt', { session: false }),
															buyingCtrl.BuyAggregateOrder);


	
	return apiRouter;

};