/**
 * Module dependencies.
 */
var mongoose = require('mongoose');

var aux = require(__utils + '/auxilaryFunc');
var ErrorParser = require(__utils + '/errorParse');
var UserUtils = require(__utils + '/users')
var LotsUtils	= require(__utils + '/lots');
var User = require(__base + '/models/user/user');
var Address     = require(__base + '/models/user/address.js');
var RegisteredLot = require(__base + '/models/lots/registered-lot');
var ApprovedLot = require(__base + '/models/lots/approved-lot');
var Lot = require(__base + '/models/lots/lot');
var SellingOrder = require(__base + '/models/orders/selling-req');
var BuyingOrder = require(__base + '/models/orders/buying-req');
var Product = require(__base + '/models/products/product');
var Wallet = require(__base + '/models/wallet/wallet');
var WalletTransaction = require(__base + '/models/wallet/wallet-transactions');
var WalletWithDrawl = require(__base + '/models/wallet/wallet-withdrawl-requests');
var BaseProduct     = require(__base + '/models/products/base-product.js');
var ProductCategory = require(__base + '/models/products/product-category.js');
var moment = require('moment');
var Transaction = require(__base + '/models/transactions/transaction');

exports.planetnamowelcome = function(req, res) {
	res.send("Welcome to the PlanetNamo API, buying module")
};


/**
@api {post} /buy/PlaceDirectBuyOrder/ Place a direct buy order
@apiName PlaceDirectBuyOrder 
@apiGroup Buy
@apiPermission authentication_required
@apiDescription Place a direct buy order for "buy" type lots. Set payFromWallet to true if you want to deduct the amount from the wallet, set to false if you want to pay via razorpay.
@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization": "JWT eyJhbGHVbz5y8i-HRRY"
	} 

@apiParam {String} lotId Id of the lot
@apiParam {Boolean} payFromWallet True if we want to pay amount from wallet.
@apiParam {String} [payMentRazorPayId] payMentRazorPayId Razor pay Id
@apiParam {Object} orderDetails Dictionary with key as the productId and value as the quantity to buy.
@apiParamExample {json} Request-Example:
	HTTP/1.1 200 OK
	{
		"lotId": "58daa1f08e1b981cb77336a9",
		"payFromWallet": true,
		"orderDetails": {
			"58daa1888e1b981cb77336a6": 1
		}
	}


@apiSuccess {String} status Status of the API Call.
@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
	{
	  "status": "success",
	  "message": "Debited 30000 from your wallet.",
	  "buyingOrderId": "58e73f256ab2b2e9687af2ca",
	  "walletTransactionId": "58e73f266ab2b2e9687af2cc"
	}


@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "Lot sold out"
	}
*/
exports.PlaceDirectBuyOrder = function(req, res) {
	var user = req.user;
	var lotId = req.body.lotId;
	var orderDetails = req.body.orderDetails;
	var payFromWallet = req.body.payFromWallet;
	if(user){
		if(lotId){
			if(orderDetails){
				if(typeof orderDetails === 'object'){
					Lot.findById(lotId, function(err, lot) {
						if (err) {
							return res.status(200).send({
								status: "error",
								message: ErrorParser.parseErrorreturnErrorMessage(err)
							});
						} else {
							if (lot) {
								if (lot.lotSoldOut == false) {
									if (lot.endDate > new Date()) {
										if(lot.lotType === "buy"){
											var verifiedOrderDetails = [];
											var totalAmountToPay = 0;
											for (var i = 0; i < lot.lotContents.length; i++) {
												if (lot.lotContents[i].pId in orderDetails) {
													if (orderDetails[lot.lotContents[i].pId] <= lot.lotContents[i].quantity) {
														var tmp = {};
														tmp['pId'] = lot.lotContents[i].pId;
														tmp['quantity'] = orderDetails[lot.lotContents[i].pId];
														totalAmountToPay = totalAmountToPay + (orderDetails[lot.lotContents[i].pId] * lot.lotContents[i].price);
														lot.lotContents[i].quantity = lot.lotContents[i].quantity - orderDetails[lot.lotContents[i].pId];
														verifiedOrderDetails.push(tmp);
													} else {
														return res.status(200).send({
															status: "error",
															message: "Cannot buy more than existing quantity"
														});
													}
												}
											}
											if (verifiedOrderDetails.length === 0) {
												return res.status(200).send({
													status: "error",
													message: "Wrong order details. No productId matches with those in the lot."
												});
											}
											var productCount = 0;
											for (var i = 0; i < lot.lotContents.length; i++) {
												productCount += lot.lotContents[i].quantity;
											}
											if (productCount === 0) {
												lot.lotSoldOut = true; //If the lot contains 0 products after this order mark it as sold out
												lot.datelotSoldOut = new Date();
											}
											var buyingOrder = new BuyingOrder;
											buyingOrder.lotId = lotId;
											buyingOrder.userId = user._id;
											buyingOrder.totalAmount = totalAmountToPay;
											buyingOrder.totalAmountPaid = false;
											buyingOrder.orderDetails = verifiedOrderDetails;
											buyingOrder.isAggregate = false;
											if(payFromWallet === true){
												UserUtils.checkAndReturnIfDirectBuyPossible(user, totalAmountToPay, buyingOrder._id, function(err, responseMessage){
													if(err){
														return res.status(200).send({
															status: "error",
															message: err.message,
															amountNeededMore: err.amountRequiredExtra
														});
													}else{
														lot.save(function(err){
															if(err){
																return res.status(200).send({
																	status: "error",
																	message: ErrorParser.parseErrorreturnErrorMessage(err)
																});
															}else{
																buyingOrder.totalAmountPaid = true;
																buyingOrder.walletTransaction = responseMessage.walletTransactionId;
																buyingOrder.save(function(err){
																	if(err){
																		return res.status(200).send({
																			status: "error",
																			message: ErrorParser.parseErrorreturnErrorMessage(err)
																		});
																	}else{
																		return res.status(200).send({status: "success", message: responseMessage.message, buyingOrderId:  buyingOrder._id, walletTransactionId: responseMessage.walletTransactionId});
																	}
																});
															}
														});
													}
												});
											}else{
												var payMentRazorPayId = req.body.razorpayPaymentID;
												UserUtils.validatePaymentRazorPay(payMentRazorPayId, totalAmountToPay, function(err, status, body) {
													if (err) {
														return res.status(200).send({
															status: "error",
															message: err
														});
													} else if (status ==='success') {
															Transaction.create({
																mihPayId: payMentRazorPayId,
																mode: 'Razor Pay',
																status: 'success',
																txnid: payMentRazorPayId,
																amount: totalAmountToPay,
																userId: user._id,
																completeData: body
															}, function(err, result){
																if(err){
																	return res.status(200).send({
																		error: ErrorParser.parseErrorreturnErrorMessage(err)
																	});
																}else{
																	lot.save(function(err){
																		if(err){
																			return res.status(200).send({
																				status: "error",
																				message: ErrorParser.parseErrorreturnErrorMessage(err)
																			});
																		}else{
																			buyingOrder.totalAmountPaid = true;
																			buyingOrder.transactionId = result._id;
																			buyingOrder.save(function(err){
																				if(err){
																					return res.status(200).send({
																						status: "error",
																						message: ErrorParser.parseErrorreturnErrorMessage(err)
																					});
																				}else{
																					return res.status(200).send({status: "success", message: responseMessage, buyingOrderId:  buyingOrder._id});
																				}
																			});
																		}
																	});
																}
															});
													} else {
														res.status(200).send({
															error: 'Unknown error occurred'
														});
													}
												});
											}
										}else{
											return res.status(200).send({
												status: "error",
												message: "Invalid lot type for direct buying."
											});
										}
									} else {
										return res.status(200).send({
											status: "error",
											message: "Lot has expired. expired on : " + lot.endDate
										});
									}
								} else {
									return res.status(200).send({
										status: "error",
										message: "Lot sold out"
									});
								}
							} else {
								return res.status(200).send({
									status: "error",
									message: "No lotId found for ID:" + lotId
								});
							}
						}
					});
				}else{
					return res.status(200).send({
						status: "error",
						message: "Invalid order details."
					});
				}
			}else{
				return res.status(200).send({
					status: "error",
					message: "Order details not sent."
				});
			}
		}else{
			return res.status(200).send({
				status: "error",
				message: "Incomplete information."
			});
		}
	}else{
		return res.status(200).send({
			status: "error",
			message: "User is not authenticated."
		});
	}
}




/**
@api {post} /buy/PayAggregateOrderDeposit/ Pay Deposit for Aggregate Buying
@apiName PayAggregateOrderDeposit 
@apiGroup Buy
@apiPermission authentication_required
@apiDescription A transaction is not created, only a wallet transaction is created and the amount is frozen.
@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization": "JWT eyJhbGHVbz5y8i-HRRY"
	} 

@apiParam {String} lotId Id of the lot
@apiParam {orderDetails} Dictionary with key as the productId and value as the quantity to buy.
@apiParamExample {json} Request-Example:
	HTTP/1.1 200 OK
	{
	  "lotId": "58d6648afb649e40d520885a",
	  "orderDetails": {
	    "58d654966a52a236f935ccb6": 1
	  }
	}


@apiSuccess {String} status Status of the API Call.
@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
	{
	  "status": "success",
	  "message": "Freezed 200 from your wallet.",
	  "buyingOrderId": "58e163b0e69f55aa67838210"
	}


@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "Insufficient funds in wallet."
	}
*/
exports.PayAggregateOrderDeposit = function(req, res){
	var user = req.user;
	var lotId = req.body.lotId;
	var orderDetails = req.body.orderDetails;

	if (user) {
		if (lotId) {
			if (orderDetails) {
				if (typeof orderDetails === 'object') {
					Lot.findById(lotId, function(err, lot) {
						if (err) {
							return res.status(200).send({
								status: "error",
								message: ErrorParser.parseErrorreturnErrorMessage(err)
							});
						} else {
							if (lot) {
								if (lot.lotSoldOut == false) {
									if (lot.endDate > new Date()) {
										if(lot.lotType != "clearance"){
											return res.status(200).send({status: "error", message: "Can only aggregate buy from a clearance type lot."});
										}
										var verifiedOrderDetails = [];
										var totalAmountToPay = 0;
										var totalEmdDeposit = 0;
										for (var i = 0; i < lot.lotContents.length; i++) {
											if (lot.lotContents[i].pId in orderDetails) {
												if (orderDetails[lot.lotContents[i].pId] <= lot.lotContents[i].quantity) {
													var tmp = {};
													tmp['pId'] = lot.lotContents[i].pId;
													tmp['quantity'] = orderDetails[lot.lotContents[i].pId];
													totalAmountToPay = totalAmountToPay + (orderDetails[lot.lotContents[i].pId] * lot.lotContents[i].price);
													totalEmdDeposit = totalEmdDeposit + (orderDetails[lot.lotContents[i].pId] * lot.lotContents[i].emdDeposit)
													lot.lotContents[i].quantity = lot.lotContents[i].quantity - orderDetails[lot.lotContents[i].pId];
													verifiedOrderDetails.push(tmp);
												} else {
													return res.status(200).send({
														status: "error",
														message: "Cannot buy more than existing quantity"
													});
												}
											}
										}
										if (verifiedOrderDetails.length === 0) {
											return res.status(200).send({
												status: "error",
												message: "Wrong order details. No productId matches with those in the lot."
											});
										}
										var productCount = 0;
										for (var i = 0; i < lot.lotContents.length; i++) {
											productCount += lot.lotContents[i].quantity;
										}
										if (productCount === 0) {
											lot.lotSoldOut = true; //If the lot contains 0 products after this order mark it as sold out
											lot.datelotSoldOut = new Date();
										}
										var buyingOrder = new BuyingOrder;
										buyingOrder.lotId = lotId;
										buyingOrder.userId = user._id;
										buyingOrder.totalAmount = totalAmountToPay;
										buyingOrder.emd = totalEmdDeposit;
										buyingOrder.emdPaid = false; //update this after emd paid
										buyingOrder.totalAmountPaid = false;
										buyingOrder.orderDetails = verifiedOrderDetails;
										buyingOrder.isAggregate = true;
										UserUtils.checkAndReturnIfAggregateOrderPossible(user, totalEmdDeposit, buyingOrder._id, function(err, responseMessage){
											if(err){
												return res.status(200).send({
													status: "error",
													message: err.message,
													amountNeededMore: err.amountRequiredExtra
												});
											}else{
												lot.save(function(err){
													if(err){
														return res.status(200).send({
															status: "error",
															message: ErrorParser.parseErrorreturnErrorMessage(err)
														});
													}else{
														buyingOrder.emdPaid = true;
														buyingOrder.save(function(err){
															if(err){
																return res.status(200).send({
																	status: "error",
																	message: ErrorParser.parseErrorreturnErrorMessage(err)
																});
															}else{
																return res.status(200).send({status: "success", message: responseMessage, buyingOrderId:  buyingOrder._id});
															}
														});
													}
												});
											}
										});
									} else {
										return res.status(200).send({
											status: "error",
											message: "Lot has expired. expired on : " + lot.endDate
										});
									}
								} else {
									return res.status(200).send({
										status: "error",
										message: "Lot sold out"
									});
								}
							} else {
								return res.status(200).send({
									status: "error",
									message: "No lotId found for ID:" + lotId
								});
							}
						}
					});
				} else {
					return res.status(200).send({
						status: "error",
						message: "Invalid order details."
					});
				}
			} else {
				return res.status(200).send({
					status: "error",
					message: "Order details not sent."
				});
			}
		} else {
			return res.status(200).send({
				status: "error",
				message: "Incomplete information."
			});
		}
	} else {
		return res.status(200).send({
			status: "error",
			message: "User is not authenticated."
		});
	}
};





/**
@api {post} /buy/BuyAggregateOrder/ Buy Aggregate Order
@apiName BuyAggregateOrder 
@apiGroup Buy
@apiPermission authentication_required
@apiDescription After the clearance lot is sold out we have the option of paying the remaining amount and completing our order.

@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization": "JWT eyJhbGHVbz5y8i-HRRY"
	} 

@apiParam {String} buyingOrderId buying order Id
@apiParam {String} [AddressId] AddressId of the user to which the order is supposed to be shipped. By default primary address will be used.
@apiParamExample {json} Request-Example:
	HTTP/1.1 200 OK
{
	"addressId": "58df5653efc4dc7cf67f4f56",
	"buyingOrderId": "58ec35bdacaba22d4ab8824f"
}


@apiSuccess {String} status Status of the API Call.
@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK



@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
{
  "status": "error",
  "message": "Wait till lot is sold out before buying.",
  "currentLotContents": [
    {
      "pId": "58d654a26a52a236f935ccb8",
      "quantity": 0,
      "price": 10000,
      "emdDeposit": 200,
      "approvedLotId": "58d656e46a52a236f935ccd0",
      "registeredByUser": "58d646a5a52e0c1a4f229a43",
      "_id": "58d6648bfb649e40d520885c",
      "specifications": [],
      "rentalPriceByNumberOfMonths": []
    },
    {
      "pId": "58d654966a52a236f935ccb6",
      "quantity": 10,
      "price": 35000,
      "emdDeposit": 5200,
      "approvedLotId": "58d656d86a52a236f935ccce",
      "registeredByUser": "58d646a5a52e0c1a4f229a43",
      "_id": "58d6648bfb649e40d520885b",
      "specifications": [],
      "rentalPriceByNumberOfMonths": []
    }
  ]
}
*/
exports.BuyAggregateOrder = function(req, res){
	var user = req.user;
	var buyingOrderId = req.body.buyingOrderId;
	var addressId = req.body.addressId;
	var address = null;

	if(user){
		User.findById(user._id)
			.populate('primaryAddress')
			.exec(function(err, userInfo){
				if(err){
					return res.status(200).send({
						status: "error",
						message: ErrorParser.parseErrorreturnErrorMessage(err)
					});
				}
				else{
					Address.findById(addressId, function(err, givenAddress){
						if(err){
							return res.status(200).send({
								status: "error",
								message: ErrorParser.parseErrorreturnErrorMessage(err)
							});
						}else{
							if(givenAddress){
								if(givenAddress.userId.equals(user._id)){
									address = givenAddress;
								}else{
									return res.status(200).send({status: "error", message: "Given address does not belong to user."});
								}
							}else if(!givenAddress && userInfo.primaryAddress){
								address = userInfo.primaryAddress;
							}else{
								return res.status(200).send({status: "error", message: "Address not specified."});
							}
							BuyingOrder.findById(buyingOrderId, function(err, buyingOrder){
								if(err){
									return res.status(200).send({
										status: "error",
										message: ErrorParser.parseErrorreturnErrorMessage(err)
									});
								}else{
									if(buyingOrder){
										if(!buyingOrder.lotId)
											return res.status(200).send({status: "error", message: "Invalid buyingOrderId"});
										if(buyingOrder.lotId.lotSoldOut === false)
											return res.status(200).send({status: "error", message: "Wait till lot is sold out before buying.", currentLotContents: buyingOrder.lotId.lotContents});
										if(buyingOrder.isAggregate === true && buyingOrder.userId.equals(user._id) && buyingOrder.emdPaid === true && buyingOrder.cancelBuyingOrder === false && buyingOrder.totalAmountPaid === false){
											var remainingAmountToPay = buyingOrder.totalAmount - buyingOrder.emd;
											UserUtils.checkAndReturnIfAggregateCanBeCompleted(user, remainingAmountToPay, buyingOrder, function(err, response){
												if(err){
													return res.status(200).send(err);
												}else{
													buyingOrder.totalAmountPaid = true;
													buyingOrder.dateTotalAmountPaid = moment();
													buyingOrder.deliveryAddress = address;
													buyingOrder.walletTransactionId = response.walletTransactionId;
													buyingOrder.transactionSuccess = true;
													buyingOrder.save(function(err){
														if(err){
															return res.status(200).send({
																status: "error",
																message: ErrorParser.parseErrorreturnErrorMessage(err)
															});
														}else{
															return res.status(200).send({status: "success", message: "Aggregate order successfully purchased."});
														}
													});
												}
											});

										}else{
											return res.status(200).send({status: "error", message: "Invalid buying order."});
										}
									}else{
										return res.status(200).send({status: "error", message: "Buying order not found."});
									}
								}
							})
							.populate('lotId');
						}
					});
				}
			});
	}else{
		return res.status(200).send({status: "error", message: "User is not authenticated."});
	}
};




/**
@api {post} /buy/WithdrawFromAggregateBuying/ Withdraw from aggregate Buying order
@apiName WithdrawFromAggregateBuying 
@apiGroup Buy
@apiPermission authentication_required
@apiDescription Withdraw from aggregate buying order.
@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization": "JWT eyJhbGHVbz5y8i-HRRY"
	} 

@apiParam {String} buyingOrderId Id of the buying order
@apiParamExample {json} Request-Example:
	HTTP/1.1 200 OK
	{
		"buyingOrderId": "58e163b0e69f55aa67838210"
	}


@apiSuccess {String} status Status of the API Call.
@apiSuccess {Object} response Response of the API Call.
@apiSuccess {Object} response.transactionId Transaction id of the wallet transaction (re-imbursment of the emd)
@apiSuccess {Object} response.buyingOrder Updated buying order details
@apiSuccess {Object} response.wallet Updated wallet details
@apiSuccess {Object} response.WalletTransaction Wallet transaction details
@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK



@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error

*/
exports.WithdrawFromAggregateBuying = function(req, res){
	var user = req.user;
	var buyingOrderId = req.body.buyingOrderId;
	var transactionId = UserUtils.createWalletTransactionId();
	if(user){
		if(buyingOrderId){
			BuyingOrder.findById(buyingOrderId)
					   .populate('lotId')
					   .exec(function(err, buyingOrder){
					   		if(err){
								return res.status(200).send({
									error: ErrorParser.parseErrorreturnErrorMessage(err)
								});					   		
							}else{
								if(buyingOrder){
									if(buyingOrder.userId.equals(user._id)){
										var lot = buyingOrder.lotId;
										if(lot.lotType === "clearance" && buyingOrder.isAggregate === true){
											if(buyingOrder.totalAmountPaid === false){
												if(buyingOrder.cancelBuyingOrder === false){

													var currentDate = moment();
													if(currentDate>lot.endDate && lot.lotSoldOut === false){
														//withdraw without penalty
									   					Wallet.findOne({
									   						user: user._id
									   					}, function(err, wallet){
									   						if(err){
																return res.status(200).send({
																	error: ErrorParser.parseErrorreturnErrorMessage(err)
																});
									   						}else{
									   							if(wallet){
									   								Wallet.update({
									   									user: user._id
									   								},{
									   									amount: buyingOrder.emd + wallet.amount,
									   									$pullAll: {freezedForAggregate: [buyingOrder._id] },
									   									freezedAmount: wallet.freezedAmount - buyingOrder.emd
									   								}, function(err, updatedWallet){
									   									if(err){
																			return res.status(200).send({
																				error: ErrorParser.parseErrorreturnErrorMessage(err)
																			});
									   									}else{
																			var createParams = {
																				transactionId: transactionid,
																				amountInvloved: buyingOrder.emd,
																				paymentType: 'credit',
																				transactionInfo: {
																					remarks: 'Credit of emd deposit to user as lot failed to sell out.',
																					otherInfo: {}
																				},
																				walletId: wallet._id
																			}
																			if (req.body.otherPaymentInfo) {
																				createParams.transactionInfo.otherInfo = req.body.otherPaymentInfo;
																			}
																			WalletTransaction.create(createParams, function(err, result) {
																				if (err) {
																					return res.status(200).send({
																						error: ErrorParser.parseErrorreturnErrorMessage(err)
																					});
																				} else {
																					buyingOrder.dateBuyingOrderCancelled = moment();
																					buyingOrder.cancelBuyingOrder = true;
																					buyingOrder.cancelledBy = 'user';
																					buyingOrder.cancellationRemarks = 'lot failed to sell out within time';
																					buyingOrder.save(function(err, buyingOrder) {
																						if (err) {
																							return res.status(200).send({
																								error: ErrorParser.parseErrorreturnErrorMessage(err)
																							});
																						} else {
																							if(!lot.backedOutUserInfo)
																								lot.backedOutUserInfo = [];
																							lot.backedOutUserInfo.push({
																								userId: user._id,
																								buyingOrderId: buyingOrder._id,
																								backedOutUsersAmount: 0
																							});
																							lot.save(function(err){
																								if(err){
																									return res.status(200).send({
																										error: ErrorParser.parseErrorreturnErrorMessage(err)
																									});
																								}else{
																									return res.status(200).send({
																										status: 'success',
																										response: {
																											'WalletTransaction': result
																										}
																									});
																								}
																							});
																						}
																					});
																				}
																			});
									   									}
									   								});
									   							}else{
									   								return res.status(200).send({status: "error", message: "Unable to find wallet."});
									   							}
									   						}
									   					});
													}else{
														//Now do not give emd back
									   					Wallet.findOne({user: user._id}, function(err, wallet){
									   						if(err){
																return res.status(200).send({
																	error: ErrorParser.parseErrorreturnErrorMessage(err)
																});
									   						}else{
									   							if(wallet){
									   								Wallet.update({user: user._id}, {$pullAll: {freezedForAggregate: [buyingOrder._id]}, freezedAmount: wallet.freezedAmount - buyingOrder.emd}, function(err, updatedWallet){
									   									if(err){
																			return res.status(200).send({
																				error: ErrorParser.parseErrorreturnErrorMessage(err)
																			});
									   									}else{
																			var createParams = {
																				transactionId: transactionId,
																				amountInvloved: buyingOrder.emd,
																				paymentType: 'debit',
																				transactionInfo: {
																					remarks: 'User backed out. Frozen EMD will be debited.',
																					otherInfo: {}
																				},
																				walletId: wallet._id
																			}
																			if (req.body.otherPaymentInfo) {
																				createParams.transactionInfo.otherInfo = req.body.otherPaymentInfo;
																			}
																			WalletTransaction.create(createParams, function(err, result) {
																				if (err) {
																					return res.status(200).send({
																						error: ErrorParser.parseErrorreturnErrorMessage(err)
																					});
																				} else {
																					buyingOrder.dateBuyingOrderCancelled = moment();
																					buyingOrder.cancelBuyingOrder = true;
																					buyingOrder.cancelledBy = 'user';
																					buyingOrder.cancellationRemarks = 'User backed out.';
																					buyingOrder.save(function(err, buyingOrder) {
																						if (err) {
																							return res.status(200).send({
																								error: ErrorParser.parseErrorreturnErrorMessage(err)
																							});
																						} else {
																							if(!lot.backedOutUserInfo)
																								lot.backedOutUserInfo = [];
																							lot.backedOutUserInfo.push({
																								userId: user._id,
																								buyingOrderId: buyingOrder._id,
																								backedOutUsersAmount: buyingOrder.emd
																							});
																							lot.save(function(err){
																								if(err){
																									return res.status(200).send({
																										error: ErrorParser.parseErrorreturnErrorMessage(err)
																									});
																								}else{
																									return res.status(200).send({
																										status: 'success',
																										message: "successfully backed out. EMD forfeited.",
																										response: {
																											'WalletTransaction': result
																										}
																									});
																								}
																							});
																						}
																					});
																				}
																			});
									   									}
									   								});
									   							}else{
									   								return res.status(200).send({status: "error", message: "Unable to find wallet."});
									   							}
									   						}
									   					});
													}
												}else{
													return res.status(200).send({status: "error", message: "Cannot backout out of a cancelled buying order."});
												}
											}else{
												return res.status(200).send({status: "error", message: "Cannot backout after paying total amount."});
											}
										}else{
											return res.status(200).send({status: "error", message: "Invalid buyingOrderId."});
										}
									}else{
										return res.status(200).send({status: "error", message: "Unauthorized use of buyingOrderId."});
									}
								}else{
									return res.status(200).send({status: "error", message: "Invalid buyingOrderId."});
								}
							}
					   });
		}else{
			return res.status(200).send({status: "error", message: "buyingOrderId not sent."});
		}
	}else{
		return res.status(200).send({status: "error", message: "User is not authenticated."});
	}
};













/**
@api {post} /buy/GetAggregateBuyingOrdersOfUser/ Get Aggregate Buy Orders of User 
@apiName GetAggregateBuyingOrdersOfUser 
@apiGroup Buy
@apiPermission authentication_required
@apiDescription Get a list of aggregate buy orders of a user. Limit and createdAt keys are used to manage pagination via the api.

@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization": "JWT eyJhbGHVbz5y8i-HRRY"
	} 

@apiParam {Number} [limit] limit Limit the number of search results, by default 10.
@apiParam {Date} [createdOnBefore] createdOnBefore Used for pagination
@apiParamExample {json} Request-Example:
	HTTP/1.1 200 OK
{
	"limit": 10
}


@apiSuccess {String} status Status of the API response
@apiSuccess {Array} response.lotId Id representing the lot from which the order was taken from
@apiSuccess {String} response.lotName Name of the lot
@apiSuccess {Date} response.createdAt Date at which the buy order was created
@apiSuccess {Boolean} response.isAggregate Is the buy order an aggregate type
@apiSuccess {Boolean} response.emdPaid Has the emd been paid for this buy order
@apiSuccess {String} response.totalAmountPaid Has the total amount been paid for this buy order
@apiSuccess {String} response.cancelBuyingOrder Has the buy order been cancelled
@apiSuccess {String} response.buyingOrderFulfilled Has the buy order been fulfilled (Delivered)
@apiSuccess {String} response.transactionSuccess Has the rest of the money for the buy order been paid successfully.
@apiSuccess {Number} response.emd Total EMD paid for this buy order
@apiSuccess {Number} response.totalAmount Total amount paid/to be paid for this buy order
@apiSuccess {Array} response.orderDetails Array of product information in the order
@apiSuccess {String} response.orderDetails.productId Id of the product
@apiSuccess {String} response.orderDetails.baseProductId Base product Id of the product
@apiSuccess {String} response.orderDetails.baseProductName Name of the base product
@apiSuccess {String} response.orderDetails.quantity Quantity remaining of the product
@apiSuccess {String} response.orderDetails.perUnitPrice Per unit price of the product
@apiSuccess {String} response.orderDetails.quantityLeft The quantity of product left till now
@apiSuccess {String} response.orderDetails.quantitySold The quantity of product sold
@apiSuccess {String} response.orderDetails.imagesArray Array of image urls representing the product
@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
	{
	  "status": "success",
	  "response": [
	    {
	      "lotId": "58d6648afb649e40d520885a",
	      "lotName": "untitled lot",
	      "createdAt": "2017-04-02T20:50:58.552Z",
	      "isAggregate": true,
	      "emdPaid": true,
	      "totalAmountPaid": false,
	      "cancelBuyingOrder": false,
	      "buyingOrderFulfilled": false,
	      "transactionSuccess": false,
	      "emd": 200,
	      "totalAmount": 10000,
	      "orderDetails": [
	        {
	          "productId": "58d654a26a52a236f935ccb8",
	          "baseProductId": "58d6532d5f5fe43655d0530b",
	          "baseProductName": "Lenovo YogaPad",
	          "quantity": 1,
	          "perUnitPrice": 10000,
	          "quantityLeft": 4,
	          "quantitySold": null,
	          "imagesArray": [
	            "http://urbanriwaaz.com:8080/api/getFile/productPicture/58dca9945513e363809fcecb"
	          ]
	        }
	      ]
	    },
	    {
	      "lotId": "58d6648afb649e40d520885a",
	      "lotName": "untitled lot",
	      "createdAt": "2017-04-02T20:48:49.814Z",
	      "isAggregate": true,
	      "emdPaid": true,
	      "totalAmountPaid": false,
	      "cancelBuyingOrder": false,
	      "buyingOrderFulfilled": false,
	      "transactionSuccess": false,
	      "emd": 200,
	      "totalAmount": 10000,
	      "orderDetails": [
	        {
	          "productId": "58d654a26a52a236f935ccb8",
	          "baseProductId": "58d6532d5f5fe43655d0530b",
	          "baseProductName": "Lenovo YogaPad",
	          "quantity": 1,
	          "perUnitPrice": 10000,
	          "quantityLeft": 4,
	          "quantitySold": null,
	          "imagesArray": [
	            "http://urbanriwaaz.com:8080/api/getFile/productPicture/58dca9945513e363809fcecb"
	          ]
	        }
	      ]
	    },
	    {
	      "lotId": "58d6648afb649e40d520885a",
	      "lotName": "untitled lot",
	      "createdAt": "2017-04-02T20:45:45.515Z",
	      "isAggregate": true,
	      "emdPaid": true,
	      "totalAmountPaid": false,
	      "cancelBuyingOrder": false,
	      "buyingOrderFulfilled": false,
	      "transactionSuccess": false,
	      "emd": 200,
	      "totalAmount": 10000,
	      "orderDetails": [
	        {
	          "productId": "58d654a26a52a236f935ccb8",
	          "baseProductId": "58d6532d5f5fe43655d0530b",
	          "baseProductName": "Lenovo YogaPad",
	          "quantity": 1,
	          "perUnitPrice": 10000,
	          "quantityLeft": 4,
	          "quantitySold": null,
	          "imagesArray": [
	            "http://urbanriwaaz.com:8080/api/getFile/productPicture/58dca9945513e363809fcecb"
	          ]
	        }
	      ]
	    },
	    {
	      "lotId": "58d6648afb649e40d520885a",
	      "lotName": "untitled lot",
	      "createdAt": "2017-04-02T20:31:13.861Z",
	      "isAggregate": true,
	      "emdPaid": true,
	      "totalAmountPaid": false,
	      "cancelBuyingOrder": false,
	      "buyingOrderFulfilled": false,
	      "transactionSuccess": false,
	      "orderDetails": [
	        {
	          "productId": "58d654a26a52a236f935ccb8",
	          "baseProductId": "58d6532d5f5fe43655d0530b",
	          "baseProductName": "Lenovo YogaPad",
	          "quantity": 1,
	          "perUnitPrice": 10000,
	          "quantityLeft": 4,
	          "quantitySold": null,
	          "imagesArray": [
	            "http://urbanriwaaz.com:8080/api/getFile/productPicture/58dca9945513e363809fcecb"
	          ]
	        }
	      ]
	    }
	  ]
	}


@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "No aggregate buying orders placed till now."
	}
*/
exports.GetAggregateBuyingOrdersOfUser = function(req, res) {
	var user = req.user;
	var limit = (req.body.limit || 10);
	var createdOnBefore = req.body.createdOnBefore;
	var getDirectBuyOrder = req.body.getDirectBuyOrder;

	var andQueries = {};
	var orQueries = [];


	andQueries.userId = user._id;
	andQueries.isAggregate = true;

	if (createdOnBefore) {
		andQueries.createdAt = {
			$lt: createdOnBefore
		};
	}


	var queryParams = {};
	if (orQueries.length > 0) {
		queryParams = {
			$and: [andQueries, {
				$or: orQueries
			}]
		};
	} else {
		queryParams = andQueries;
	}

	if(user){
		BuyingOrder.find(queryParams)
				   .populate({
				   		path: 'lotId',
				   		model: Lot
				   })
				   .populate({
				   		path: 'orderDetails.pId',
				   		model: Product,
				   		populate: {
				   			path: 'baseProductId',
				   			model: BaseProduct
				   		}
				   })
				   .limit(limit)
				   .sort('-createdAt')
				   .exec(function(err, responseArray){
				   		if(err){
							return res.status(200).send({
								status: "error",
								message: ErrorParser.parseErrorreturnErrorMessage(err)
							});
				   		}else{
				   			if(responseArray && responseArray.length>0){
				   				//return res.status(200).send(responseArray);
				   				let parsedResponseArray = [];
				   				let i = 0;
				   				function asyncFor(i){
				   					if(i<responseArray.length){
				   						let aggregateOrderDetails = responseArray[i];
				   						let tmp = {};
				   						tmp['lotId'] = aggregateOrderDetails.lotId._id;
				   						tmp['buyingOrderId'] = aggregateOrderDetails._id;
				   						tmp['lotName'] = aggregateOrderDetails.lotId.lotName;
				   						tmp['startDate'] = aggregateOrderDetails.lotId.startDate;
				   						tmp['endDate'] = aggregateOrderDetails.lotId.endDate;
				   						tmp['createdAt'] = aggregateOrderDetails.createdAt;
				   						tmp['isAggregate'] = aggregateOrderDetails.isAggregate;
				   						tmp['emdPaid'] = aggregateOrderDetails.emdPaid;  
				   						tmp['totalAmountPaid'] = aggregateOrderDetails.totalAmountPaid; 
				   						tmp['cancelBuyingOrder'] = aggregateOrderDetails.cancelBuyingOrder;
				   						tmp['buyingOrderFulfilled'] = aggregateOrderDetails.buyingOrderFulfilled;
				   						tmp['transactionSuccess'] = aggregateOrderDetails.transactionSuccess;
				   						tmp['emd'] = aggregateOrderDetails.emd; //TOTAL
				   						tmp['totalAmount'] = aggregateOrderDetails.totalAmount; //TOTAL
				   						tmp['orderDetails'] = [];
				   						tmp['usersAllowedToBuy'] = false;
				   						tmp['timeLeftLocal'] = 0;
				   						let endDateNow = new Date(aggregateOrderDetails.lotId.endDate).getTime();
				   						var currentTime = new Date().getTime();
				   						tmp['timeLeftLocal'] = endDateNow + 2*60*1000 - currentTime;
				   						
				   						if(aggregateOrderDetails.lotId.lotSoldOut === true)
				   							tmp['usersAllowedToBuy'] = true;



				   						parsedResponseArray.push(tmp);
				   						let j = 0;
				   						function asyncFor1(j){
				   							if(j<aggregateOrderDetails.orderDetails.length){
				   								let orderDetails = {};
				   								orderDetails['productId'] = aggregateOrderDetails.orderDetails[j].pId._id;
				   								orderDetails['baseProductId'] = aggregateOrderDetails.orderDetails[j].pId.baseProductId._id;
				   								orderDetails['baseProductName'] = aggregateOrderDetails.orderDetails[j].pId.baseProductId.productName;
				   								orderDetails['quantity'] = aggregateOrderDetails.orderDetails[j].quantity;

				   								let productInfoInLot = {};
				   								for(let k=0; k<aggregateOrderDetails.lotId.lotContents.length; k++){
				   									let perUnitPrice = aggregateOrderDetails.lotId.lotContents[k].price;
				   									let quantityLeft = aggregateOrderDetails.lotId.lotContents[k].quantity;
				   									let initialTotalQuantity = aggregateOrderDetails.lotId.lotContents[k].initialTotalQuantity;
				   									let quantitySold = null;
				   									productInfoInLot[aggregateOrderDetails.lotId.lotContents[k].pId] = {};
				   									productInfoInLot[aggregateOrderDetails.lotId.lotContents[k].pId]['perUnitPrice'] = perUnitPrice;
				   									productInfoInLot[aggregateOrderDetails.lotId.lotContents[k].pId]['quantityLeft'] = quantityLeft;
				   									productInfoInLot[aggregateOrderDetails.lotId.lotContents[k].pId]['initialTotalQuantity'] = initialTotalQuantity;
				   									productInfoInLot[aggregateOrderDetails.lotId.lotContents[k].pId]['quantitySold'] = quantitySold;
				   								}
				   								orderDetails['perUnitPrice'] = productInfoInLot[aggregateOrderDetails.orderDetails[j].pId._id].perUnitPrice;
				   								orderDetails['quantityLeft'] = productInfoInLot[aggregateOrderDetails.orderDetails[j].pId._id].quantityLeft;
				   								orderDetails['quantitySold'] = productInfoInLot[aggregateOrderDetails.orderDetails[j].pId._id].quantitySold;
				   								orderDetails['initialTotalQuantity'] = productInfoInLot[aggregateOrderDetails.orderDetails[j].pId._id].initialTotalQuantity;
				   								orderDetails['imagesArray'] = [];
				   								LotsUtils.GetProductImagesURLArray(aggregateOrderDetails.orderDetails[j].pId._id, function(err, imagesArray){
				   									if(err){
														return res.status(200).send({
															status: "error",
															message: ErrorParser.parseErrorreturnErrorMessage(err)
														});
				   									}else{
				   										orderDetails['imagesArray'] = imagesArray;
				   										tmp['orderDetails'].push(orderDetails);
				   										j+=1;
				   										asyncFor1(j);
				   									}
				   								});
				   							}else{
				   								i+=1;
				   								asyncFor(i);
				   							}
				   						}
				   						asyncFor1(j);

				   					}else{
				   						return res.status(200).send({status: "success", response: parsedResponseArray});
				   					}
				   				}
				   				asyncFor(i);		
				   			}
							else if(createdOnBefore && responseArray.length === 0){
								return res.status(200).send({status: "success", response: []});
							}else if(!createdOnBefore && responseArray.length === 0){
								return res.status(200).send({status: "error", message: "No aggregate buying orders placed till now."});
							}
							else{
								return res.status(200).send({status: "error", message: "No aggregate buying orders placed till now."});
							}
				   		}
				   });
	}else{
		return res.status(200).send({
			status: "error",
			message: "User not authenticated."
		});
	}

};


/**
@api {post} /buy/GetDirectBuyingOrdersOfUser/ Get Direct Buy Orders of User 
@apiName GetDirectBuyingOrdersOfUser 
@apiGroup Buy
@apiPermission authentication_required
@apiDescription Get a list of direct buy orders of a user. Limit and createdAt keys are used to manage pagination via the api.

@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization": "JWT eyJhbGHVbz5y8i-HRRY"
	} 

@apiParam {Number} [limit] limit Limit the number of search results, by default 10.
@apiParam {Date} [createdOnBefore] createdOnBefore Used for pagination
@apiParamExample {json} Request-Example:
	HTTP/1.1 200 OK
{
	"limit": 10
}

@apiSuccess {String} status Status of the API response
@apiSuccess {Array} response.lotId Id representing the lot from which the order was taken from
@apiSuccess {String} response.lotName Name of the lot
@apiSuccess {Date} response.createdAt Date at which the buy order was created
@apiSuccess {Boolean} response.isAggregate Is the buy order an aggregate type
@apiSuccess {Boolean} response.emdPaid Has the emd been paid for this buy order
@apiSuccess {String} response.totalAmountPaid Has the total amount been paid for this buy order
@apiSuccess {String} response.cancelBuyingOrder Has the buy order been cancelled
@apiSuccess {String} response.buyingOrderFulfilled Has the buy order been fulfilled (Delivered)
@apiSuccess {String} response.transactionSuccess Has the rest of the money for the buy order been paid successfully.
@apiSuccess {Number} response.emd Total EMD paid for this buy order
@apiSuccess {Number} response.totalAmount Total amount paid/to be paid for this buy order
@apiSuccess {Array} response.orderDetails Array of product information in the order
@apiSuccess {String} response.orderDetails.productId Id of the product
@apiSuccess {String} response.orderDetails.baseProductId Base product Id of the product
@apiSuccess {String} response.orderDetails.baseProductName Name of the base product
@apiSuccess {String} response.orderDetails.quantity Quantity remaining of the product
@apiSuccess {String} response.orderDetails.perUnitPrice Per unit price of the product
@apiSuccess {String} response.orderDetails.quantityLeft The quantity of product left till now
@apiSuccess {String} response.orderDetails.quantitySold The quantity of product sold
@apiSuccess {String} response.orderDetails.imagesArray Array of image urls representing the product
@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
{
  "status": "success",
  "response": [
    {
      "lotId": "58daa1f08e1b981cb77336a9",
      "lotName": "untitled lot",
      "createdAt": "2017-04-07T06:02:57.988Z",
      "isAggregate": false,
      "emdPaid": false,
      "totalAmountPaid": true,
      "cancelBuyingOrder": false,
      "buyingOrderFulfilled": false,
      "transactionSuccess": false,
      "totalAmount": 300000000,
      "orderDetails": [
        {
          "productId": "58daa1888e1b981cb77336a6",
          "baseProductId": "58d64822ecd24d1ea45e8cc7",
          "baseProductName": "iPhone 7",
          "quantity": 1,
          "perUnitPrice": 300000000,
          "quantityLeft": 0,
          "quantitySold": null,
          "imagesArray": [
            "http://urbanriwaaz.com:8080/api/getFile/productPicture/58e734c054f682e67e113475"
          ]
        }
      ]
    }
  ]
}
@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "No aggregate buying orders placed till now."
	}
*/
exports.GetDirectBuyingOrdersOfUser = function(req, res) {
	var user = req.user;
	var limit = (req.body.limit || 10);
	var createdOnBefore = req.body.createdOnBefore;

	var andQueries = {};
	var orQueries = [];

	andQueries.userId = user._id;
	andQueries.isAggregate = false;

	if (createdOnBefore) {
		andQueries.createdAt = {
			$lt: createdOnBefore
		};
	}


	var queryParams = {};
	if (orQueries.length > 0) {
		queryParams = {
			$and: [andQueries, {
				$or: orQueries
			}]
		};
	} else {
		queryParams = andQueries;
	}

	if(user){
		BuyingOrder.find(queryParams)
				   .populate({
				   		path: 'lotId',
				   		model: Lot
				   })
				   .populate({
				   		path: 'orderDetails.pId',
				   		model: Product,
				   		populate: {
				   			path: 'baseProductId',
				   			model: BaseProduct
				   		}
				   })
				   .limit(limit)
				   .sort('-createdAt')
				   .exec(function(err, responseArray){
				   		if(err){
							return res.status(200).send({
								status: "error",
								message: ErrorParser.parseErrorreturnErrorMessage(err)
							});
				   		}else{
				   			if(responseArray && responseArray.length>0){
				   				//return res.status(200).send(responseArray);
				   				let parsedResponseArray = [];
				   				let i = 0;
				   				function asyncFor(i){
				   					if(i<responseArray.length){
				   						let aggregateOrderDetails = responseArray[i];
				   						let tmp = {};
				   						tmp['buyingOrderId'] =  aggregateOrderDetails._id;
				   						tmp['lotId'] = aggregateOrderDetails.lotId._id;
				   						tmp['lotName'] = aggregateOrderDetails.lotId.lotName;
				   						tmp['startDate'] = aggregateOrderDetails.startDate;
				   						tmp['endDate'] = aggregateOrderDetails.endDate;
				   						tmp['createdAt'] = aggregateOrderDetails.createdAt;
				   						tmp['isAggregate'] = aggregateOrderDetails.isAggregate;
				   						tmp['transactionId'] = aggregateOrderDetails.transactionId;
				   						tmp['deliveryId'] = aggregateOrderDetails.deliveryId;
				   						tmp['walletTransactionId'] = aggregateOrderDetails.walletTransactionId;
				   						tmp['emdPaid'] = aggregateOrderDetails.emdPaid;  
				   						tmp['totalAmountPaid'] = aggregateOrderDetails.totalAmountPaid; 
				   						tmp['cancelBuyingOrder'] = aggregateOrderDetails.cancelBuyingOrder;
				   						tmp['buyingOrderFulfilled'] = aggregateOrderDetails.buyingOrderFulfilled;
				   						tmp['transactionSuccess'] = aggregateOrderDetails.transactionSuccess;
				   						tmp['emd'] = aggregateOrderDetails.emd; //TOTAL
				   						tmp['totalAmount'] = aggregateOrderDetails.totalAmount; //TOTAL
				   						tmp['orderDetails'] = [];
				   						parsedResponseArray.push(tmp);
				   						let j = 0;
				   						function asyncFor1(j){
				   							if(j<aggregateOrderDetails.orderDetails.length){
				   								let orderDetails = {};
				   								orderDetails['productId'] = aggregateOrderDetails.orderDetails[j].pId._id;
				   								orderDetails['baseProductId'] = aggregateOrderDetails.orderDetails[j].pId.baseProductId._id;
				   								orderDetails['baseProductName'] = aggregateOrderDetails.orderDetails[j].pId.baseProductId.productName;
				   								orderDetails['quantity'] = aggregateOrderDetails.orderDetails[j].quantity;

				   								let productInfoInLot = {};
				   								for(let k=0; k<aggregateOrderDetails.lotId.lotContents.length; k++){
				   									let perUnitPrice = aggregateOrderDetails.lotId.lotContents[k].price;
				   									let quantityLeft = aggregateOrderDetails.lotId.lotContents[k].quantity;
				   									let initialTotalQuantity = aggregateOrderDetails.lotId.lotContents[k].initialTotalQuantity;
				   									let quantitySold = null;
				   									productInfoInLot[aggregateOrderDetails.lotId.lotContents[k].pId] = {};
				   									productInfoInLot[aggregateOrderDetails.lotId.lotContents[k].pId]['perUnitPrice'] = perUnitPrice;
				   									productInfoInLot[aggregateOrderDetails.lotId.lotContents[k].pId]['quantityLeft'] = quantityLeft;
				   									productInfoInLot[aggregateOrderDetails.lotId.lotContents[k].pId]['initialTotalQuantity'] = initialTotalQuantity;
				   									productInfoInLot[aggregateOrderDetails.lotId.lotContents[k].pId]['quantitySold'] = quantitySold;
				   								}
				   								orderDetails['perUnitPrice'] = productInfoInLot[aggregateOrderDetails.orderDetails[j].pId._id].perUnitPrice;
				   								orderDetails['quantityLeft'] = productInfoInLot[aggregateOrderDetails.orderDetails[j].pId._id].quantityLeft;
				   								orderDetails['quantitySold'] = productInfoInLot[aggregateOrderDetails.orderDetails[j].pId._id].quantitySold;
				   								orderDetails['initialTotalQuantity'] = productInfoInLot[aggregateOrderDetails.orderDetails[j].pId._id].initialTotalQuantity;
				   								orderDetails['imagesArray'] = [];
				   								LotsUtils.GetProductImagesURLArray(aggregateOrderDetails.orderDetails[j].pId._id, function(err, imagesArray){
				   									if(err){
														return res.status(200).send({
															status: "error",
															message: ErrorParser.parseErrorreturnErrorMessage(err)
														});
				   									}else{
				   										orderDetails['imagesArray'] = imagesArray;
				   										tmp['orderDetails'].push(orderDetails);
				   										j+=1;
				   										asyncFor1(j);
				   									}
				   								});
				   							}else{
				   								i+=1;
				   								asyncFor(i);
				   							}
				   						}
				   						asyncFor1(j);

				   					}else{
				   						return res.status(200).send({status: "success", response: parsedResponseArray});
				   					}
				   				}
				   				asyncFor(i);		
				   			}
							else if(createdOnBefore && responseArray.length === 0){
								return res.status(200).send({status: "success", response: []});
							}else if(!createdOnBefore && responseArray.length === 0){
								return res.status(200).send({status: "error", message: "No aggregate buying orders placed till now."});
							}
							else{
								return res.status(200).send({status: "error", message: "No aggregate buying orders placed till now."});
							}
				   		}
				   });
	}else{
		return res.status(200).send({
			status: "error",
			message: "User not authenticated."
		});
	}

};



