var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/**
 * Buying Schema
 */


var BuyingSchema = new Schema({
  productId: {
    type: Schema.ObjectId,
    ref: 'Product',
    required: true
  },
  userId: {
    type: Schema.ObjectId,
    ref: 'User',
    required: true
  },
  transactionId: {
    type: Schema.ObjectId,
    ref: 'Transaction'
  },
  deliveryId:{
    type: Schema.ObjectId,
    ref: 'Delivery'
  },
  amount: {
    type: Number,
    required: true
  },
  boughtOnSale: {
    type: Boolean,
    default: false
  },
  quantityToBeBought: {
    type: Number,
    default: 1
  },
  dateBuyingOrderPlaced: {
    type: Date,
    default: new Date()
  },
  dateBuyingOrderCancelled: {
    type: Date
  },
  dateBuyingOrderFulfilled: {
    type: Date
  },
  cancelBuyingOrder: {
    type: Boolean,
    default: false
  },
  cancelledBy: {
    type: String
  },
  cancellationRemarks: {
    type: String
  },
  buyingOrderFulfilled: {
    type: Boolean,
    default: false
  },
  transactionSuccess: {
    type: Boolean,
    default: false
  }

});


module.exports = mongoose.model('Buying', BuyingSchema);
