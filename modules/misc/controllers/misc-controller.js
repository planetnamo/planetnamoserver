var mongoose = require('mongoose');
var multer  = require('multer');
var fs = require('fs');
var mime = require('mime');
var crypto = require('crypto');

var User            = require(__base + '/models/user/user.js')
var ProductCategory = require(__base + '/models/products/product-category.js');
var BaseProduct     = require(__base + '/models/products/base-product.js');
var Product         = require(__base + '/models/products/product.js')
var DefaultImages   = require(__base + '/models/defaultImages/defaultImages.js');
var FileHandler  = require(__utils + '/FileHandler.js');
var EmailHelper = require(__utils + '/MailSender.js');

var ErrorParser = require(__utils + '/errorParse.js');
var aux         = require(__utils + '/auxilaryFunc.js');
var usersUtils  = require(__utils + '/users.js');


exports.planetnamowelcome = function (req, res) {
  res.send("Welcome to the PlanetNamo API, this is the misc module")
};


var storage_defaultImages = multer.diskStorage({
	destination: function (request, file, callback) {
		callback(null, __base + '/uploads/defaults');
	},
	filename: function (request, file, callback) {
		console.log(file);
		//callback(null, file.originalname);
		crypto.pseudoRandomBytes(16, function (err, raw) {
		  callback(null, raw.toString('hex') + Date.now() + '.' + mime.extension(file.mimetype));
		});
	}
});
var upload_defaultImages = multer({storage: storage_defaultImages}).single('defaultImage');




/**
@api {post} /misc/SendEmail Send Email
@apiName Send Email
@apiGroup Misc
@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeader {String="ENWJNJENDjnksdja329njdkaknwk"} ClientKey Client Key for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"ClientKey": 	  "ENWJNJENDjnksdja329njdkaknwk"
	} 
@apiParam {String} name Name of customer
@apiParam {String} email Email of customer
@apiParam {String} message Message body of email
@apiParamExample {json} Request-Example:
	HTTP/1.1 200 OK
	{
		"name": "test_user",
		"email": "kaurn@kms.cim",
		"message": "Hi hello"
	}
@apiSuccess {String} status Status of the API Call.
@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
	{
	  "status": "success! Email sent to admin email",
	  "adminEmail": "karunk@live.com"
	}
@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
	  "error": "Inadequate information sent"
	}
*/
exports.SendEmail = function(req, res){
	var name = req.body.name;
	var email = req.body.email;
	var message = req.body.message;
	if(name && message && email){
	    EmailHelper.sendTextMail(message, aux.constants.admin_email, "Contact - Us form: "+String(name), function(err, response){
	      if(err){
	        res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)}); 
	      }else{
	        res.status(200).send({status: 'success! Email sent to admin email', adminEmail: aux.constants.admin_email});
	      }
	    });
	}else{
		return res.status(200).send({error: "Inadequate information."})
	}
};


exports.UploadDefaultImage = function(req, res){
	upload_defaultImages(req, res, function(err) {
		if(err) {
			return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
		}else {
			var file 	= req.file;
			var defaultImageId 	= req.params.defaultImageId; 
			console.log(file);
			if(file && defaultImageId){
				DefaultImages.findById(defaultImageId, function(err, defaultImage){
					if(err){
						return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
					}else{
						if(defaultImage){
							if(defaultImage.defaultImage.path){
								fs.unlink(defaultImage.defaultImage.path, function(err){
									if(err){
										defaultImage.defaultImage = file;
										defaultImage.save(function(err, updatedLot){
											if(err){
												return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
											}else{
												return res.status(200).send({status: "success", message: "Successfully added image. Previous image could not be found in database."});
											}
										});
									}else{
										defaultImage.defaultImage = file;
										defaultImage.save(function(err, updatedLot){
											if(err){
												return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
											}else{
												return res.status(200).send({status: "success", message: "Successfully added image. Previous image Successfully replaced and deleted."});
											}
										});
									}
								});
							}else{

								defaultImage.defaultImage = file;
								defaultImage.save(function(err, updatedLot){
									if(err){
										return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
									}else{
										return res.status(200).send({status: "success", message: "Successfully added image."});
									}
								});
							}
						}else{
							return res.status(200).send({status: 'error', message: "DefaultImage not found with defaultImageId: "+defaultImageId});
						}
					}
				});
			}else{
				return res.status(200).send({status: 'error', message: "Inadequate information sent"});
			}
		}
	});
};


exports.GetDefaultImages = function(req, res){
	DefaultImages.find({}, function(err, defaultImages){
		if(err){
			return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
		}else{
			return res.status(200).send({status: 'success', response: defaultImages});
		}
	});	
};

exports.CreateDefaultImage = function(req, res){
	var defaultImageName = req.body.defaultImageName;
	if(defaultImageName){
		var defaultImage = new DefaultImages();
		defaultImage.defaultImageName = defaultImageName;
		defaultImage.save(function(err, defaultImage){
			if(err){
				return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
			}else{
				return res.status(200).send({status: "success", message: "Default Image created successfully. Upload image next."})
			}
		});
	}else{
		return res.status(200).send({status: "error", message: "Inadequate information"});
	}
};





