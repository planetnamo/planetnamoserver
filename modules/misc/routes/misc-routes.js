var passport   = require('passport');
var Middleware = require(__base + '/middlewares/middleware.js');
var miscCtrl = require(__modules + '/misc/controllers/misc-controller.js');


module.exports = function(app, express){

	var apiRouter = express.Router();

	apiRouter.route('/welcome').get(miscCtrl.planetnamowelcome);
	
	apiRouter.route('/SendEmail').post(miscCtrl.SendEmail);

	apiRouter.route('/uploadDefaultImage/:defaultImageId').post(passport.authenticate('jwt', { session: false }), //done
											  Middleware.CheckIfAdmin,
											  miscCtrl.UploadDefaultImage);

	apiRouter.route('/getDefaultImages').get(passport.authenticate('jwt', { session: false }), //done
											  Middleware.CheckIfAdmin,
											  miscCtrl.GetDefaultImages);

	apiRouter.route('/createDefaultImage').post(passport.authenticate('jwt', { session: false }), //done
											  Middleware.CheckIfAdmin,
											  miscCtrl.CreateDefaultImage);

	return apiRouter;

};

