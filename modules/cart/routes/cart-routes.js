var cartCtrl = require(__modules + '/cart/controllers/cart-controller');
var Middleware = require(__base + '/middlewares/middleware.js');
var passport   = require('passport');


module.exports = function(app, express){

	var apiRouter = express.Router();

	apiRouter.route('/welcome').get(passport.authenticate('jwt', { session: false }),
									cartCtrl.planetnamowelcome);

	apiRouter.route('/addToCart/:lotId').post(passport.authenticate('jwt', { session: false }),
												cartCtrl.addToCart);

	apiRouter.route('/editCartItem/:cartItemId').post(passport.authenticate('jwt', { session: false }),
														cartCtrl.editCartItem);

	apiRouter.route('/removeFromCart/:cartItemId').delete(passport.authenticate('jwt', { session: false }),
															cartCtrl.removeFromCart);

	apiRouter.route('/getCart').get(passport.authenticate('jwt', { session: false }),
									cartCtrl.getCart);




	
	
	return apiRouter;

};