/**
 * Module dependencies.
 */
 
var mongoose = require('mongoose');

var User        = require(__base + '/models/user/user.js')
var Wishlist    = require(__base + '/models/user/wishlist.js');
var Address     = require(__base + '/models/user/address.js');
var Lot 	        = require(__base + '/models/lots/lot');
var BaseProduct 	= require(__base + '/models/products/base-product');
var Product 		= require(__base + '/models/products/product');
var Cart 			= require(__base + '/models/user/cart');
var CartItem 		= require(__base + '/models/user/cartItem');


var ErrorParser = require(__utils + '/errorParse.js');
var aux         = require(__utils + '/auxilaryFunc.js');
var UsersUtils  = require(__utils + '/users.js');
var FileHandler  = require(__utils + '/FileHandler.js');

var multer  = require('multer');
var fs = require('fs');
var mime = require('mime');
var crypto = require('crypto');


var storage = multer.diskStorage({
	destination: function (request, file, callback) {
		callback(null, __base + '/uploads/userDocs');
	},
	filename: function (request, file, callback) {
		console.log(request.user.username);
		callback(null, request.user.username + Date.now() + '.' + mime.extension(file.mimetype));
		/*crypto.pseudoRandomBytes(16, function (err, raw) {
		  callback(null, raw.toString('hex') + Date.now() + '.' + mime.extension(file.mimetype));
		});*/
	}
});
var upload = multer({storage: storage}).single('registerationDoc');

exports.planetnamowelcome = function (req, res) {
  res.send("Welcome to the PlanetNamo API, cart module")
};







//-----------------A D D----------T O----------------C A R T----------------------------------------------------------------------------

/**
@api {post} /cart/addToCart/:lotId Add to cart

@apiName AddToCart
@apiGroup Cart
@apiDescription Add an order (specifying which products and their quantities) from a final lot to the user cart
@apiPermission authorization_required

@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization":  "JWT eyJhbGHVbz5y8i-HRRY"
	} 

@apiParam {String} :lotId Unique lotId representing a final lot. Sent as a URL parameter
@apiParam {String} orderDetails A dictionary with key as productId present in the lot and value as the quantity to buy.
@apiParamExample {json} Request-Example:
{
	"orderDetails": {
		"58a73bbf71cd35dab7a47ade": 1	
	}
}

@apiSuccess {string} status status of the api result
@apiSuccess {JSON} cart cart will an object containing newly updated cart information
@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
	{
	  "status": "success",
	  "cart": {
	    "__v": 0,
	    "updatedAt": "2017-03-08T13:07:39.360Z",
	    "createdAt": "2017-03-08T13:07:39.360Z",
	    "userId": "58a70a784ded7c86e5e829e6",
	    "_id": "58c0021b24f499069bc5702a",
	    "cartItems": [
	      "58c0021b24f499069bc5702b"
	    ]
	  }
	}

@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "Inadequate information sent"
	}
*/
exports.addToCart = function (req, res) {
	var user = res.req.user;
	var lotId = req.params.lotId;
	var orderDetails = req.body.orderDetails;

	if(user && lotId && orderDetails){
		Lot.findById(lotId, function(err, lot){
			if(err){
				return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
			}else{
				if(lot){
					if(lot.lotSoldOut == false){
						if(lot.endDate > new Date()){
							if(lot.lotType == 'buy' || lot.lotType == 'rent'){
								var verifiedOrderDetails = [];
								var totalAmountToPay = 0;
								for(var i=0; i<lot.lotContents.length; i++){
									if(lot.lotContents[i].pId in orderDetails){
										if(orderDetails[lot.lotContents[i].pId] <= lot.lotContents[i].quantity){
											var tmp = {};
											tmp['pId'] = lot.lotContents[i].pId;
											tmp['quantity'] = orderDetails[lot.lotContents[i].pId];
											tmp['priceOfProductWhileAdding'] = lot.lotContents[i].price;
											verifiedOrderDetails.push(tmp);
											totalAmountToPay = totalAmountToPay + (orderDetails[lot.lotContents[i].pId]*lot.lotContents[i].price);
										}else{
											return res.status(200).send({status: 'error', message: "Cannot buy more than existing quantity. Incorrect orderDetails. Cannot add this to cart."});
										}
									}
								}
								if(verifiedOrderDetails.length == 0){
									return res.status(200).send({status: "error", message: "Cart Item cannot be empty. Either your order details is empty or no product from order details matches the lot contents."});
								}
								CartItem.findOne({userId: user._id, lotId: lotId})
										.exec(function(err, cartItem){
											if(err){
												return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
											}else{
												if(cartItem){
													return res.status(200).send({status: 'error', message: 'Lot already added to cart.', cartItem: cartItem});
												}else{
													Cart.findOne({userId: user._id})
														.exec(function(err, cart){
															if(err){
																return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
															}else{
																if(!cart){
																	cart = new Cart();
																	cart.userId = user._id;
																	cart.cartItems = [];
																}else{
																	if(cart.cartItems.length > aux.constants.maxCartItems){
																		return res.status(200).send({status: 'error', message: "Max cart items limit exceeded"});
																	}
																}
																let cartItem = new CartItem();
																cartItem.userId = user._id;
																cartItem.cartId = cart._id;
																cartItem.lotId = lotId;
																cartItem.orderDetails = verifiedOrderDetails;
																console.log(verifiedOrderDetails);
																console.log(cartItem);
																cartItem.save(function(err, saverCartItem){
																	if(err){
																		return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
																	}else{
																		cart.cartItems.push(saverCartItem._id);
																		cart.save(function(err, updatedCart){
																			if(err){
																				return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
																			}else{
																				//return res.status(200).send({status: 'success', cart: updatedCart});
																				Cart.findOne({userId: user._id})
																					.populate({
																						path: 'cartItems',
																						model: CartItem
																					})
																					.exec(function(err, userCart){
																						if(err){
																							return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
																						}else{
																							return res.status(200).send({status: 'success', cart: userCart});
																						}
																					});
																			}
																		});
																	}
																});
															}
														});
												}
											}
										});
							}else{
								return res.status(200).send({status: 'error', message: "Only buying and rental type lots can be added to cart."})
							}
						}else{
							return res.status(200).send({status: 'error', message: "Lot has expired. expired on : " + lot.endDate +". Cannot add to cart."});
						}
					}else{
						return res.status(200).send({status: 'error', message: "Lot sold out, cannot add to cart."});
					}
				}else{
					return res.status(200).send({status: 'error', message: "No lotId found for ID:"+lotId});
				}
			}
		});
	}else{
		return res.status(200).send({status: 'error', message: "Inadequate information sent"});
	}

};

//-------------------------------------------------------------------------------------------------------------------------------------








//----------------------------R E M O V E---------------F R O M-------------C A R T---------------------------------------------------------------

/**
@api {delete} /cart/removeFromCart/:cartItemId Remove from cart
@apiName RemoveFromCart
@apiGroup Cart
@apiDescription Remove a cartItem from cart. CartItem is specified by cartItemId.
@apiPermission authorization_required

@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization":  "JWT eyJhbGHVbz5y8i-HRRY"
	} 

@apiParam {String} :cartItemId Unique cartItemId representing a cart Item. Sent as a URL parameter
@apiParamExample {url} Request-Example:
	http://localhost:8080/api/cart/removeFromCart/58c0021b24f499069bc5702b

@apiSuccess {string} status status of the api result
@apiSuccess {JSON} response Newly updated cart
@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
	{
	  "status": "success",
	  "cart": {
	    "_id": "58c0021b24f499069bc5702a",
	    "updatedAt": "2017-03-08T13:38:51.956Z",
	    "createdAt": "2017-03-08T13:07:39.360Z",
	    "userId": "58a70a784ded7c86e5e829e6",
	    "__v": 1,
	    "cartItems": []
	  }
	}

@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "Cart Item not present in cart."
	}
*/
exports.removeFromCart = function (req, res) {
	var cartItemId = req.params.cartItemId;
	var user = res.req.user;
	Cart.findOne({userId: user._id})	
		.exec(function(err, cart){
			if(err){
				return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
			}else{
				if(cart){
					if(cart.cartItems.some(function(existingCartItem){
		   				return existingCartItem.equals(cartItemId);	
		   			})){
						var index = cart.cartItems.indexOf(cartItemId);
						if(index!=-1)
							cart.cartItems.splice(index, 1);
						cart.save(function(err, updatedCart){
							if(err){
								return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
							}else{
								CartItem.remove({_id: cartItemId}, function(err){
									if(err){
										return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
									}else{
										return res.status(200).send({status: 'success', response: updatedCart});
									}
								});
							}
						});
					}else{
						//cart item not present in cart array but just making sure a dead cart item isnt present
						CartItem.remove({_id: cartItemId}, function(err){
							if(err){
								return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
							}else{
								return res.status(200).send({status: 'success', response: cart});
							}
						});
					}
				}else{
					return res.status(200).send({status: 'error', message: "Cart not found"});
				}
			}
		});

};

//-------------------------------------------------------------------------------------------------------------------------------------










//------------------------------G E T--------------C A R T-------------D E T A I L S-------------------------------------------------------------


/**
@api {get} /cart/getCart Get cart details

@apiName Get cart details
@apiGroup Cart
@apiDescription Get details of user cart
@apiPermission authorization_required

@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization":  "JWT eyJhbGHVbz5y8i-HRRY"
	} 


@apiSuccess {string} status status of the api result
@apiSuccess {JSON} cart cart will an object containing cart information
@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
{
  "status": "success",
  "cart": {
    "_id": "58cbc87660f62b3c3b0bf50b",
    "updatedAt": "2017-03-17T12:31:28.134Z",
    "createdAt": "2017-03-17T11:28:54.168Z",
    "userId": "58c14475ef21b44e74ed817d",
    "__v": 15,
    "cartItems": [
      {
        "_id": "58cbd7202e34ac46e8b52769",
        "updatedAt": "2017-03-17T12:40:33.537Z",
        "createdAt": "2017-03-17T12:31:28.122Z",
        "lotId": "58c80b1439548bf3ce47304d",
        "cartId": "58cbc87660f62b3c3b0bf50b",
        "userId": "58c14475ef21b44e74ed817d",
        "__v": 2,
        "orderDetails": [
          {
            "pId": {
              "_id": "58c2531708b1918e667c853d",
              "productRegisteredByUser": "58c14475ef21b44e74ed817d",
              "workingcondition": true,
              "originalbox": true,
              "validbill": true,
              "condition": 3,
              "ageinmonths": 3,
              "baseProductId": {
                "_id": "58c159feab6d4359ad8234ec",
                "color": "Silver, Gold",
                "item_weight": "159 g",
                "operating_system": "iOS",
                "warranty_n_support": "1 year manufacturer warranty for device and 6 months manufacturer warranty for in-box accessories including batteries from the date of purchase",
                "model_number": "7",
                "brand_name": "Apple",
                "productCategoryId": "58c151d3ab6d4359ad8234d4",
                "productName": "iPhone 5s",
                "__v": 5,
                "finalLots": [
                  "58c3b0f9248e1526a93b6c70",
                  "58c3b7265520b02d9f76a1a0",
                  "58c3e1447573273ed747a1c5",
                  "58c80ab53096cdf3988fc97b",
                  "58c80b1439548bf3ce47304d"
                ],
                "problemsList": [
                  {
                    "problemname": "23",
                    "score": 50,
                    "remarks": "8978766",
                    "_id": "58c159feab6d4359ad8234ee"
                  }
                ],
                "accessoriesList": [
                  {
                    "accessoryname": "Charger",
                    "score": 50,
                    "_id": "58c159feab6d4359ad8234ed"
                  }
                ],
                "marketCost": null
              },
              "__v": 5,
              "finalLots": [
                "58c3b7265520b02d9f76a1a0",
                "58c3e1447573273ed747a1c5",
                "58c80ab53096cdf3988fc97b",
                "58c80b1439548bf3ce47304d"
              ],
              "problemslistavail": [
                false
              ],
              "accessorieslistavail": [
                true
              ],
              "planetNamoCost": 1000
            },
            "quantity": 1,
            "priceOfProductWhileAdding": 30000,
            "_id": "58cbd9411b2ffb4891938774"
          },
          {
            "pId": {
              "_id": "58c2531708b1918e667c853d",
              "productRegisteredByUser": "58c14475ef21b44e74ed817d",
              "workingcondition": true,
              "originalbox": true,
              "validbill": true,
              "condition": 3,
              "ageinmonths": 3,
              "baseProductId": {
                "_id": "58c159feab6d4359ad8234ec",
                "color": "Silver, Gold",
                "item_weight": "159 g",
                "operating_system": "iOS",
                "warranty_n_support": "1 year manufacturer warranty for device and 6 months manufacturer warranty for in-box accessories including batteries from the date of purchase",
                "model_number": "7",
                "brand_name": "Apple",
                "productCategoryId": "58c151d3ab6d4359ad8234d4",
                "productName": "iPhone 5s",
                "__v": 5,
                "finalLots": [
                  "58c3b0f9248e1526a93b6c70",
                  "58c3b7265520b02d9f76a1a0",
                  "58c3e1447573273ed747a1c5",
                  "58c80ab53096cdf3988fc97b",
                  "58c80b1439548bf3ce47304d"
                ],
                "problemsList": [
                  {
                    "problemname": "23",
                    "score": 50,
                    "remarks": "8978766",
                    "_id": "58c159feab6d4359ad8234ee"
                  }
                ],
                "accessoriesList": [
                  {
                    "accessoryname": "Charger",
                    "score": 50,
                    "_id": "58c159feab6d4359ad8234ed"
                  }
                ],
                "marketCost": null
              },
              "__v": 5,
              "finalLots": [
                "58c3b7265520b02d9f76a1a0",
                "58c3e1447573273ed747a1c5",
                "58c80ab53096cdf3988fc97b",
                "58c80b1439548bf3ce47304d"
              ],
              "problemslistavail": [
                false
              ],
              "accessorieslistavail": [
                true
              ],
              "planetNamoCost": 1000
            },
            "quantity": 1,
            "priceOfProductWhileAdding": 35000,
            "_id": "58cbd9411b2ffb4891938773"
          }
        ]
      }
    ]
  }
}

@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "Cart is empty"
	}
*/
exports.getCart = function (req, res) {
	var user = res.req.user;
	Cart.findOne({userId: user._id})
		.populate({
			path: 'cartItems',
			model: CartItem,
			populate: {
				path: 'orderDetails.pId',
				model: Product,
				populate: {
					path: 'baseProductId',
					model: BaseProduct
				}
			}
		})
		.exec(function(err, cart){
			if(err){
				return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
			}else{
				if(cart.cartItems.length>0){
					return res.status(200).send({status: 'success', cart: cart});
				}else{
					return res.status(200).send({status: 'error', message: "Cart is empty"});
				}
			}
		});
};



//-------------------------------------------------------------------------------------------------------------------------------------






//------------------E D I T------------C A R T----------I T E M---------------------------------------------------------------------------------

/**
@api {post} /cart/editCartItem/:cartItemId Edit Cart Item
@apiName EditCartItem
@apiGroup Cart
@apiDescription Edit the quantities of a cart Item
@apiPermission authorization_required

@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization":  "JWT eyJhbGHVbz5y8i-HRRY"
	} 

@apiParam {String} :cartItemId Unique cartItemId representing a cart Item. Sent as a URL parameter
@apiParam {String} orderDetails A dictionary with key as productId present in the lot and value as the quantity to buy. The product Ids must match the product Ids already present in the cart item. New product Ids present in the lot contents can be added. 
@apiParamExample {json} Request-Example:
{
	"orderDetails": {
		"58a73bbf71cd35dab7a47ade": 1	
	}
}

@apiSuccess {string} status status of the api result
@apiSuccess {JSON} cart cart will an object containing newly updated cart information
@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
{
  "status": "success",
  "message": "Cart updated",
  "response": {
    "_id": "58cbd7202e34ac46e8b52769",
    "updatedAt": "2017-03-17T12:40:33.537Z",
    "createdAt": "2017-03-17T12:31:28.122Z",
    "lotId": {
      "_id": "58c80b1439548bf3ce47304d",
      "endDate": "2018-01-01T18:01:23.000Z",
      "startDate": "2017-01-01T18:01:23.000Z",
      "lotType": "buy",
      "createdByUser": "58c14475ef21b44e74ed817d",
      "__v": 1,
      "condition": [
        3
      ],
      "validbill": [
        true
      ],
      "originalbox": [
        true
      ],
      "workingcondition": [
        true
      ],
      "ratings": [],
      "releaseDate": [],
      "processor_brand": [],
      "processor_count": [],
      "color": [
        "Silver, Gold"
      ],
      "operating_system": [
        "iOS"
      ],
      "model_number": [
        "7"
      ],
      "hard_drive": [],
      "ram": [],
      "display_technology": [],
      "processor": [],
      "screen_resolution": [],
      "screen_size": [],
      "productCategories": [
        "58c151d3ab6d4359ad8234d4"
      ],
      "brands": [
        "Apple"
      ],
      "isClearanceStock": false,
      "isChildAuctionLot": false,
      "rentalLotDetails": [],
      "lotSoldOut": false,
      "lotContents": [
        {
          "pId": "58c2531708b1918e667c853d",
          "quantity": 1,
          "price": 30000,
          "emdDeposit": 5000,
          "approvedLotId": "58c3b058248e1526a93b6c6e",
          "registeredByUser": "58c14475ef21b44e74ed817d",
          "_id": "58c80b1439548bf3ce47304f"
        },
        {
          "pId": "58c2531708b1918e667c853d",
          "quantity": 1,
          "price": 35000,
          "emdDeposit": 5200,
          "approvedLotId": "58c3b058248e1526a93b6c6e",
          "registeredByUser": "58c14475ef21b44e74ed817d",
          "_id": "58c80b1439548bf3ce47304e"
        }
      ],
      "dateCreated": "2017-03-14T15:24:04.631Z"
    },
    "cartId": "58cbc87660f62b3c3b0bf50b",
    "userId": "58c14475ef21b44e74ed817d",
    "__v": 2,
    "orderDetails": [
      {
        "pId": "58c2531708b1918e667c853d",
        "quantity": 1,
        "priceOfProductWhileAdding": 30000,
        "_id": "58cbd9411b2ffb4891938774"
      }
    ]
  }
}

@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "Cannot buy more than existing quantity. Incorrect orderDetails. Cannot add this to cart. Either you have made this error or some quantity is less than or equal to zero."
	}
*/
exports.editCartItem = function (req, res){
	var user = res.req.user;
	var orderDetails = req.body.orderDetails;
	var cartItemId = req.params.cartItemId;

	if(user && orderDetails && cartItemId){
		CartItem.findById(cartItemId)
				.populate({
					path: 'lotId',
					model: Lot
				})
			    .exec(function(err, cartItem){
			    	if(err){
			    		return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
			    	}else{
			    		if(cartItem){
			    			var lotContents = cartItem.lotId.lotContents;
							var verifiedOrderDetails = [];
							var totalAmountToPay = 0;
							for(var i=0; i<lotContents.length; i++){
								if(lotContents[i].pId in orderDetails){
									if(orderDetails[lotContents[i].pId] <= lotContents[i].quantity && orderDetails[lotContents[i].pId]>0){
										var tmp = {};
										tmp['pId'] = lotContents[i].pId;
										tmp['quantity'] = orderDetails[lotContents[i].pId];
										tmp['priceOfProductWhileAdding'] = lotContents[i].price;
										verifiedOrderDetails.push(tmp);
										totalAmountToPay = totalAmountToPay + (orderDetails[lotContents[i].pId]*lotContents[i].price);
									}else{
										return res.status(200).send({status: 'error', message: "Cannot buy more than existing quantity. Incorrect orderDetails. Cannot add this to cart. Either you have made this error or some quantity is less than or equal to zero."});
									}
								}
							}
							if(verifiedOrderDetails.length == 0){
								return res.status(200).send({status: "error", message: "Cart Item cannot be empty. Either your order details is empty or no product from order details matches the lot contents."});
							}
							cartItem.orderDetails = verifiedOrderDetails;
							cartItem.save(function(err, updatedCartItem){
								if(err){
									return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
								}else{
									return res.status(200).send({status: 'success', message: 'Cart updated', response: updatedCartItem});
								}
							});
			    		}else{
			    			return res.status(200).send({status: 'error', message: "Cart Item not found."});
			    		}
			    	}
			    });
	}else{
		return res.status(200).send({status: 'error', message: "Inadequate information"});
	}
};


//-------------------------------------------------------------------------------------------------------------------------------------



