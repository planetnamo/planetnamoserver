var express = require('express');
var router = express.Router();

var path = require('path');
var fs = require('fs');
var mime = require('mime');

var User = require(__base + '/models/user/user.js');
var BaseProduct = require(__base + '/models/products/base-product');
var Product = require(__base + '/models/products/product');
var Lot = require(__base + '/models/lots/lot');
var DefaultImages = require(__base + '/models/defaultImages/defaultImages.js');
var ProductImage = require(__base + '/models/products/productImages.js');


var ErrorParser = require(__utils + '/errorParse.js');


const defaultProductImageId = '58db8a9e5261c42a9183c692';
const defaultLotImageId = '58db8a9e5261c42a9183c692';
const defaultBaseProductImageId = '58db8a9e5261c42a9183c692';
const defaultProfilePictureId = '58db8a9e5261c42a9183c692';

sendDefaultImageById = function(defaultImageId, res) {
  // DefaultImages.findById(defaultImageId, function(err, defaultImage) {
  //   if (err) {
  //     res.writeHead(404, {
  //       'Content-Type': 'text/plain'
  //     });
  //     res.write('404 Not Found\n');
  //     res.end();
  //     return;
  //   } else {
      var filename = "defaultImage.png";
      if (filename) {
        fs.stat('./uploads/defaults/' + filename, function(err, stat) {
          if (err) {
            res.writeHead(404, {
              'Content-Type': 'text/plain'
            });
            res.write('404 Not Found\n');
            res.end();
            return;
          }
          var mimeType = mime.lookup('defaultImage.png');
          res.writeHead(200, {
            'Content-Type': mimeType
          });

          var fileStream = fs.createReadStream('./uploads/defaults/' + filename);
          fileStream.pipe(res);
        }); //end path.exists
      } else {
        return res.status(200).send({
          status: 'error',
          message: "Default Image not uploaded."
        });
      }
    // }
  // });
};


/**
@api {get} getFile/userDocs/:userId Get User Registration Document
@apiName GetUserRegistrationDocument
@apiGroup getFile
@apiDescription Get the registration document corresponding to :userId

@apiError error Error message.
@apiErrorExample Error-Response:
  HTTP/1.1 200 Error
  {
    "status": "error",
    "message": "Invalid userId"
  }
*/
//VIEW USER REGISTERATION DOCUMENT by Id-----------------------------------------------------------------------------------
router.get('/userDocs/:userId', function(req, res) {
  var loggedInUser = req.user;
  var userId = req.params.userId;

  User.findById(userId, function(err, user) {
    if (err) {
      return res.status(200).send({
        status: 'error',
        message: ErrorParser.parseErrorreturnErrorMessage(err)
      });
    } else {
      if (user) {
        var filename = user.registerationDoc.filename;
        fs.stat('./uploads/userDocs/' + filename, function(err, stat) {
          if (err) {
            res.writeHead(404, {
              'Content-Type': 'text/plain'
            });
            res.write('404 Not Found\n');
            res.end();
            return;
          }
          //var mimeType = mime.lookup('defaultImage.jpg');
          //res.writeHead(200, {'Content-Type':mimeType});

          var fileStream = fs.createReadStream('./uploads/userDocs/' + filename);
          fileStream.pipe(res);

        }); //end path.exists
      } else {
        return res.status(200).send({
          status: "error",
          message: "Invalid user Id"
        });
      }
    }
  });
});
//----------------------------------------------------------------------------------------------------------------



/**
@api {get} getFile/profilePicture/:userId Get User Profile Picture
@apiName GetUserProfilePicture
@apiGroup getFile
@apiDescription Get the profile picture corresponding to :userId

@apiError error Error message.
@apiErrorExample Error-Response:
  HTTP/1.1 200 Error
  {
    "status": "error",
    "message": "Invalid userId"
  }
*/
//VIEW PROFILE PICTURE by id---------------------------------------------------------------------------------------------
router.get('/profilePicture/:userId', function(req, res) {
  var userId = req.params.userId;
  User.findById(userId, function(err, user) {
    if (err) {
      return sendDefaultImageById(defaultProfilePictureId, res);
    } else {
      if (user) {
        var filename = user.profilePicture.filename;
        fs.stat('./uploads/profilePictures/' + filename, function(err, stat) {
          if (err) {
            return sendDefaultImageById(defaultProfilePictureId, res);
          }
          var mimeType = mime.lookup('defaultImage.jpg');
          res.writeHead(200, {
            'Content-Type': mimeType
          });

          var fileStream = fs.createReadStream('./uploads/profilePictures/' + filename);
          fileStream.pipe(res);

        }); //end path.exists
      } else {
        return sendDefaultImageById(defaultProfilePictureId, res);
      }
    }
  });
});
//----------------------------------------------------------------------------------------------------------------



/**
@api {get} getFile/baseProductPicture/:baseProductId Get Base Product Picture
@apiName GetBaseProductPicture
@apiGroup getFile
@apiDescription Get the base product image for the corresponding baseProductId

@apiError error Error message.
@apiErrorExample Error-Response:
  HTTP/1.1 200 Error
  {
    "status": "error",
    "message": "Invalid baseProductId"
  }
*/
//VIEW BASE PRODUCT IMAGES by :baseProductId------------------------------------------------------------------------------------------
router.get('/baseProductPicture/:baseProductId', function(req, res) {
  var baseProductId = req.params.baseProductId;
  BaseProduct.findById(baseProductId, function(err, baseProduct) {
    if (err) {
      return sendDefaultImageById(defaultBaseProductImageId, res);
    } else {
      if (baseProduct) {
        var filename = baseProduct.productImage.filename;
        console.log(filename);
        fs.stat('./uploads/baseProductPictures/' + filename, function(err, stat) {
          if (err) {
            return sendDefaultImageById(defaultBaseProductImageId, res);
          }
          var mimeType = mime.lookup('defaultImage.jpg');
          res.writeHead(200, {
            'Content-Type': mimeType
          });

          var fileStream = fs.createReadStream('./uploads/baseProductPictures/' + filename);
          fileStream.pipe(res);
        }); //end path.exists
      } else {
        return sendDefaultImageById(defaultBaseProductImageId, res);
      }
    }
  });
});
//----------------------------------------------------------------------------------------------------------------



/**
@api {get} getFile/lotPicture/:lotId Get Lot Image
@apiName GetLotImage
@apiGroup getFile
@apiDescription Get the image representation of the lot

@apiError error Error message.
@apiErrorExample Error-Response:
  HTTP/1.1 200 Error
  {
    "status": "error",
    "message": "Invalid lotId"
  }
*/
//VIEW LOT IMAGE------------------------------------------------------------------------------------------
router.get('/lotPicture/:lotId', function(req, res) {
  var lotId = req.params.lotId;
  Lot.findById(lotId, function(err, lot) {
    if (err) {
      return sendDefaultImageById(defaultLotImageId, res);
    } else {
      if (lot) {
        var filename = lot.lotImage.filename;
        if (filename) {
          fs.stat('./uploads/lotPictures/' + filename, function(err, stat) {
            if (err) {
              return sendDefaultImageById(defaultLotImageId, res);
            }
            var mimeType = mime.lookup('defaultImage.jpg');
            res.writeHead(200, {
              'Content-Type': mimeType
            });

            var fileStream = fs.createReadStream('./uploads/lotPictures/' + filename);
            fileStream.pipe(res);
          }); //end path.exists
        } else {
          return sendDefaultImageById(defaultLotImageId, res);
        }
      } else {
          return sendDefaultImageById(defaultLotImageId, res);
      }
    }
  });
});
//----------------------------------------------------------------------------------------------------------------



/**
@api {get} getFile/productPicture/:productImageId Get Product Image
@apiName GetProductImage
@apiGroup getFile
@apiDescription Get a single product image corresponding to productImageId

@apiError error Error message.
@apiErrorExample Error-Response:
  HTTP/1.1 200 Error
  {
    "status": "error",
    "message": "Invalid productImageId"
  }
*/
//VIEW PRODUCT IMAGES------------------------------------------------------------------------------------------

router.get('/productPicture/:productImageId', function(req, res) {
  var productImageId = req.params.productImageId;
  ProductImage.findById(productImageId, function(err, productImage) {
    if (err) {
      return sendDefaultImageById(defaultProductImageId, res);
    } else {
      if (productImage) {
        var filename = productImage.image.filename;
        fs.stat('./uploads/productPictures/' + filename, function(err, stat) {
          if (err) {
            return sendDefaultImageById(defaultProductImageId, res);
          }
          var mimeType = mime.lookup('defaultImage.jpg');
          res.writeHead(200, {
            'Content-Type': mimeType
          });

          var fileStream = fs.createReadStream('./uploads/productPictures/' + filename);
          fileStream.pipe(res);
        }); //end path.exists
      } else {
        return sendDefaultImageById(defaultProductImageId, res);
      }
    }
  });
});
//----------------------------------------------------------------------------------------------------------------



//VIEW PRODUCT CATEGORY IMAGES------------------------------------------------------------------------------------------
router.get('/productCategoryPicture/:filename', function(req, res) {
  fs.stat('./uploads/productCategoryPictures/' + req.params.filename, function(err, stat) {
    if (err) {
      res.writeHead(404, {
        'Content-Type': 'text/plain'
      });
      res.write('404 Not Found\n');
      res.end();
      return;
    }
    var mimeType = mime.lookup('defaultImage.jpg');
    res.writeHead(200, {
      'Content-Type': mimeType
    });

    var fileStream = fs.createReadStream('./uploads/productCategoryPictures/' + req.params.filename);
    fileStream.pipe(res);
  }); //end path.exists
});
//----------------------------------------------------------------------------------------------------------------



//----------------------------------------------------------------------------------------------------------------
router.get('/:filename', function(req, res) {

  fs.stat('./uploads/' + req.params.filename, function(err, stat) {
    if (err) {
      res.writeHead(404, {
        'Content-Type': 'text/plain'
      });
      res.write('404 Not Found\n');
      res.end();
      return;
    }
    var mimeType = mime.lookup('defaultImage.jpg');
    res.writeHead(200, {
      'Content-Type': mimeType
    });

    var fileStream = fs.createReadStream('./uploads/' + req.params.filename);
    fileStream.pipe(res);

  }); //end path.exists
});
//----------------------------------------------------------------------------------------------------------------



//VIEW DEFAULT IMAGES--------------------------------------------------------------------------------------------------
router.get('/defaults/:filename', function(req, res) {

  fs.stat('./uploads/defaults/' + req.params.filename, function(err, stat) {
    if (err) {
      res.writeHead(404, {
        'Content-Type': 'text/plain'
      });
      res.write('404 Not Found\n');
      res.end();
      return;
    }
    var mimeType = mime.lookup('defaultImage.jpg');
    res.writeHead(200, {
      'Content-Type': mimeType
    });

    var fileStream = fs.createReadStream('./uploads/defaults/' + req.params.filename);
    fileStream.pipe(res);

  }); //end path.exists
});
//----------------------------------------------------------------------------------------------------------------



module.exports = router;