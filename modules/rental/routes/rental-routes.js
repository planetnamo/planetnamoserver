var passport 	= require("passport");
var rentalCtrl  = require(__modules + '/rental/controllers/rental-controller');
var Middleware  = require(__base + '/middlewares/middleware');

module.exports = function(app, express){

	var apiRouter = express.Router();


	
	apiRouter.route('/welcome').get(rentalCtrl.planetnamowelcome);


	
	apiRouter.route('/checkAvailability/:lotId').post(passport.authenticate('jwt', { session: false }),
													rentalCtrl.RentAvailability);

	
	apiRouter.route('/ReturnRental/:rentalOrderId').post(passport.authenticate('jwt', { session: false }),
													Middleware.CheckIfAdmin,
													rentalCtrl.ReturnRental);	

	apiRouter.route('/GetAllRentalOrdersByUser/:userId').get(passport.authenticate('jwt', { session: false }),
													rentalCtrl.GetAllRentalOrdersByUser);

	apiRouter.route('/GetAllRentalOrdersOnALot/:lotId').post(passport.authenticate('jwt', { session: false }),
													Middleware.CheckIfAdmin,
													rentalCtrl.GetAllRentalOrdersOnALot);	

	apiRouter.route('/PayRemainingRent/:lotId').post(passport.authenticate('jwt', { session: false }),
													rentalCtrl.PayRemainingRent);

	apiRouter.route('/Rent/:lotId').post(passport.authenticate('jwt', { session: false }),
													rentalCtrl.Rent);

	apiRouter.route('/GetNumberOfSuccessfulRentalOrdersPlacedTillNow').get(rentalCtrl.GetNumberOfSuccessfulRentalOrdersPlacedTillNow);


	return apiRouter;

};