var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/**
 * Rental Schema
 */


var RentalSchema = new Schema({
  productId: {
    type: Schema.ObjectId,
    ref: 'Product',
    required: true
  },
  userId: {
    type: Schema.ObjectId,
    ref: 'User',
    required: true
  },
  transactionId: {
    type: Schema.ObjectId,
    ref: 'Transaction'
  },
  wasLastTransactionASuccess: {
    type: Boolean,
    default: false
  },
  deliveryId:{
    type: Schema.ObjectId,
    ref: 'Delivery'
  },
  quantityToBeRented: {
    type: Number,
    required: true
  },
  rentalAmountTotal: {
    type: Number,
    required: true
  },
  dateRentalOrderPlaced: {
    type: Date,
    default: new Date()
  },
  startRentalDate: {
    type: Date,
    required: true
  },
  endRentalDate: {
    type: Date,
    required: true
  },
  isRentalActive: {
    type: Boolean,
    default: false
  },
  cancelRentalOrder: {
    type: Boolean,
    default: false
  },
  cancelledBy: {
    type: String
  },
  cancellationRemarks: {
    type: String
  },
  dateRentalOrderCancelled: {
    type: Date
  },
  numberOfExtensions: {
    type: Number,
    default: 0   // Number of times rent is extended (not counting the first rental)
  },
  extensionInformation: [{  //Start filling when numberOfExtensions>1
    startRentalDate: Date,
    endRentalDate: Date,
    transactionId: Schema.ObjectId,
    transactionSuccess: Boolean,
    extensionNumber: Number
  }]

});


module.exports = mongoose.model('Rental', RentalSchema);
