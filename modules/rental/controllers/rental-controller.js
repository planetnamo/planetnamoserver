/**
 * Module dependencies.
 */
var mongoose 	= require('mongoose');

var aux 		= require(__utils + '/auxilaryFunc');
var ErrorParser = require(__utils + '/errorParse');
var UsersUtils  = require(__utils + '/users')

var User 			= require(__base + '/models/user/user');
var RegisteredLot 	= require(__base + '/models/lots/registered-lot');
var ApprovedLot 	= require(__base + '/models/lots/approved-lot');
var Lot 	        = require(__base + '/models/lots/lot');
var SellingOrder 	= require(__base + '/models/orders/selling-req'); 
var BuyingOrder     = require(__base + '/models/orders/buying-req');
var RentalOrder     = require(__base + '/models/orders/rental-req');
var Product 		= require(__base + '/models/products/product');


exports.planetnamowelcome = function (req, res) {
  res.send("Welcome to the PlanetNamo API, selling module")
};

exports.RentAvailability = function (req, res) {
	var user = res.req.user;
	var lotId = req.params.lotId;
	var orderDetails = req.body.orderDetails;
	var rentalStartDate = new Date(req.body.rentalStartDate);
	var rentalEndDate = new Date(req.body.rentalEndDate);

	if(user && lotId && orderDetails){
		Lot.findOne({'_id': lotId, 'lotType': 'rent'}, function(err, lot){
			if(err){
				return res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});
			}else{
				if(lot){
					if(lot.lotSoldOut == false){
						if(lot.endDate > new Date()){
							var verifiedOrderDetails = [];
							var totalQuantityAvail = {};
							var rentalPriceMap = {};
							var totalRentToPay = 0;
							for(var i=0; i<lot.lotContents.length; i++){
								if(lot.lotContents[i].pId in orderDetails){
									//console.log(lot.lotContents[i]);
									totalQuantityAvail[lot.lotContents[i].pId] = lot.lotContents[i].quantity;
									rentalPriceMap[lot.lotContents[i].pId] = lot.lotContents[i].rentalPrice;
									//to calculate totalRentToPay
									var productPerMonthPrice = rentalPriceMap[lot.lotContents[i].pId];
									var rentAmountForThisProduct = aux.CalculateRentAmountForProduct(productPerMonthPrice,rentalStartDate,rentalEndDate);
									if(rentAmountForThisProduct == -1){
										return res.status(200).send("Rental period is too short");
									}else{
										totalRentToPay = totalRentToPay + rentAmountForThisProduct;
									}
								}
							}
							//console.log('-------')
							for(var i=0; i<lot.rentalLotDetails.length; i++){
								var tmpRentalDetails = lot.rentalLotDetails[i];
								var tmpOrderDetails = lot.rentalLotDetails[i].orderDetails; //array
								if(tmpRentalDetails.startDate>=rentalStartDate && rentalStartDate<=tmpRentalDetails.endDate){
									for(var j=0; j<tmpOrderDetails.length; j++){
										if(tmpOrderDetails[j].pId in totalQuantityAvail){
											console.log(tmpOrderDetails[j],'$');
											totalQuantityAvail[tmpOrderDetails[j].pId]-=tmpOrderDetails[j].quantity;
										}
									}
								}
							}
							//console.log(totalQuantityAvail);
							//res.send(totalQuantityAvail);
							for(pId in orderDetails){
								if(totalQuantityAvail[pId]>=orderDetails[pId]){
									verifiedOrderDetails.push({
										'pId': pId,
										'quantity': orderDetails[pId]
									});
								}else{
									return res.status(200).jsonp({error: "quantity more than available requested", QuantityAvailable: totalQuantityAvail});
								}
							}

							return res.status(200).jsonp({"QuantityAvailable": totalQuantityAvail, "rentalStartDate": rentalStartDate, "rentalEndDate": rentalEndDate, "totalRentToPay": totalRentToPay});
							

						}else{
							return res.status(200).send("Lot has expired. expired on : " + lot.endDate);
						}
					}else{
						return res.status(200).send("Lot sold out");
					}
				}else{
					return res.status(200).send("No lotId found for ID:"+lotId);
				}
			}
		});
	}else{
		return res.status(200).send("Inadequate information sent");
	}
};


exports.Rent = function (req, res) {
	var user = res.req.user;
	var lotId = req.params.lotId;
	var orderDetails = req.body.orderDetails;
	var rentalStartDate = new Date(req.body.rentalStartDate);
	var rentalEndDate = new Date(req.body.rentalEndDate);

	if(user && lotId && orderDetails){
		Lot.findOne({'_id': lotId, 'lotType': 'rent'}, function(err, lot){
			if(err){
				return res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});
			}else{
				if(lot){
					if(lot.lotSoldOut == false){
						if(lot.endDate > new Date()){
							var verifiedOrderDetails = [];
							var totalQuantityAvail = {};
							var rentalPriceMap = {};
							var totalRentToPay = 0;
							for(var i=0; i<lot.lotContents.length; i++){
								if(lot.lotContents[i].pId in orderDetails){
									console.log(lot.lotContents[i]);
									totalQuantityAvail[lot.lotContents[i].pId] = lot.lotContents[i].quantity;
									rentalPriceMap[lot.lotContents[i].pId] = lot.lotContents[i].rentalPrice;
								}
							}
							//console.log('-------')
							for(var i=0; i<lot.rentalLotDetails.length; i++){
								var tmpRentalDetails = lot.rentalLotDetails[i];
								var tmpOrderDetails = lot.rentalLotDetails[i].orderDetails; //array
								if(tmpRentalDetails.startDate>=rentalStartDate && rentalStartDate<=tmpRentalDetails.endDate){
									for(var j=0; j<tmpOrderDetails.length; j++){
										if(tmpOrderDetails[j].pId in totalQuantityAvail){
											console.log(tmpOrderDetails[j],'$');
											totalQuantityAvail[tmpOrderDetails[j].pId]-=tmpOrderDetails[j].quantity;
										}
									}
								}
							}
							//console.log(totalQuantityAvail);
							//res.send(totalQuantityAvail);
							for(pId in orderDetails){
								if(totalQuantityAvail[pId]>=orderDetails[pId]){
									verifiedOrderDetails.push({
										'pId': pId,
										'quantity': orderDetails[pId]
									});
								}else{
									return res.status(200).send("quantity more than available requested");
								}
							}

							var rentalOrder = new RentalOrder();
							rentalOrder.lotId = lotId;
							rentalOrder.userId = user._id;
							rentalOrder.rentAmountToPay = totalRentToPay;
							rentalOrder.startRentalDate = rentalStartDate;
							rentalOrder.endRentalDate = rentalEndDate;
							rentalOrder.orderDetails = verifiedOrderDetails;
							//console.log(rentalOrder);

							Wallet.findOne({user: user._id}, function(err, wallet){
								if(err){
									return res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});
								}else if(wallet){
									if(wallet.amount>0){
										var remainingWalletAmount = Math.max(wallet.amount-rentalOrder.rentAmountToPay,0);
										var transactionid = UserUtils.createWalletTransactionId();
										if(wallet.amount>=rentalOrder.rentAmountToPay){
											rentalOrder.rentAmountPaid = rentalOrder.rentAmountToPay;
											rentalOrder.rentPaymentSuccess = true;
										}else{
											rentalOrder.rentAmountPaid = wallet.amount;
										}
										Wallet.update({user: user._id}, {amount: remainingWalletAmount}, function(err, result){
											if(err){
												return res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});
											}else{
												var createParams = {
													transactionId: transactionid,
													amountInvloved: rentalOrder.rentAmountPaid,
													paymentType: 'debit',
													transactionInfo: {
														remarks: 'Payment to PlanetNamo',
														otherInfo: {}
													},
													walletId: wallet._id
												}
												if(req.body.otherPaymentInfo){
													createParams.transactionInfo.otherInfo = req.body.otherPaymentInfo;
												}
												WalletTransaction.create(createParams, function(err, result){
													if(err){
														return res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});
													}else{
														rentalOrder.save(function(err, rentalOrder){
															if(err){
																return res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});
															}else{
																lot.rentalLotDetails.push({
																	'startDate': rentalStartDate,
																	'endDate': rentalEndDate,
																	'orderDetails': verifiedOrderDetails,
																	'rentalId': rentalOrder._id
																});
																lot.save(function(err, lot){
																	if(err){
																		return res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});
																	}else{
																		return res.status(200).jsonp({"message": "success", "lot":lot, "rentalOrder":rentalOrder, transactionId: transactionid});
																	}
																})
															}
														});
													}
												});
											}
										});
									}else{
										return res.status(200).send({error: 'LOW_BALANCE, rent amount not paid. Buying order failed!', balanceAmount: wallet.amount});
									}
								}else{
									Wallet.create({user: userId, amount: req.body.amount}, function(err, wallet){
										if(err){
											return res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});
										}else{
											return res.status(200).send({error: 'LOW_BALANCE, rent amount not paid. Buying order failed!', balanceAmount: 0});
										}
									});
								}
							});
						}else{
							return res.status(200).send("Lot has expired. expired on : " + lot.endDate);
						}
					}else{
						return res.status(200).send("Lot sold out");
					}
				}else{
					return res.status(200).send("No lotId found for ID:"+lotId);
				}
			}
		});
	}else{
		return res.status(200).send("Inadequate information sent");
	}
};

exports.PayRemainingRent = function (req, res){
	var user = res.req.user;
	var rentalOrderId = req.params.rentalOrderId;
	var transactionId = req.body.razorpayPaymentID;

	if(user && rentalOrderId && transactionId){
		RentalOrder.findById(rentalOrderId, function(err, rentalOrder){
			if(err){
				return res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});
			}else{
				if(rentalOrder){
					if(rentalOrder.userId == user._id){
						var remainingRentToPay = rentalOrder.rentAmountToPay - rentalOrder.rentAmountPaid;
						if(remainingRentToPay>0){
							UserUtils.validatePaymentRazorPay(transactionId, remainingRentToPay, function(err, status, body){
								if(err){
									res.status(200).send({error: err});
								}else if(status == 'success'){
									Transaction.create({mihPayId: transactionId, mode: 'Razor Pay', status: 'success',
							                                            txnid: transactionId, 'amount': remainingRentToPay, userId: userId,
							                                            completeData: body}, function(err, result){
							        	if(err){
							        		res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});
							        	}else{
							        		rentalOrder.rentAmountPaid = rentalOrder.rentAmountPaid + remainingRentToPay;
							        		rentalOrder.dateRentalPaymentSuccess = new Date();
							        		rentalOrder.rentPaymentSuccess = true;
							        		rentalOrder.save(function(err, rentalOrder){
							        			if(err){
							        				return res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});
							        			}else{
							        				return res.jsonp({status: 'success', transactionId: transactionid, 'rentalOrder': rentalOrder});
							        			}
							        		});
							        	}
							        });
								}
							});
						}else{
							return res.status(200).send("You have already paid the full rental amount");
						}
					}else{
						return res.status(200).send("Unauthorized");
					}
				}else{
					return res.status(200).send("No rentalOrder found for ID:"+rentalOrderId)
				}
			}
		});
	}else{
		return res.status(200).send("Inadequate information sent");
	}
};


exports.ReturnRental = function (req, res) {
	//Only ADMIN can call this
	var user = res.req.user;
	var rentalOrderId = req.params.rentalOrderId;
	if(user && rentalOrderId){
		RentalOrder.findById(rentalOrderId, function(err, rentalOrder){
			if(err){
				return res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});
			}else{
				if(rentalOrder){
					if(rentalOrder.rentalReturned == false){
						if(1){
							console.log('here');
							var lotId = rentalOrder.lotId;
							Lot.findOneAndUpdate(
								{'_id': lotId},
								{$pull: {rentalLotDetails: {rentalId: rentalOrder._id}}},
								function(err, lotAfterRemovingRentalOrder){
									if(err){
										return res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});
									}else{
										//res.status(200).send(lotAfterRemovingRentalOrder);
										rentalOrder.rentalReturned = true;
										rentalOrder.dateReturned = new Date;
										rentalOrder.save(function(err, rentalOrder){
											if(err){
												return res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});
											}else{
												res.status(200).jsonp({message: "Rental order closed", lot: lotAfterRemovingRentalOrder, rentalOrder: rentalOrder});
											}
										});
									}
								});
						}else{
							return res.status(200).send("Rental period not over");
						}
					}else{
						return res.status(200).send("Rental order returned on : " + rentalOrder.dateReturned);
					}
				}else{
					return res.status(200).send("No rentalId found for ID:"+rentalOrderId);
				}
			}
		});
	}else{
		return res.status(200).send("Inadequate information sent");
	}
};

exports.GetAllRentalOrdersOnALot = function (req, res){
	var lotId = req.params.lotId;
	RentalOrder.find({'lotId': lotId}, function(err, rentalOrders){
		if(err){
			return res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});
		}else{
			return res.jsonp({status: 'success', 'orders': rentalOrders});
		}
	});
};

exports.GetAllRentalOrdersByUser = function (req, res){
	var user = res.req.user;
	var userToGet = req.params.userId;
	if(user.role != 'admin'){
		userToGet = user._id;
	}
	RentalOrder.find({'userId': userToGet}, function(err, rentalOrders){
		if(err){
			return res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});
		}else{
			return res.jsonp({status: 'success', 'orders': rentalOrders});
		}
	});
};

exports.GetNumberOfSuccessfulRentalOrdersPlacedTillNow = function (req, res) {
	RentalOrder.count({rentPaymentSuccess: true, cancelRentalOrder: false}, function(err, rentCount){
		if(err){
			return res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});
		}else{
			return res.jsonp({status: 'success', 'rentCount': rentCount});
		}
	});
};

