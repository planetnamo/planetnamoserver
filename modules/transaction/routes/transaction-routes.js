var transactionCtrl = require('../controllers/transaction-controller');
var passport = require("passport");
var Middleware = require('../../../middlewares/middleware');



module.exports = function(app, express){

	var apiRouter = express.Router();

	apiRouter.route('/welcome').get(transactionCtrl.planetnamowelcome);

	apiRouter.route('/sell').post(passport.authenticate('jwt', { session: false }),
											transactionCtrl.sell);

	apiRouter.route('/buy').post(passport.authenticate('jwt', { session: false }),
											transactionCtrl.buy);

	apiRouter.route('/changedeliverystage').put(passport.authenticate('jwt', { session: false }),
											Middleware.CheckIfAdmin,
											transactionCtrl.changedeliverystage);

	apiRouter.route('/changepickupstage').put(passport.authenticate('jwt', { session: false }),
											Middleware.CheckIfAdmin,
											transactionCtrl.changepickupstage);

	apiRouter.route('/closetransaction').put(passport.authenticate('jwt', { session: false }),
											Middleware.CheckIfAdmin,
											transactionCtrl.closetransaction);

	apiRouter.route('/canceltransaction').put(passport.authenticate('jwt', { session: false }),
											Middleware.CheckIfAdmin,
											transactionCtrl.canceltransaction);

	apiRouter.route('/getspecifictransaction').post(passport.authenticate('jwt', { session: false }),
											transactionCtrl.getspecifictransaction);
	
	apiRouter.route('/getsmytransactions').get(passport.authenticate('jwt', { session: false }),
											transactionCtrl.GetMyTransactions);

	apiRouter.route('/getproducttransactions').post(passport.authenticate('jwt', { session: false }),
											Middleware.CheckIfAdmin,
											transactionCtrl.GetAllTransactionsonProduct);

	apiRouter.route('/getusertransactions').post(passport.authenticate('jwt', { session: false }),
											Middleware.CheckIfAdmin,
											transactionCtrl.GetAllTransactionsofUser);

	return apiRouter;

};