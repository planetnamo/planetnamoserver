/**
 * Module dependencies.
 */
var mongoose = require('mongoose');
var Transaction     = require('../model/transaction-model');
var Product     = require('../../product/model/product-model');
var BaseProduct     = require('../../product/model/baseproduct-model');
var aux =  require('../../../utils/aux');
var User     = require('../../user/model/user-model');
var ErrorParser = require('../../../utils/errorParse');
var UsersUtils =  require('../../../utils/users');



exports.planetnamowelcome = function (req, res) {
  res.send("Welcome to the PlanetNamo API, this is the transactions module")
};

exports.sell = function (req, res) {
	
	/*var userId = res.req.user._id;
	var transactionId = req.body.razorpayPaymentID;
	var amount = req.body.amount;
	var productId = req.body.productId;

	if(userId && transactionId && amount && productId){
		Product.findById(productId, function(err, product){
			if(err) return res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});
			if(!product){
				return	res.status(400).json({
							error: "Product with corresponding productId not found!"
						});
			}


		})
	}*/
};

exports.buy = function (req, res) {
	var userId = res.req.user._id;
	var transactionId = req.body.razorpayPaymentID;
	var amount = req.body.amount;
	var productId = req.body.productId;
	var quantity = req.body.quantity;

	if(userId && transactionId && amount && productId && quantity){
		if(quantity<aux.constants.minbussinessprofilelimit && res.req.user.accounttype == 'Company')
			return	res.status(400).json({
						success: false,
						message: 'Bussiness profile limit not satisfied!'
					});

		Product.findById(productId, function(err, product){
			if(err) return res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});
			if(!product){
				return	res.status(400).json({
							error: "Product with corresponding productId not found!"
						});
			}
			if(product.quantity <= product.quantitysold){
				return	res.status(400).json({
							success: false,
							message: 'Cannot buy a sold out product!'
						});
			}
			if(quantity>(product.quantity-product.quantitysold)){
				return	res.status(400).json({
							success: false,
							message: 'Quantity to buy is higher than product quantity!'
						});		
			}

			UsersUtils.validatePaymentRazorPay(transactionId, amount, function(err, status, body){
				if(err){
					res.status(200).send({error: err});
				}else if(status == 'success'){
					var transaction = new Transaction();
					transaction.userid = userId;
					transaction.productid = productId;
					transaction.ordertype = 'buying';
					transaction.buyingquantity = quantity;
					transaction.mode = 'Razor Pay';
					transaction.status = 'success';
					transaction.txnid = transactionId;
					transaction.amount = amount;
					transaction.completeData = body;

					transaction.save(function(err, transaction){
						if(err){
							res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});
						}else{
							product.quantitysold+=quantity;
							product.save(function(err, product){
								if(err)
									return res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});
								else{
						            return res.json({ message: 'New buying transaction created!',
						            		   transaction: transaction });							
								}
							});
						}
					});
				}else{
					return res.status(200).send({error: 'Unknown error occurred'});
				}
			});
		});
	}else{
		return res.status(200).send({error: 'Incomplete information'});
	}
};


exports.changedeliverystage = function (req, res) {
	if(req.body.transactionid){
		Transaction.findById(req.body.transactionid, function(err, transaction){
			if(err) return res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});
			if(!transaction) return res.status(200).send({error: "Transaction with transactionid not found."});
			if(transaction.ordertype == 'selling'){
				return	res.status(400).json({
							success: false,
							message: 'deliverystage not applicable to selling type transactions!'
						});	
			}

			transaction.deliverystage = req.body.deliverystage;
	        transaction.save(function(err, transaction) {
	            if (err) return res.send(err);
	            return res.json({ message: 'Transaction updated!', 'transaction': transaction });
	        });
		});
	}else{
		res.status(200).send({error: 'Incomplete information'});
	}
};

exports.changepickupstage = function (req, res) {
	if(req.body.transactionid){
		Transaction.findById(req.body.transactionid, function(err, transaction){
			if(err) return res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});
			if(!transaction) return res.status(200).send({error: "Transaction with transactionid not found."});
			if(transaction.ordertype == 'buying'){
				return	res.status(400).json({
							success: false,
							message: 'Pickupstage not applicable to buying type transactions!'
						});	
			}


			transaction.pickupstage = req.body.pickupstage;
	        transaction.save(function(err, transaction) {
	            if (err) return res.send(err);
	            return res.json({ message: 'Transaction updated!', 'transaction': transaction });
	        });
		});
	}else{
		return res.status(200).send({error: 'Incomplete information'});
	}
};

exports.closetransaction = function (req, res) {
	if(req.body.transactionid){
		Transaction.findById(req.body.transactionid, function(err, transaction){
			if(err) return res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});
			if(!transaction) return res.status(200).send({error: "Transaction with transactionid not found."});
			if(transaction.activeorder == false){
				return	res.status(400).json({
							success: false,
							message: 'Transaction is already closed.',
							dateofclosing: transaction.endofactivitydate
						});	
			}


			transaction.activeorder = false;
			transaction.endofactivitydate = new Date();
			
	        transaction.save(function(err, transaction) {
	            if (err) return res.send(err);
	            return res.json({ message: 'Transaction closed!', 'transaction': transaction });
	        });
		});
	}else{
		return res.status(200).send({error: 'Incomplete information'});
	}
};

exports.canceltransaction = function (req, res) {
	if(req.body.transactionid){
		Transaction.findById(req.body.transactionid, function(err, transaction){
			if(err) return res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});
			if(!transaction) return res.status(200).send({error: "Transaction with transactionid not found."});
			else{
				if(!transaction.transactionCancelled){
					return res.status(200).send({error: "Cannot cancel already cancelled transaction."});
				}else if(!transaction.activeorder){ //compare dates -- TODO
					return res.status(200).send({error: "Cannot cancel already inactive transaction/order."});
				}else{
					transaction.transactionCancelled = true;
					transaction.endofactivitydate = new Date();
					transaction.activeorder = false;
					transaction.save(function(err, transaction){
						if(err) return res.status(200).send({error: "Transaction with transactionid not found."});
						else{
							return res.json({ message: 'Transaction cancelled!', 'transaction': transaction });
						}
					})
				}
			}
		});
	}else{
		return res.status(200).send({error: 'Incomplete information'});
	}
};

exports.getspecifictransaction = function (req, res) {
	if(req.params.transactionid){
		Transaction.findById(req.params.transactionid, function(err, transaction) {
			if(err) res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});
			else if(!transaction){
				return res.status(200).send({error: "Transaction with transactionid not found."});
			}
			else
				res.send(transaction);
		}).populate({
			path: 'productid',
			model:  'Product',
			populate: {
				path: 'baseproduct',
				model: 'BaseProduct'
			}
		});
	}else{
		return res.status(200).send({error: 'Incomplete information'});
	}
};

exports.GetMyTransactions = function (req, res) {
	if(res.req.user){
		  var queryParams = {
		  	'userid': res.req.user._id
		  }
		  Transaction.find(queryParams, function (err, transactions) {
			  if (err) {
			    res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});	
			  }else{
		        if(transactions != null){
		        	res.status(200).jsonp({success: "Transactions found", transactions: transactions });
		        }else{
		        	res.status(200).jsonp({error: "Transactions not found", transactions: null });
		        }
			  }
			});
	}else{
		return res.status(200).send({error: 'Unauthorized'});
	}
};

exports.GetAllTransactionsofUser = function (req, res) {
	if(req.body.userid){
		  var queryParams = {
		  	'userid': req.body.userid
		  }
		  Transaction.find(queryParams, function (err, transactions) {
			  if (err) {
			    res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});	
			  }else{
		        if(transactions != null){
		        	res.status(200).jsonp({success: "Transactions found", transactions: transactions });
		        }else{
		        	res.status(200).jsonp({error: "Transactions not found", transactions: null });
		        }
			  }
			});
	}else{
		return res.status(200).send({error: 'Unauthorized'});
	}
};

exports.GetAllTransactionsonProduct = function (req, res) {
	if(req.body.productid){
		var queryParams = {
			'productid': req.body.productid
		}
		Transaction.find(queryParams, function (err, transactions) {
		  if (err) {
		    res.status(200).send({error: ErrorParser.parseErrorreturnErrorMessage(err)});	
		  }else{
		    if(transactions != null){
		    	res.status(200).jsonp({success: "Transactions found", transactions: transactions });
		    }else{
		    	res.status(200).jsonp({error: "Transactions not found", transactions: null });
		    }
		  }
		});
	}else{
		return res.status(200).send({error: 'Incomplete information'});
	}
};

