var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/**
 * Transaction Schema
 */


var TransactionSchema = new Schema({
  date_created: {
    type: Date,
    default: Date.now
  },
  userId: {
    type: Schema.ObjectId,
    ref: 'User',
    required: true
  },
  orderType: {
    type: String,
    required: true,
    enum: ['buying','selling','rental','auction']
  },
  mode: {
    type: String,
    required: true
  },
  status: {
    type: String,
    required: true
  },
  txnid: {
    type: String,
    unique: true,
    required: true
  },
  amount: {
    type: String,
    required: true
  },
  completeData: {
    type: Schema.Types.Mixed,
    required: true
  },
  transactionCancelled: {
    type: Boolean,
    default: false
  },
  date_cancelled: {
    type: Date
  }

});

module.exports = mongoose.model('Transaction', TransactionSchema);