var passport 	= require("passport");
var sellingCtrl  = require(__modules + '/selling/controllers/selling-controller');
var Middleware  = require(__base + '/middlewares/middleware');

module.exports = function(app, express){

	var apiRouter = express.Router();


	//Guests
	apiRouter.route('/welcome').get(sellingCtrl.planetnamowelcome);


	//Authenticated Users Only
	apiRouter.route('/CreateSellingOrder').post(passport.authenticate('jwt', { session: false }),
													sellingCtrl.CreateSellingOrder);

	apiRouter.route('/CancelSellingOrder/:sellingOrderId').post(passport.authenticate('jwt', { session: false }),
													sellingCtrl.CancelSellingOrder);

	apiRouter.route('/deleteSellingOrder/:sellingOrderId').delete(passport.authenticate('jwt', { session: false }),
												Middleware.CheckIfAdmin,
												sellingCtrl.deleteSellingOrder);

	apiRouter.route('/GetUserSellingOrders').post(passport.authenticate('jwt', { session: false }),
													sellingCtrl.GetUserSellingOrders);

	apiRouter.route('/getSellingOrderByID/:sellingOrderId').get(passport.authenticate('jwt', { session: false }),
												sellingCtrl.getSellingOrderByID);


	apiRouter.route('/GetNumberOfSuccessfulSellingOrdersPlacedTillNow').get(sellingCtrl.GetNumberOfSuccessfulSellingOrdersPlacedTillNow);

	apiRouter.route('/AcceptPriceAfterPhysicalCheck/:sellingOrderId').get(passport.authenticate('jwt', { session: false }),
												sellingCtrl.AcceptPriceAfterPhysicalCheck);


	return apiRouter;

};