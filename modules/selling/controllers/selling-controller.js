/**
 * Module dependencies.
 */
var mongoose = require('mongoose');
var moment = require('moment');
var aux = require(__utils + '/auxilaryFunc');
var ErrorParser = require(__utils + '/errorParse');
var UsersUtils = require(__utils + '/users')
var LotsUtils = require(__utils + '/lots');
var moment = require('moment');
var User = require(__base + '/models/user/user');
var RegisteredLot = require(__base + '/models/lots/registered-lot');
var ApprovedLot = require(__base + '/models/lots/approved-lot');
var Lot = require(__base + '/models/lots/lot');
var SellingOrder = require(__base + '/models/orders/selling-req');
var Product = require(__base + '/models/products/product');
var ProductCategory = require(__base + '/models/products/product-category.js');
var BaseProduct = require(__base + '/models/products/base-product.js');
var Delivery = require(__base + '/models/delivery/delivery');
var Address = require(__base + '/models/user/address');

exports.planetnamowelcome = function(req, res) {
	res.send("Welcome to the PlanetNamo API, selling module")
};



//------------C R E A T E----------S E L L I N G---------O R D E R------------------------------------------------------------


/**
@api {post} /sell/createSellingOrder Create Selling Order Request
@apiName CreateSellingOrder
@apiGroup Sell 
@apiPermission authentication_required
@apiDescription Create a selling order. Selling order can be combined with either a productId or a requested Lot Id. 

If combined with a productId, the quantity of the product is by default singular. If combined with a requested lot, we need to create a requested lot prior to calling this api.

The address from where to pick this product up must be specified either in the form of an address Id of the user or new address fields.

The schedule pickup date must also be sent.

Selling orders created for registered lots already having selling orders will be throw an error.

@apiHeader {String="application/json"} Content-Type Content Type for the api 
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization": "JWT eyJhbGHVbz5y8i-HRRY"
	}


@apiParam {String} [rLotId] Unique registered lot Id which is associated with the selling order.
@apiParam {String} [productId] Id representing the product.
@apiParam {Date} schedulePickUpDate The date at which the pickup is scheduled.
@apiParam {String} [addressId] An address of the user. Specified by the corresponding Id.
@apiParam {JSONT} [address] Object representing the address of the user.
@apiParam {String} [address.line1] Line 1 of the location address
@apiParam {String} [address.line2] line2 Line 2 of the location address
@apiParam {String} [address.city] City of the location
@apiParam {String} [address.state] State of the location
@apiParam {String} [address.country] Country of the location
@apiParam {String} [address.zipCode] Zipcode of the address
@apiParam {String} [address.locality] locality Describe the locality of the location
@apiParam {String} [address.locationLat] locationLat Location lattitude
@apiParam {String} [address.locationLng] locationLng Location longitude

@apiParamExample {json} Request-Example:
{
	"productId": "58d6549c6a52a236f935ccb7",
	"address": {
		"line1": "amkdsknjfa",
		"line2": "dajknna",
		"locality": "dajns",
		"city": "dw",
		"state": "dwao",
		"country": "damo",
		"zipCode": "1291920"
	},
	"schedulePickUpDate": "2017-03-25"
}

@apiParamExample {json} Request-Example:
{
	"rLotId": "58df57e0768a717d3a281af8",
	"addressId": "58df5653efc4dc7cf67f4f56",
	"schedulePickUpDate": "2017-03-25"
}

@apiSuccess {String} status Status of the API Call.
@apiSuccess {String} response Response of the API Call.
{
  "status": "success",
  "response": {
    "message": "SellingOrder registered!",
    "status": "success"
  }
}

@apiSuccess {JSON} lot Details (json) of the newly lot (FinalLot)
@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
	{
	  "status": "success",
	  "message": "SellingOrder registered!"
	}

@apiError error Error message.
@apiErrorExample Error-Response:
    HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "Selling order already present."
	}


 */

var CreateSellingOrderUtillFunction2 = function(product, registeredLot, addressId, schedulePickUpDate, user, callback){
	var rLotId = registeredLot._id;
	SellingOrder.findOne({registeredLotId: rLotId}, function(err, alreadyPresentSellingOrder){
		if(err){
				let errResponse = {};
				errResponse['message'] = err;								
				callback(errResponse);
		}else{
			if(alreadyPresentSellingOrder){
				let errResponse = {};
				errResponse['message'] = "Selling order already present.";								
				callback(errResponse);	
			}else{
				var sellingOrder = new SellingOrder();
				sellingOrder.registeredLotId = rLotId;
				sellingOrder.userId = user._id;
				sellingOrder.amount = product.planetNamoCost * registeredLot.quantity;
				console.log('here');
				sellingOrder.save(function(err, sellingOrder) {
					if (err) {
							console.log("HERE");
							let errResponse = {};
							errResponse['message'] = err;								
							callback(errResponse);	
					} else {
						var delivery = new Delivery();
						delivery.orderType = "sell";
						delivery.sellingId = sellingOrder._id;
						delivery.deliveryType = "pickup";
						var remark = {};
						var todayDate =  moment().format();
						remark['remarkComment'] = "Pickup initiated for selling order : "+sellingOrder._id+" on "+todayDate;
						remark['remarkDate'] = todayDate;
						delivery.remarks = [];
						delivery.remarks.push(remark);
						delivery.deliveryAddress = addressId;
						delivery.requestedPickUpDate = schedulePickUpDate;
						delivery.save(function(err){
							if(err){
								let errResponse = {};
								errResponse['message'] = err;								
								callback(errResponse);				
							}else{
								sellingOrder.deliveryId = delivery._id;
								sellingOrder.save(function(err){
									if(err){
										callback(err);	
									}else{
										var response = {};
										response['message'] = "SellingOrder registered!";
										response['status'] = "success";
										response['response'] = {};
										response['response']['baseProductId'] = product.baseProductId;
										response['response']['productId'] = product._id;
										response['response']['createdAt'] = product.createdAt;
										response['response']['sellingOrderId'] = sellingOrder._id;
										response['response']['deliveryId'] = sellingOrder.deliveryId;
										response['response']['registeredLotId'] = sellingOrder.registeredLotId;
										RegisteredLot.findById(response['response']['registeredLotId'])
													 .populate({
													 	path: 'pId',
													 	model: Product,
													 	populate: {
													 		path: 'baseProductId',
													 		model: BaseProduct
													 	}
													 })
													 .exec(function(err, regLot){
													 	if(err){
															let errResponse = {};
															errResponse['message'] = err;								
															callback(errResponse);	
													 	}else{
													 		response['response']['quantity'] = regLot.quantity;
													 		response['response']['baseProductName'] = regLot.pId.baseProductId.productName;
													 		response['response']['planetNamoCost'] = regLot.pId.planetNamoCost;
													 		callback(null, response);
													 	}
													 });
									}
								});
							}
						});
					}
				});
			}
		}
	});
};

var CreateSellingOrderUtillFunction1 = function(product, registeredLot, addressId, address, schedulePickUpDate, user, callback){
	if(addressId){
		Address.findById(addressId, function(err, foundAddress){
			if(err){
				callback(err);
			}else{
				if(foundAddress){
					if(foundAddress.userId.equals(user._id)){
						CreateSellingOrderUtillFunction2(product, registeredLot, foundAddress._id, schedulePickUpDate, user, function(err, response){
							if(err){
								callback(err);
							}else{
								callback(null, response);
							}
						});
					}else{
						let errResponse = {};
						errResponse['message'] = "Unauthorized use of addressId.";
						callback(errResponse);
					}
				}else{
					let errResponse = {};
					errResponse['message'] = "Invalid addressId.";
					callback(errResponse);
				}
			}
		});
	}else if(address){
		var newAddress = new Address();
		newAddress.line1 = address.line1;
		newAddress.line2 = address.line2;
		newAddress.locality = address.locality;
		newAddress.city = address.city;
		newAddress.state = address.state;
		newAddress.country = address.country;
		newAddress.zipCode = address.zipCode;
		newAddress.locationLat = address.locationLat;
		newAddress.locationLat = address.locationLat;
		newAddress.userId = user._id;
		newAddress.save(function(err){
			if(err){
				callback(err);
			}else{
					CreateSellingOrderUtillFunction2(product, registeredLot, newAddress._id, schedulePickUpDate, user, function(err, response){
						if(err){
							callback(err);
						}else{
							callback(null, response);
						}
					});
			}
		});
	}else{
		let errResponse = {};
		errResponse['message'] = "Incomplete data.";
		callback(errResponse);
	}

};
exports.CreateSellingOrder = function(req, res) {
	var rLotId = req.body.rLotId;
	var productId = req.body.productId;
	var address = req.body.address;
	var schedulePickUpDate = moment(req.body.schedulePickUpDate, 'YYYY-MM-DD hh:mm:ss a', false); 
	var addressId = req.body.addressId;
	var user = req.user;
	if(user){
		if(productId){
			Product.findById(productId, function(err, product){
				if(err){
					return res.status(200).send({
						status: "error",
						message: ErrorParser.parseErrorreturnErrorMessage(err)
					});					
				}else{
					if(product){
						if(product.productRegisteredByUser.equals(user._id)){
							var registeredLot = new RegisteredLot();
							registeredLot.pId = product._id;
							registeredLot.quantity = 1;
							registeredLot.registeredByUser = user._id;
							registeredLot.save(function(err){
								if(err){
									return res.status(200).send({
										status: "error",
										message: ErrorParser.parseErrorreturnErrorMessage(err)
									});	
								}else{
									CreateSellingOrderUtillFunction1(product, registeredLot, addressId, address, schedulePickUpDate, user, function(err, response){
										if(err){
											return res.status(200).send({
												status: "error",
												message: ErrorParser.parseErrorreturnErrorMessage(err)
											});						
										}else{
											return res.status(200).send(response);
										}
									});
								}
							});
						}else{
							return res.status(200).send({status: "error", message: "Unauthorized user."});
						}
					}else{
						return res.status(200).send({status: "error", message: "No product found."});
					}
				}
			});
		}else if(rLotId){
			RegisteredLot.findById(rLotId)
						.populate({
							path: 'pId',
							model: Product
						})
						.exec(function(err, registeredLot){
							if(err){
								return res.status(200).send({
									status: "error",
									message: ErrorParser.parseErrorreturnErrorMessage(err)
								});									
							}else{
								if(registeredLot){
									CreateSellingOrderUtillFunction1(registeredLot.pId, registeredLot, addressId, address, schedulePickUpDate, user, function(err, response){
										if(err){
											return res.status(200).send({
												status: "error",
												message: ErrorParser.parseErrorreturnErrorMessage(err)
											});						
										}else{
											return res.status(200).send(response);
										}
									});
								}else{
									return res.status(200).send({status: "error", message: "Cannot find registeredLot."});
								}
							}
						});
		}else{
			return res.status(200).send({status: "error", message: "Invalid data."});
		}
	}else{
		return res.status(200).send({status: "error", message: "User is not authenticated."});
	}

};

//-------------------------------------------------------------------------------------------------------------------



//------------C A N C E L----------S E L L I N G---------O R D E R------------------------------------------------------------


/**
@api {post} /sell/CancelSellingOrder/:sellingOrderId Cancel Selling Order
@apiName cancelSellingOrder
@apiGroup Sell 
@apiPermission authentication_required
@apiDescription Cancel a selling order. A user who registered the selling order can cancel it or the admin can cancel it.

To cancel a selling order, you need to send the selling order Id associated with the selling order.

@apiHeader {String="application/json"} Content-Type Content Type for the api 
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization": "JWT eyJhbGHVbz5y8i-HRRY"
	}


@apiParam {String} :sellingOrderId Unique Id identifying a selling order.
@apiParamExample {json} Request-Example:
http://localhost:8080/api/sell/sellingreq/58a74b83f776cff579f5ca9d

@apiSuccess {String} status Status of the API Call.
@apiSuccess {String} message Message describing the status of the API call.
@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
	{
	  "status": "success",
	  "message": "Selling order successfully cancelled."
	}


@apiError error Error message.
@apiErrorExample Error-Response:
    HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "Cannot cancel selling order not registered by user."
	}


 */

exports.CancelSellingOrder = function(req, res) {
	var user = req.user;
	var sellingOrderId = req.params.sellingOrderId;
	var cancellationRemarks = req.body.cancellationRemarks;

	if(user){
		if(sellingOrderId){
			SellingOrder.findById(sellingOrderId, function(err, sellingOrder){
				if(err){
					return res.status(200).send({
						status: "error",
						message: ErrorParser.parseErrorreturnErrorMessage(err)
					});					
				}else{
					if(sellingOrder){
						if(sellingOrder.userId.equals(user._id)){
							var registeredLotId = sellingOrder.registeredLotId;
							if (sellingOrder.cancelSellingOrder == false) {
								sellingOrder.cancelSellingOrder = true;
								sellingOrder.dateSellingOrderCancelled = new Date();
								sellingOrder.cancellationRemarks = cancellationRemarks;
								sellingOrder.cancelledBy = user._id;
								if(user.role === 2)
									sellingOrder.cancelledByAdminOrUser = "ADMIN";
								else
									sellingOrder.cancelledByAdminOrUser = "USER";

								sellingOrder.save(function(err, sellingOrder) {
									if (err) {
										return res.status(200).send({
											status: "error",
											message: ErrorParser.parseErrorreturnErrorMessage(err)
										});
									} else {
										return res.status(200).send({
											status: "success",
											message: "Selling order successfully cancelled."
										});
									}
								});

							} else {
								return res.send(200).send({
									status: "error",
									message: "Selling order already cancelled"
								});
							}

						}else{
							return res.status(200).send({status: "error", message: "User is not authenticated."});
						}
					}else{
						return res.status(200).send({status: "error", message: "Selling order not found."});
					}
				}
			});
		}else{
			return res.status(200).send({status: "error", message: "Incomplete data."});
		}
	}else{
		return res.status(200).send({status: "error", message: "User is not authenticated."});
	}
};


//-------------------------------------------------------------------------------------------------------------------



//------------D E L E T E----------S E L L I N G---------O R D E R------------------------------------------------------------

/**
@api {delete} /sell/deleteSellingOrder/:sellingOrderId Delete Selling Order
@apiName deleteSellingOrder
@apiGroup Sell 
@apiPermission admin
@apiDescription Remove a selling order from the database. Only cancelled selling orders can be removed.

@apiHeader {String="application/json"} Content-Type Content Type for the api 
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization": "JWT eyJhbGHVbz5y8i-HRRY"
	}


@apiParam {String} :sellingOrderId Unique Id identifying a selling order.
@apiParamExample {json} Request-Example:
http://localhost:8080/api/sell/sellingreq/58a74b83f776cff579f5ca9d

@apiSuccess {String} status Status of the API Call.
@apiSuccess {String} message Message describing the status of the API call.
@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
	{
	  "status": "success",
	  "message": "Successfully deleted."
	}


@apiError error Error message.
@apiErrorExample Error-Response:
    HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "Cancel order first to delete it."
	}


 */
exports.deleteSellingOrder = function(req, res) {
	var user = res.req.user;
	var sellingOrderId = req.params.sellingOrderId;
	if (user.role == 'admin') {
		if (sellingOrderId) {
			SellingOrder.findById(sellingOrderId, function(err, sellingOrder) {
				if (err) {
					return res.status(200).send({
						status: "error",
						message: ErrorParser.parseErrorreturnErrorMessage(err)
					});
				} else {
					if (sellingOrder) {
						if (sellingOrder.cancelSellingOrder == true) {
							SellingOrder.remove({
								_id: sellingOrderId
							}, function(err, sellingOrder) {
								if (err) {
									return res.status(200).send({
										status: "error",
										message: ErrorParser.parseErrorreturnErrorMessage(err)
									});
								} else {
									return res.status(200).send({
										status: "success",
										message: "Successfully deleted."
									});
								}
							});
						} else {
							return res.status(200).send({
								status: "error",
								message: "Cancel order first to delete it."
							});
						}
					} else {
						return res.status(200).send({
							status: "error",
							message: "No selling order found."
						});
					}
				}
			});
		} else {
			return res.status(200).send({
				status: "error",
				message: "Incomplete data."
			});
		}
	} else {
		return res.status(200).send({
			status: "error",
			message: "Unauthorized access."
		});
	}
};
//-------------------------------------------------------------------------------------------------------------------



//------------G E T------A L L----S E L L I N G---------O R D E R-----B Y-----------U S E R--------------------------------------

/**
@api {post} /sell/GetUserSellingOrders Get All Selling Orders of User
@apiName GetUserSellingOrders
@apiGroup Sell 
@apiPermission authentication_required
@apiDescription Get all the sellings associated with the user. 

@apiHeader {String="application/json"} Content-Type Content Type for the api 
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization": "JWT eyJhbGHVbz5y8i-HRRY"
	}

@apiParam {Number} limit The number of selling orders to be fetched.
@apiParam {Date} createdOnBefore createdOn or before, used for pagination.
@apiParamExample {json} Request-Example:
{
  "limit": 1
}

@apiSuccess {String} status Status of the API Call.
@apiSuccess {JSON} response Response of the API Call.
@apiSuccess {String} response.SellingOrderInfo Selling order associated information.
@apiSuccess {String} response.SellingOrderInfo.sellingOrderId Id associated with the selling order.
@apiSuccess {String} response.SellingOrderInfo.amount Total amount associated with the selling order (in case of selling more than one similar type of product).
@apiSuccess {Boolean} response.SellingOrderInfo.sellingOrderApproved True if selling order has been approved by the user.
@apiSuccess {Boolean} response.SellingOrderInfo.sellingOrderFulfilled True if selling order has been fullfilled by planetNamo.
@apiSuccess {Boolean} response.SellingOrderInfo.cancelSellingOrder True if selling order has been cancelled.
@apiSuccess {String} response.SellingOrderInfo.cancelledBy "ADMIN" or "USER", selling order can be cancelled by the user who placed the order or by the admin.
@apiSuccess {String} response.SellingOrderInfo.cancellationRemarks Cancellation remarks associated with a cancelled selling order.
@apiSuccess {String} response.SellingOrderInfo.dateSellingOrderCancelled Date the selling order was cancelled, if it was.
@apiSuccess {String} response.SellingOrderInfo.dateSellingOrderFulfilled Date the selling order was fullfilled, if it was.


@apiSuccess {JSON} response.productInfo Product information associated with the selling order.
@apiSuccess {Array} response.productInfo.productImageURL Array of product Image URLs representing the product involved in the selling order.
@apiSuccess {String} response.productInfo.productName Name of the product involved in the selling Name.  (baseProduct name)
@apiSuccess {Array} response.productInfo.productSpecifications Specifications of the product.
@apiSuccess {Array} response.productInfo.planetNamoQuote The planetNamo Quoting price of the product involved. The quoting price is per piece.


@apiSuccess {JSON} response.deliveryInfo Delivery information associated with the selling order.
@apiSuccess {String} response.deliveryInfo.deliveryId Id associated with the delivery.
@apiSuccess {String} response.deliveryInfo.orderType Order type associated with the delivery intance.
@apiSuccess {String} response.deliveryInfo.deliveryType Delivery type associated with the delivery instance. It is "pickup" for all selling orders. The product is "picked up" from the user.
@apiSuccess {Number} response.deliveryInfo.pickupStage The pick up stage at which the delivery instance is currently in.
@apiSuccess {Date} response.deliveryInfo.schedulePickUpDate The date at which the pick up is scheduled to happen.
@apiSuccess {Date} response.deliveryInfo.remarks Remarks associated with the delivery instance.



@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
	{
	  "status": "success",
	  "response": [
	    {
	      "SellingOrderInfo": {
	        "sellingOrderId": "58de778006893d6cf213873d",
	        "amount": 10000,
	        "sellingOrderApproved": false,
	        "sellingOrderFulfilled": false,
	        "cancelSellingOrder": false,
	        "createdAt": "2017-03-31T15:36:32.160Z"
	      },
	      "productInfo": {
	        "productName": "iPhone 5s",
	        "productSpecifications": [
	          {
	            "displayName": "Color",
	            "specificationValue": "Black"
	          }
	        ],
	        "planetNamoQuote": 1000,
	        "productImageURL": [
	          "http://urbanriwaaz.com:8080/api/getFile/productPicture/58dca3f6b8d70960336cec74",
	          "http://urbanriwaaz.com:8080/api/getFile/productPicture/58dcb440cb4d156b1d66b305",
	          "http://urbanriwaaz.com:8080/api/getFile/productPicture/58dcb57b1d215c6e8c0f90a2",
	          "http://urbanriwaaz.com:8080/api/getFile/productPicture/58dcb5801d215c6e8c0f90a3",
	          "http://urbanriwaaz.com:8080/api/getFile/productPicture/58dcb5871d215c6e8c0f90a4",
	          "http://urbanriwaaz.com:8080/api/getFile/productPicture/58dce1e81d215c6e8c0f90a5",
	          "http://urbanriwaaz.com:8080/api/getFile/productPicture/58dce22da1e677432fb8f48a",
	          "http://urbanriwaaz.com:8080/api/getFile/productPicture/58dce237a1e677432fb8f48b",
	          "http://urbanriwaaz.com:8080/api/getFile/productPicture/58dce245a1e677432fb8f48c",
	          "http://urbanriwaaz.com:8080/api/getFile/productPicture/58dce247a1e677432fb8f48d"
	        ]
	      },
	      "deliveryInfo": {
	        "deliveryId": "58de778006893d6cf213873e",
	        "orderType": "sell",
	        "deliveryType": "pickup",
	        "pickupStage": 1,
	        "schedulePickUpDate": "2017-04-05T15:36:32.579Z",
	        "remarks": [
	          {
	            "remarkComment": "Pickup initiated for selling order : 58de778006893d6cf213873d on 2017-03-31T21:06:32+05:30",
	            "remarkDate": "2017-03-31T15:36:32.000Z"
	          }
	        ]
	      }
	    }
	  ]
	}


@apiError error Error message.
@apiErrorExample Error-Response:
    HTTP/1.1 200 Error

 */

exports.GetUserSellingOrders = function(req, res) {
	var user = req.user;
	var limit = (req.body.limit || 10);
	var createdOnBefore = req.body.createdOnBefore;
	console.log(user._id);
	var andQueries = {};
	var orQueries = [];

	andQueries.userId = user._id;
    if(createdOnBefore){
      console.log(createdOnBefore);
      andQueries.createdAt = { $lt: createdOnBefore };
    }
    var queryParams= {};
	if(orQueries.length > 0){
		queryParams = {$and: [andQueries, { $or: orQueries }]};
	}else{
		queryParams = andQueries;
	}

	if(user){
		SellingOrder.find(queryParams)
		.populate({
			path: "registeredLotId",
			model: RegisteredLot,
			populate: {
				path: "pId",
				model: Product,
				populate: {
					path: "baseProductId",
					model: BaseProduct
				}
			}
		})
		.populate({
			path: "deliveryId",
			model: Delivery
		})
		.limit(limit)
		.sort('-createdAt')
		.exec(function(err, responseArray){
			if(err){
				return res.status(200).send({
					status: "error",
					message: ErrorParser.parseErrorreturnErrorMessage(err)
				});				
			}else{
				console.log(responseArray);
				if(responseArray && responseArray.length>0){
					var parsedResponseArray = [];
					var i = 0;
					function asyncFor(i){
						if(i<responseArray.length){
							var response = {};
							response['SellingOrderInfo'] = {};
							response['productInfo'] = {};
							response['deliveryInfo'] = {};

							response['SellingOrderInfo']['sellingOrderId'] = responseArray[i]._id;
							response['SellingOrderInfo']['amount'] = responseArray[i].amount;
							response['SellingOrderInfo']['sellingOrderApproved'] = responseArray[i].sellingOrderApproved;
							response['SellingOrderInfo']['sellingOrderFulfilled'] = responseArray[i].sellingOrderFulfilled;
							response['SellingOrderInfo']['cancelSellingOrder'] = responseArray[i].cancelSellingOrder;
							response['SellingOrderInfo']['cancelledBy'] = responseArray[i].cancelledBy;
							response['SellingOrderInfo']['cancellationRemarks'] = responseArray[i].cancellationRemarks;
							response['SellingOrderInfo']['dateSellingOrderCancelled'] = responseArray[i].dateSellingOrderCancelled;
							response['SellingOrderInfo']['dateSellingOrderFulfilled'] = responseArray[i].dateSellingOrderFulfilled;
							response['SellingOrderInfo']['createdAt'] = responseArray[i].createdAt;
							response['SellingOrderInfo']['cancelledByAdminOrUser'] =  responseArray[i].cancelledByAdminOrUser;
							response['SellingOrderInfo']['amountAfterPhysicalCheckPerItem'] = responseArray[i].amountAfterPhysicalCheckPerItem;
							response['SellingOrderInfo']['amountAfterPhysicalCheckTotal'] = responseArray[i].amountAfterPhysicalCheckTotal;
							response['SellingOrderInfo']['userApproved'] = responseArray[i].userApproved;

							try{
								response['productInfo']['productName'] = responseArray[i].registeredLotId.pId.baseProductId.productName;
							}catch(e){
								response['productInfo']['productName'] = null;
							}
							try{
								response['productInfo']['productSpecifications'] = responseArray[i].registeredLotId.pId.baseProductId.specifications;
							}catch(e){
								response['productInfo']['productSpecifications']  = null;
							}
							try{
								response['productInfo']['planetNamoQuote'] = responseArray[i].registeredLotId.pId.planetNamoCost;
							}catch(e){
								response['productInfo']['planetNamoQuote'] = null
							}
							
							response['deliveryInfo']['deliveryId'] = responseArray[i].deliveryId._id;
							response['deliveryInfo']['orderType'] = responseArray[i].deliveryId.orderType;
							response['deliveryInfo']['deliveryType'] = responseArray[i].deliveryId.deliveryType;
							response['deliveryInfo']['pickupStage'] = responseArray[i].deliveryId.pickupStage;
							response['deliveryInfo']['schedulePickUpDate'] = responseArray[i].deliveryId.schedulePickUpDate;
							response['deliveryInfo']['remarks'] = responseArray[i].deliveryId.remarks;
							response['productInfo']['productImageURL'] = [];
							LotsUtils.GetProductImagesURLArray(responseArray[i].registeredLotId.pId._id, function(err, imagesArray) {
								if (err) {
									return res.status(200).send({
										status: "error",
										message: ErrorParser.parseErrorreturnErrorMessage(err)
									});
								} else {
									response['productInfo']['productImageURL'] = imagesArray;
									parsedResponseArray.push(response);
									i += 1;
									asyncFor(i);
								}
							});
						}else{
							return res.status(200).send({status: "success", response: parsedResponseArray});
						}
					}
					asyncFor(i);
				}else{
					if(createdOnBefore){
						return res.status(200).send({status: "success", response: []});
					}else{
						return res.status(200).send({status: "error", message: "User does not have any selling orders."});
					}
				}
			}
		});

	}else{
		return res.status(200).send({status: "error", message: "User is not authenticated."});
	}
};

//-------------------------------------------------------------------------------------------------------------------



//------------G E T---------S E L L I N G---------O R D E R-----B Y-----------ID--------------------------------------

/**
@api {post} /sell/getSellingOrderByID/:sellingOrderId Get Selling Orders of by Id
@apiName getSellingOrderByID
@apiGroup Sell 
@apiPermission authentication_required
@apiDescription Get a specific selling order. Admin can view any selling order, normal users are limited to their own selling orders.

@apiHeader {String="application/json"} Content-Type Content Type for the api 
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization": "JWT eyJhbGHVbz5y8i-HRRY"
	}

@apiSuccess {String} status Status of the API Call.
@apiSuccess {Number} noOfSellings Number of selling orders placed by user.
@apiSuccess {Array} sellingOrders Dictionary containing details of the selling order
@apiSuccess {String} sellingOrders.baseProductId Base product of the sold product
@apiSuccess {String} sellingOrders.registeredLotId Registered lot id of the sold product
@apiSuccess {String} sellingOrders.productId Product Id of the sold product
@apiSuccess {String} sellingOrders.baseProductName Product Name of the sold product
@apiSuccess {String} sellingOrders.brand Brand Name of the sold product
@apiSuccess {String} sellingOrders.planetnamoCost PlanetNamoCost i.e. The quote (for a single piece) for the sold product which has to be paid to the seller.
@apiSuccess {String} sellingOrders.totalQuote PlanetNamoCost i.e. The quote(total products) for the sold product which has to be paid to the seller.
@apiSuccess {String} sellingOrders.quantity Quantity of products sold
@apiSuccess {String} sellingOrders.specifications A dictionary of specifications of the sold product
@apiSuccess {String} sellingOrders.baseProductImages An image url representing the base product
@apiSuccess {String} sellingOrders.productImages An array of image urls representing the product
@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
	{
	  "status": "success",
	  "sellingOrder": {
	    "baseProductId": "58c15963ab6d4359ad8234e0",
	    "registeredLotId": "58d1456f44678b2dcef605d8",
	    "productId": "58c252d608b1918e667c8539",
	    "baseProductName": "iPhone 6",
	    "brand": "Apple",
	    "planetnamoCost": 1000,
	    "totalQuote": 100000,
	    "specifications": {
	      "color": "Rose Gold, Space Grey",
	      "item_weight": "159 g",
	      "operating_system": "iOS",
	      "warranty_n_support": "1 year manufacturer warranty for device and 6 months manufacturer warranty for in-box accessories including batteries from the date of purchase",
	      "brand_name": "Apple",
	      "productCategoy": {
	        "productCategoryName": "Mobile",
	        "categoryImageURL": "http://localhost:8080/api/getFile/productCategoryPicture/d720f73f4c7cbb7b4e95fc3e72b376b21489820653040.png"
	      },
	      "model_number": "MHJDSJ290HS"
	    },
	    "baseProductImages": "http://localhost:8080/api/getFile/baseProductPicture/669166b46c7601840a3beeed65ea86961490121536929.png",
	    "productImages": [
	      "http://localhost:8080/api/getFile/productPicture/e956ad124774826748a848b89a5d86b91490121626522.jpeg",
	      "http://localhost:8080/api/getFile/productPicture/4d385f530d1be00e4bce04a13bf7153f1490121630688.jpeg"
	    ]
	  }
	}


@apiError error Error message.
@apiErrorExample Error-Response:
    HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "Unauthorized access."
	}


 */
exports.getSellingOrderByID = function(req, res) {
	var user = res.req.user;
	var sellingOrderId = req.params.sellingOrderId;
	SellingOrder.findById(sellingOrderId, function(err, sellingOrder) {
		if (err) {
			return res.status(200).send({
				status: "error",
				message: ErrorParser.parseErrorreturnErrorMessage(err)
			});
		} else {
			if (user.role == 2 || user._id.equals(sellingOrder.userId)) {
				var sellingOrders = [];
				sellingOrders.push(sellingOrder);
				var selling = {};
				var i = 0;
				//Have to add DELIVERY INFORMATION -- Pending

				//Ids
				selling['baseProductId'] = sellingOrders[i].registeredLotId.pId.baseProductId._id;
				selling['registeredLotId'] = sellingOrders[i].registeredLotId._id;
				selling['productId'] = sellingOrders[i].registeredLotId.pId._id;

				selling['baseProductName'] = sellingOrders[i].registeredLotId.pId.baseProductId.productName;
				selling['brand'] = sellingOrders[i].registeredLotId.pId.baseProductId.brand_name;
				selling['planetnamoCost'] = sellingOrders[i].registeredLotId.pId.planetNamoCost;
				selling['totalQuote'] = sellingOrders[i].amount;
				selling['quantity'] = sellingOrders[i].registeredLotId.amount;
				selling['specifications'] = {};

				if (sellingOrders[i].registeredLotId.pId.baseProductId.releaseDate) selling['specifications']['releaseDate'] = sellingOrders[i].registeredLotId.pId.baseProductId.releaseDate;
				if (sellingOrders[i].registeredLotId.pId.baseProductId.marketCost) selling['specifications']['marketCost'] = sellingOrders[i].registeredLotId.pId.baseProductId.marketCost;
				if (sellingOrders[i].registeredLotId.pId.baseProductId.color) selling['specifications']['color'] = sellingOrders[i].registeredLotId.pId.baseProductId.color;
				if (sellingOrders[i].registeredLotId.pId.baseProductId.item_weight) selling['specifications']['item_weight'] = sellingOrders[i].registeredLotId.pId.baseProductId.item_weight;
				if (sellingOrders[i].registeredLotId.pId.baseProductId.operating_system) selling['specifications']['operating_system'] = sellingOrders[i].registeredLotId.pId.baseProductId.operating_system;
				if (sellingOrders[i].registeredLotId.pId.baseProductId.warranty_n_support) selling['specifications']['warranty_n_support'] = sellingOrders[i].registeredLotId.pId.baseProductId.warranty_n_support;
				if (sellingOrders[i].registeredLotId.pId.baseProductId.brand_name) selling['specifications']['brand_name'] = sellingOrders[i].registeredLotId.pId.baseProductId.brand_name;
				if (sellingOrders[i].registeredLotId.pId.baseProductId.productCategoryId) selling['specifications']['productCategoy'] = {};
				if (sellingOrders[i].registeredLotId.pId.baseProductId.productCategoryId.categoryName) selling['specifications']['productCategoy']['productCategoryName'] = sellingOrders[i].registeredLotId.pId.baseProductId.productCategoryId.categoryName;
				if (sellingOrders[i].registeredLotId.pId.baseProductId.productCategoryId.categoryImageURL) selling['specifications']['productCategoy']['categoryImageURL'] = sellingOrders[i].registeredLotId.pId.baseProductId.productCategoryId.categoryImageURL;
				if (sellingOrders[i].registeredLotId.pId.baseProductId.screen_size) selling['specifications']['screen_size'] = sellingOrders[i].registeredLotId.pId.baseProductId.screen_size;
				if (sellingOrders[i].registeredLotId.pId.baseProductId.screen_resolution) selling['specifications']['screen_resolution'] = sellingOrders[i].registeredLotId.pId.baseProductId.screen_resolution;
				if (sellingOrders[i].registeredLotId.pId.baseProductId.processor) selling['specifications']['processor'] = sellingOrders[i].registeredLotId.pId.baseProductId.processor;
				if (sellingOrders[i].registeredLotId.pId.baseProductId.display_technology) selling['specifications']['display_technology'] = sellingOrders[i].registeredLotId.pId.baseProductId.display_technology;
				if (sellingOrders[i].registeredLotId.pId.baseProductId.ram) selling['specifications']['ram'] = sellingOrders[i].registeredLotId.pId.baseProductId.ram;
				if (sellingOrders[i].registeredLotId.pId.baseProductId.hard_drive) selling['specifications']['hard_drive'] = sellingOrders[i].registeredLotId.pId.baseProductId.hard_drive;
				if (sellingOrders[i].registeredLotId.pId.baseProductId.series) selling['specifications']['series'] = sellingOrders[i].registeredLotId.pId.baseProductId.series;
				if (sellingOrders[i].registeredLotId.pId.baseProductId.model_number) selling['specifications']['model_number'] = sellingOrders[i].registeredLotId.pId.baseProductId.model_number;
				if (sellingOrders[i].registeredLotId.pId.baseProductId.hardware_platform) selling['specifications']['hardware_platform'] = sellingOrders[i].registeredLotId.pId.baseProductId.hardware_platform;
				if (sellingOrders[i].registeredLotId.pId.baseProductId.product_dimensions) selling['specifications']['product_dimensions'] = sellingOrders[i].registeredLotId.pId.baseProductId.product_dimensions;
				if (sellingOrders[i].registeredLotId.pId.baseProductId.item_dimensions) selling['specifications']['item_dimensions'] = sellingOrders[i].registeredLotId.pId.baseProductId.item_dimensions;
				if (sellingOrders[i].registeredLotId.pId.baseProductId.processor_count) selling['specifications']['processor_count'] = sellingOrders[i].registeredLotId.pId.baseProductId.processor_count;
				if (sellingOrders[i].registeredLotId.pId.baseProductId.processor_brand) selling['specifications']['processor_brand'] = sellingOrders[i].registeredLotId.pId.baseProductId.processor_brand;
				if (sellingOrders[i].registeredLotId.pId.baseProductId.processor_speed) selling['specifications']['processor_speed'] = sellingOrders[i].registeredLotId.pId.baseProductId.processor_speed;
				if (sellingOrders[i].registeredLotId.pId.baseProductId.computer_memory_type) selling['specifications']['computer_memory_type'] = sellingOrders[i].registeredLotId.pId.baseProductId.computer_memory_type;
				if (sellingOrders[i].registeredLotId.pId.baseProductId.hard_drive_interface) selling['specifications']['hard_drive_interface'] = sellingOrders[i].registeredLotId.pId.baseProductId.hard_drive_interface;
				if (sellingOrders[i].registeredLotId.pId.baseProductId.hard_drive_rotational_speed) selling['specifications']['hard_drive_rotational_speed'] = sellingOrders[i].registeredLotId.pId.baseProductId.hard_drive_rotational_speed;
				if (sellingOrders[i].registeredLotId.pId.baseProductId.graphics_card) selling['specifications']['graphics_card'] = sellingOrders[i].registeredLotId.pId.baseProductId.graphics_card;
				if (sellingOrders[i].registeredLotId.pId.baseProductId.graphics_card_description) selling['specifications']['graphics_card_description'] = sellingOrders[i].registeredLotId.pId.baseProductId.graphics_card_description;
				if (sellingOrders[i].registeredLotId.pId.baseProductId.wireless_type) selling['specifications']['wireless_type'] = sellingOrders[i].registeredLotId.pId.baseProductId.wireless_type;
				if (sellingOrders[i].registeredLotId.pId.baseProductId.speaker_description) selling['specifications']['speaker_description'] = sellingOrders[i].registeredLotId.pId.baseProductId.speaker_description;
				if (sellingOrders[i].registeredLotId.pId.baseProductId.no_of_usb_ports_2_0) selling['specifications']['no_of_usb_ports_2_0'] = sellingOrders[i].registeredLotId.pId.baseProductId.no_of_usb_ports_2_0;
				if (sellingOrders[i].registeredLotId.pId.baseProductId.no_of_usb_ports_3_0) selling['specifications']['no_of_usb_ports_3_0'] = sellingOrders[i].registeredLotId.pId.baseProductId.no_of_usb_ports_3_0;
				if (sellingOrders[i].registeredLotId.pId.baseProductId.optical_drive_type) selling['specifications']['optical_drive_type'] = sellingOrders[i].registeredLotId.pId.baseProductId.optical_drive_type;
				if (sellingOrders[i].registeredLotId.pId.baseProductId.batteries) selling['specifications']['batteries'] = sellingOrders[i].registeredLotId.pId.baseProductId.batteries;
				if (sellingOrders[i].registeredLotId.pId.baseProductId.warranty_n_support) selling['specifications']['warranty_n_support'] = sellingOrders[i].registeredLotId.pId.baseProductId.warranty_n_support;
				if (sellingOrders[i].registeredLotId.pId.baseProductId.miscellaneous) selling['specifications']['miscellaneous'] = sellingOrders[i].registeredLotId.pId.baseProductId.miscellaneous;
				if (sellingOrders[i].registeredLotId.pId.baseProductId.chipset_brand) selling['specifications']['chipset_brand'] = sellingOrders[i].registeredLotId.pId.baseProductId.chipset_brand;

				selling['baseProductImages'] = sellingOrders[i].registeredLotId.pId.baseProductId.baseProductImageURL;

				LotsUtils.GetProductImagesURLArray(sellingOrders[i].registeredLotId.pId._id, function(err, imagesArray) {
					if (err) {
						return res.status(200).send({
							status: "error",
							message: ErrorParser.parseErrorreturnErrorMessage(err)
						});
					} else {
						console.log(i, imagesArray);
						selling['productImages'] = imagesArray;
						return res.status(200).send({
							status: "success",
							sellingOrder: selling
						});

					}
				});

			} else {
				return res.status(200).send({
					status: "error",
					message: "Unauthorized access."
				});
			}
		}
	}).populate({
		path: "registeredLotId",
		model: RegisteredLot,
		populate: {
			path: "pId",
			model: Product,
			populate: {
				path: "baseProductId",
				model: BaseProduct,
				populate: {
					path: "productCategoryId",
					model: ProductCategory
				}
			}
		}
	});
};
//-------------------------------------------------------------------------------------------------------------------



//---------------T O T A L----------S E L L I N G-------O R D E R----------P L A C E D------T I L L----------N O W---------------------------

/**
@api {post} /sell/GetNumberOfSuccessfulSellingOrdersPlacedTillNow Get Total sellings
@apiName GetNumberOfSuccessfulSellingOrdersPlacedTillNow
@apiGroup Sell 
@apiDescription Statistic (total sellings) that have happened

@apiHeader {String="application/json"} Content-Type Content Type for the api 
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization": "JWT eyJhbGHVbz5y8i-HRRY"
	}

@apiSuccess {String} status Status of the API Call.
@apiSuccess {Number} sellCount Total successfull sellings till now
@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
	{
	  "status": "success",
	  "sellCount": 2
	}

@apiError error Error message.
@apiErrorExample Error-Response:
    HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "Unauthorized access."
	}


 */
exports.GetNumberOfSuccessfulSellingOrdersPlacedTillNow = function(req, res) {
	SellingOrder.count({}, function(err, sellCount) {
		if (err) {
			return res.status(200).send({
				status: "error",
				message: ErrorParser.parseErrorreturnErrorMessage(err)
			});
		} else {
			return res.status(200).send({
				status: "success",
				sellCount: sellCount
			});
		}
	});
};
//-------------------------------------------------------------------------------------------------------------------




exports.CompletePhysicalCheckOfSellingOrder = function(req, res) {
	var sellingOrderId = req.body.sellingOrderId;
	var user = req.user;
	

};

exports.CompleteSellingOfSellingOrder = function(req, res) {
	var sellingOrderId = req.body.sellingOrderId;

};



/**
@api {get} /sell/AcceptPriceAfterPhysicalCheck/:sellingOrderId Approved revised selling price
@apiName AcceptPriceAfterPhysicalCheck
@apiGroup Sell 
@apiPermission authentication_required
@apiDescription Aprove the revised quoted selling price and finally sell the product to earn green points.

@apiHeader {String="application/json"} Content-Type Content Type for the api 
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization": "JWT eyJhbGHVbz5y8i-HRRY"
	}


@apiParam {String} sellingOrderId Id representing the selling order. Sent as a URL parameter.


@apiSuccess {String} status Status of the API Call.
@apiSuccess {String} message Message describing the status of the API call.

 */
exports.AcceptPriceAfterPhysicalCheck = function(req, res) {
	var sellingOrderId = req.params.sellingOrderId;
	var user = req.user;
	if(user){
		if(sellingOrderId){
			SellingOrder.findById(sellingOrderId, function(err, sellingOrder){
				if(sellingOrder.userId.equals(user._id)){
					if(sellingOrder.userApproved === false){
						if(sellingOrder.cancelSellingOrder === false){
							if(sellingOrder.deliveryId && sellingOrder.amountAfterPhysicalCheckTotal){
								sellingOrder.userApproved = true;
								sellingOrder.save(function(err){
									if(err){
										return res.status(200).send({
											status: "error",
											message: ErrorParser.parseErrorreturnErrorMessage(err)
										});									
									}else{
										return res.status(200).send({status: "success", message: "Selling order revised price approved by user."});
									}
								});
							}else{
								return res.status(200).send({status: "error", message: "Wait for selling order to be under physical check and revised quote price to be release."});
							}
						}else{
							return res.status(200).send({status: "error", message: "Cannot interact with a cancelled selling order."});
						}
					}else{
						return res.status(200).send({status: "error", message: "Selling order has been already approved by the user for the revised quoted price after physical check."});
					}
				}else{
					return res.status(200).send({status: "error", message: "User is not authenticated."});
				}
			});
		}else{
			return res.status(200).send({status: "error", message: "Invalid information."});
		}
	}else{
		return res.status(200).send({status: "error", message: "User is not authenticated."});
	}
};




