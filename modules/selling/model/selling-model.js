var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/**
 * Selling Schema
 */


var SellingSchema = new Schema({
  productId: {
    type: Schema.ObjectId,
    ref: 'Product',
    required: true
  },
  userId: {
    type: Schema.ObjectId,
    ref: 'User',
    required: true
  },
  transactionId: {
    type: Schema.ObjectId,
    ref: 'Transaction'
  },
  deliveryId:{
    type: Schema.ObjectId,
    ref: 'Delivery'
  },
  amount: {
    type: Number,
    required: true
  },
  quantityToBeSold: {
    type: Number,
    default: 1
  },
  dateSellingOrderPlaced: {
    type: Date,
    default: new Date()
  },
  dateSellingOrderCancelled: {
    type: Date
  },
  dateSellingOrderFulfilled: {
    type: Date
  },
  cancelSellingOrder: {
    type: Boolean,
    default: false
  },
  cancelledBy: {
    type: String
  },
  cancellationRemarks: {
    type: String
  },
  sellingOrderFulfilled: {
    type: Boolean,
    default: false
  },
  transactionSuccess: {
    type: Boolean,
    default: false
  }

});


module.exports = mongoose.model('Selling', SellingSchema);
