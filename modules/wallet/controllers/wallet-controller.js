/**
 * Module dependencies.
 */

var mongoose = require('mongoose');

var User = require(__base + '/models/user/user');
var Wallet = require(__base + '/models/wallet/wallet');
var WalletTransaction = require(__base + '/models/wallet/wallet-transactions');
var WalletWithDrawl = require(__base + '/models/wallet/wallet-withdrawl-requests');
var Transaction = require(__base + '/models/transactions/transaction');
var ErrorParser = require(__utils + '/errorParse.js');
var aux = require(__utils + '/auxilaryFunc.js');
var UserUtils = require(__utils + '/users.js');
var WithDrawWalletRequests = require(__base + '/models/wallet/wallet-withdrawl-requests');

exports.planetnamowelcome = function(req, res) {
	res.send("Welcome to the PlanetNamo API wallet module")
};


/**
@apiDefine Wallet Wallet
APIs to access the user wallet
 */


/**
@api {post} /wallet/payFromWallet Pay from wallet 
@apiName payFromWallet
@apiGroup Wallet
@apiPermission authentication_required
@apiDescription Pay for a transaction from wallet. If the money in the wallet is less than the transaction amount, then it would lead to an error.

If the user's wallet has not been initialized, it would also lead to an error. A successfull api call leads to a new wallet transaction for the said amount.

@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization": "JWT eyJhbGHVbz5y8i-HRRY"
	} 
	
@apiParam {Number} amount The amount to be deducted from the wallet.
@apiParamExample {json} Request-Example:
	HTTP/1.1 200 OK
	{
		"amount": 500
	}


@apiSuccess {String} status Status of the API Call.
@apiSuccess {String} message Message describing the status
@apiSuccess {String} transactionId TransactionId of the newly created wallet transaction which is created if status is success
@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
	{
	  "status": "success",
	  "message": "New wallet transaction created",
	  "transactionId": "209jeawnjcewad903jcqa"
	}

@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "Inadequate information sent"
	}
*/
exports.payFromWallet = function(req, res) {
	var userId = res.req.user._id;
	var amount = req.body.amount;
	var transactionid = UserUtils.createWalletTransactionId();
	if (amount) {
		Wallet.findOne({
			user: userId
		}, function(err, wallet) {
			if (err) {
				return res.status(200).send({
					status: "error",
					message: ErrorParser.parseErrorreturnErrorMessage(err)
				});
			} else if (wallet) {
				if (wallet.amount < amount) {
					return res.status(200).send({
						status: "error",
						message: 'LOW_BALANCE',
						balanceAmount: 0
					});
				} else {
					Wallet.update({
						user: userId
					}, {
						amount: wallet.amount - amount
					}, function(err, result) {
						if (err) {
							return res.status(200).send({
								status: "error",
								message: ErrorParser.parseErrorreturnErrorMessage(err)
							});
						} else {
							var createParams = {
								transactionId: transactionid,
								amountInvloved: amount,
								paymentType: 'debit',
								transactionInfo: {
									remarks: 'Payment to PlanetNamo',
									otherInfo: {}
								},
								walletId: wallet._id
							}
							if (req.body.otherPaymentInfo) {
								createParams.transactionInfo.otherInfo = req.body.otherPaymentInfo;
							}
							WalletTransaction.create(createParams, function(err, result) {
								if (err) {
									return res.status(200).send({
										status: "error",
										message: ErrorParser.parseErrorreturnErrorMessage(err)
									});
								} else {
									return res.status(200).send({
										status: "success",
										message: "New wallet transaction created",
										transactionId: transactionid
									});
								}
							});
						}
					});
				}
			} else {
				Wallet.create({
					user: userId,
					amount: req.body.amount
				}, function(err, wallet) {
					if (err) {
						return res.status(200).send({
							status: "error",
							message: ErrorParser.parseErrorreturnErrorMessage(err)
						});
					} else {
						return res.status(200).send({
							status: "error",
							message: 'LOW_BALANCE',
							balanceAmount: 0
						});
					}
				});
			}
		});
	} else {
		return res.status(200).send({
			status: "error",
			message: "No Amount given"
		});
	}
};



/**
@api {post} /wallet/withDrawAmountFromWallet Withdraw From Wallet 
@apiName withDrawAmountFromWallet
@apiGroup Wallet
@apiPermission authentication_required
@apiDescription Request withdrawl of certain amount from wallet. 

The admin can then see the withdrawl request and send the money back to the user's bank account.

@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization": "JWT eyJhbGHVbz5y8i-HRRY"
	} 
	
@apiParam {Number} amount The amount to be withdrawn from the wallet.
@apiParamExample {json} Request-Example:
	HTTP/1.1 200 OK
	{
		"amount": 500
	}


@apiSuccess {String} status Status of the API Call.
@apiSuccess {String} message Message describing the status
@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
	{
	  "status": "success",
	  "message": "We received your request. Your withdrawl will be processed."
	}

@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "Unable to process your request"
	}
*/
exports.withDrawAmountFromWallet = function(req, res) {
	var userId = res.req.user._id;
	var amountLocal = req.body.amount;
	if (userId && amountLocal) {
		Wallet.findOne({
			user: userId,
			amount: {
				$gte: amountLocal
			}
		}, function(err, wallet) {
			if (err) {
				return res.status(200).send({
					status: "error",
					message: ErrorParser.parseErrorreturnErrorMessage(err)
				});
			} else if (wallet) {
				WithDrawWalletRequests.create({
					amount: amountLocal,
					createdByUser: userId
				}, function(err, result) {
					if (err) {
						return res.status(200).send({
							status: "error",
							message: ErrorParser.parseErrorreturnErrorMessage(err)
						});
					} else {
						return res.status(200).send({
							status: "success",
							message: "We received your request. Your withdrawl will be processed."
						});
					}
				});
			} else {
				return res.status(200).send({
					status: "error",
					message: "Unable to process your request"
				});
			}
		});
	} else if (amountLocal) {
		return res.status(200).send({
			status: "error",
			message: "No user id found"
		});
	} else {
		return res.status(200).send({
			status: "error",
			message: "No Amount given"
		});
	}
};


/**
@api {post} /wallet/getWalletTransactions Get Wallet Transactions
@apiName getWalletTransactions
@apiGroup Wallet
@apiPermission authentication_required
@apiDescription Get all transactions associated with the wallet

@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization": "JWT eyJhbGHVbz5y8i-HRRY"
	} 
	
@apiSuccess {String} status Status of the API Call.
@apiSuccess {String} message Message describing the status
@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
	{
	  "status": "success",
	  "message": "We received your request. Your withdrawl will be processed."
	}

@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "Unable to process your request"
	}
*/
exports.getWalletTransactions = function(req, res) {
	var userId = res.req.user._id;
	if (userId) {
		Wallet.findOne({
			user: userId
		}, function(err, wallet) {
			if (err) {
				res.status(200).send({
					error: ErrorParser.parseErrorreturnErrorMessage(err)
				});
			} else if (wallet) {
				var queryParams = {};
				queryParams['walletId'] = wallet._id;
				if (req.body.createdOnBefore) {
					queryParams['createdAt'] = {
						$lte: new Date(req.body.createdOnBefore).toGMTString()
					};
				} else if (req.body.createdAfter) {
					queryParams['createdAt'] = {
						$gte: new Date(req.body.createdAfter).toGMTString()
					};
				}
				WalletTransaction.find(queryParams).limit(100).sort('-createdAt').exec(function(err, transactions) {
					if (err) {
						res.status(200).send({
							error: ErrorParser.parseErrorreturnErrorMessage(err)
						});
					} else if (transactions) {
						var walletTransactionsLocal = [];
						for (var i = 0; i < transactions.length; i++) {
							var transactionLocal = transactions[i].toJSON();
							if (transactionLocal.transactionInfo) {
								if (transactionLocal.transactionInfo.remarks) {
									transactionLocal.remarks = transactionLocal.transactionInfo.remarks;
								}
								delete transactionLocal.transactionInfo;
							}
							walletTransactionsLocal.push(transactionLocal);
						}
						res.jsonp({
							status: 'success',
							walletTransactions: walletTransactionsLocal
						});
					} else {
						res.jsonp({
							status: 'success',
							walletTransactions: []
						});
					}
				});
			} else {
				Wallet.create({
					user: userId,
					amount: 0
				}, function(err, wallet) {
					if (err) {
						res.status(200).send({
							error: ErrorParser.parseErrorreturnErrorMessage(err)
						});
					} else {
						res.jsonp({
							status: 'success',
							walletTransactions: []
						});
					}
				});
			}
		});

	} else {
		res.status(200).send({
			error: 'No user id found'
		});
	}
}


/**
@api {get} /wallet/getWalletAmount Get Wallet Amount
@apiName GetWalletAmount
@apiGroup Wallet
@apiPermission authentication_required
@apiDescription Get wallet amount

@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization": "JWT eyJhbGHVbz5y8i-HRRY"
	} 
	
@apiSuccess {String} status Status of the API Call.
@apiSuccess {String} response Response of the API call
@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
	{
	  "status": "success",
	  "response": {
	    "walletAmount": 0,
	    "frozenAmount": 0,
	    "totalAmount": 0
	  }
	}

@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "No userId found"
	}
*/
exports.getWalletAmount = function(req, res) {
	var userId = res.req.user._id;
	if (userId) {
		Wallet.findOne({
			user: userId
		}, function(err, wallet) {
			if (err) {
				res.status(200).send({
					error: ErrorParser.parseErrorreturnErrorMessage(err)
				});
			} else if (wallet) {
				var amountFinal = wallet.amount;
				var freezedAmountLocal = wallet.freezedAmount;
				var totalAmountLocal = amountFinal + freezedAmountLocal;
				var response = {};
				response['walletAmount'] = amountFinal;
				response['frozenAmount'] = freezedAmountLocal;
				response['totalAmount'] = totalAmountLocal;
				return res.status(200).send({
					status: 'success',
					response: response
				});
			} else {
				Wallet.create({
					user: userId,
					amount: 0
				}, function(err, wallet) {
					if (err) {
						return res.status(200).send({
							error: ErrorParser.parseErrorreturnErrorMessage(err)
						});
					} else {
						var response = {};
						response['walletAmount'] = 0;
						response['frozenAmount'] = 0;
						response['totalAmount'] = 0;
						return res.status(200)({
							status: 'success',
							response: response
						});
					}
				});
			}
		});
	} else {
		return res.status(200).send({
			error: 'No user id found'
		});
	}
};


/**
@api {post} /wallet/addAmountToWalletWithNewRazorPayId Add amount to wallet with new razor pay Id
@apiName AddAmountToWalletWithNewRazorPayId
@apiGroup Wallet
@apiPermission authentication_required
@apiDescription Add amount to wallet with new razor pay Id

@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization": "JWT eyJhbGHVbz5y8i-HRRY"
	} 
	

@apiParam {String} razorpayPaymentID The razor pay Id
@apiParam {Number} amount to be added to wallet

@apiSuccess {String} status Status of the API Call.
@apiSuccess {String} response Response of the API call
@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
	{
		status: 'success',
		successData: "Added Rs. 100 to wallet."
	}

@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "Payment Id Missing"
	}
*/
exports.addAmountToWalletWithNewRazorPayId = function(req, res) {
	var userId = res.req.user._id;
	var payMentRazorPayId = req.body.razorpayPaymentID;
	var amountLocal = req.body.amount;
	if (userId && payMentRazorPayId && amountLocal) {
		UserUtils.validatePaymentRazorPay(payMentRazorPayId, amountLocal, function(err, status, body) {
			if (err) {
				res.status(200).send({
					error: err
				});
			} else if (status == 'success') {
				Wallet.findOne({
					user: userId
				}, function(err, wallet) {
					if (err) {
						res.status(200).send({
							error: ErrorParser.parseErrorreturnErrorMessage(err)
						});
					} else {
						Transaction.create({
							mihPayId: payMentRazorPayId,
							mode: 'Razor Pay',
							status: 'success',
							txnid: payMentRazorPayId,
							amount: amountLocal,
							userId: userId,
							completeData: body
						}, function(err, result) {
							if (err) {
								return res.status(200).send({
									error: ErrorParser.parseErrorreturnErrorMessage(err)
								});
							} else {
								if (wallet) {
									var amountToAdd = parseInt(amountLocal, 10) / 100;
									var amountFinal = wallet.amount + amountToAdd;
									Wallet.update({
										_id: wallet._id
									}, {
										amount: amountFinal
									}, function(err, result) {
										if (err) {
											res.status(200).send({
												error: ErrorParser.parseErrorreturnErrorMessage(err)
											});
										} else {
											var createParams = {
												transactionId: payMentRazorPayId,
												amountInvloved: amountToAdd,
												paymentType: 'credit',
												transactionInfo: {
													remarks: 'Added into wallet via Razor Pay',
													otherInfo: {}
												},
												walletId: wallet._id
											}
											WalletTransaction.create(createParams, function(err, result) {
												if (err) {
													res.status(200).send({
														error: ErrorParser.parseErrorreturnErrorMessage(err)
													});
												} else {
													res.jsonp({
														status: 'success',
														successData: 'Added Rs.' + amountToAdd + ' to your wallet.'
													});
												}
											});
										}
									});
								} else {
									var amountToAdd = parseInt(amountLocal, 10) / 100;
									Wallet.create({
										user: userId,
										amount: amountToAdd
									}, function(err, wallet) {
										if (err) {
											res.status(200).send({
												error: ErrorParser.parseErrorreturnErrorMessage(err)
											});
										} else {
											var createParams = {
												transactionId: payMentRazorPayId,
												amountInvloved: amountToAdd,
												paymentType: 'credit',
												transactionInfo: {
													remarks: 'Added into wallet via Razor Pay',
													otherInfo: {}
												},
												walletId: wallet._id
											}
											WalletTransaction.create(createParams, function(err, result) {
												if (err) {
													res.status(200).send({
														error: ErrorParser.parseErrorreturnErrorMessage(err)
													});
												} else {
													res.jsonp({
														status: 'success',
														successData: 'Added Rs.' + amountToAdd + ' to your wallet.'
													});
												}
											});
										}
									});
								}
							}
						});
					}
				});
			} else {
				res.status(200).send({
					error: 'Unknown error occurred'
				});
			}
		});
	} else if (userId && amountLocal) {
		res.status(200).send({
			error: 'Payment Id Missing'
		});
	} else if (amountLocal) {
		res.status(200).send({
			error: 'User Id Missing'
		});
	} else {
		res.status(200).send({
			error: 'Amount missing'
		});
	}
};