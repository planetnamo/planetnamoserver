var walletCtrl = require(__modules + '/wallet/controllers/wallet-controller');
var Middleware  = require(__base + '/middlewares/middleware');
var passport 	= require("passport");


module.exports = function(app, express){

	var apiRouter = express.Router();

	apiRouter.route('/welcome').get(walletCtrl.planetnamowelcome);

	apiRouter.route('/payFromWallet').post(passport.authenticate('jwt', { session: false })
											,walletCtrl.payFromWallet);

	apiRouter.route('/withDrawAmountFromWallet').post(passport.authenticate('jwt', { session: false })
													,walletCtrl.withDrawAmountFromWallet);

	apiRouter.route('/getWalletTransactions').get(passport.authenticate('jwt', { session: false })
													,walletCtrl.getWalletTransactions);

	apiRouter.route('/getWalletAmount').get(passport.authenticate('jwt', { session: false })
											,walletCtrl.getWalletAmount);

	apiRouter.route('/addAmountToWalletWithNewRazorPayId').post(passport.authenticate('jwt', { session: false })
														,walletCtrl.addAmountToWalletWithNewRazorPayId);

	apiRouter.route('/getWalletTransactions').post(passport.authenticate('jwt', { session: false })
														,walletCtrl.getWalletTransactions);

	return apiRouter;

};