var userCtrl = require(__modules + '/user/controllers/user-controller');
var Middleware = require(__base + '/middlewares/middleware.js');
var passport   = require('passport');


module.exports = function(app, express){

	var apiRouter = express.Router();


	apiRouter.route('/getUser/:userId').get(passport.authenticate('jwt', { session: false }), 
											Middleware.CheckIfAdmin, 
											userCtrl.getUser); //done


	apiRouter.route('/me').get(passport.authenticate('jwt', { session: false }), 
								userCtrl.me);//done
 	


 	apiRouter.route('/addAddress').post(passport.authenticate('jwt', { session: false }), 
 										userCtrl.addAddress); //done

 	apiRouter.route('/deleteAddress/:addressId').delete(passport.authenticate('jwt', { session: false }), 
														userCtrl.deleteAddress); //done

 	apiRouter.route('/setPrimaryAddress/:addressId').post(passport.authenticate('jwt', { session: false }), 
 										userCtrl.setPrimaryAddress); //done


 	apiRouter.route('/uploadRegisterationDoc').post(passport.authenticate('jwt', { session: false }), 
 													userCtrl.uploadRegisterationDoc); //done



	apiRouter.route('/editUser').put(passport.authenticate('jwt', { session: false }), 
									userCtrl.editUser); //done

	apiRouter.route('/editAddress/:addressId').post(passport.authenticate('jwt', { session: false }), 
									userCtrl.editAddress); //done



	apiRouter.route('/deleteUser/userId').delete(passport.authenticate('jwt', { session: false }), 
												Middleware.CheckIfAdmin, 
												userCtrl.deleteUser); //done



	apiRouter.route('/addToWishlist/:lotId').post(passport.authenticate('jwt', { session: false }), 
												userCtrl.addToWishlist); //done



	apiRouter.route('/removeFromWishlist/:lotId').delete(passport.authenticate('jwt', { session: false }), 
														userCtrl.removeFromWishlist); //done



	apiRouter.route('/getWishlist').get(passport.authenticate('jwt', { session: false }), 
										userCtrl.getWishlist); //done



	apiRouter.route('/changePassword').post(passport.authenticate('jwt', { session: false }), 
											userCtrl.changePassword); //done



	apiRouter.route('/uploadPicture').post(passport.authenticate('jwt', { session: false }),
											 userCtrl.uploadPicture); //done


 	apiRouter.route('/AddUserBankData').post(passport.authenticate('jwt', { session: false }), 
 													userCtrl.AddUserBankData); //done
	

 	apiRouter.route('/GetUserBankData').get(passport.authenticate('jwt', { session: false }), 
 													userCtrl.GetUserBankData); //done
	
	return apiRouter;

};