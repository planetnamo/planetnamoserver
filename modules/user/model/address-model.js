var mongoose        = require('mongoose');
var Schema          = mongoose.Schema;


var AddressSchema = new Schema({
    line1: {
      type: String,
      trim: true,
      required: true
    },
    line2: {
      type: String,
      trim: true
    },
    locality: {
      type: String,
      trim: true
    },
    city: {
      type: String,
      trim: true,
      required: true
    },
    state: {
      type: String,
      trim: true,
      required: true
    },
    country: {
      type: String,
      trim: true,
      required: true
    },
    zipCode: {
      type: String,
      trim: true,
      required: true
    },
    locationLat: {
      type: Number
    },
    locationLng: {
      type: Number
    }
});

module.exports = mongoose.model('Address', AddressSchema);