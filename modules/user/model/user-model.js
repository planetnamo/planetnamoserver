var mongoose        = require('mongoose');
var Schema          = mongoose.Schema;
var bcrypt          = require('bcrypt-nodejs');


var UserSchema = new Schema({
    username: {
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    password: {
        type: String,
        required: true
    },
    name: {
      type: String,
      required: true,
      trim: true
    },
    email: {
      type: String,
      required: true,
      trim: true
    },
    panCard: {
      type: String,
      trim: true,

    },
    mobileNumber: {
      type: String,
      trim: true
    },
    mobileVerified: {
      type: Boolean,
      default: false
    },
    emailVerified: {
      type: Boolean,
      default: false
    },
    emailVerificationToken: {
      type: String
    },
    mobileVerificationOtp: {
      type: String
    },
    facebookUserId: {
      type: String
    },
    googleUserId: {
      type: String
    },
    passwordUpdateToken: {
      type: String
    },
    role: {
      type: Number,
      enum: [1,2,3], // 1 - default_user 2 - admin 3 - banned_user
      default: 1
    },
    buysellPermission: {
      type: Number,
      enum: [1,2,3],  // 1 - buy only 2 - sell only 3 - both buy and sell
      default: 3
    },
    accounttype: {
      type: Number,
      enum: [1,2], // 1 - induvidual 2 - bussiness
      default: 1
    },
    greenpoints: {
      type: Number,
      default: 0
    },
    treesgrown: {
      type: Number,
      default: 0
    },
    address: [{
      type: Schema.ObjectId,
      ref: 'Address'
    }]

});

UserSchema.pre('save', function(callback) {
  var user = this;

  // Break out if the password hasn't changed
  if (!user.isModified('password')) return callback();

  // Password changed so we need to hash it
  bcrypt.genSalt(5, function(err, salt) {
    if (err) return callback(err);

    bcrypt.hash(user.password, salt, null, function(err, hash) {
      if (err) return callback(err);
      user.password = hash;
      callback();
    });
  });
});


UserSchema.methods.comparePassword = function(password) {
    var user = this;
    return bcrypt.compareSync(password, user.password);
};

module.exports = mongoose.model('User', UserSchema);