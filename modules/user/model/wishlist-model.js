var mongoose        = require('mongoose');
var Schema          = mongoose.Schema;


var WishlistSchema = new Schema({
    user: {
      type: Schema.ObjectId,
      ref: 'User',
      unique: true,
      required: true
    },
    products: [{
      type: Schema.ObjectId,
      ref: 'Product'
    }]

});


module.exports = mongoose.model('Wishlist', WishlistSchema);