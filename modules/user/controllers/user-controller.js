/**
 * Module dependencies.
 */
 
var mongoose = require('mongoose');

var User        = require(__base + '/models/user/user.js');
var Wishlist    = require(__base + '/models/user/wishlist.js');
var Address     = require(__base + '/models/user/address.js');
var Lot 	        = require(__base + '/models/lots/lot');
var BaseProduct 	= require(__base + '/models/products/base-product');
var Product 		= require(__base + '/models/products/product');


var ErrorParser = require(__utils + '/errorParse.js');
var aux         = require(__utils + '/auxilaryFunc.js');
var UsersUtils  = require(__utils + '/users.js');
var FileHandler  = require(__utils + '/FileHandler.js');
var LotsUtils	= require(__utils + '/lots');


var multer  = require('multer');
var fs = require('fs');
var mime = require('mime');
var crypto = require('crypto');


var storage_of_regDoc = multer.diskStorage({
	destination: function (request, file, callback) {
		callback(null, __base + '/uploads/userDocs');
	},
	filename: function (request, file, callback) {
		callback(null, request.user.email + Date.now() + '.' + mime.extension(file.mimetype));
		/*crypto.pseudoRandomBytes(16, function (err, raw) {
		  callback(null, raw.toString('hex') + Date.now() + '.' + mime.extension(file.mimetype));
		});*/
	}
});

var storage_of_profile_pictures = multer.diskStorage({
	destination: function (request, file, callback) {
		callback(null, __base + '/uploads/profilePictures');
	},
	filename: function (request, file, callback) {
		callback(null, request.user.email + Date.now() + '.' + mime.extension(file.mimetype));
		/*crypto.pseudoRandomBytes(16, function (err, raw) {
		  callback(null, raw.toString('hex') + Date.now() + '.' + mime.extension(file.mimetype));
		});*/
	}
});

var upload_regDoc = multer({storage: storage_of_regDoc}).single('registerationDoc');
var upload_profile_picture = multer({storage: storage_of_profile_pictures}).single('photo');








/**
 * @apiDefine admin Admin access only
 * Only users with administrator rights can access
 */

 /**
 * @apiDefine authentication_required User access only
 * Only logged in users can access
 */

//------------------------G E T-------------U S E R------------D A T A----------------------------------------------------------------

/**
@api {get} /user/getUser/:userId Get User Data

@apiName GetUserData
@apiGroup User
@apiDescription Get user details with the id userId
@apiPermission admin

@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization":  "JWT eyJhbGHVbz5y8i-HRRY"
	} 

@apiParam {String} :userId Unique user Id representing a user. Sent as a URL parameter
@apiParamExample {url} Request-Example:
	http://localhost:8080/api/user/getUser/58a70a784ded7c86e5e829e6

@apiSuccess {String} status Status of the API call
@apiSuccess {JSON} response It will contain the user information
@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
	{
	  "status": "success",
	  "response": {
	    "name": "Test User",
	    "email": "vkarun_k@yahoo.in",
	    "username": "test_user",
	    "address": [
	      {
	        "_id": "58bfbf0ce6113559ccc7a03f",
	        "locality": "Near sector 12 market",
	        "line2": "Sector 12 RK Puram",
	        "zipCode": "110022",
	        "country": "India",
	        "state": "Delhi",
	        "city": "New Delhi",
	        "line1": "1174 Type 4 Special"
	      }
	    ],
	    "panCard": "11001203232",
	    "role": 2,
	    "buySellPermission": 3,
	    "emailVerified": true,
	    "createdAt": "2017-03-08T07:21:54.125Z"
	  }
	}

@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "Inadequate information sent"
	}
*/
exports.getUser = function (req, res) {
	var userId = req.params.userId;
	var user = res.req.user;
	if(userId){
		if(user.role == 2){
			User.findById(req.params.userId)
				.populate({
					path: 'address',
					select: 'line1 line2 locality city state country zipCode locationLat locationLng'
				})
				.exec(function(err, result){
					if(err){
						return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
					}else{
						if(result){
							let userDetails = {};
							userDetails['name'] 				= result.name;
							userDetails['email'] 				= result.email;
							userDetails['username'] 			= result.username;
							userDetails['address'] 				= result.address;
							userDetails['primaryAddress'] 		= result.primaryAddress;
							userDetails['panCard'] 				= result.panCard;
							userDetails['vatNo'] 				= result.vatNo;
							userDetails['serviceTaxNo'] 		= result.serviceTaxNo;
							userDetails['role'] 				= result.role;
							userDetails['buySellPermission'] 	= result.buySellPermission; 
							userDetails['emailVerified'] 		= result.emailVerified;
							userDetails['lastLoginTime'] 		= result.lastLoginTime;
							userDetails['createdAt'] 			= result.createdAt;
							return res.status(200).send({status: 'success', response: userDetails});

						}else{
							return res.status(200).send({status: 'error', message: "User not found with userId:"+userId});
						}
					}
				});
		}else{
			return res.status(200).send({status: 'error', message: "Only Admin can access this route"});
		}
	}else {
		return res.status(200).send({status: 'error', message: "UserId not send in request."});
	}
};

//---------------------------------------------------------------------------------------------------------------------------







//------------------------G E T--------------L O G G E D-------I N-------U S E R-----D A T A----------------------------------------------

/**
@api {get} /user/me Get Logged In User Info
@apiName GetLoggedInUserInfo
@apiGroup User
@apiDescription Get user details of logges in user
@apiPermission authentication_required

@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization":  "JWT eyJhbGHVbz5y8i-HRRY"
	} 

@apiSuccess {String} status Status of the API Call.
@apiSuccess {JSON} result It will contain the user information
@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
	{
	  "status": "success",
	  "response": {
	    "name": "Test User",
	    "email": "vkarun_k@yahoo.in",
	    "username": "test_user",
	    "address": [
	      {
	        "_id": "58bfbf0ce6113559ccc7a03f",
	        "locality": "Near sector 12 market",
	        "line2": "Sector 12 RK Puram",
	        "zipCode": "110022",
	        "country": "India",
	        "state": "Delhi",
	        "city": "New Delhi",
	        "line1": "1174 Type 4 Special"
	      },
	      {
	        "_id": "58bfbf4ee6113559ccc7a040",
	        "locality": "Near sector 12 market",
	        "line2": "Sector 12 RK Puram",
	        "zipCode": "110022",
	        "country": "India",
	        "state": "Delhi",
	        "city": "New Delhi",
	        "line1": "1174 Type 4 Special"
	      },
	      {
	        "_id": "58bfbf5d32d9195a1636690d",
	        "locality": "Near sector 12 market",
	        "line2": "Sector 12 RK Puram",
	        "zipCode": "110022",
	        "country": "India",
	        "state": "Delhi",
	        "city": "New Delhi",
	        "line1": "1174 Type 4 Special"
	      }
	    ],
	    "panCard": "11001203232",
	    "role": 2,
	    "buySellPermission": 3,
	    "emailVerified": true,
	    "createdAt": "2017-03-08T07:21:54.125Z"
	  }
	}

@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "Inadequate information sent"
	}
*/
exports.me = function (req, res) {
	var user = req.user;
	console.log(res.req.user);
	if(user){
		if(1){
			User.findById(user._id)
				.populate({
					path: 'address',
					select: 'line1 line2 locality city state country zipCode locationLat locationLng'
				})
				.populate({
					path: 'primaryAddress',
					select: 'line1 line2 locality city state country zipCode locationLat locationLng'
				})
				.exec(function(err, result){
					if(err){
						return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
					}else{
						if(result){
							let userDetails = {};
							userDetails['userId'] 				= result._id;
							userDetails['name'] 				= result.name;
							userDetails['email'] 				= result.email;
							userDetails['username'] 			= result.username;
							userDetails['mobile']			    = result.mobileNumber;
							userDetails['address'] 				= result.address;
							userDetails['primaryAddress'] 		= result.primaryAddress;
							userDetails['panCard'] 				= result.panCard;
							userDetails['vatNo'] 				= result.vatNo;
							userDetails['serviceTaxNo'] 		= result.serviceTaxNo;
							userDetails['role'] 				= result.role;
							userDetails['buySellPermission'] 	= result.buySellPermission; 
							userDetails['accountType']			= result.accountType;
							userDetails['emailVerified'] 		= result.emailVerified;
							userDetails['lastLoginTime'] 		= result.lastLoginTime;
							userDetails['createdAt'] 			= result.createdAt;
							return res.status(200).send({status: 'success', response: userDetails});

						}else{
							return res.status(200).send({status: 'error', message: "User not found with userId:"+userId});
						}
					}
				});
		}else{
			return res.status(200).send({status: 'error', message: "Only Admin can access this route"});
		}
	}else {
		return res.status(200).send({status: 'error', message: "UserId not send in request."});
	}
};


//---------------------------------------------------------------------------------------------------------------------------







//---------------------A D D-----------------A D D R E S S--------------------------------------------------------------------------------


/**
@api {post} /user/addAddress Add Address
@apiName AddAddress
@apiGroup User
@apiDescription Add user address. A user can have more than one address.
@apiPermission authentication_required
@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization":  "JWT eyJhbGHVbz5y8i-HRRY"
	} 

@apiParam {String} line1 Line 2 of the location address
@apiParam {String} [line2] line2 Line 2 of the location address
@apiParam {String} city City of the location
@apiParam {String} state State of the location
@apiParam {String} country Country of the location
@apiParam {String} zipCode Zipcode of the address
@apiParam {String} [locality] locality Describe the locality of the location
@apiParam {String} [locationLat] locationLat Location lattitude
@apiParam {String} [locationLng] locationLng Location longitude
@apiParamExample {json} Request-Example:
{
	"line1": "1174 Type 4 Special",
	"line2": "Sector 12 RK Puram",
	"city": "New Delhi",
	"state": "Delhi",
	"country": "India",
	"zipCode": 110022,
	"locality": "Near sector 12 market"
}

@apiSuccess {String} status Status of the API Call.
@apiSuccess {JSON} response It will contain the newly added address.
@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
	{
	  "status": "success",
	  "response": {
	    "__v": 0,
	    "locality": "Near sector 12 market",
	    "line2": "Sector 12 RK Puram",
	    "userId": "58a70a784ded7c86e5e829e6",
	    "zipCode": "110022",
	    "country": "India",
	    "state": "Delhi",
	    "city": "New Delhi",
	    "line1": "1174 Type 4 Special",
	    "_id": "58bfbf5d32d9195a1636690d"
	  }
	}

@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "Missing fields"
	}
*/
exports.addAddress = function(req, res) {
	var loggedInUserData = res.req.user;
	if(loggedInUserData){
		User.findById(loggedInUserData._id, function(err, user){
			if(err){
				return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
			}else{
				let line1 = req.body.line1;
				let line2 = req.body.line2;
				let locality = req.body.locality;
				let city = req.body.city;
				let state = req.body.state;
				let country = req.body.country;
				let zipCode = req.body.zipCode;
				let locationLat = req.body.locationLat;
				let locationLng = req.body.locationLng;


				if(line1 && city && state && country && zipCode){
					var address = new Address();
					address.line1 = line1;
					address.city = city;
					address.state = state;
					address.country = country;
					address.zipCode = zipCode;
					address.userId = loggedInUserData._id;

					if(line2) address.line2 = line2;
					if(locality) address.locality = locality;
					if(locationLat) address.locationLat = locationLat;
					if(locationLng) address.locationLng = locationLng;
					
					address.save(function(err, address) {
						if(err){
							return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
						}
						else{
							user.address.push(address._id);
							if(user.address.length == 1){
								user.primaryAddress = address._id;
							}
							user.save(function(err){
								if(err){
									return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
								}else{
						            return res.status(200).send({ 
						            	status: 'success',
						            	response: address
						        	});
								}
							});

						}
					});
				}else{
					return res.status(200).send({status: "error", message: "Missing fields"});
				}
			}
		});
	}else {
		return res.status(200).send({status: "error", message: "User not logged in."});
	}
};

//---------------------------------------------------------------------------------------------------------------------------







//---------------------S E T-----------------P R I M A R  Y-----------A D D R E S S--------------------------------------------------------------


/**
@api {post} /user/setPrimaryAddress/:addressId Set Primary Address
@apiName setPrimaryAddress
@apiGroup User
@apiDescription Set the primary address of the user
@apiPermission authentication_required
@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization":  "JWT eyJhbGHVbz5y8i-HRRY"
	} 

@apiParam {addressId} id of the address to be set as primary


@apiSuccess {String} status Status of the API Call.
@apiSuccess {JSON} response It will contain the new primary address
@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
	{
	  "status": "success",
	  "response": {
	    "__v": 0,
	    "locality": "Near sector 12 market",
	    "line2": "Sector 12 RK Puram",
	    "userId": "58a70a784ded7c86e5e829e6",
	    "zipCode": "110022",
	    "country": "India",
	    "state": "Delhi",
	    "city": "New Delhi",
	    "line1": "1174 Type 4 Special",
	    "_id": "58bfbf5d32d9195a1636690d"
	  }
	}

@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "No address found for address Id"
	}
*/
exports.setPrimaryAddress = function(req, res) {
	var user = req.user;
	var addressId = req.params.addressId;
	User.findById(user._id, function(err, user){
		Address.findOne({userId: user._id, _id: addressId}, function(err, userAddress){
			if(err){
				return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
			}else{
				if(userAddress){
					console.log("first check");
					console.log(user.address);
					var present = false;
					for(var i=0; i<user.address.length; i++){
						console.log(user.address[i], userAddress._id);
						if(user.address[i].equals(userAddress._id)){
							present = true;
						}
					}
					if(present){
						user.primaryAddress = userAddress._id;
						user.save(function(err){
							if(err){
								return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
							}else{
								return res.status(200).send({status: "success", message: "Primary address changed", primaryAddress: userAddress});
							}
						});
					}else{
						return res.status(200).send({status: "error", message: "No address found for specified addressId"});
					}
				}else{
					return res.status(200).send({status: "error", message: "No address found for specified addressId"});
				}
			}
		});
	});
};

//---------------------------------------------------------------------------------------------------------------------------









//---------------U P L O A D--------R E G I S T R A T I O N------D O C U M E N T--------------------------------------------------------------------------------


/**
@api {post} /user/uploadRegisterationDoc Upload Registeration Document
@apiName  UploadRegisterationDocument
@apiGroup User
@apiDescription Upload the registeration document. One user has can have only one registeration document.

If the document is already uploaded and this route is used again to upload another one, the old one will be deleted and replaced with the new one.

@apiPermission authentication_required

@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization":  "JWT eyJhbGHVbz5y8i-HRRY"
	} 

@apiParam {String} :categoryId Unique Id representing a product category. Sent as a URL parameter
@apiParam {File}	registerationDoc The file which is the registeration document which is to be uploaded.
@apiParamExample {json} Request-Example:
{
	"registerationDoc": regDoc
}


@apiSuccess {String} status Status of the API Call.
@apiSuccess {String} message Message explaining the 
@apiSuccess {String} registerationDocURL Registeration link of the doc
@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
	{
	  "status": "success",
	  "message": "Registeration document uploaded and successfully. Old registeration doc was not found.",
	  "registerationDocURL": "http://localhost:8080/api/getFile/userDocs/ladygaga1490024022114.pdf"
	}

@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
	  "status: "error",
	  "message": "User not found for corresponding Id"
	}
*/
exports.uploadRegisterationDoc = function (req, res) {
	var user = res.req.user;
	if(user){
		User.findById(user._id)
			.exec(function(err, user){
				if(!user.registerationDoc.path){
					upload_regDoc(req, res, function(err){
						if(err){
							return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
						}else{
							var file = req.file;
							user.registerationDoc = file;
							user.save(function(err, updatedUser){
								if(err){
									return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
								}else{
									return res.status(200).send({status: "success", message: "Registeration document uploaded successfully."});
								}
							});
						}
					});
				}else{
					upload_regDoc(req, res, function(err){
						if(err){
							return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
						}else{
							fs.unlink(user.registerationDoc.path, function(err){
								if(err){
									var file = req.file;
									user.registerationDoc = file;
									user.save(function(err, updatedUser){
										if(err){
											return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
										}else{
											return res.status(200).send({status: "success", message: "Registeration document uploaded and successfully. Old registeration doc was not found."});
										}
									});
								}else{
									var file = req.file;
									user.registerationDoc = file;
									user.save(function(err, updatedUser){
										if(err){
											return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
										}else{
											return res.status(200).send({status: "success", message: "Registeration document uploaded successfully. The new registeration doc replaced the old one successfully."});
										}
									});
								}
							});
						}
					});
				}
			});
	}else{
		return res.status(200).send({status: "error", message: "User not logged in."});
	}
};

//---------------------------------------------------------------------------------------------------------------------------






//------------------------------E D I T---------U S E R----------------------------------------------------------------------------



/**
@api {put} /user/editUser Edit User
@apiName EditUser
@apiGroup User
@apiDescription Edit user details
@apiPermission authentication_required
@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization":  "JWT eyJhbGHVbz5y8i-HRRY"
	} 

@apiParam {String} name [name] Name of the user
@apiParam {String} mobileNumber [mobileNumber] Mobile Number of the user
@apiParam {String} panCard [panCard] Pancard of the user. Only bussiness type users can edit this field.
@apiParam {String} vatNo [vatNo] Vat number  of the user. Only bussiness type users can edit this field.
@apiParam {String} serviceTaxNo [serviceTaxNo] Service tax number of the user. Only bussiness type users can edit this field.

@apiParam {String} line1 Line 2 of the primary address
@apiParam {String} [line2] line2 Line 2 of the primary address
@apiParam {String} city City of the primary address
@apiParam {String} state State of the primary address
@apiParam {String} country Country of the primary address
@apiParam {String} zipCode Zipcode of the primary address
@apiParam {String} [locality] locality Describe the locality of the primary address
@apiParam {String} [locationLat] locationLat Location primary address
@apiParam {String} [locationLng] locationLng Location primary address


@apiParamExample {json} Request-Example:
{
	name: "Different name"
}

@apiSuccess {String} status Status of the API Call.
@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
	{
	  "status": "success",
	  "message": 'User details edited'
	}

@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
	  "status": 'error',
	  "message": "User not logged in"
	}
*/
exports.editUser = function (req, res) {
	var loggedInUserData = res.req.user;

	if(loggedInUserData){
		User.findById(loggedInUserData._id, function(err, user){
			if(err){
				return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
			}else{
				if(user){
					let name 		 = req.body.name;
					let panCard 	 = req.body.panCard;
					let vatNo 		 = req.body.vatNo;
					let serviceTaxNo = req.body.serviceTaxNo;
					let mobileNumber = req.body.mobileNumber;
					let line1		 = req.body.line1;
					let line2		 = req.body.line2;
					let city		 = req.body.city;
					let state		 = req.body.state;
					let country		 = req.body.country;
					let zipCode		 = req.body.zipCode;
					let locality	 = req.body.locality;
					let locationLat	 = req.body.locationLat;
					let locationLng	 = req.body.locationLng;

					if(name) user.name = name;
					if(mobileNumber) user.mobileNumber = mobileNumber;

					if(loggedInUserData.accountType == 2){
						if(panCard) user.panCard = panCard;
						if(vatNo) user.vatNo = vatNo;
						if(serviceTaxNo) user.serviceTaxNo = serviceTaxNo;
					}
					console.log((vatNo || serviceTaxNo || panCard) && loggedInUserData.accountType == 1, '$');
					if((vatNo || serviceTaxNo || panCard) && loggedInUserData.accountType == 1){
						return res.status(200).send({status: "error", message: "Induvidual type user cannot change/access vatNo or serviceTaxNo or panCard."});
					}
					if((line1 || line2 || city || state || country || zipCode || locality || locationLat || locationLng)){
						if(user.primaryAddress){
							Address.findById(user.primaryAddress, function(err, primaryAddress){
								if(err){
									return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
								}else{
									if(primaryAddress){
										if(line1) primaryAddress.line1 = line1;
										if(line2) primaryAddress.line2 = line2;
										if(city)  primaryAddress.city = city;
										if(state) primaryAddress.state = state;
										if(country) primaryAddress.country = country;
										if(zipCode) primaryAddress.zipCode = zipCode;
										if(locality) primaryAddress.locality = locality;
										if(locationLat) primaryAddress.locationLat = locationLat;
										if(locationLng) primaryAddress.locationLng = locationLng;
										primaryAddress.save(function(err){
											if(err){
												return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
											}else{
												user.save(function(err, user){
													if(err){
														return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
													}else{
														return res.status(200).send({status: 'success', message: 'User details edited'});
													}
												});
											}
										});
									}else{
										var address = new Address();
										var address = new Address();
										address.line1 = line1;
										address.city = city;
										address.state = state;
										address.country = country;
										address.zipCode = zipCode;
										address.userId = loggedInUserData._id;

										if(line2) address.line2 = line2;
										if(locality) address.locality = locality;
										if(locationLat) address.locationLat = locationLat;
										if(locationLng) address.locationLng = locationLng;
										
										address.save(function(err, address) {
											if(err){
												return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
											}
											else{
												user.address.push(address._id);
												if(user.address.length == 1){
													user.primaryAddress = address._id;
												}
												user.save(function(err){
													if(err){
														return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
													}else{
											            return res.status(200).send({ 
											            	status: 'success',
											            	response: address
											        	});
													}
												});

											}
										});
									}
								}
							});
						}else{
								var address = new Address();
								var address = new Address();
								address.line1 = line1;
								address.city = city;
								address.state = state;
								address.country = country;
								address.zipCode = zipCode;
								address.userId = loggedInUserData._id;

								if(line2) address.line2 = line2;
								if(locality) address.locality = locality;
								if(locationLat) address.locationLat = locationLat;
								if(locationLng) address.locationLng = locationLng;
								
								address.save(function(err, address) {
									if(err){
										return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
									}
									else{
										user.address.push(address._id);
										if(user.address.length == 1){
											user.primaryAddress = address._id;
										}
										user.save(function(err){
											if(err){
												return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
											}else{
									            return res.status(200).send({ 
									            	status: 'success',
									            	response: address
									        	});
											}
										});

									}
								});
						}
					}else{
						user.save(function(err, user){
							if(err){
								return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
							}else{
								return res.status(200).send({status: 'success', message: 'User details edited'});
							}
						});
					}
				}else{
					return res.status(200).send({status: 'error', message: 'No user found'});
				}
			}
		});
	}else {
		return res.status(200).send({status: 'error', message: "User not logged in."});
	}
};



//---------------------------------------------------------------------------------------------------------------------------







//---------------------------------------------------------------------------------------------------------------------------

//TO BE BUILT---------------------------------------------
exports.EditMobileNumber = function(req, res){

};

exports.EditPrimaryEmail = function(req, res){

};

//--------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------------------




//-------------------------E D I T--------------------A D D R E S S-------------------------------------------------------------------
/**
@api {post} /user/editAddress/:addressId Edit Address
@apiName editAddress
@apiGroup User
@apiDescription Edit a particular address which the user added. Adress with id :addressId is edited.
@apiPermission authentication_required

@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization":  "JWT eyJhbGHVbz5y8i-HRRY"
	} 

@apiParam {String} line1 Line 2 of the location address
@apiParam {String} [line2] line2 Line 2 of the location address
@apiParam {String} city City of the location
@apiParam {String} state State of the location
@apiParam {String} country Country of the location
@apiParam {String} zipCode Zipcode of the address
@apiParam {String} [locality] locality Describe the locality of the location
@apiParam {String} [locationLat] locationLat Location lattitude
@apiParam {String} [locationLng] locationLng Location longitude
@apiParamExample {json} Request-Example:
{
	"line1": "1174 Type 4 Special",
	"line2": "Sector 12 RK Puram",
	"city": "New Delhi",
	"state": "Delhi",
	"country": "India",
	"zipCode": 110022,
	"locality": "Near sector 12 market"
}

@apiSuccess {String} status Status of the API Call.
@apiSuccess {JSON} response It will contain the newly added address.
@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
	{
	  "status": "success",
	  "response": {
	    "__v": 0,
	    "locality": "Near sector 12 market",
	    "line2": "Sector 12 RK Puram",
	    "userId": "58a70a784ded7c86e5e829e6",
	    "zipCode": "110022",
	    "country": "India",
	    "state": "Delhi",
	    "city": "New Delhi",
	    "line1": "1174 Type 4 Special",
	    "_id": "58bfbf5d32d9195a1636690d"
	  }
	}

@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "Inadequate information sent"
	}
*/
exports.editAddress = function (req, res) {
	var loggedInUserData = req.user;
	var addressId = req.params.addressId;
	if(loggedInUserData && addressId){
		User.findById(loggedInUserData._id, function(err, user){
			if(err){
				return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
			}else{
				let line1 = req.body.line1;
				let line2 = req.body.line2;
				let locality = req.body.locality;
				let city = req.body.city;
				let state = req.body.state;
				let country = req.body.country;
				let zipCode = req.body.zipCode;
				let locationLat = req.body.locationLat;
				let locationLng = req.body.locationLng;


				if(line1 && city && state && country && zipCode){
					Address.findById(addressId, function(err, address){
						if(err){
							return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
						}else{
							address.line1 = line1;
							address.city = city;
							address.state = state;
							address.country = country;
							address.zipCode = zipCode;
							address.userId = loggedInUserData._id;

							if(line2) address.line2 = line2;
							if(locality) address.locality = locality;
							if(locationLat) address.locationLat = locationLat;
							if(locationLng) address.locationLng = locationLng;
							
							address.save(function(err, address) {
								if(err){
									return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
								}
								else{
							            return res.status(200).send({ 
							            	status: 'success',
							            	response: address
							        	});
								}
							});
						}
					});
				}else{
					return res.status(200).send({status: "error", message: "Missing fields"});
				}
			}
		});
	}else {
		return res.status(200).send({status: "error", message: "User not logged in."});
	}
};



//---------------------------------------------------------------------------------------------------------------------------





//-------------------------D E L E T E--------------------A D D R E S S-------------------------------------------------------------------
/**
@api {delete} /user/deleteAddress/:addressId Delete Address
@apiName DeleteAddress
@apiGroup User
@apiDescription Delete a particular address which the user added. Adress with id :addressId is deleted.
@apiPermission authentication_required

@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization":  "JWT eyJhbGHVbz5y8i-HRRY"
	} 

@apiParam {String} :addressId Unique Id representing an address registered by a user.
@apiParamExample {URL} Request-Example:
	http://localhost:8080/api/user/deleteAddress/58aecb442c64ec26bd759b76

@apiSuccess {String} status Status of the API Call.
@apiSuccess {JSON} response It will contain the remaining addresses of the user.
@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
	{
	  "status": "success",
	  "response": []
	  
	}

@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "Inadequate information sent"
	}
*/
exports.deleteAddress = function (req, res) {
	var addressId = req.params.addressId;
	var loggedInUserData = res.req.user;
	if(addressId && loggedInUserData){
		Address.findById(addressId, function(err, address){
			if(err){
				return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
			}
			if(!address) return res.status(200).send({status: 'error', message: 'No such address found.'});

			Address.remove({
				_id: addressId
			}, function(err){
				if(err){
					return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
				}
				else{
					User.findById(res.req.user._id, function(err, user){
						if(err){
							return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
						}
						if(!user){
							return res.status(200).send({status: 'error', message: 'No such user found.'});
						}
						var index = user.address.indexOf(req.body.addressId);
						if(index!=-1)
							user.address.splice(index, 1);
				        user.save(function(err, user) {
				            if (err){
				            	return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
				            }
				            return res.status(200).send({ status: 'success', response: user.address });
				        });
					});
				}
			});
		});
	}else {
		return res.status(200).send({status: 'error', message: "User not logged in or addressId not send in request."});
	}
};



//---------------------------------------------------------------------------------------------------------------------------





//-------------------------------D E L E T E---------------U S E R---------------------------------------------------------------------
/**
@api {delete} /user/deleteUser/:userId Delete User
@apiName DeleteUser
@apiGroup User
@apiDescription Simply delete a user
@apiPermission admin

@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization":  "JWT eyJhbGHVbz5y8i-HRRY"
	} 

@apiParam {String} :userId Unique Id representing a user. Sent as a URL parameter

@apiSuccess {String} status Status of the API Call.
@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
	{
 	  "status": "success",
	  "message": "User deleted successfully."
	}

@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "Inadequate information sent"
	}
*/
exports.deleteUser = function(req, res) {
	var loggedInUserData = res.params.userId;

	if(loggedInUserData){
		User.findById(res.req.user._id, function(err, user){
			if(err){
				return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
			}
			if(!user){
				return res.status(200).send({status: 'error', message: 'No such user found.'});
			}

			User.remove({
				_id: res.req.user._id
			}, function(err, user){
				if(err){ 
					return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
				}
				else{
					return res.status(200).json({
						status: 'success',
						message: 'User deleted successfully.'
					});
				}
			});
		});
	}else {
		return res.status(200).send({status: 'error', message: "User not logged in."});
	}
};

//---------------------------------------------------------------------------------------------------------------------------






//-----------------------A D D---------T O -------------------W I S H L I S T------------------------------------------------------------

/**
@api {post} /user/addToWishlist/:lotId Add to Wishlist
@apiName AddToWishlist
@apiGroup User
@apiDescription Add lots to user wishlist
@apiPermission authentication_required

@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization":  "JWT eyJhbGHVbz5y8i-HRRY"
	} 

@apiParam {String} :lotId Unique lotId representing a final lot. Sent as a URL parameter

@apiSuccess {String} status Status of the API Call.
@apiSuccess {JSON} response Response of the API Call. It contains the wishlist of the user.
@apiSuccess {JSON} response.lots Array of lotIds which are in the user's wishlist

@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
	{
	  "status": "success",
	  "response": {
	    "__v": 0,
	    "userId": "58c14475ef21b44e74ed817d",
	    "_id": "58cbb806c5f68f2edecc5dd4",
	    "lots": [
	      "58c80b1439548bf3ce47304d"
	    ]
	  }
	}

@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "Lot already included in wishlist"
	}
*/
exports.addToWishlist = function (req, res) {
	var lotId = req.params.lotId;
	var user = res.req.user;
	if(lotId){
		Lot.findById(lotId)
		   .exec(function(err, lot){
		   		if(err){
		   			return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
		   		}else{
		   			if(lot){
		   				Wishlist.findOne({userId: user._id})
		   						.exec(function(err, wishlist){
		   							if(wishlist){
		   								if(wishlist.lots.some(function(existinglotId){
		   									return existinglotId.equals(lotId);	
		   								})){
		   									return res.status(200).send({status: 'error', message: 'Lot already included in wishlist'});
		   								}else{
		   									wishlist.lots.push(lotId);
			   								wishlist.save(function(err, updatedWishList){
			   									if(err){
			   										return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
			   									}else{
			   										return res.status(200).send({status: 'success', response: updatedWishList});
			   									}
			   								});
		   								}
		   							}else{
		   								let wishlist = new Wishlist();
		   								wishlist.userId = user._id;
		   								wishlist.lots = [];
		   								wishlist.lots.push(lotId);
		   								wishlist.save(function(err, updatedWishList){
		   									if(err){
		   										return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
		   									}else {
		   										return res.status(200).send({status: 'success', response: updatedWishList});
		   									}
		   								});
		   							}
		   						});
		   			}else{
		   				return res.status(200).send({status: 'error', message: "Lot with lotId not found"});
		   			}
		   		}
		   });
	}else{
		return res.status(200).send({status: 'error', message: "lotId not sent"});
	}

};


//---------------------------------------------------------------------------------------------------------------------------




//--------------------------G E T-----------W I S H L I S T------------------------------------------------------------------------------

/**
@api {get} /user/getWishlist Get Wishlist
@apiName  GetWishlist
@apiGroup User
@apiDescription Get the wishlist of the user
@apiPermission authentication_required

@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization":  "JWT eyJhbGHVbz5y8i-HRRY"
	} 

@apiSuccess {String} status Status of the API Call.
@apiSuccess {JSON} response Array of wishlist items.
@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
	{
	  "status": "success",
	  "response": [
	    {
	      "startDate": "2015-01-11T18:30:00.000Z",
	      "endDate": "2017-01-11T18:30:00.000Z",
	      "lotType": "buy",
	      "lotId": "58aca8f5f32dad673c09dac9",
	      "lotContents": [
	        {
	          "productId": "58a73bbf71cd35dab7a47ade",
	          "baseProductId": "58a718c4e03f379fcad55b7a",
	          "baseProductName": "Oneplus 3T",
	          "baseProductBrandName": "Oneplus",
	          "quantityAvailableInLot": 1,
	          "priceInLot": 343
	        }
	      ]
	    }
	  ]
	}

@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
	  "error": "Inadequate information sent"
	}
*/
exports.getWishlist = function (req, res) {
	var loggedInUserData = res.req.user;
	if(loggedInUserData){
		var queryParams = {
			'userId': loggedInUserData._id
		}
		Wishlist.findOne(queryParams)
				.populate({
					path: 'lots',
					model: Lot,
					populate: {
						path: 'lotContents.pId',
						model: Product,
						populate: {
							path: 'baseProductId',
							model: BaseProduct
						}
					}
				})
				.exec(function(err, wishlist){
					if(err){
						return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});	
					}else{
						if(wishlist){
							var wishlistLots = wishlist.lots;
				            var response = {};
				            response['status'] = "success";
				            response['numberOfLots'] = wishlist.length;
				            response['response'] = [];

				            let i = 0;
				            function asyncFor(i){
				              console.log('i = ',i);
				              console.log("filter len = ", wishlistLots.length);
				              if(i<wishlistLots.length){
				                LotsUtils.ParseFewLotDetails(wishlistLots[i], null, function(err, induvidualParsedLot){
				                  if(err){
				                    console.log("error occured in ", i);
				                    console.log(err);
				                    return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
				                  }else{
				                    console.log("response pushed", i);
				                    response['response'].push(induvidualParsedLot);
				                  }
				                  console.log("incrementing i");
				                  i+=1;
				                  asyncFor(i);
				                });
				              }else{
				                console.log("done async work");
				                return res.status(200).send(response);
				              }
				            }
				            asyncFor(i);

						}else{
							return res.status(200).send({status: 'error', message: 'User has not yet a created a wishlist'});
						}
					}
				});
	}else {
		res.status(200).send({status: 'error', message: "User not logged in."});
	}
};


//---------------------------------------------------------------------------------------------------------------------------






//---------------------------R E M O V E----------F R O M-----------------W I S H L I S T------------------------------------------------------
/**
@api {delete} /user/removeFromWishlist/:lotId Remove from Wishlist
@apiName RemoveFromWishlist
@apiGroup User
@apiDescription Remove lot from user wishlist
@apiPermission authentication_required

@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization":  "JWT eyJhbGHVbz5y8i-HRRY"
	} 

@apiParam {String} :lotId Unique Id representing a final lot. Sent as a URL parameter
@apiParamExample {URL} Request-Example:
	http://localhost:8080/api/user/removeFromWishlist/58aecb442c64ec26bd759b76

@apiSuccess {String} status Status of the API Call.
@apiSuccess {JSON} wishlist Wishlist will be an object containing the updated wishlist
@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
	{
	  "status": "success",
	  "wishlist": {
	    "__v": 0,
	    "userId": "58a70a784ded7c86e5e829e6",
	    "_id": "58bfce5a637d8666d91d7388",
	    "lots": [
	      "58aca8f5f32dad673c09dac9"
	    ]
	  }
	}

@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
		"status": 'error',
		"error": "Inadequate information sent"
	}
*/

exports.removeFromWishlist = function (req, res) {
	var loggedInUserData = res.req.user;
	var lotId = req.params.lotId;

	if(loggedInUserData){
		if(lotId){
			var queryParams = {
				'userId': loggedInUserData._id
			}
		 	Wishlist.findOne(queryParams, function (err, wishlist) {
				if (err) {
					return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});	
				}else{
					if(wishlist){
						var index = wishlist.lots.indexOf(lotId);
						if(index!=-1)
							wishlist.lots.splice(index, 1);
						wishlist.save(function(err, wishlist) {
							if (err){
								return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
							}
							else
								return res.status(200).send({status: "success", wishlist: wishlist});
						});
					}else{
						return res.status(200).send({status: 'error', message: "Wishlist is empty."});
					}
				}
			});
		}else {
			return res.status(200).send({status: 'error', message: "lotId not sent in request."});
		}
	}else {
		return res.status(200).send({status: 'error', message: "User not logged in."});
	}
};

//---------------------------------------------------------------------------------------------------------------------------





//--------------------------C H A N G E----------------P A S S W O R D-----------------------------------------------------------------------
/**
@api {post} /user/changePassword Change Password
@apiName changePassword
@apiGroup User
@apiDescription Change a user account password by confirming the old password. 

Cannot change password if the login type is via social media.
@apiPermission authentication_required

@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization":  "JWT eyJhbGHVbz5y8i-HRRY"
	} 

@apiParam {String} oldPassword The old password which is to be changed.
@apiParam {String} newPassword The new password which will replace the old password.
@apiParamExample {json} Request-Example:
{
	"newPassword": "sylvester2017",
	"oldPassword": "sylvester2016"
}

@apiSuccess {String} status Status of the API Call.
@apiSuccess {JSON} wishlist Wishlist will be an object containing the updated wishlist
@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
	{
	  "status": "success",
	  "message": "Password Changed"
	}

@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "New password cannot be same as the old password"
	}
*/
exports.changePassword = function(req, res) {
	var user = res.req.user;
	var newPassword = req.body.newPassword;
	var oldPassword = req.body.oldPassword;
	if(user){
		if(!user.facebookUserId && !user.googleUserId){
			if(oldPassword && newPassword){
				var validPassword = user.comparePassword(oldPassword);
				if(validPassword){
					if(newPassword != oldPassword){
						User.findById(user._id)
							.exec(function(err, user){
								if(err){
									return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
								}
								else{
									if(user){
										user.password = newPassword;
										user.save(function(err){
											if(err){
												return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
											}else{
												return res.status(200).send({status: "success", message: "Password Changed"});
											}
										});
									}else{
										return res.status(200).send({status: "error", message: "Unexpected error, user not found"});
									}
								}
							});
					}else{
						return res.status(200).send({status: "error", message: "New password cannot be same as the old password"});
					}
				}else{
					return res.status(200).send({status: "error", message: "Old password does not match"});
				}
			}else{
				return res.status(200).send({status: "error", message: "Incomplete information"});
			}
		}else{
			if(user.facebookUserId){
				return res.status(200).send({status: "error", message: "Cannot change password if you have logged in via Facebook."});
			}	
			if(user.googleUserId){
				return res.status(200).send({status: "error", message: "Cannot change password if you have logged in via Google."});
			}	
		}
	}else{
		return res.status(200).send({status: "error", message: "User not logged in"});
	}
};

//---------------------------------------------------------------------------------------------------------------------------






//----------------------U P L O A D-----------------P R O F I L E ----------------P I C T U R E-----------------------------------------------------


/**
@api {post} /user/uploadPicture Upload profile picture
@apiName UploadDisplayPicture
@apiGroup User
@apiDescription Upload user profile picture. If profile picture already exists, then the older one will be replaced.
@apiPermission authentication_required

@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization":  "JWT eyJhbGHVbz5y8i-HRRY"
	} 

@apiParam {file} photo The file to be uploaded
@apiParamExample {json} Request-Example:
{
	"photo": profile_pic.jpg
}

@apiSuccess {String} status Status of the API Call.
@apiSuccess {String} message Message describing the status.
@apiSuccess {String} imageURL Profile picture URL
@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
	{
	  "status": "success",
	  "message": "Profile picture uploaded successfully.",
	  "imageURL": "http://localhost:8080/api/getFile/profilePicture/karunk1489430174195.jpeg"
	}

@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "File could not be uploaded."
	}
*/
exports.uploadPicture = function(req, res){
	upload_profile_picture(req, res, function(err) {
		if(err) {
			return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
		}else {
			var file = req.file;
			if(file){
				var user = res.req.user;
				if(user){
					User.findById(user._id)
						.exec(function(err, user){
							if(err){
								return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
							}else{
									if(!user.profilePicture.path){
										user.profilePicture = file;
										user.save(function(err, user){
											if(err){
												return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
											}else{
												return res.status(200).send({status: 'success', message: "Profile picture uploaded successfully."});
											}
										});
									}else{
										fs.stat(user.profilePicture.path, function (err, stats) {
											if (err) {
											   return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
											}
											fs.unlink(user.profilePicture.path,function(err){
											    if(err){
											    	 return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
											    }else {
													user.profilePicture = file;
													user.save(function(err, user){
														if(err){
															return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
														}else{
															return res.status(200).send({status: 'success', message: "Profile picture uploaded successfully."});
														}
													});
											    }
											    
											});  
										});
									}
							}
						});
				}else{
					return res.status(200).send({status: 'error', message: "User not logged in."});
				}
			}else{
				return res.status(200).send({status: 'error', message: "Inadequate information sent"});
			}
		}
	});
};


//---------------------------------------------------------------------------------------------------------------------------


//-------------add user bank data--------------------------------------------



/**
@api {post} /user/AddUserBankData Add user bank data
@apiName AddUserBankData
@apiGroup User
@apiDescription Add the user bank data
@apiPermission authentication_required

@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization":  "JWT eyJhbGHVbz5y8i-HRRY"
	} 

@apiParam {String} beneficiaryName Benificiary name
@apiParam {String} bankName Name of the bank
@apiParam {String} IFSCCode IFSC Code
@apiParam {String} beneficiaryAccountNumber Benificiary account number
@apiParam {String} branchName Branch name
@apiParam {String} city City of the bank
@apiParamExample {JSON} Request-Example:
{
	"bankData": {
		"beneficiaryName": "desnk",
		"bankName": "ewdw",
		"IFSCCode": "ewjd",
		"beneficiaryAccountNumber": "wedm",
		"branchName": "wej",
		"city": "ewmk"
	}
}


@apiSuccess {String} status Status of the API Call.
@apiSuccess {String} message Message describing the status

@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
	{
	  "status": "success",
	  "message": "Bank details saved."
	}

@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "Unauthorized user"
	}
*/
exports.AddUserBankData = function(req, res){
	var user = req.user;
	var bankData = req.body.bankData;
	if(user){
		if(bankData){
			if(typeof bankData === 'object'){
				if('beneficiaryName' in bankData && bankData['beneficiaryName']!=''){
					if('bankName' in bankData && bankData['bankName']!=''){
						if('IFSCCode' in bankData && bankData['IFSCCode']!=''){
							if('beneficiaryAccountNumber' in bankData && bankData['beneficiaryAccountNumber']!=''){
								if('branchName' in bankData && 'city' in bankData && bankData['city']!='' && bankData['branchName']!=''){

									User.findById(user._id, function(err, user){
										if(err){
											return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
										}else{
											if(user){
												user.bankAccountInfo = bankData;
												user.save(function(err){
													if(err){
														return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
													}else{
														return res.status(200).send({status: "success", message: "Bank details saved."});
													}
												});
											}else{
												return res.status(200).send({status: "error", message: "Invalid user."});
											}
										}
									});	
								}else{
									return res.status(200).send({status: "error", message: "Invalid bank data."});
								}
							}else{
								return res.status(200).send({status: "error", message: "Invalid bank data."});
							}
						}else{
							return res.status(200).send({status: "error", message: "Invalid bank data."});
						}
					}else{
						return res.status(200).send({status: "error", message: "Invalid bank data."});
					}

				}else{
					return res.status(200).send({status: "error", message: "Invalid bank data."});
				}
			}else{
				return res.status(200).send({status: "error", message: "Invalid bank data."});
			}
		}else{
			return res.status(200).send({status: "error", message: "Bank data not sent."});
		}
	}else{
		return res.status(200).send({status: "error", message: "User not authenticated."})
	}
};


//---------------------------------------------------------------------------------------------------------------------------



//--------GET USER BANK DATA--------------------------------------------------------------------------------------------------------------


/**
@api {get} /user/GetUserBankData Get user bank details
@apiName GetUserBankData
@apiGroup User
@apiDescription Get user bank details if available
@apiPermission authentication_required


@apiSuccess {String} status Status of the API Call.
@apiSuccess {String} response Response will contain the bank details

@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
	{
	  "status": "success",
	  "response": {
	    "bankDetails": {
	      "branchName": "wej",
	      "beneficiaryAccountNumber": "wedm",
	      "IFSCCode": "ewjd",
	      "bankName": "ewdw",
	      "beneficiaryName": "desnk"
	    }
	  }
	}

@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "Unauthorized user"
	}
*/
exports.GetUserBankData = function(req, res){
	var user = req.user;
	if(user){
		User.findById(user._id, function(err, user){
			if(err){
				return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
			}else{
				if(user){
					if(user.bankAccountInfo){	
							var response = {};
							response["bankDetails"] = user.bankAccountInfo;
							return res.status(200).send({status: "success", response: response});
					}else{
						return res.status(200).send({status: "error", message: "User has not saved any bank details."});
					}
				}else{
					return res.status(200).send({status: "error", message: "Invalid user"});
				}
			}
		});
	}else{
		return res.status(200).send({status: "error", message: "User not authenticated."});
	}
};


//---------------------------------------------------------------------------------------------------------------------------



//---------G E T-------------U S E R-----------P R O F I L E----------P I C T U R E-----------------------------------------------------------------


/**
@api {post} /getFile/profilePicture/:userId Get profile picture
@apiName GetProfilePicture
@apiGroup User
@apiDescription Get the user profile picture

@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
	} 

@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "Invalid userId"
	}
*/

//---------------------------------------------------------------------------------------------------------------------------


//---------G E T-------------U S E R-----------R E G----------D O C----------------------------------------------------


/**
@api {post} /getFile/userDocs/:userId Get user Reg Doc
@apiName GetUserDocs
@apiGroup User
@apiDescription Get the user registration document. Only user can see their own registration document. 

Admin can see anyone's registration document.
@apiPermission authentication_required

@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization":  "JWT eyJhbGHVbz5y8i-HRRY"
	} 

@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "Invalid userId"
	}
*/

//---------------------------------------------------------------------------------------------------------------------------



//-------------G E T----------------U S E R------------------S T A T I S T I C S----------INCOMPLETE--------------------------------------------


/**
@api {post} /user/getStats/:userId Get user statistics
@apiName getStats
@apiGroup User
@apiDescription Get the number of auctions user has participated in, won, lost and the green points of the user.

Normal user can access only his statistics, whereas the admin can access any user's statistics.

@apiPermission authentication_required

@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization":  "JWT eyJhbGHVbz5y8i-HRRY"
	} 


@apiSuccess {String} status Status of the API call.
@apiSuccess {String} message Message describing the status.
@apiSuccess {String} response Response of the API call.
@apiSuccess {String} response.auctionsParticipated Number of auctions user has participated in.
@apiSuccess {String} response.auctionsWon Number of auctions user has won.
@apiSuccess {String} response.auctionsLost Number of auctions user has participated in.
@apiSuccess {String} response.greenPoints Green points rewarded to the user.

@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
	{
	  "status": "success",
	  "message": "Profile picture uploaded successfully.",
	  "response": {
		"auctions"
	  }
	}

@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "Invalid userId"
	}
*/

//---------------------------------------------------------------------------------------------------------------------------
