var passport   = require('passport');
var Middleware = require(__base + '/middlewares/middleware.js');

var productCategoryCtrl = require(__modules + '/product/controllers/productcategory-controller.js');
var baseProductCtrl     = require(__modules + '/product/controllers/baseproduct-controller.js');
var productCtrl         = require(__modules + '/product/controllers/product-controller.js');



module.exports = function(app, express){

	var apiRouter = express.Router();

//APIDOC -- DEFINITIONS (Modules Used in this route)---------------------------------------------------------------------------------------------


/**
@apiDefine Product Product
It signifies induvidual products of a base-product type. A product is an iPhone 7 whose camera does not work. This particular product is registered as a "product", belongs to the base-product "iPhone 7" and the base-product in turn belongs to the product-category "mobile". It is a three level deep structure : Product-Category > Base-Product > Product
*/

/**
@apiDefine BaseProduct Base Product
Base product refers to a product which is available in the market. It does not take into account any of the defects etc. It contains all the product specifications and other market details. "iPhone 7" is a base product of Product-Category "mobile".
*/

/**
@apiDefine ProductCategory Product Category
Product category is the top level in the product stack. It classifies different products into categories. "iPhone 7" is of Product-Category "mobile".
*/

//--------------------------------------------------------------------------------------------------------------------



//PRODUCT CATEGORY ROUTES

	apiRouter.route('/GetSpecificCategory/:categoryId').get(productCategoryCtrl.GetSpecificCategory); 

	apiRouter.route('/AddProductCategory').post(passport.authenticate('jwt', { session: false }), 
										 Middleware.CheckIfAdmin,
										 productCategoryCtrl.AddProductCategory);

	apiRouter.route('/EditProductCategory/:categoryId').put(passport.authenticate('jwt', { session: false }), 
											  Middleware.CheckIfAdmin,
											  productCategoryCtrl.EditProductCategory);

	apiRouter.route('/DeleteProductCategory/:categoryId').delete(passport.authenticate('jwt', { session: false }), 
											  Middleware.CheckIfAdmin,
											  productCategoryCtrl.DeleteProductCategory);

	apiRouter.route('/GetProductCategories').post(productCategoryCtrl.GetProductCategories);  

	apiRouter.route('/GetBaseProductsBelongingToCategory/:categoryId').get(productCategoryCtrl.GetBaseProductsBelongingToCategory); 



//BASE PRODUCT ROUTES

	apiRouter.route('/AddBaseProduct').post(passport.authenticate('jwt', { session: false }), 
										 Middleware.CheckIfAdmin,
										 baseProductCtrl.AddBaseProduct);

	apiRouter.route('/AddBaseProductAsArray').post(
										 baseProductCtrl.AddBaseProductAsArray);

	apiRouter.route('/EditBaseProduct/:baseProductId').put(passport.authenticate('jwt', { session: false }),
											  Middleware.CheckIfAdmin,
											  baseProductCtrl.EditBaseProduct);

	apiRouter.route('/AddBaseProductImage/:baseProductId').post(passport.authenticate('jwt', { session: false }),
											  Middleware.CheckIfAdmin,
											  baseProductCtrl.AddBaseProductImage); 

	apiRouter.route('/DeleteBaseProduct/:baseProductId').delete(passport.authenticate('jwt', { session: false }), //done
											  Middleware.CheckIfAdmin,
											  baseProductCtrl.DeleteBaseProduct);

	apiRouter.route('/GetBaseProducts').post(baseProductCtrl.GetBaseProducts); 

	apiRouter.route('/GetSpecificBaseproduct/:baseProductId').get(baseProductCtrl.GetSpecificBaseproduct);

	apiRouter.route('/GetAllBrandNames').get(baseProductCtrl.GetAllBrandNames);

	apiRouter.route('/addBaseProductSpecifications/:baseProductId').post(passport.authenticate('jwt', { session: false }), //done
											  Middleware.CheckIfAdmin,
											  baseProductCtrl.AddBaseProductSpecifications);


	apiRouter.route('/AddBaseProductAdminPanel').post(passport.authenticate('jwt', { session: false }), 
										 Middleware.CheckIfAdmin,
										 baseProductCtrl.CreateNewBaseProductAdminPanel);



//PRODUCT ROUTES

	apiRouter.route('/CreateNewProduct').post(passport.authenticate('jwt', { session: false }),
										 productCtrl.CreateNewProduct);	

	apiRouter.route('/DeleteProduct/:productId').delete(passport.authenticate('jwt', { session: false }),
											  productCtrl.DeleteProduct);

	apiRouter.route('/GetSpecificProduct/:productId').get(productCtrl.GetSpecificProduct);

	apiRouter.route('/AddProductImage/:productId').post(passport.authenticate('jwt', { session: false }), 
											  productCtrl.AddProductImage);

	apiRouter.route('/DeleteProductImage/:imageId').delete(passport.authenticate('jwt', { session: false }), 
											  productCtrl.DeleteProductImage);

	apiRouter.route('/GetProductImages/:productId').get(productCtrl.GetProductImages); 




	return apiRouter;

};

