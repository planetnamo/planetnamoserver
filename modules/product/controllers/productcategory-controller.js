var mongoose = require('mongoose');
var User = require(__base + '/models/user/user.js')
var ProductCategory = require(__base + '/models/products/product-category');
var BaseProduct = require(__base + '/models/products/base-product.js');
var Product = require(__base + '/models/products/product.js');
var ErrorParser = require(__utils + '/errorParse.js');
var aux = require(__utils + '/auxilaryFunc.js');
var UsersUtils = require(__utils + '/users.js');
var FileHandler = require(__utils + '/FileHandler.js');
var multer = require('multer');
var fs = require('fs');
var mime = require('mime');
var crypto = require('crypto');


//API ROUTES in use ------6 APIS --------------------------------------------------------------------------------------------------------
// api/product/getSpecificCategory/:categoryId
// api/product/addProductCategory
// api/product/deleteProductCategory/:categoryId
// api/product/getProductCategories
// api/product/getBaseProductsBelongingToCategory/:categoryId
// api/product/editProductCategory/:categoryId
//------------------------------------------------------------------------------------------------------------------------------------



//-------------------G E T-------S P E C I F I C-----C A T E G O R Y------------------------------------------------------------

/**
@api {get} /product/GetSpecificCategory/:categoryId Get Category Information
@apiName GetSpecificCategory 
@apiGroup ProductCategory
@apiDescription Get information about a particular product category. Send category Id as a URL parameter.

Category name is fetched. Also, the number of base products belonging to this category is also fetched.

@apiSuccess {String} status Status of the API Call.
@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
  {
    "status": "success",
    "response": {
      "_id": "58d4b30b7b396b0011b2f65f",
      "createdAt": "2017-03-24T05:47:55.270Z",
      "categoryName": "I MAC",
      "baseProductCount": 0
    }
  }


*/
exports.GetSpecificCategory = function(req, res) {
  var categoryId = req.params.categoryId;
  if (categoryId) {
    ProductCategory.findById(categoryId)
      .select('createdAt _id categoryName')
      .exec(function(err, QueryResult) {
        if (err) {
          return res.status(200).send({
            status: 'error',
            message: ErrorParser.parseErrorreturnErrorMessage(err)
          });
        } else {
          if (QueryResult) {
            var result = {};
            result['_id'] = QueryResult._id
            result['createdAt'] = QueryResult.createdAt;
            result['categoryName'] = QueryResult.categoryName;
            result['accessories'] = QueryResult.accessories;
            result['problems'] = QueryResult.problems;
            result['specifications'] = QueryResult.specifications;
            BaseProduct.find({
              productCategoryId: result._id
            }).count().exec(function(err, countResult) {
              if (err) {
                return res.status(200).send({
                  error: ErrorParser.parseErrorreturnErrorMessage(err),
                  response: result
                });
              } else {
                result['baseProductCount'] = countResult;
                return res.status(200).json({
                  status: 'success',
                  response: result
                });
              }
            });
          } else {
            return res.status(200).send({
              status: 'error',
              message: 'No category found for categoryId: ' + categoryId
            });
          }
        }
      });
  } else {
    res.status(200).send({
      status: 'error',
      message: "categoryId not sent."
    });
  }
};

//---------------------------------------------------------------------------------------------------------------------------



//-------------------A D D-------P R O D U C T----C A T E G O R Y------------------------------------------------------------

/**
@api {post} /product/AddProductCategory Add Product Category
@apiName Add Product Category
@apiGroup ProductCategory
@apiPermission admin
@apiDescription Create a new product category using this route. Only ADMIN users can access this route.

@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization": "JWT eyJhbGHVbz5y8i-HRRY"
	} 

@apiParam {String} CategoryName Name of the product category ('mobile', 'laptop')
@apiParam {File} productCategoryImage Image file representing the product category
@apiParamExample {JSON} Request-Example:
	{
		"categoryName": "TV"
	}

@apiSuccess {String} status Status of the API Call.
@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
	{
	  "status": "Product Category saved."
	}

@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "E11000 duplicate key error index: projectnamo-scratch.productcategories.$categoryName_1 dup key: { : \"TV\" }"
	}
*/
exports.AddProductCategory = function(req, res) {
  var categoryName = req.body.categoryName;
  if (categoryName) {
    var productCategory = new ProductCategory();
    productCategory.categoryName = categoryName;
    if(req.body.accessories){
      productCategory.accessories = req.body.accessories;
    }
    if(req.body.problems){
      productCategory.problems = req.body.problems;
    }
    if(req.body.specifications){
      productCategory.specifications = req.body.specifications;
    }
    productCategory.save(function(err, productCategory) {
      if (err) {
        return res.status(200).send({
          status: 'error',
          message: ErrorParser.parseErrorreturnErrorMessage(err)
        });
      } else {
        return res.status(200).send({
          status: 'success',
          message: 'Product Category saved.'
        });
      }
    });
  } else {
    return res.status(200).send({
      status: 'error',
      message: "Inadequate information sent"
    });
  }
};
//---------------------------------------------------------------------------------------------------------------------------



//-------------------D E L E T E-------P R O D U C T----C A T E G O R Y------------------------------------------------------------

/**
@api {delete} /product/DeleteProductCategory/:categoryId Delete Product Category
@apiName DeleteProductCategory
@apiGroup ProductCategory
@apiPermission admin
@apiDescription To delete a product category send the categoryId as a URL parameter. 

@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization": "JWT eyJhbGHVbz5y8i-HRRY"
	} 

@apiParam {String} :categoryId Unique Id representing a product category. Sent as a URL parameter
@apiParamExample {URL} Request-Example:
	http://localhost:8080/api/product/DeleteProductCategory/58a716dcc1d2f99bfc985206

@apiSuccess {String} status Status of the API Call.
@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
	{
		status: 'success',
		message: 'Successfully deleted!'
	}

@apiError error Error message.
	HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "No category exists with Id:58a716dcc1d2f99bfc985206"
	}
*/
exports.DeleteProductCategory = function(req, res) {
  var categoryId = req.params.categoryId;
  if (categoryId) {
    ProductCategory.findById(categoryId, function(err, productCategory) {
      if (err) {
        return res.status(200).send({
          status: 'error',
          message: ErrorParser.parseErrorreturnErrorMessage(err)
        });
      } else {
        if (!productCategory) {
          return res.status(200).send({
            status: 'error',
            message: "No category exists with Id:" + categoryId
          });
        }
        var queryParams = {
          'productCategoryId': categoryId
        }
        BaseProduct.find(queryParams, function(err, baseProducts) {
          if (err) {
            return res.status(200).send({
              status: 'error',
              message: ErrorParser.parseErrorreturnErrorMessage(err)
            });
          } else {
            if (!baseProducts || baseProducts.length == 0) {
              ProductCategory.remove({
                _id: categoryId
              }, function(err, productCategory) {
                if (err) {
                  return res.status(200).send({
                    status: 'error',
                    message: ErrorParser.parseErrorreturnErrorMessage(err)
                  });
                } else {
                  return res.status(200).send({
                    status: 'success',
                    message: 'Successfully deleted!'
                  });
                }
              });
            } else {
              return res.status(200).send({
                status: 'error',
                message: "Cannot delete product category as base products exist"
              });
            }
          }
        });
      }
    });
  } else {
    res.status(200).send({
      status: 'error',
      message: "categoryId not sent."
    });
  }
};

//---------------------------------------------------------------------------------------------------------------------------



//-------------------G E T---A L L----P R O D U C T----C A T E G O R I E S------------------------------------------------------------

/**
@api {get} /product/GetProductCategories Get Product Categories
@apiName GetAllProductCategories
@apiGroup ProductCategory
@apiDescription Get a list of the latest 20 product categories sorted by the date they were created in descending order.

@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json"
	} 

@apiParam {filters} The filtering which is required.
@apiParam {filters.queryParam} Search for product categories matching the queryParam
@apiParam {filters.createdOnBefore} Search for product categories created on or before this date.
@apiParamExample {JSON} Request-Example:
  {
    "filters": {
      "queryParam": "mac",
      "createdOnBefore": "2018-03-24T05:47:55.270Z"
    }
  }

@apiSuccess {String} status Status of the API Call.
@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
  {
    "status": "success",
    "response": [
      {
        "_id": "58d4b30b7b396b0011b2f65f",
        "createdAt": "2017-03-24T05:47:55.270Z",
        "categoryName": "I MAC"
      }
    ]
  }
  
@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "No product categories found"
	}
*/
exports.GetProductCategories = function(req, res) {
  var filters = req.body.filters;
  var andQueries = {};
  var orQueries = [];

  if(filters){
    if(filters.createdOnBefore){
      andQueries.createdAt = { $lte: filters.createdOnBefore };
    }
    if(filters.queryParam){
      orQueries.push(({categoryName: { "$regex": filters.queryParam, "$options": "i" }}));
    }
  }

  var queryParams= {};
  if(orQueries.length > 0){
    queryParams = {$and: [andQueries, { $or: orQueries }]};
  }else{
    queryParams = andQueries;
  }

  ProductCategory.find(queryParams)
                 .limit(20)
                 .sort('-createdAt')
                 .exec(function(err, QueryResult){
                    if (err) {
                      return res.status(200).send({
                        error: ErrorParser.parseErrorreturnErrorMessage(err)
                      });
                    } else {
                      if (QueryResult) {
                        var result = [];
                        for (let i = 0; i < QueryResult.length; i++) {
                          var productCategory = {};
                          productCategory['_id'] = QueryResult[i]._id;
                          productCategory['createdAt'] = QueryResult[i].createdAt;
                          productCategory['categoryName'] = QueryResult[i].categoryName;
                          productCategory['specifications'] = QueryResult[i].specifications;
                          productCategory['problems'] = QueryResult[i].problems;
                          productCategory['accessories'] = QueryResult[i].accessories;
                          result.push(productCategory);
                        }
                        return res.status(200).send({
                          status: 'success',
                          response: result
                        });
                      } else {
                        return res.status(200).send({
                          status: 'error',
                          message: 'No product categories found'
                        });
                      }
                    }
                 });
};

//---------------------------------------------------------------------------------------------------------------------------



//-------------------G E T------B A S E----P R O D U C T------B E L O N G I N G ---T O----A-----C A T E G O R Y------------------------------------------------------------

/**
@api {get} /product/GetBaseProductsBelongingToCategory/:categoryId Get Base Products of category
@apiName GetBaseProductsOfCategory
@apiGroup ProductCategory
@apiDescription Get list of all base products belonging to a particular category. Some information regarding the base product and products under base product is also fetched.

Category Id should be sent as a URL parameter.


@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json"
	} 


@apiParam {String} :categoryId Unique Id representing a product category. Sent as a URL parameter
@apiParamExample {URL} Request-Example:
	http://localhost:8080/api/product/GetBaseProductsBelongingToCategory/58c151b2ab6d4359ad8234d1

@apiSuccess {String} status Status of the API Call.
@apiSuccess {JSON} response  A dictionary with keys as Base Product Ids.
@apiSuccess {JSON} response.products_registered  The products registered under the base product

@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
	{
	  "status": "success",
	  "response": [
	    {
	      "productName": "OnePlus3T",
	      "_id": "58c156aaab6d4359ad8234d7",
	      "Number_of_products_registered": 5
	    },
	    {
	      "productName": "iPhone 6",
	      "_id": "58c15963ab6d4359ad8234e0",
	      "Number_of_products_registered": 2
	    },
	    {
	      "productName": "S7 edge",
	      "_id": "58c15987ab6d4359ad8234e3",
	      "Number_of_products_registered": 2
	    },
	    {
	      "productName": "iPhone 7",
	      "_id": "58c159edab6d4359ad8234e9",
	      "Number_of_products_registered": 0
	    },
	    {
	      "productName": "iPhone 5s",
	      "_id": "58c159feab6d4359ad8234ec",
	      "Number_of_products_registered": 3
	    }
	  ]
	}

@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
	  "error": "Inadequate information sent"
	}
*/
exports.GetBaseProductsBelongingToCategory = function(req, res) {
  let categoryId = req.params.categoryId;
  if (categoryId) {
    ProductCategory.findById(categoryId, function(err, productCategory) {
      if (err) {
        return res.status(200).send({
          error: ErrorParser.parseErrorreturnErrorMessage(err)
        });
      } else {
        if (!productCategory) {
          return res.status(200).send({
            status: 'error',
            message: "No category exists with Id:" + categoryId
          });
        }
        BaseProduct.find({
            productCategoryId: categoryId
          })
          .select('productName baseProductImageURL _id')
          .exec(function(err, QueryResult) {
            if (err) {
              return res.status(200).send({
                status: 'error',
                message: ErrorParser.parseErrorreturnErrorMessage(err)
              });
            } else {
              var result = {};
              for (let i = 0; i < QueryResult.length; i++) {
                result[QueryResult[i]._id] = {};
                result[QueryResult[i]._id]['productName'] = QueryResult[i].productName;
                result[QueryResult[i]._id]['_id'] = QueryResult[i]._id;
                //result[QueryResult[i]._id]['products_registered'] 				= [];
                result[QueryResult[i]._id]['Number_of_products_registered'] = 0;
                result[QueryResult[i]._id]['baseProductImageURL'] = QueryResult[i].baseProductImageURL;
              }
              let baseProductIds = QueryResult.map(x => x._id);
              Product.where('baseProductId')
                .in(baseProductIds)
                .select('planetNamoCost ageinmonths rating baseProductId')
                .exec(function(err, resultProducts) {
                  if (err) {
                    return res.status(200).send({
                      status: 'error',
                      message: ErrorParser.parseErrorreturnErrorMessage(err),
                      result: result
                    });
                  } else {
                    for (let i = 0; i < resultProducts.length; i++) {
                      let tmp = {};
                      tmp['cost'] = resultProducts[i].planetNamoCost;
                      tmp['ageinmonths'] = resultProducts[i].ageinmonths;
                      tmp['rating'] = resultProducts[i].rating;
                      //result[resultProducts[i].baseProductId]['products_registered'].push(tmp); <---Induvidual Product details commented out
                      result[resultProducts[i].baseProductId]['Number_of_products_registered'] += 1;
                    }
                  }
                  result = aux.ConvertDictToArrayPushValues(result);
                  return res.status(200).send({
                    status: 'success',
                    response: result
                  });
                });
              //return res.status(200).send(result);
            }
          });
      }
    });
  } else {
    res.status(200).send({
      status: 'error',
      message: "categoryId not sent."
    });
  }
};

//---------------------------------------------------------------------------------------------------------------------------



//--------------------E D I T-------------------C A T E G O R Y-----------------------------------------------------------------------

/**
@api {put} /product/EditProductCategory/:categoryId Edit Product Category
@apiName EditProductCategory
@apiGroup ProductCategory
@apiPermission admin


@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization": "JWT eyJhbGHVbz5y8i-HRRY"
	} 


@apiParam {String} :categoryId Unique Id representing a product category. Sent as a URL parameter
@apiParam {String} [categoryName] categoryName Name of the category
@apiParamExample {URL} Request-Example:
	http://localhost:8080/api/product/EditProductCategory/58a71492a89606987e60a235


@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
	  "error": "Inadequate information sent"
	}
*/
exports.EditProductCategory = function(req, res) {
  var categoryId = req.params.categoryId;
  if (categoryId) {
    ProductCategory.findById(categoryId, function(err, productCategory) {
      if (err) {
        return res.status(200).send({
          error: 'error',
          message: ErrorParser.parseErrorreturnErrorMessage(err)
        });
      } else {
        if (req.body.categoryName) productCategory.categoryName = req.body.categoryName;
        if (req.body.accessories) productCategory.accessories = req.body.accessories;
        if (req.body.problems) productCategory.problems = req.body.problems;
        if (req.body.specifications) productCategory.specifications = req.body.specifications;
        productCategory.save(function(err, productCategory) {
          if (err) {
            return res.status(200).send({
              error: 'error',
              message: ErrorParser.parseErrorreturnErrorMessage(err)
            });
          } else {
            return res.status(200).send({
              status: 'success',
              message: 'Product Category updated!',
              response: productCategory
            });
          }
        });
      }
    });
  } else {
    res.status(200).send({
      status: 'success',
      message: "Inadequate data sent."
    });
  }
};


//---------------------------------------------------------------------------------------------------------------------------