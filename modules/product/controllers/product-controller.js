/**
 * ACTUAL PRODUCT related functions
 */

var mongoose = require('mongoose');

var User            = require(__base + '/models/user/user.js')
var ProductCategory = require(__base + '/models/products/product-category.js');
var BaseProduct     = require(__base + '/models/products/base-product.js');
var Product         = require(__base + '/models/products/product.js');
var ProductImage    = require(__base + '/models/products/productImages.js');
var Lot 	        = require(__base + '/models/lots/lot');
var ApprovedLot 	= require(__base + '/models/lots/approved-lot');
var RegisteredLot 	= require(__base + '/models/lots/registered-lot');

var FileHandler  = require(__utils + '/FileHandler.js');


var ErrorParser = require(__utils + '/errorParse.js');
var aux         = require(__utils + '/auxilaryFunc.js');
var usersUtils  = require(__utils + '/users.js');

var multer  = require('multer');
var fs = require('fs');
var mime = require('mime');
var crypto = require('crypto');

var storage_productImages = multer.diskStorage({
	destination: function (request, file, callback) {
		callback(null, __base + '/uploads/productPictures');
	},
	filename: function (request, file, callback) {
		console.log(file);
		//callback(null, file.originalname);
		crypto.pseudoRandomBytes(16, function (err, raw) {
		  callback(null, raw.toString('hex') + Date.now() + '.' + mime.extension(file.mimetype));
		});
	}
});
var upload_productImages = multer({storage: storage_productImages}).single('productImage');



/**
 * @apiDefine admin Admin access only
 * Only users with administrator rights can access
 */
 /**
 * @apiDefine authentication_required User access only
 * Only logged in users can access
 */





















//----------------C R E A T E-----------N E W--------------P R O D U C T------------------------------------------------------------


/**
@api {post} /product/CreateNewProduct/ Create product 
@apiName CreateProduct 
@apiGroup Product
@apiPermission authentication_required

@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization": "JWT eyJhbGHVbz5y8i-HRRY"
	} 

@apiParam {String} baseProductId Name of the base product
@apiParam {String} ageinmonths Category Id to which base product belongs to
@apiParam {String} condition Brand name of the base product
@apiParam {String} validbill Model number of base product
@apiParam {String} originalbox Warranty and support Information
@apiParam {String} workingcondition 

@apiParam {String} [accessorieslistavail] accessorieslistavail
@apiParam {String} [problemslistavail] problemslistavail
@apiParamExample {json} Request-Example:
	HTTP/1.1 200 OK
	{
		"productName": "OnePlus X",
		"productCategoryId": "58a71492a89606987e60a235",
		"brandName": "OnePlus",
		"modelNumber": "2390280",
		"warranty_n_support": "1 YEAR WARRANTY",
		"marketCost": 13000,
		"releaseDate": "2015",
		"goodRemark": "Everything works",
		"averageRemark": "Minor scratches on body and screen",
		"belowAverageRemark": "Major scratches and defects",
		"problems": [{
			"problemname": "Screen cracked",
			"score": 50,
			"remarks": "Screen is visibly shattered"
		}],
		"accessories": [{
			"accessoryname": "Charger",
			"score": 50
		}]
	}


@apiSuccess {String} status Status of the API Call.
@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
	{
	  "status": "success",
	  "response": {
	    "__v": 0,
	    "productRegisteredByUser": "58c14475ef21b44e74ed817d",
	    "workingcondition": true,
	    "originalbox": true,
	    "validbill": true,
	    "condition": 3,
	    "ageinmonths": 3,
	    "baseProductId": "58c159feab6d4359ad8234ec",
	    "_id": "58cba577cbe897279bfac93b",
	    "finalLots": [],
	    "problemslistavail": [
	      false
	    ],
	    "accessorieslistavail": [
	      true
	    ],
	    "planetNamoCost": 1000
	  }
	}

@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "Inadequate data sent"
	}
*/

Costeval = function(product, baseProduct) {
	return 1000;
	//algo to be implemented
	//also add default for when no problemslistavail and/or accessorieslistavail
};

verifyProductReq = function(product, baseProduct) {
	var accessorieslistavail = product.accessorieslistavail;
	var problemslistavail = product.problemslistavail;
	var accessoriesList = baseProduct.accessoriesList;
	var problemsList = baseProduct.problemsList;

	if(accessorieslistavail && problemslistavail){
		if(problemsList.length == problemslistavail.length && accessoriesList.length == accessorieslistavail.length){
			return true;
		}else{
			return false;
		}
	}else{
		return true;
	}
};

exports.CreateNewProduct = function (req, res) {

	var baseProductId = req.body.baseProductId;
	var ageinmonths = req.body.ageinmonths;
	var condition = req.body.condition;
	var validbill = req.body.validbill; //Boolean
	var originalbox = req.body.originalbox; //Boolean
	var workingcondition = req.body.workingcondition; //Boolean
	var userId = res.req.user._id;
	var accessorieslistavail = req.body.accessorieslistavail;
	var problemslistavail = req.body.problemslistavail;
	var specifications = req.body.specifications;


	if(userId && baseProductId && ageinmonths && condition){


		if(!(condition==1 || condition==2 || condition==3)){
			return res.status(200).send({status: 'error', message: "Invalid value for condition, should be in the range of 1-3"});
		}
		if(isNaN(ageinmonths)){
			return res.status(200).send({status: 'error', message: "ageinmonths should be expressed as a number"});
		}

		var product = new Product();
		product.baseProductId = baseProductId;
		product.ageinmonths = ageinmonths;
		product.condition = condition;
		product.validbill = validbill;
		product.originalbox = originalbox;
		product.workingcondition = workingcondition;
		product.productRegisteredByUser = userId;
		product.specifications = specifications;
		if(accessorieslistavail){
			if(!(Array.isArray(accessorieslistavail))){
				return res.status(200).send({status: 'error', message: "Invalid value for accessorieslistavail, should be a boolean array"});
			}else{
				product.accessorieslistavail = accessorieslistavail;
			}
		}
		if(problemslistavail){
			if(!(Array.isArray(problemslistavail))){
				return res.status(200).send({status: 'error', message: "Invalid value for problemslistavail, should be a boolean array"});
			}else{
				product.problemslistavail = problemslistavail;
			}
		}

		BaseProduct.findById(baseProductId, function(err, baseProduct){
			if(err) {
				return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
			}else{
				if(baseProduct){
					if(verifyProductReq(product, baseProduct)){
						product.planetNamoCost = Costeval(product, baseProduct);
						product.save(function(err, product){
							if(err){
								return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
							}else{
								return res.status(200).send({status: 'success', response: product});
							}
						});
					}else{
						return res.status(200).send({status: 'error', message: "Invalid accessorieslistavail or problemslistavail, send according to registered base product", baseProduct: baseProduct});
					}
				}else{
					return res.status(200).send({status: 'error', message: "No base product found for baseProductId: "+baseProductId});
				}
			}
	});
	}else {
		res.status(200).send({status: 'error', message: "Inadequate data sent."});
	}
}





//---------------------------------------------------------------------------------------------------------------------------
















//--------------------D E L E T E---------------P R O D U C T-----------------------------------------------------------------------------

/**
@api {delete} /product/DeleteProduct/:productId Delete product 
@apiName Delete product 
@apiGroup Product
@apiDescription Delete a product. If some final lot lot exists which uses this product then the product cannot be deleted.

All images associated with the product will also be deleted.
@apiPermission authentication_required

@apiHeader {String="application/json"} Content-Type Content Type for the api 
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
  {
    	"Content-Type": "application/json",
		 "Authorization": "JWT eyJhbGHVbz5y8i-HRRY"
  }

@apiParam {String} productId unique id identyfing the product
@apiParamExample {URL} Request-Example:
	http://localhost:8080/api/product/DeleteProduct/58c80b1439548bf
 
@apiSuccess {String} status Status of the API Call.
@apiSuccessExample Success-Response:
  HTTP/1.1 200 OK
	{
	  "status": "success",
	  "message": "Successfully deleted!",
	}
 
@apiError error Error message.
@apiErrorExample Error-Response:
  HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "Inadequate information sent"
	}

 */
function deleteFiles(files, callback){
	var i = files.length;
	files.forEach(function(filepath){
		fs.unlink(filepath, function(err) {
			i--;
			if(err){
				callback(err);
	        	return;
			}else if(i <= 0){
				callback(null);
	      	}
		});
	});
};
function deleteProductImages(files, callback){
	var i = files.length;
	files.forEach(function(imageId){
		ProductImage.remove({
			_id: imageId
		}, function(err){
			i--;
			if(err){
				callback(err);
				return;
			}else if(i<=0){
				callback(null);
			}
		});
	});
};
exports.DeleteProduct = function (req, res) {
	var productId 	= req.params.productId;
	var user 		= res.req.user;

	if(productId && user){
		Product.findById(productId)
			   .exec(function(err, product){
			   		if(product){
			   			if(user.equals(product.productRegisteredByUser) || user.role == 2){
			   				RegisteredLot.find({pId: productId}, function(err, registeredLots){
			   					if(err){
			   						return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
			   					}else{
			   						if(!registeredLots || registeredLots.length > 0){
			   							console.log(registeredLots);
			   							return res.status(200).send({status: "error", message: "Cannot delete as lots are registered under this product."});
			   						}else{
			   							ProductImage.find({pId: productId}, function(err, productImages){
			   								if(err){
			   									return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
			   								}else{
			   									var productImagePaths	= productImages.map(x => x.image.path);
			   									var productImageIds		= productImages.map(x => x._id);	
												deleteFiles(productImagePaths, function(err) {
													if(err){
												    	return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
													}else{
															deleteProductImages(productImageIds, function(err){
																if(err){
																	return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
																}else{
																		Product.remove({
																			_id: productId
																		}, function(err){
																			if(err){
																				return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
																			}
																			else{
																				return res.status(200).send({
																					status: 'success',
																					message: 'Successfully deleted! Product images also deleted.'
																				});
																			}
																		});	
																}
															});
													}
												});
			   								}
			   							});
			   						}
			   					}
			   				});
			   			}else{
			   				return res.status(200).send({status: "error", message: "Product not registered by userId:" + userId+". Only admin can delete cross user created products."});
			   			}
			   		}else{
			   			return res.status(200).send({status: "error", message: "Product with productId: "+productId+" does not exist."});
			   		}
			   });
	}else{
		return res.status(200). send({status: "error", message: "Inadequate information"});
	}
};

//---------------------------------------------------------------------------------------------------------------------------

















//----------------G E T-------------S P E C I F I C--------------P R O D U C T--------------------------------------------------------------
/**
@api {get} /product/GetSpecificProduct/:productId Get details of a product
@apiName GetSpecificProduct
@apiGroup Product
@apiDescription Get specific details of a product
 
@apiHeader {String="application/json"} Content-Type Content Type for the api 
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
	}
 
@apiSuccess {String} status Status of the API Call.
@apiSuccess {JSON} result Result will contain product details 
@apiSuccessExample Success-Response:
      HTTP/1.1 200 OK
{
  "status": "success",
  "response": {
    "_id": "58a72056b134bbad2b5f6b21",
    "productRegisteredByUser": "58a70a784ded7c86e5e829e6",
    "workingcondition": true,
    "originalbox": true,
    "validbill": true,
    "condition": 1,
    "ageinmonths": 2,
    "baseProductId": {
      "_id": "58a71886e03f379fcad55b72",
      "discount_amount": 0,
      "discount_percent": 0,
      "warranty_n_support": "1 year warranty",
      "model_number": "38288hea3",
      "brand_name": "Samsung",
      "productCategoryId": {
        "_id": "58a71492a89606987e60a235",
        "categoryName": "Mobile"
      },
      "productName": "Samsung S7 edge",
      "__v": 2,
      "finalLots": [
        "58b5509b020e3e6086a3288a",
        "58b599ff550576993dcafc42"
      ],
      "problemsList": [
        {
          "problemname": "Scratch on edge",
          "score": 3,
          "remarks": "Sk",
          "_id": "58a71886e03f379fcad55b75"
        }
      ],
      "accessoriesList": [
        {
          "accessoryname": "charger",
          "score": 10,
          "_id": "58a71886e03f379fcad55b74"
        },
        {
          "accessoryname": "earphones",
          "score": 10,
          "_id": "58a71886e03f379fcad55b73"
        }
      ],
      "marketCost": null,
      "productImage": {
        "fieldname": "productImage",
        "originalname": "download.jpeg",
        "encoding": "7bit",
        "mimetype": "image/jpeg",
        "destination": "/Users/karun/sylv/projectnamo/scratch/uploads",
        "filename": "7ed69ecf68db0ccd0931c8ed9e3a23611487346485606.jpeg",
        "path": "/Users/karun/sylv/projectnamo/scratch/uploads/7ed69ecf68db0ccd0931c8ed9e3a23611487346485606.jpeg",
        "size": 1910
      }
    },
    "__v": 8,
    "finalLots": [
      "58b5440added16582a4afb59",
      "58b5456e15a72a5964ad76fc",
      "58b546297303c559b04f022d",
      "58b5465b1857a95a163ce3f1",
      "58b546b14583735a562218fa",
      "58b54754dd1e5a5ae22cb450",
      "58b5509b020e3e6086a3288a",
      "58b599ff550576993dcafc42"
    ],
    "problemslistavail": [],
    "accessorieslistavail": [],
    "planetNamoCost": 1000,
    "productImage": {
      "fieldname": "productImage",
      "originalname": "download (1).jpeg",
      "encoding": "7bit",
      "mimetype": "image/jpeg",
      "destination": "/Users/karun/sylv/projectnamo/scratch/uploads",
      "filename": "d08ba3207ad9572944d42ae9db8c5e021487348069429.jpeg",
      "path": "/Users/karun/sylv/projectnamo/scratch/uploads/d08ba3207ad9572944d42ae9db8c5e021487348069429.jpeg",
      "size": 6715
    }
  }
}
 
@apiError error Error message. 
@apiErrorExample Error-Response:
      HTTP/1.1 200 Error
		{
		  "status": "error",
		  "message": "Inadequate information sent"
		}
 */
exports.GetSpecificProduct = function (req, res) {
	var productId = req.params.productId;
	if(productId){
		Product.findById(productId, function(err, product) {
			if(err) {
				return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
			}
			else{
				if(product){
					return res.status(200).send({status: 'success', response: product});
				}else{
					return res.status(200).send({status: 'error', message: 'No product found for productId:'+ productId});
				}
			}
		}).populate({
			path: 'baseProductId',
			model:  'BaseProduct',
			populate: {
				path: 'productCategoryId',
				model: 'ProductCategory',
				select: 'categoryName'
			}
		});
	}else {
		return res.status(200).send({status: 'error', message: "Inadequate data sent."});
	}
};


//---------------------------------------------------------------------------------------------------------------------------


















//--------------------A D D-------------P R O D U C T--------------I M A G E------------------------------------------------------------

/**
@api {post} /product/AddProductImage/:productId Add product Image
@apiName Add product Image
@apiGroup Product
@apiPermission authentication_required

@apiHeader {String="application/json"} Content-Type Content Type for the api 
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
	 	"Authorization": "JWT eyJhbGHVbz5y8i-HRRY"
	}
 
@apiParam {File} productImage
@apiParam {String} productId Id describing the unique product. Sent as URL parameter.
@apiParamExample {JSON} Request-Example:
	{
		productImage: xyz.jpg
	}
 
@apiSuccess {String} status Status of the API Call.
@apiSuccess {JSON} response Response of the api call.
@apiSuccess {JSON} response.noOfImages Number of images the product has
@apiSuccess {JSON} response.images The image array 
@apiSuccessExample Success-Response:
  HTTP/1.1 200 OK
	{
	  "status": "success",
	  "message": "Image Successfully added",
	  "response": {
	    "noOfImages": 5,
	    "images": [
	      "http://localhost:8080/api/getFile/productPicture/115b1e4b5b902383367c988edb1096161489828766706.png",
	      "http://localhost:8080/api/getFile/productPicture/fcdcaecca6584b2249ec75dab99af2d01489828770669.jpeg",
	      "http://localhost:8080/api/getFile/productPicture/77c8fa3585a6528585e37c8eced47dfc1489828774251.png",
	      "http://localhost:8080/api/getFile/productPicture/d93fc83f5dfbed85b59014b26c1f07351489829043256.png",
	      "http://localhost:8080/api/getFile/productPicture/de243f2da76d46daec7ee75456e63d4b1489829069895.png"
	    ]
	  }
	}
 
@apiError error Error message.
@apiErrorExample Error-Response:
  HTTP/1.1 200 Error
	{
	  "error": "Inadequate information sent"
	}
 */
exports.AddProductImage = function (req, res){
	upload_productImages(req, res, function(err) {
		if(err) {
			return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
		}else {
			var file = req.file;
			var productId = req.params.productId;
			if(file && productId){
				Product.findById(productId)
				       .exec(function(err, product){
				       		if(err){
				       			return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
				       		}else{
				       			if(product){
				       				ProductImage.count({pId: productId}, function(err, countOfImages){
				       					if(err){
				       						return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
				       					}else{
				       						if(countOfImages<10){
												var productImage = new ProductImage();
												productImage.image = file;
												productImage.pId = productId;
												productImage.save(function(err, productImage){
													if(err){
														return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
													}else{
														ProductImage.find({pId: productId}, function(err, images){
															if(err){
																return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
															}else{
																if(images){
																	var imageLinksArray = images.map(x => x.imageURL);
																	var response = {};
																	response['noOfImages'] = imageLinksArray.length;
																	response['images'] = imageLinksArray;
																	return res.status(200).send({status: 'success', message: "Image Successfully added", response: response});
																}else{
																	return res.status(200).send({status: 'error', message: "No images found"});
																}
															}
														});
													}
												}); 
				       						}else{
				       							return res.status(200).send({status: "error", message: "Cannot upload more than 10 images for a product."});
				       						}
				       					}
				       				});  				
				       			}else{
				       				return res.status(200).send({status: 'error', message: "Product not found with productId:"+productId});
				       			}
				       		}
				       });
			}else{
				res.status(200).send({status: 'error', message: "Inadequate information sent"});
			}
		}
	});
};



//---------------------------------------------------------------------------------------------------------------------------
























//-------------------------D E L E T E-------------P R O D U C T----------I M A G E--------------------------------------------------------------


/**
@api {delete} /product/DeleteProductImage/:imageId Delete product Image (:imageId)
@apiName Delete product Image
@apiGroup Product

@apiHeader {String="application/json"} Content-Type Content Type for the api 
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization": "JWT eyJhbGHVbz5y8i-HRRY"
	}

@apiParam {String} imageId unique id identyfing an image
@apiParamExample {URL} Request-Example:
	http://localhost:8080/api/product/DeleteProductImage/58c80b1439548bf
 
@apiSuccess {String} status Status of the API Call.
@apiSuccessExample Success-Response:
  HTTP/1.1 200 OK
	{
	  "status": "success",
	  "message": "Successfully deleted!",
	}
 
@apiError error Error message.
@apiErrorExample Error-Response:
  HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "Inadequate information sent"
	}
 */
exports.DeleteProductImage = function (req, res){
	var imageId = req.params.imageId;
	var user 	= res.req.user;

	if(imageId && user){
		ProductImage.findById(imageId)
					.populate('pId', 'productRegisteredByUser')
					.exec(function(err, productImage){
						if(err){
							return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
						}else{
							if(productImage){
								if(user._id.equals(productImage.pId.productRegisteredByUser)){
									ProductImage.remove({
										_id: imageId
									}, function(err, images){
										if(err){
											return res.status(200).send({status: "error", message: ErrorParser.parseErrorreturnErrorMessage(err)});
										}
										else{
											if(productImage.image.path){
												fs.unlink(productImage.image.path, function(err){
													if(err){
														return res.status(200).send({
															status: 'success',
															message: 'Successfully deleted!'
														});
													}else{
														return res.status(200).send({
															status: 'success',
															message: 'Successfully deleted!'
														});
													}
												});
											}else{
												return res.status(200).send({
													status: 'success',
													message: 'Successfully deleted!'
												});
											}
										}
									});
								}else{
									return res.status(200).send({status: "error", message: "Cannot alter a product not created by you."});
								}
							}else{
								return res.status(200).send({status: "error", message: "No image found with id: "+imageId});
							}
						}
					});
	}else{
		return res.status(200).send({status: "error", message: "Inadequate information"});
	}
};

//---------------------------------------------------------------------------------------------------------------------------

























//-------------------G E T----------P R O D U C T----------I M A G E S------------------------------------------------------------------------


/**
@api {get} /product/GetProductImages/:productId Get product Image
@apiName Get product Image
@apiGroup Product
@apiDescription Get a collection of product images.

@apiHeader {String="application/json"} Content-Type Content Type for the api 
@apiHeader {String="ENWJNJENDjnksdja329njdkaknwk"} ClientKey Client Key for accessing the server

@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		 "ClientKey": 	  "ENWJNJENDjnksdja329njdkaknwk",
		 "Authorization": "JWT eyJhbGHVbz5y8i-HRRY"
	}

@apiParam {String} productId unique id identyfing a product
@apiParamExample {URL} Request-Example:
	http://localhost:8080/api/product/GetProductImages/58c80b1439548bf

@apiSuccess {String} status Status of the API Call.
@apiSuccess {JSON} response Result will contain the response of the API CALL
@apiSuccess {JSON} response.noOfImages Number of images the product has
@apiSuccess {JSON} response.images The image array 
@apiSuccessExample Success-Response:
  HTTP/1.1 200 OK
	{
	  "status": "success",
	  "response": {
	    "noOfImages": 3,
	    "images": [
	      "http://localhost:8080/api/getFile/productPicture/115b1e4b5b902383367c988edb1096161489828766706.png",
	      "http://localhost:8080/api/getFile/productPicture/fcdcaecca6584b2249ec75dab99af2d01489828770669.jpeg",
	      "http://localhost:8080/api/getFile/productPicture/77c8fa3585a6528585e37c8eced47dfc1489828774251.png"
	    ]
	  }
	}
 
@apiError error Error message.
@apiErrorExample Error-Response:
  HTTP/1.1 200 Error
	{
	  "status": "error",
	  "message": "Inadequate information sent"
	}
 */
exports.GetProductImages = function (req, res){
	var productId = req.params.productId;
	if(productId){
		Product.findById(productId)
		       .exec(function(err, product){
		       		if(err){
		       			return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
		       		}else{
		       			if(product){
							ProductImage.find({pId: productId}, function(err, images){
								if(err){
									return res.status(200).send({status: 'error', message: ErrorParser.parseErrorreturnErrorMessage(err)});
								}else{
									if(images){
										var imageLinksArray = images.map(x => x.imageURL);
										var response = {};
										response['noOfImages'] = imageLinksArray.length;
										response['images'] = imageLinksArray;
										return res.status(200).send({status: 'success', response: response});
									}else{
										return res.status(200).send({status: 'error', message: "No images found"});
									}
								}
							});
		       			}else{
		       				return res.status(200).send({status: 'error', message: "Product not found with productId:"+productId});
		       			}
		       		}
		       });
	}else{
		res.status(200).send({status: 'error', message: "Inadequate information sent"})
	}
};


var createProductOneByoneAndUpdateId = function(currentIndex, allKeys, newCreateProducts, productidsHolder, cb){
	if(currentIndex < allKeys.length){
		Product.create(newCreateProducts[allKeys[currentIndex]], function(err, createdProd){
			if(createdProd){
				productidsHolder[allKeys[currentIndex]] = createdProd._id;
			}
			createProductOneByoneAndUpdateId(currentIndex+1, allKeys, newCreateProducts, productidsHolder, cb);
		});
	}else{
		cb(productidsHolder);
	}
}

//---------------------------------------------------------------------------------------------------------------------------
var createNewProductsFromAdminPanel = function(user, productsArray, cb){
	var createProductsArray = {};
	var allKeys = [];
	var userId = user._id;
	for(var j=0; j<productsArray.length; j++){
		var localObj = productsArray[j];
		var localPushObj = {};
		localPushObj.baseProductId = mongoose.Types.ObjectId(localObj.baseProductId);
		localPushObj.planetNamoCost = localObj.planetNamoCost;
		localPushObj.ageinmonths = localObj.ageInMonths;
		localPushObj.condition = 1;
		localPushObj.validbill = true;
		localPushObj.originalbox = true;
		localPushObj.workingcondition = true;
		localPushObj.accessorieslistavail = [];
		localPushObj.problemslistavail = [];
		localPushObj.productRegisteredByUser = mongoose.Types.ObjectId(userId);
		localPushObj.specifications = [];
		var excelAnswers = localObj.completeExcelAnswers;
		if(localObj.accessoriesList){
			for(var i=0; i<localObj.accessoriesList.length; i++){
				if(excelAnswers[localObj.accessoriesList[i].accessoryname] == 'Y'){
					localPushObj.accessorieslistavail.push(true);
				}else{
					localPushObj.accessorieslistavail.push(false);
				}
			}
		}
		if(localObj.problemsList){
			for(var i=0; i<localObj.problemsList.length; i++){
				if(excelAnswers[localObj.problemsList[i].problemname] == 'Y'){
					localPushObj.problemslistavail.push(true);
				}else{
					localPushObj.problemslistavail.push(false);
				}
			}
		}
		if(localObj.specifications){
			for(var i=0; i<localObj.specifications.length; i++){
				var localSpec = localObj.specifications[i];
				if(excelAnswers[localObj.specifications[i].displayName]){
					localSpec.specificationValue = excelAnswers[localObj.specifications[i].displayName];
					localPushObj.specifications.push(localSpec);
				}else{
					localPushObj.specifications.push(localSpec);
				}
			}
		}
		createProductsArray[j+"_"] = (localPushObj);
		allKeys.push(j+"_");
	}
	var prodIdsHolder = {};
	createProductOneByoneAndUpdateId(0, allKeys, createProductsArray, prodIdsHolder, function(productsArrayRes){
		cb(productsArrayRes);
	});
}


exports.createNewProductsFromAdminPanel = createNewProductsFromAdminPanel;
































