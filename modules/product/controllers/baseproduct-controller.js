var mongoose = require('mongoose');
var User = require(__base + '/models/user/user.js')
var ProductCategory = require(__base + '/models/products/product-category.js');
var BaseProduct = require(__base + '/models/products/base-product.js');
var Product = require(__base + '/models/products/product.js')
var FileHandler = require(__utils + '/FileHandler.js');
var ErrorParser = require(__utils + '/errorParse.js');
var aux = require(__utils + '/auxilaryFunc.js');
var usersUtils = require(__utils + '/users.js');
var FileHandler = require(__utils + '/FileHandler.js');
var multer = require('multer');
var fs = require('fs');
var mime = require('mime');
var crypto = require('crypto');



//BASE PRODUCT IMAGE STORAGE---------------------------------------------------------------------------------------------------
var storage_BaseProductImages = multer.diskStorage({
	destination: function(request, file, callback) {
		callback(null, __base + '/uploads/baseProductPictures');
	},
	filename: function(request, file, callback) {
		console.log(file);
		//callback(null, file.originalname);
		crypto.pseudoRandomBytes(16, function(err, raw) {
			callback(null, raw.toString('hex') + Date.now() + '.' + mime.extension(file.mimetype));
		});
	}
});
var upload_BaseProductImages = multer({
	storage: storage_BaseProductImages
}).single('baseProductImages');


//------------------------------------------------------------------------------------------------------------------------



//API ROUTES in use ------6 APIS --------------------------------------------------------------------------------------------------------
// api/product/AddBaseProductAsArray
// api/product/AddBaseProduct
// api/product/EditBaseProduct/:baseProductId
// api/product/AddBaseProductImage/:baseProductId
// api/product/DeleteBaseProduct/:baseProductId
// api/product/GetBaseProducts
// api/product/GetSpecificBaseproduct/:baseProductId
// api/product/GetAllBrandNames
// api/product/addBaseProductSpecifications/:baseProductId
//------------------------------------------------------------------------------------------------------------------------------------








//----------------A D D---------B A S E----------P R O D U C T-----------------------------------------------------------------------

/**
@api {post} /product/AddBaseProduct Add Base Product 
@apiName Add Base Product
@apiGroup BaseProduct
@apiPermission admin
@apiDescription Create a new base product. A base product will belong to a particular category and under this base product many products will be registered.

Examples of base product includes iPhone 7 etc.

@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization": "JWT eyJhbGHVbz5y8i-HRRY"
	} 
	
@apiParam {String} productName Name of the base product
@apiParam {String} productCategoryId Category Id to which base product belongs to
@apiParam {String} brandName Brand name of the base product
@apiParam {String} modelNumber Model number of base product
@apiParam {String} warranty_n_support Warranty and support Information
@apiParam {JSON} accessories An array of objects containing information about the accesories that come with the base product. Each object in the array must have the following fields - accessoryname(String), score(Number).  
@apiParam {JSON} problems An array of objects containing information about the problems associated with the base product. Each object in the array must have the following fields - problemname(String), score(Number), remarks(String).  
@apiParam {String} goodremark 
@apiParam {String} averageremark 
@apiParam {String} belowaverageremark 

@apiParam {String} [screen_size] screen_size
@apiParam {String} [screen_resolution] screen_resolution
@apiParam {String} [processor] processor
@apiParam {String} [display_technology] display_technology
@apiParam {String} [ram] ram
@apiParam {String} [hard_drive] hard_drive
@apiParam {String} [series] series
@apiParam {String} [hardware_platform] hardware_platform
@apiParam {String} [operating_system] operating_system
@apiParam {String} [item_weight] item_weight
@apiParam {String} [product_dimensions] product_dimensions
@apiParam {String} [item_dimensions] item_dimensions
@apiParam {String} [color] color
@apiParam {String} [processor_count] processor_count
@apiParam {String} [processor_brand] processor_brand
@apiParam {String} [processor_speed] processor_speed
@apiParam {String} [computer_memory_type] computer_memory_type
@apiParam {String} [hard_drive_interface] hard_drive_interface
@apiParam {String} [hard_drive_rotational_speed] hard_drive_rotational_speed
@apiParam {String} [graphics_card] graphics_card
@apiParam {String} [graphics_card_description] graphics_card_description
@apiParam {String} [wireless_type] wireless_type
@apiParam {String} [speaker_description] speaker_description
@apiParam {String} [no_of_usb_ports_2_0] no_of_usb_ports_2_0
@apiParam {String} [no_of_usb_ports_3_0] no_of_usb_ports_3_0
@apiParam {String} [optical_drive_type]  optical_drive_type
@apiParam {String} [batteries] batteries
@apiParam {String} [miscellaneous] miscellaneous
@apiParam {String} [chipset_brand] chipset_brand
@apiParam {String} [no_of_usb_ports_2_0] no_of_usb_ports_2_0

@apiParamExample {json} Request-Example:
	HTTP/1.1 200 OK
	{
		"productName": "OnePlus X",
		"productCategoryId": "58a71492a89606987e60a235",
		"brandName": "OnePlus",
		"modelNumber": "2390280",
		"warranty_n_support": "1 YEAR WARRANTY",
		"marketCost": 13000,
		"releaseDate": "2015",
		"goodRemark": "Everything works",
		"averageRemark": "Minor scratches on body and screen",
		"belowAverageRemark": "Major scratches and defects",
		"problems": [{
			"problemname": "Screen cracked",
			"score": 50,
			"remarks": "Screen is visibly shattered"
		}],
		"accessories": [{
			"accessoryname": "Charger",
			"score": 50
		}]
	}


@apiSuccess {String} status Status of the API Call.
@apiSuccessExample Success-Response:
		HTTP/1.1 200 OK
	{
		"status": "success",
		"message": "Base Product created!",
		"response": {
			"__v": 0,
			"warranty_n_support": "1 YEAR WARRANTY",
			"model_number": "2390280",
			"brand_name": "OnePlus",
			"productCategoryId": "58a71492a89606987e60a235",
			"productName": "OnePlus X",
			"_id": "58b6be4d12d605106feabdb3",
			"finalLots": [],
			"problemsList": [
				{
					"problemname": "Screen cracked",
					"score": 50,
					"remarks": "Screen is visibly shattered",
					"_id": "58b6be4d12d605106feabdb5"
				}
			],
			"accessoriesList": [
				{
					"accessoryname": "Charger",
					"score": 50,
					"_id": "58b6be4d12d605106feabdb4"
				}
			],
			"marketCost": null
		}
	}

@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
		"status": "error",
		"message": "Inadequate information sent"
	}
*/
exports.AddBaseProduct = function(req, res) {

	var productName = req.body.productName;
	var productCategoryId = req.body.productCategoryId;
	var brandName = req.body.brandName;
	var modelNumber = req.body.modelNumber;
	var warranty_n_support = req.body.warranty_n_support;
	var accessories = req.body.accessories;
	var specifications = req.body.specifications;
	var problems = req.body.problems;
	var goodremark = req.body.goodremark;
	var averageremark = req.body.averageremark;
	var belowaverageremark = req.body.belowaverageremark;
	var marketCost = req.body.marketCost;

	if (productName && productCategoryId && brandName && modelNumber && warranty_n_support && accessories && problems) {

		var baseProduct = new BaseProduct();

		baseProduct.productName = productName;
		baseProduct.productCategoryId = productCategoryId;
		baseProduct.brand_name = brandName;
		baseProduct.model_number = modelNumber;
		baseProduct.warranty_n_support = warranty_n_support;
		baseProduct.marketCost = marketCost;

		if(req.body.releaseDate) baseProduct.releaseDate = req.body.releaseDate;
		if (req.body.screen_size) baseProduct.screen_size = req.body.screen_size;
		if (req.body.screen_resolution) baseProduct.screen_resolution = req.body.screen_resolution;
		if (req.body.processor) baseProduct.processor = req.body.processor;
		if (req.body.display_technology) baseProduct.display_technology = req.body.display_technology;
		if (req.body.ram) baseProduct.ram = req.body.ram;
		if (req.body.hard_drive) baseProduct.hard_drive = req.body.hard_drive;
		if (req.body.series) baseProduct.series = req.body.series;
		if (req.body.hardware_platform) baseProduct.hardware_platform = req.body.hardware_platform;
		if (req.body.operating_system) baseProduct.operating_system = req.body.operating_system;
		if (req.body.item_weight) baseProduct.item_weight = req.body.item_weight;
		if (req.body.product_dimensions) baseProduct.product_dimensions = req.body.product_dimensions;
		if (req.body.item_dimensions) baseProduct.item_dimensions = req.body.item_dimensions;
		if (req.body.color) baseProduct.color = req.body.color;
		if (req.body.processor_count) baseProduct.processor_count = req.body.processor_count;
		if (req.body.processor_brand) baseProduct.processor_brand = req.body.processor_brand;
		if (req.body.processor_speed) baseProduct.processor_speed = req.body.processor_speed;
		if (req.body.computer_memory_type) baseProduct.computer_memory_type = req.body.computer_memory_type;
		if (req.body.hard_drive_interface) baseProduct.hard_drive_interface = req.body.hard_drive_interface;
		if (req.body.hard_drive_rotational_speed) baseProduct.hard_drive_rotational_speed = req.body.hard_drive_rotational_speed;
		if (req.body.graphics_card) baseProduct.graphics_card = req.body.graphics_card;
		if (req.body.graphics_card_description) baseProduct.graphics_card_description = req.body.graphics_card_description;
		if (req.body.wireless_type) baseProduct.wireless_type = req.body.wireless_type;
		if (req.body.speaker_description) baseProduct.speaker_description = req.body.speaker_description;
		if (req.body.no_of_usb_ports_2_0) baseProduct.no_of_usb_ports_2_0 = req.body.no_of_usb_ports_2_0;
		if (req.body.no_of_usb_ports_3_0) baseProduct.no_of_usb_ports_3_0 = req.body.no_of_usb_ports_3_0;
		if (req.body.optical_drive_type) baseProduct.optical_drive_type = req.body.optical_drive_type;
		if (req.body.batteries) baseProduct.batteries = req.body.batteries;
		if (req.body.miscellaneous) baseProduct.miscellaneous = req.body.miscellaneous;
		if (req.body.chipset_brand) baseProduct.chipset_brand = req.body.chipset_brand;

		//remarks
		baseProduct.goodremark = goodremark;
		baseProduct.averageremark = averageremark;
		baseProduct.belowaverageremark = belowaverageremark;

		//Checking if accesories and problems are fine 
		if (Array.isArray(accessories) && Array.isArray(problems)) {
			for (var i = 0; i < accessories.length; i++) {
				var tmp = accessories[i];
				if (('accessoryname' in tmp && 'score' in tmp)) {
					try {
						accessories[i]['accessoryname'] = String(accessories[i]['accessoryname']);
						accessories[i]['score'] = parseInt(accessories[i]['score']);
					} catch (err) {
						return res.status(200).send({
							status: 'error',
							message: "Invalid data sent"
						});
					}
				} else {
					return res.status(200).send({
						status: 'error',
						message: "Invalid data sent"
					});
				}
			}
			for (var i = 0; i < problems.length; i++) {
				var tmp = problems[i];
				if (('problemname' in tmp && 'score' in tmp && 'remarks' in tmp)) {
					try {
						problems[i]['problemname'] = String(problems[i]['problemname']);
						problems[i]['remarks'] = String(problems[i]['remarks']);
						problems[i]['score'] = parseInt(problems[i]['score']);
					} catch (err) {
						return res.status(200).send({
							status: 'error',
							message: "Invalid data sent"
						});
					}
				} else {
					return res.status(200).send({
						status: 'error',
						message: "Invalid data sent"
					});
				}
			}
		} else {
			return res.status(200).send({
				status: 'error',
				message: "Invalid data sent"
			});
		}

		if (Array.isArray(specifications) ){
			baseProduct.specifications = specifications;
		}



		//-------------------------------------------
		baseProduct.accessoriesList = accessories;
		baseProduct.problemsList = problems;

		baseProduct.save(function(err, baseProduct) {
			if (err)
				return res.status(200).send({
					status: 'error',
					message: ErrorParser.parseErrorreturnErrorMessage(err)
				});
			else {
				return res.status(200).send({
					status: 'success',
					message: 'Base Product created!',
					response: baseProduct
				});
			}
		});

	} else {
		res.status(200).send({
			status: 'error',
			message: "Inadequate data sent."
		});
	}
};



exports.AddBaseProductAsArray = function(req, res) {
	console.log('array');
	var baseProductArray = req.body.baseProductArray;
	console.log(req.body.baseProductArray);
	console.log(Array.isArray(baseProductArray));
	if (Array.isArray(baseProductArray)) {
		var invalidBaseProducts = [];
		for (var I = 0; I < baseProductArray.length; I++) {
			var invalidFlag = false;
			var productName = baseProductArray[I].productName;
			var productCategoryId = baseProductArray[I].productCategoryId;
			var brandName = baseProductArray[I].brandName;
			var modelNumber = baseProductArray[I].modelNumber;
			var warranty_n_support = baseProductArray[I].warranty_n_support;
			var accessories = baseProductArray[I].accessories;
			var problems = baseProductArray[I].problems;
			var goodremark = baseProductArray[I].goodremark;
			var averageremark = baseProductArray[I].averageremark;
			var belowaverageremark = baseProductArray[I].belowaverageremark;
			var marketCost = baseProductArray[I].marketCost;

			if (productName && productCategoryId && brandName && modelNumber && warranty_n_support && accessories && problems) {

				var baseProduct = new BaseProduct();

				baseProduct.productName = productName;
				baseProduct.productCategoryId = productCategoryId;
				baseProduct.brand_name = brandName;
				baseProduct.model_number = modelNumber;
				baseProduct.warranty_n_support = warranty_n_support;
				baseProduct.marketCost = marketCost;

				if (baseProductArray[I].screen_size) baseProduct.screen_size = baseProductArray[I].screen_size;
				if (baseProductArray[I].screen_resolution) baseProduct.screen_resolution = baseProductArray[I].screen_resolution;
				if (baseProductArray[I].processor) baseProduct.processor = baseProductArray[I].processor;
				if (baseProductArray[I].display_technology) baseProduct.display_technology = baseProductArray[I].display_technology;
				if (baseProductArray[I].ram) baseProduct.ram = baseProductArray[I].ram;
				if (baseProductArray[I].hard_drive) baseProduct.hard_drive = baseProductArray[I].hard_drive;
				if (baseProductArray[I].series) baseProduct.series = baseProductArray[I].series;
				if (baseProductArray[I].hardware_platform) baseProduct.hardware_platform = baseProductArray[I].hardware_platform;
				if (baseProductArray[I].operating_system) baseProduct.operating_system = baseProductArray[I].operating_system;
				if (baseProductArray[I].item_weight) baseProduct.item_weight = baseProductArray[I].item_weight;
				if (baseProductArray[I].product_dimensions) baseProduct.product_dimensions = baseProductArray[I].product_dimensions;
				if (baseProductArray[I].item_dimensions) baseProduct.item_dimensions = baseProductArray[I].item_dimensions;
				if (baseProductArray[I].color) baseProduct.color = baseProductArray[I].color;
				if (baseProductArray[I].processor_count) baseProduct.processor_count = baseProductArray[I].processor_count;
				if (baseProductArray[I].processor_brand) baseProduct.processor_brand = baseProductArray[I].processor_brand;
				if (baseProductArray[I].processor_speed) baseProduct.processor_speed = baseProductArray[I].processor_speed;
				if (baseProductArray[I].computer_memory_type) baseProduct.computer_memory_type = baseProductArray[I].computer_memory_type;
				if (baseProductArray[I].hard_drive_interface) baseProduct.hard_drive_interface = baseProductArray[I].hard_drive_interface;
				if (baseProductArray[I].hard_drive_rotational_speed) baseProduct.hard_drive_rotational_speed = baseProductArray[I].hard_drive_rotational_speed;
				if (baseProductArray[I].graphics_card) baseProduct.graphics_card = baseProductArray[I].graphics_card;
				if (baseProductArray[I].graphics_card_description) baseProduct.graphics_card_description = baseProductArray[I].graphics_card_description;
				if (baseProductArray[I].wireless_type) baseProduct.wireless_type = baseProductArray[I].wireless_type;
				if (baseProductArray[I].speaker_description) baseProduct.speaker_description = baseProductArray[I].speaker_description;
				if (baseProductArray[I].no_of_usb_ports_2_0) baseProduct.no_of_usb_ports_2_0 = baseProductArray[I].no_of_usb_ports_2_0;
				if (baseProductArray[I].no_of_usb_ports_3_0) baseProduct.no_of_usb_ports_3_0 = baseProductArray[I].no_of_usb_ports_3_0;
				if (baseProductArray[I].optical_drive_type) baseProduct.optical_drive_type = baseProductArray[I].optical_drive_type;
				if (baseProductArray[I].batteries) baseProduct.batteries = baseProductArray[I].batteries;
				if (baseProductArray[I].miscellaneous) baseProduct.miscellaneous = baseProductArray[I].miscellaneous;
				if (baseProductArray[I].chipset_brand) baseProduct.chipset_brand = baseProductArray[I].chipset_brand;

				//remarks
				baseProduct.goodremark = goodremark;
				baseProduct.averageremark = averageremark;
				baseProduct.belowaverageremark = belowaverageremark;

				//Checking if accesories and problems are fine 
				if (Array.isArray(accessories) && Array.isArray(problems)) {
					for (var i = 0; i < accessories.length; i++) {
						var tmp = accessories[i];
						if (('accessoryname' in tmp && 'score' in tmp)) {
							try {
								accessories[i]['accessoryname'] = String(accessories[i]['accessoryname']);
								accessories[i]['score'] = parseInt(accessories[i]['score']);
							} catch (err) {
								invalidBaseProducts.push(baseProductArray[I]);
								invalidFlag = true;
								break;
							}
						} else {
							invalidBaseProducts.push(baseProductArray[I]);
							invalidFlag = true;
							break;
						}
					}
					if (invalidFlag) {
						continue;
					}
					for (var i = 0; i < problems.length; i++) {
						var tmp = problems[i];
						if (('problemname' in tmp && 'score' in tmp && 'remarks' in tmp)) {
							try {
								problems[i]['problemname'] = String(problems[i]['problemname']);
								problems[i]['remarks'] = String(problems[i]['remarks']);
								problems[i]['score'] = parseInt(problems[i]['score']);
							} catch (err) {
								invalidBaseProducts.push(baseProductArray[I]);
								invalidFlag = true;
								break;
							}
						} else {
							invalidBaseProducts.push(baseProductArray[I]);
							invalidFlag = true;
							break;
						}
					}
					if (invalidFlag) {
						continue;
					}
				} else {
					invalidBaseProducts.push(baseProductArray[I]);
					continue;
				}

				//-------------------------------------------
				baseProduct.accessoriesList = accessories;
				baseProduct.problemsList = problems;

				baseProduct.save(function(err, baseProduct) {
					if (err)
						invalidBaseProducts.push(baseProductArray[I]);
				});

			} else {
				invalidBaseProducts.push(baseProductArray[I]);
				continue;
			}

		}

		return res.status(200).send({
			status: "success",
			message: (baseProductArray.length - invalidBaseProducts.length) + " Base Product(s) created!",
			response: {
				errorBaseProducts: invalidBaseProducts
			}
		});


	} else {
		return res.status(200).send({
			status: "error",
			message: "Array not sent"
		});
	}
};
//---------------------------------------------------------------------------------------------------------------------------



//---------------D E L E T E------------B A S E-----------P R O D U C T--------------------------------------------------------------

/**
@api {delete} /product/DeleteBaseProduct/:baseProductId Delete Base Product 
@apiName Delete Base Product
@apiGroup BaseProduct
@apiPermission admin
@apiDescription Delete a base product. A base product can only be deleted if no products are registered under this base product.

Corresponding base product images if any will also be deleted.

@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization": "JWT eyJhbGHVbz5y8i-HRRY"
	} 

@apiParam {String} baseProductId Base product Id 
@apiParamExample {URL} Request-Example:
	http://localhost:8080/api/product/DeleteBaseProduct/58c80b1439548bf


@apiSuccess {String} status Status of the API Call.
@apiSuccessExample Success-Response:
		HTTP/1.1 200 OK
	{
		"status": "success",
		"message": "Successfully deleted BaseProduct!"
	}


@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
		"status": "error",
		"message": "Cannot delete base product as 2 product(s) have been registered under this base product."
	}
*/
exports.DeleteBaseProduct = function(req, res) {
	var baseProductId = req.params.baseProductId;
	if (baseProductId) {
		BaseProduct.findById(baseProductId, function(err, baseProduct) {
			if (err) {
				return res.status(200).send({
					status: 'error',
					message: ErrorParser.parseErrorreturnErrorMessage(err)
				});
			} else {
				if (baseProduct) {
					Product.find({
							baseProductId: baseProductId
						})
						.exec(function(err, products) {
							if (products || products.length > 0) {
								return res.status(200).send({
									status: 'error',
									message: 'Cannot delete base product as ' + products.length + ' product(s) have been registered under this base product.'
								});
							} else {
								if (baseProduct.productImage.path) {
									fs.unlink(baseProduct.productImage.path, function(err) {
										if (err) {
											BaseProduct.remove({
												_id: baseProductId
											}, function(err, baseProduct) {
												if (err) {
													return res.status(200).send({
														status: 'error',
														message: ErrorParser.parseErrorreturnErrorMessage(err)
													});
												} else {
													return res.status(200).send({
														status: 'success',
														message: 'Successfully deleted BaseProduct! No base product image present to delete.'
													});
												}
											});
										} else {
											BaseProduct.remove({
												_id: baseProductId
											}, function(err, baseProduct) {
												if (err) {
													return res.status(200).send({
														status: 'error',
														message: ErrorParser.parseErrorreturnErrorMessage(err)
													});
												} else {
													return res.status(200).send({
														status: 'success',
														message: 'Successfully deleted BaseProduct! Base product Image deleted Successfully.'
													});
												}
											});
										}
									});
								} else {
									BaseProduct.remove({
										_id: baseProductId
									}, function(err, baseProduct) {
										if (err) {
											return res.status(200).send({
												status: 'error',
												message: ErrorParser.parseErrorreturnErrorMessage(err)
											});
										} else {
											return res.status(200).send({
												status: 'success',
												message: 'Successfully deleted BaseProduct! No base product image present to delete.'
											});
										}
									});
								}
							}
						});
				} else {
					return res.status(200).send({
						status: 'error',
						message: "No base product found for Id: " + baseProductId
					});
				}
			}
		});
	} else {
		return res.status(200).send({
			status: 'error',
			message: "Inadequate data sent."
		});
	}
};

//---------------------------------------------------------------------------------------------------------------------------



//-------------G E T-----------B A S E---------------P R O D U C T S----(only baseProductName and baseProductId)------------------------------------------------------------

/**
@api {post} /product/GetBaseProducts Get all Base Products 
@apiName GetBaseProducts
@apiGroup BaseProduct
@apiDescription Get a list of all base products. Only baseProduct name and Id is fetched.

@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json"
	} 

@apiParam {String} baseProductId Base product Id 
@apiParamExample {JSON} Request-Example:
	{
		"filters": {
			"categoyIds": ["58d4b3797b396b0011b2f662"]
		}
	}
	
@apiSuccess {String} status Status of the API Call.
@apiSuccess {String} response A dictionary with key as base product Id.
@apiSuccessExample Success-Response:
		HTTP/1.1 200 OK
	{
	  "status": "success",
	  "response": [
	    {
	      "_id": "58d6532d5f5fe43655d0530b",
	      "productName": "Lenovo YogaPad"
	    },
	    {
	      "_id": "58d6532d5f5fe43655d05311",
	      "productName": "HP Spectre"
	    },
	    {
	      "_id": "58d6532d5f5fe43655d052ff",
	      "productName": "Macbook Air 13"
	    },
	    {
	      "_id": "58d6532d5f5fe43655d05305",
	      "productName": "Dell XPS 13"
	    }
	  ]
	}

@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
		"status": "error",
		"message": "some error"
	}
*/
exports.GetBaseProducts = function(req, res) {
	var filters = req.body.filters;
	var orQueries = [];
	var andQueries = {};

	if(filters){
		if(filters.categoyIds && filters.categoyIds.length>0){
			andQueries.productCategoryId = {"$in": filters.categoyIds};
		}
	}

	var queryParams= {};
	if(orQueries.length > 0){
		queryParams = {$and: [andQueries, { $or: orQueries }]};
	}else{
    queryParams = andQueries;
  }

	BaseProduct.find(queryParams)
						 .select('productName _id')
						 .sort('-createdAt')
						 .exec(function(err, baseProducts){
						  	if(err){
									return res.status(200).send({
										status: 'error',
										message: ErrorParser.parseErrorreturnErrorMessage(err)
									});
						  	}else{
						  		return res.status(200).send({
						  			status: "success",
						  			response: baseProducts
						  		});
						  	}
						 });
};

//---------------------------------------------------------------------------------------------------------------------------



//-------------------------E D I T-----------B A S E---------------P R O D U C T S----------------------------------------------

/**
@api {put} /product/EditBaseProduct Edit Base Product 
@apiName EditBaseProduct
@apiGroup BaseProduct
@apiPermission admin
@apiDescription Edit a base product.

@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization": "JWT eyJhbGHVbz5y8i-HRRY"
	} 

@apiParam {String} [productName] productName Name of the base product
@apiParam {String} productCategoryId Category Id to which base product belongs to
@apiParam {String} [brandName] brandName Brand name of the base product
@apiParam {String} [modelNumber] modelNumber Model number of base product
@apiParam {String} [warranty_n_support] warranty_n_support Warranty and support Information
@apiParam {String} [goodremark] goodremark 
@apiParam {String} [averageremark] averageremark 
@apiParam {String} [belowaverageremark] belowaverageremark 

@apiParam {String} [screen_size] screen_size
@apiParam {String} [screen_resolution] screen_resolution
@apiParam {String} [processor] processor
@apiParam {String} [display_technology] display_technology
@apiParam {String} [ram] ram
@apiParam {String} [hard_drive] hard_drive
@apiParam {String} [series] series
@apiParam {String} [hardware_platform] hardware_platform
@apiParam {String} [operating_system] operating_system
@apiParam {String} [item_weight] item_weight
@apiParam {String} [product_dimensions] product_dimensions
@apiParam {String} [item_dimensions] item_dimensions
@apiParam {String} [color] color
@apiParam {String} [processor_count] processor_count
@apiParam {String} [processor_brand] processor_brand
@apiParam {String} [processor_speed] processor_speed
@apiParam {String} [computer_memory_type] computer_memory_type
@apiParam {String} [hard_drive_interface] hard_drive_interface
@apiParam {String} [hard_drive_rotational_speed] hard_drive_rotational_speed
@apiParam {String} [graphics_card] graphics_card
@apiParam {String} [graphics_card_description] graphics_card_description
@apiParam {String} [wireless_type] wireless_type
@apiParam {String} [speaker_description] speaker_description
@apiParam {String} [no_of_usb_ports_2_0] no_of_usb_ports_2_0
@apiParam {String} [no_of_usb_ports_3_0] no_of_usb_ports_3_0
@apiParam {String} [optical_drive_type]  optical_drive_type
@apiParam {String} [batteries] batteries
@apiParam {String} [miscellaneous] miscellaneous
@apiParam {String} [chipset_brand] chipset_brand
@apiParam {String} [no_of_usb_ports_2_0] no_of_usb_ports_2_0
@apiParamExample {json} Request-Example:
	HTTP/1.1 200 OK
	{
		"productName": "OnePlus 1"
	}


@apiSuccess {String} status Status of the API Call.
@apiSuccess {String} response Response contains the newly edited base product.
@apiSuccessExample Success-Response:
		HTTP/1.1 200 OK
	{
		"status": "Base Product updated!",
		"response": {
			"_id": "58b6c0afec0a3d11f698f5cc",
			"warranty_n_support": "1 YEAR WARRANTY",
			"model_number": "2390280",
			"brand_name": "OnePlus",
			"productCategoryId": "58a71492a89606987e60a235",
			"productName": "OnePlus 1",
			"__v": 0,
			"finalLots": [],
			"problemsList": [
				{
					"problemname": "23",
					"score": 50,
					"remarks": "Screen is visibly shattered",
					"_id": "58b6c0afec0a3d11f698f5ce"
				}
			],
			"accessoriesList": [
				{
					"accessoryname": "Charger",
					"score": 50,
					"_id": "58b6c0afec0a3d11f698f5cd"
				}
			],
			"marketCost": null
		}
	}

@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
		"status": "error",
		"message": "Inadequate information sent"
	}
*/
exports.EditBaseProduct = function(req, res) {
	var baseProductId = req.params.baseProductId;

	if (baseProductId) {
		BaseProduct.findById(baseProductId, function(err, baseProduct) {
			if (err) {
				return res.status(200).send({
					status: 'error',
					message: ErrorParser.parseErrorreturnErrorMessage(err)
				});
			} else {
				if (req.body.productName) baseProduct.productName = req.body.productName;
				if (req.body.productCategoryId) baseProduct.productCategoryId = req.body.productCategoryId;
				if (req.body.brandName) baseProduct.brandName = req.body.brandName;
				if (req.body.modelNumber) baseProduct.modelNumber = req.body.modelNumber;
				if (req.body.warranty_n_support) baseProduct.warranty_n_support = req.body.warranty_n_support;
				if (req.body.releaseDate) baseProduct.releaseDate = req.body.releaseDate;

				if (req.body.screen_size) baseProduct.screen_size = req.body.screen_size;
				if (req.body.screen_resolution) baseProduct.screen_resolution = req.body.screen_resolution;
				if (req.body.processor) baseProduct.processor = req.body.processor;
				if (req.body.display_technology) baseProduct.display_technology = req.body.display_technology;
				if (req.body.ram) baseProduct.ram = req.body.ram;
				if (req.body.hard_drive) baseProduct.hard_drive = req.body.hard_drive;
				if (req.body.series) baseProduct.series = req.body.series;
				if (req.body.hardware_platform) baseProduct.hardware_platform = req.body.hardware_platform;
				if (req.body.operating_system) baseProduct.operating_system = req.body.operating_system;
				if (req.body.item_weight) baseProduct.item_weight = req.body.item_weight;
				if (req.body.product_dimensions) baseProduct.product_dimensions = req.body.product_dimensions;
				if (req.body.item_dimensions) baseProduct.item_dimensions = req.body.item_dimensions;
				if (req.body.color) baseProduct.color = req.body.color;
				if (req.body.processor_count) baseProduct.processor_count = req.body.processor_count;
				if (req.body.processor_brand) baseProduct.processor_brand = req.body.processor_brand;
				if (req.body.processor_speed) baseProduct.processor_speed = req.body.processor_speed;
				if (req.body.computer_memory_type) baseProduct.computer_memory_type = req.body.computer_memory_type;
				if (req.body.hard_drive_interface) baseProduct.hard_drive_interface = req.body.hard_drive_interface;
				if (req.body.hard_drive_rotational_speed) baseProduct.hard_drive_rotational_speed = req.body.hard_drive_rotational_speed;
				if (req.body.graphics_card) baseProduct.graphics_card = req.body.graphics_card;
				if (req.body.graphics_card_description) baseProduct.graphics_card_description = req.body.graphics_card_description;
				if (req.body.wireless_type) baseProduct.wireless_type = req.body.wireless_type;
				if (req.body.speaker_description) baseProduct.speaker_description = req.body.speaker_description;
				if (req.body.no_of_usb_ports_2_0) baseProduct.no_of_usb_ports_2_0 = req.body.no_of_usb_ports_2_0;
				if (req.body.no_of_usb_ports_3_0) baseProduct.no_of_usb_ports_3_0 = req.body.no_of_usb_ports_3_0;
				if (req.body.optical_drive_type) baseProduct.optical_drive_type = req.body.optical_drive_type;
				if (req.body.batteries) baseProduct.batteries = req.body.batteries;
				if (req.body.miscellaneous) baseProduct.miscellaneous = req.body.miscellaneous;
				if (req.body.chipset_brand) baseProduct.chipset_brand = req.body.chipset_brand;

				//remarks
				if (req.body.goodRemark) baseProduct.goodRemark = req.body.goodRemark;
				if (req.body.averageRemark) baseProduct.averageRemark = req.body.averageRemark;
				if (req.body.belowAverageRemark) baseProduct.belowAverageRemark = req.body.belowAverageRemark;

				console.log(baseProduct);
				baseProduct.save(function(err, baseProduct) {
					if (err) {
						return res.status(200).send({
							status: 'error',
							message: ErrorParser.parseErrorreturnErrorMessage(err)
						});
					}
					return res.status(200).send({
						status: 'success',
						message: 'Base Product updated!',
						response: baseProduct
					});
				});
			}
		});
	} else {
		res.status(200).send({
			status: 'error',
			message: "Inadequate data sent."
		});
	}
};

//---------------------------------------------------------------------------------------------------------------------------



//--------------------G E T----------S P E C I F I C-------------B A S E------------P R O D U C T-------------------------------------------

/**
@api {get} /product/GetSpecificBaseproduct/:baseProductId Get Base Product Information
@apiName GetSpecificBaseproduct
@apiGroup BaseProduct
@apiDescription Get Information about a specific base product.

@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json"
	}

@apiParam {String} :baseProductId Unique Id representing a base product. Sent as a URL parameter
@apiParamExample {URL} Request-Example:
	http://localhost:8080/api/product/GetSpecificBaseproduct/58c80b1439548bf

@apiSuccess {String} status Status of the API Call.
@apiSuccess {JSON} result Result will an object containing category information
@apiSuccessExample Success-Response:
	{
		"status": "success",
		"response": {
			"_id": "58b6c0afec0a3d11f698f5cc",
			"warranty_n_support": "1 YEAR WARRANTY",
			"model_number": "2390280",
			"brand_name": "OnePlus",
			"productCategoryId": {
				"_id": "58a71492a89606987e60a235",
				"categoryName": "mobile"
			},
			"productName": "OnePlus 1",
			"__v": 0,
			"finalLots": [],
			"problemsList": [
				{
					"problemname": "23",
					"score": 50,
					"remarks": "Screen is visibly shattered",
					"_id": "58b6c0afec0a3d11f698f5ce"
				}
			],
			"accessoriesList": [
				{
					"accessoryname": "Charger",
					"score": 50,
					"_id": "58b6c0afec0a3d11f698f5cd"
				}
			],
			"marketCost": null
		}
	}

@apiError error Error message.
	HTTP/1.1 200 Error
	{
		"status": "error",
		"message": "Inadequate information sent"
	}
*/
exports.GetSpecificBaseproduct = function(req, res) {
	var baseProductId = req.params.baseProductId;
	if (baseProductId) {
		BaseProduct.findById(baseProductId, function(err, baseProduct) {
			if (err) {
				return res.status(200).send({
					status: 'error',
					message: ErrorParser.parseErrorreturnErrorMessage(err)
				});
			} else {
				if(baseProduct){
					var result = {};
					result['_id'] = baseProduct._id;
					result['color'] = baseProduct.color;
					result['item_weight'] = baseProduct.item_weight;
					result['operating_system'] = baseProduct.operating_system;
					result['warranty_n_support'] = baseProduct.warranty_n_support;
					result['model_number'] = baseProduct.model_number;
					result['brand_name'] = baseProduct.brand_name;
					result['productCategoryId'] = baseProduct.productCategoryId;
					result['productName'] = baseProduct.productName;
					result['finalLots'] = baseProduct.finalLots;
					result['problemsList'] = baseProduct.problemsList;
					result['accessoriesList'] = baseProduct.accessoriesList;
					result['marketCost'] = baseProduct.marketCost;
					result['specifications'] = baseProduct.specifications;
					result['releaseDate'] = baseProduct.releaseDate;
					result['goodRemark'] = baseProduct.goodRemark;
					result['averageRemark'] = baseProduct.averageRemark;
					result['belowAverageRemark'] = baseProduct.belowAverageRemark;
					return res.status(200).send({
						status: 'success',
						response: result
					});
				}else{
					return res.status(200).send({status: "error", message: "Base product not found."});
				}
			}
		}).populate('productCategoryId', 'categoryName');
	} else {
		res.status(200).send({
			status: 'error',
			message: "Inadequate data sent."
		});
	}
};

//---------------------------------------------------------------------------------------------------------------------------



//--------------A D D---------B A S E-------P R O D U C T------I M A G E-------2.0------------------------------------------------------

/**
@api {post} /product/AddBaseProductImage/:baseProductId Add Base Product Image
@apiName AddBaseProductImage
@apiGroup BaseProduct
@apiPermission admin
@apiDescription Add a new base product image. If image is already present then the old one will be deleted and replaced with the new one. If a dead image is present it will also be replaced. 

Only one base product image is stored per base product.

@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeader {String="JWT eyJhbGHVbz5y8i-HRRY"} Authorization  JWT Authorization for accessing the server
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization": "JWT eyJhbGHVbz5y8i-HRRY"
	} 

@apiParam {File} baseProductImages Image file representing the product category
@apiParam {String} :baseProductId Unique Id representing a base product. Sent as a URL parameter
	{
		baseProductImages: xyz.jpg
	}

@apiSuccess {String} status Status of the API Call.
@apiSuccess {String} message Message describing the status
@apiSuccess {String} response Response of the API
@apiSuccess {String} response.baseProductImageURL the new image URL

@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
		"status": "error",
		"message": "Inadequate information sent"
	}
*/
exports.AddBaseProductImage = function(req, res) {
	upload_BaseProductImages(req, res, function(err) {
		if (err) {
			return res.status(200).send({
				status: 'error',
				message: ErrorParser.parseErrorreturnErrorMessage(err)
			});
		} else {
			var file = req.file;
			var baseProductId = req.params.baseProductId;
			if (file && baseProductId) {
				BaseProduct.findById(baseProductId, function(err, baseProduct) {
					if (err) {
						return res.status(200).send({
							status: 'error',
							message: ErrorParser.parseErrorreturnErrorMessage(err)
						});
					} else {
						if (baseProduct.productImage.path) {
							fs.unlink(baseProduct.productImage.path, function(err) {
								if (err) {
									//file not found
									baseProduct.productImage = file;
									baseProduct.save(function(err, baseProduct) {
										if (err) {
											return res.status(200).send({
												status: 'error',
												message: ErrorParser.parseErrorreturnErrorMessage(err)
											});
										} else {
											return res.status(200).send({
												status: 'success',
												message: 'Baseproduct image saved, old image not found in image collection.'
											});
										}
									});
								} else {
									baseProduct.productImage = file;
									baseProduct.save(function(err, baseProduct) {
										if (err) {
											return res.status(200).send({
												status: 'error',
												message: ErrorParser.parseErrorreturnErrorMessage(err)
											});
										} else {
											return res.status(200).send({
												status: 'success',
												message: 'Baseproduct image saved.'
											});
										}
									});
								}
							});
						} else {
							baseProduct.productImage = file;
							baseProduct.save(function(err, baseProduct) {
								if (err) {
									return res.status(200).send({
										status: 'error',
										message: ErrorParser.parseErrorreturnErrorMessage(err)
									});
								} else {
									return res.status(200).send({
										status: 'success',
										message: 'Baseproduct image saved.'
									});
								}
							});
						}
					}
				});
			}
		}
	});
};


//---------------------------------------------------------------------------------------------------------------------------



//--------------------G E T----------A L L--------------B R A N D N A M E S---------------------------------------------------------------


/**
@api {get} /product/GetAllBrandNames Get All Brand Names Of base Products 
@apiName GetAllBrandNames
@apiGroup BaseProduct
@apiDescription Get a list of all brand names in the registered base products

@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json"
	} 

@apiSuccess {String} status Status of the API Call.
@apiSuccess {String} response Response of the API Call
@apiSuccess {String} response.noOfBrands Number of brands
@apiSuccess {String} response.brands An array of brand names
@apiSuccessExample Success-Response:
		HTTP/1.1 200 OK
	{
		"status": "success",
		"response": {
			"noOfBrands": 3,
			"brands": [
				"OnePlus",
				"Apple",
				"Samsung"
			]
		}
	}

@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
		"error": "some error"
	}
*/
exports.GetAllBrandNames = function(req, res) {
	BaseProduct.collection.distinct("brand_name", function(err, results) {
		if (err) {
			return res.status(200).send({
				status: 'error',
				message: ErrorParser.parseErrorreturnErrorMessage(err)
			});
		} else {
			var response = {};
			response['noOfBrands'] = results.length;
			response['brands'] = results;
			return res.status(200).send({
				status: 'success',
				response: response
			});
		}
	});
};

//---------------------------------------------------------------------------------------------------------------------------



//--------------------ADD BASEPRODUCT SPECIFICATIONS---------------------------------------------------------------


/**
@api {get} /product/addBaseProductSpecifications/:baseProductId Add Base Product Specifications
@apiName AddBaseProductSpecifications
@apiGroup BaseProduct
@apiPermission admin

@apiDescription Add or update the specification details of the base product corresponding to :baseProductId.

Each specification is of the form of a dictionary containing 3 keys - displayName, specificationDescription, specificationValue. All three of them are strings.

@apiHeader {String="application/json"} Content-Type Content Type for the api
@apiHeaderExample {json} Request-Header-Example:
	{
		"Content-Type": "application/json",
		"Authorization": "JWT eyJhbGHVbz5y8i-HRRY"
	} 



@apiError error Error message.
@apiErrorExample Error-Response:
	HTTP/1.1 200 Error
	{
		"error": "some error"
	}
*/
exports.AddBaseProductSpecifications = function(req, res) {
	var baseProductId = req.params.baseProductId;
	var specifications = req.body.specifications;
	if (baseProductId) {
		if (specifications) {
			if (specifications.constructor === Array) {
				BaseProduct.findById(baseProductId, function(err, baseProduct) {
					if (err) {
						return res.status(200).send({
							status: 'error',
							message: ErrorParser.parseErrorreturnErrorMessage(err)
						});
					} else {
						if (baseProduct) {
							if (specifications.length > 0) {
								var invalidSpecifications = false;
								specifications.forEach(function(specification) {
									if (specification === null || typeof specification != 'object' || !(('displayName' in specification) && ('specificationValue' in specification))) {
										invalidSpecifications = true;
									}
								});
								if (invalidSpecifications === true) {
									return res.status(200).send({
										status: "error",
										message: "Invalid specification."
									});
								}
								baseProduct.specifications = specifications;
								baseProduct.save(function(err) {
									if (err) {
										return res.status(200).send({
											status: 'error',
											message: ErrorParser.parseErrorreturnErrorMessage(err)
										});
									} else {
										return res.status(200).send({
											status: "success",
											message: "Specifications saved."
										});
									}
								});
							} else {
								return res.status(200).send({
									status: "error",
									message: "specifications cannot be empty."
								});
							}
						} else {
							return res.status(200).send({
								status: "error",
								message: "Invalid baseProductId"
							});
						}
					}
				});
			} else {
				return res.status(200).send({
					status: "error",
					message: "Invalid specifications sent."
				});
			}
		} else {
			return res.status(200).send({
				status: "error",
				message: "specifications not sent."
			});
		}
	} else {
		return res.status(200).send({
			status: "error",
			message: "baseProductId not specified."
		})
	}
};

//---------------------------------------------------------------------------------------------------------------------------


exports.CreateNewBaseProductAdminPanel = function(req, res){
	var baseProductInfo = req.body.baseProductInfo;
	var user = res.req.user;
	if(baseProductInfo){
		BaseProduct.findOne({'productCategoryId': baseProductInfo.productCategoryId, 'productName': baseProductInfo.productName})
						 .sort('-createdAt')
						 .exec(function(err, baseProduct){
						  	if(err){
									res.status(200).send({
										status: 'error',
										message: ErrorParser.parseErrorreturnErrorMessage(err)
									});
						  	}else if(baseProduct){
						  		res.status(200).send({status: 'error', message: 'Base product already exists with the product name: '+baseProductInfo.productName});
						  	}else{
						  		BaseProduct.create(baseProductInfo, function(err, newProduct){
						  			if(err){
						  				res.status(200).send({
											status: 'error',
											message: ErrorParser.parseErrorreturnErrorMessage(err)
										});
						  			}else{
						  				res.status(200).send({
											status: 'success'
										});
						  			}
						  		});
						  	}
						 });
	}else{	
		res.status(200).send({status: 'error', message: 'No Base Products Found'});
	}
}



var getBaseProductsIdsIfNotCreateAndReturn = function(categoryId, arrayOfBaseProductsInfo, cb){

	if(arrayOfBaseProductsInfo){
		if(categoryId){
			BaseProduct.find({'productCategoryId': categoryId})
						 .sort('-createdAt')
						 .exec(function(err, baseProducts){
						  	if(err){
									cb({
										status: 'error',
										message: ErrorParser.parseErrorreturnErrorMessage(err)
									});
						  	}else{
						  		for(var i=0; i<baseProducts.length; i++){
						  			if(arrayOfBaseProductsInfo[baseProducts[i].productName.replace(/\s/g, '')]){
						  				arrayOfBaseProductsInfo[baseProducts[i].productName.replace(/\s/g, '')].baseProductId = baseProducts[i]._id;
						  			}
						  		}
						  		var toCreateNewBaseProducts = [];
						  		for(key in arrayOfBaseProductsInfo){
						  			var localObj = arrayOfBaseProductsInfo[key];
						  			if(!localObj.baseProductId){
						  				toCreateNewBaseProducts.push({'productName': localObj.productName, 'productCategoryId': mongoose.Types.ObjectId(categoryId),
						  												'specifications': localObj.specifications, 'accessoriesList': localObj.accessoriesList, 
						  												'problemsList': localObj.problemsList, 'brand_name': localObj.brandName, 'model_number': localObj.modelNumber,
						  												'warranty_n_support': localObj.warrantyNSupport});
						  			}
						  		}
						  		if(toCreateNewBaseProducts.length > 0){
						  			BaseProduct.collection.insert(toCreateNewBaseProducts, function(err, newCreatedProducts){
							  			if(err){
							  				cb({
												status: 'error',
												message: ErrorParser.parseErrorreturnErrorMessage(err)
											});
							  			}else{
							  				for(var i=0; i<newCreatedProducts.length; i++){
							  					if(arrayOfBaseProductsInfo[newCreatedProducts[i].productName.replace(/\s/g, '')]){
									  				arrayOfBaseProductsInfo[newCreatedProducts[i].productName.replace(/\s/g, '')].baseProductId = newCreatedProducts[i]._id;
									  			}
							  				}
							  				cb({status: 'success', data: arrayOfBaseProductsInfo});
							  			}
							  		});
						  		}else{
						  			cb({status: 'success', data: arrayOfBaseProductsInfo});
						  		}
						  	}
						 });
		}else{
			cb({status: 'error',message: "No Category Id Provided"});
		}
	}else{
		cb({status: "success", data: {}});
	}
}


exports.getBaseProductsIdsIfNotCreateAndReturn = getBaseProductsIdsIfNotCreateAndReturn;